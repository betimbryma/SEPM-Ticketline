package at.ac.tuwien.inso.ticketline.dto;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@ApiModel(value = "RoomDto", description = "Data transfer object for a room")
public class RoomDto {


    @ApiModelProperty(value = "Unique id of room")
    private Integer id;

    @ApiModelProperty(value = "Unique id of room")
    private String name;
    @ApiModelProperty(value = "Unique id of room")
    private String description;
    @ApiModelProperty(value = "Unique id of room")
    private List<ShowDto> shows;

    @NotNull
    @ApiModelProperty(value = "Unique id of room")
    private LocationDto location;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<ShowDto> getShows() {
        return shows;
    }

    public void setShows(List<ShowDto> shows) {
        this.shows = shows;
    }

    public LocationDto getLocation() {
        return location;
    }

    public void setLocation(@Valid LocationDto location) {
        this.location = location;
    }
}
