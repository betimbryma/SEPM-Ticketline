package at.ac.tuwien.inso.ticketline.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

@ApiModel(value = "ArtistDto", description = "Data transfer object for an artist")
public class ArtistDto {

    @ApiModelProperty(value = "Unique id of artist")
    private int id;

    @NotNull
    @Size(min = 2)
    @ApiModelProperty(value = "First name of the artist")
    private String firstName;

    @NotNull
    @Size(min = 2)
    @ApiModelProperty(value = "Last name of the artist")
    private String lastName;

    @ApiModelProperty(value = "Description of the artist")
    private String description;

    public int getId() {
        return id;
    }

    public void setId( int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(@Valid String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(@Valid String lastName) {
        this.lastName = lastName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
