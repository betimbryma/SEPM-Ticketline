package at.ac.tuwien.inso.ticketline.dto;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@ApiModel(value = "MerchandiseDto", description = "Data transfer Object for a Merchandise")
public class MerchandiseDto {


    @ApiModelProperty(value = "Unique ID of the article")
    private Integer id;

    @ApiModelProperty(value = "available of the article")
    private Integer available;
    @ApiModelProperty(value = "description of the article")
    private String description;

    @NotNull
    @Size(min = 2)
    @ApiModelProperty(value = "name ID of the article")
    private String name;

    @NotNull
    @ApiModelProperty(value = "price ID of the article")
    private Integer price;

    @NotNull
    @ApiModelProperty(value = "per_id ID of the article's performance")
    private Integer perf_id;

    @NotNull
    @ApiModelProperty(value = "article's performance")
    private PerformanceDto performanceDto;

    public Integer getId() {
        return id;
    }

    public void setId( Integer id) {
        this.id = id;
    }

    public Integer getAvailable() {
        return available;
    }

    public void setAvailable(Integer available) {
        this.available = available;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(@Valid String name) {
        this.name = name;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(@Valid Integer price) {
        this.price = price;
    }

    public Integer getPerf_id() {
        return perf_id;
    }

    public void setPerf_id(@Valid Integer perf_id) {
        this.perf_id = perf_id;
    }

    public PerformanceDto getPerformanceDto() {
        return performanceDto;
    }

    public void setPerformanceDto(@Valid PerformanceDto performanceDto) {
        this.performanceDto = performanceDto;
    }

    @Override
    public String toString() {
        return "Merchandise [name=" + name + ", perfid=" + perf_id + "]";
    }
}