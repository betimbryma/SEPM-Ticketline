package at.ac.tuwien.inso.ticketline.dto;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@ApiModel(value = "ReservationDto", description = "DTO for ReservationDto")
public class ReservationDto {
    @ApiModelProperty(value = "ID of Reservation")
    private Integer id;

    @NotNull
    @ApiModelProperty(value = "number of Reservation")
    private Integer reservationNumber;

    @ApiModelProperty(value = "Customer of Reservation")
    private CustomerDto customer;
    @ApiModelProperty(value = "Employee that made Reservation")
    private EmployeeDto employee;
/*
    @ApiModelProperty(value = "TicketIdentifier of Reservation")
    private List<TicketIdentifierDto> ticketIdentifiers;
*/
    /**
     * Gets the id.
     *
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId( Integer id) {
        this.id = id;
    }

    /**
     * Gets the reservation number.
     *
     * @return the reservation number
     */
    public Integer getReservationNumber() {
        return reservationNumber;
    }

    /**
     * Sets the reservation number.
     *
     * @param reservationNumber the new reservation number
     */
    public void setReservationNumber(@Valid Integer reservationNumber) {
        this.reservationNumber = reservationNumber;
    }

    /**
     * Gets the customer.
     *
     * @return the customer
     */
    public CustomerDto getCustomer() {
        return customer;
    }

    /**
     * Sets the customer.
     *
     * @param customer the new customer
     */
    public void setCustomer(CustomerDto customer) {
        this.customer = customer;
    }

    /**
     * Gets the employee.
     *
     * @return the employee
     */
    public EmployeeDto getEmployee() {
        return employee;
    }

    /**
     * Sets the employee.
     *
     * @param employee the new employee
     */
    public void setEmployee(EmployeeDto employee) {
        this.employee = employee;
    }

    /**
     * Gets the ticket identifiers.
     *
     * @return the ticket identifiers

    public List<TicketIdentifierDto> getTicketIdentifiers() {
        return ticketIdentifiers;
    }

    /**
     * Sets the ticket identifiers.
     *
     * @param ticketIdentifiers the new ticket identifiers

    public void setTicketIdentifiers(List<TicketIdentifierDto> ticketIdentifiers) {
        this.ticketIdentifiers = ticketIdentifiers;
    }
     */
}
