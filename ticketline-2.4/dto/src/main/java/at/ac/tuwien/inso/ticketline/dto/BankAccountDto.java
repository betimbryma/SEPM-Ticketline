package at.ac.tuwien.inso.ticketline.dto;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@ApiModel(value = "BankAccountDto", description = "Data transfer object for a Bank Account")
public class BankAccountDto {

    @ApiModelProperty(value = "owner of the receipt")
    private String owner;

    @NotNull
    @Size(min = 2)
    @ApiModelProperty(value = "bank of the receipt",required = true)
    private String bank;

    @NotNull
    @Size(min = 5)
    @ApiModelProperty(value = "accountNumber of the receipt",required = true)
    private String accountNumber;

    @NotNull
    @Size(min = 1)
    @ApiModelProperty(value = "bankCode of the receipt",required = true)
    private String bankCode;

    @ApiModelProperty(value = "IBAN of the receipt")
    private String IBAN;
    @ApiModelProperty(value = "BIC of the receipt")
    private String BIC;

    @ApiModelProperty(value = "id of the receipt")
    protected int id;

    @ApiModelProperty(value = "customer of the receipt",required = true)
    protected CustomerDto customer;
    @ApiModelProperty(value = "state of payment")
    protected Boolean deleted;

    public int getId() {
        return id;
    }

    public void setId( int id) {
        this.id = id;
    }

    public CustomerDto getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerDto customer) {
        this.customer = customer;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(@Valid String bank) {
        this.bank = bank;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(@Valid String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(@Valid String bankCode) {
        this.bankCode = bankCode;
    }

    public String getIBAN() {
        return IBAN;
    }

    public void setIBAN(String IBAN) {
        this.IBAN = IBAN;
    }

    public String getBIC() {
        return BIC;
    }

    public void setBIC(String BIC) {
        this.BIC = BIC;
    }
}
