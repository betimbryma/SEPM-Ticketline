package at.ac.tuwien.inso.ticketline.dto;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@ApiModel(value = "GalleryDto", description = "DTO for GalleryDto")
public class GalleryDto {

    @ApiModelProperty(value = "ID of Gallery")
    private Integer id;

    @ApiModelProperty(value = "Name of Gallery")
    private String name;
    @ApiModelProperty(value = "Description of Gallery")
    private String description;

    @NotNull
    @ApiModelProperty(value = "Order of Gallery")
    private Integer order;
/*
    @ApiModelProperty(value = "Description of Gallery")
    private List<SeatDto> seats;
*/
    /**
     * Gets the id.
     *
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId( Integer id) {
        this.id = id;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name.
     *
     * @param name the new name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the description.
     *
     * @param description the new description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets the order.
     *
     * @return the order
     */
    public Integer getOrder() {
        return order;
    }

    /**
     * Sets the order.
     *
     * @param order the new order
     */
    public void setOrder(@Valid Integer order) {
        this.order = order;
    }

    /**
     * Gets the seats.
     *
     * @return the seats

    public List<SeatDto> getSeats() {
        return seats;
    }

    /**
     * Sets the seats.
     *
     * @param seats the new seats

    public void setSeats(List<SeatDto> seats) {
        this.seats = seats;
    }
     */
}
