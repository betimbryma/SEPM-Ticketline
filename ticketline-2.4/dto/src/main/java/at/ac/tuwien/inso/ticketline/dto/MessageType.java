package at.ac.tuwien.inso.ticketline.dto;

public enum MessageType {
    SUCCESS,
    INFO,
    ERROR,
}
