package at.ac.tuwien.inso.ticketline.dto;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@ApiModel(value = "TicketDto", description = "Data transfer object for a ticket")
public class TicketDto {
    private static final long serialVersionUID = 2355163364458707580L;

    @ApiModelProperty(value = "ID of the ticket")
    private Integer id;

    @ApiModelProperty(value = "Description of the ticket")
    private String description;

    @NotNull
    @ApiModelProperty(value = "Price of ticket")
    private Integer price;

    @NotNull
    @ApiModelProperty(value = "The tshow that this ticket is made for")
    private ShowDto show;

    @NotNull
    @ApiModelProperty(value = "Seat for which this ticket is made for")
    private SeatDto seat;

    @ApiModelProperty(value = "Flag to show if it is in cart")
    private Integer flag;
    @ApiModelProperty(value = "Date flag valid")
    private Date flagdate;
    @ApiModelProperty(value = "TicketIdentifiers of Ticket")
    private List<TicketIdentifierDto> ticketIdentifiers;
    private TicketIdentifierDto oldTE;

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId( Integer id) {
        this.id = id;
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the description.
     *
     * @param description the new description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets the price.
     *
     * @return the price
     */
    public Integer getPrice() {
        return price;
    }

    /**
     * Sets the price.
     *
     * @param price the new price
     */
    public void setPrice(@Valid Integer price) {
        this.price = price;
    }

    /**
     * Gets the show.
     *
     * @return the show
     */
    public ShowDto getShow() {
        return show;
    }

    /**
     * Sets the show.
     *
     * @param show the new show
     */
    public void setShow(@Valid ShowDto show) {
        this.show = show;
    }

    /**
     * Gets the seat.
     *
     * @return the seat
     */
    public SeatDto getSeat() {
        return seat;
    }

    /**
     * Sets the seat.
     *
     * @param seat the new seat
     */
    public void setSeat(@Valid SeatDto seat) {
        this.seat = seat;
    }

    /**
     * Gets the ticket identifiers.
     *
     * @return the ticket identifiers
     */
    public List<TicketIdentifierDto> getTicketIdentifiers() {
        return ticketIdentifiers;
    }

    /**
     * Sets the ticket identifiers.
     *
     * @param ticketIdentifiers the new ticket identifiers
        */
    public void setTicketIdentifiers(List<TicketIdentifierDto> ticketIdentifiers) {
        this.ticketIdentifiers = ticketIdentifiers;
    }

    public TicketIdentifierDto getOldTE() {
        return oldTE;
    }

    public void setOldTE(TicketIdentifierDto oldTE) {
        this.oldTE = oldTE;
    }


    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public Date getFlagdate() {
        return flagdate;
    }

    public void setFlagdate(Date flagdate) {
        this.flagdate = flagdate;
    }

    @Override
    public boolean equals(Object that){
        if (this == that) {
            return true;
        }
        if (that == null || that.getClass() != this.getClass()) {
            return false;
        }

        TicketDto ticketDto = (TicketDto) that;
        return ticketDto.getId().equals(this.getId());
    }


}
