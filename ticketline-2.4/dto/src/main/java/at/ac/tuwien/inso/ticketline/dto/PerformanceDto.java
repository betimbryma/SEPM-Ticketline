package at.ac.tuwien.inso.ticketline.dto;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@ApiModel(value = "PerformanceDto", description = "Data transfer object for a Performance")
public class PerformanceDto {


    @ApiModelProperty(value = "Unique ID of the performance")
    private Integer id;

    @NotNull
    @Size(min = 2)
    @ApiModelProperty(value = "name of the performance")
    private String name;

    @NotNull
    @ApiModelProperty(value = "description of the performance")
    private String description;

    @ApiModelProperty(value = "duration of the performance")
    private int duration;
    @ApiModelProperty(value = "performancetype of the performance")
    private String performancetype;
    @ApiModelProperty(value = "articles of the performance")
    private List<MerchandiseDto> articles = new ArrayList<>();
    @ApiModelProperty(value = "shows of the performance")
    private List<ShowDto> shows;
    @ApiModelProperty(value = "participations of the performance")
    private List<ParticipationDto> participations;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPerformancetype() {
        return performancetype;
    }

    public void setPerformancetype(String performancetype) {
        this.performancetype = performancetype;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(@Valid String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(@Valid String name) {
        this.name = name;
    }


    public List<ParticipationDto> getParticipations() {
        return participations;
    }

    public void setParticipations(List<ParticipationDto> participations) {
        this.participations = participations;
    }

    public List<ShowDto> getShows() {
        return shows;
    }

    public void setShows(List<ShowDto> shows) {
        this.shows = shows;
    }

    public List<MerchandiseDto> getArticles() {
        return articles;
    }

    public void setArticles(List<MerchandiseDto> articles) {
        this.articles = articles;
    }


    public void addArticle(MerchandiseDto merchandiseDto) {
        articles.add(merchandiseDto);
    }


    @Override
    public String toString() {
        return "Performance [name=" + name + ", perfid=" + id + "]";
    }
}

