package at.ac.tuwien.inso.ticketline.dto;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@ApiModel(value = "SeatDto", description = "DTO for SeatDto")
public class SeatDto {

    @ApiModelProperty(value = "ID of seat")
    private Integer id;

    @ApiModelProperty(value = "name of seat")
    private String name;
    @ApiModelProperty(value = "Description of seat")
    private String description;

    @NotNull
    @ApiModelProperty(value = "Order of seat")
    private Integer order;

    @NotNull
    @ApiModelProperty(value = "Room of seat")
    private RoomDto room;

    @NotNull
    @ApiModelProperty(value = "Category of seat")
    private CategoryDto category;

    @NotNull
    @ApiModelProperty(value = "Row of seat")
    private RowDto row;


    @ApiModelProperty(value = "Gallery of seat")
    private GalleryDto gallery;

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId( Integer id) {
        this.id = id;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name.
     *
     * @param name the new name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the description.
     *
     * @param description the new description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets the order.
     *
     * @return the order
     */
    public Integer getOrder() {
        return order;
    }

    /**
     * Sets the order.
     *
     * @param order the new order
     */
    public void setOrder(@Valid Integer order) {
        this.order = order;
    }

    /**
     * Gets the room.
     *
     * @return the room
     */
    public RoomDto getRoom() {
        return room;
    }

    /**
     * Sets the room.
     *
     * @param room the new room
     */
    public void setRoom(@Valid RoomDto room) {
        this.room = room;
    }

    /**
     * Gets the category.
     *
     * @return the category
     */
    public CategoryDto getCategory() {
        return category;
    }

    /**
     * Sets the category.
     *
     * @param category the new category
     */
    public void setCategory(@Valid CategoryDto category) {
        this.category = category;
    }

    /**
     * Gets the row.
     *
     * @return the row
     */
    public RowDto getRow() {
        return row;
    }

    /**
     * Sets the row.
     *
     * @param row the new row
     */
    public void setRow(@Valid RowDto row) {
        this.row = row;
    }

    /**
     * Gets the gallery.
     *
     * @return the gallery
     */
    public GalleryDto getGallery() {
        return gallery;
    }

    /**
     * Sets the gallery.
     *
     * @param gallery the new gallery
     */
    public void setGallery( GalleryDto gallery) {
        this.gallery = gallery;
    }
}
