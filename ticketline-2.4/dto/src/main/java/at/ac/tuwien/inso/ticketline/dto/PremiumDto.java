package at.ac.tuwien.inso.ticketline.dto;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by eni on 04.06.2016.
 */
@ApiModel(value = "PremiumDto", description = "Data transfer object for Premium")
public class PremiumDto {


    @ApiModelProperty(value = "Unique ID of the PremiumDto")
    private Integer id;

    @ApiModelProperty(value = "available of the PremiumDto")
    private Integer available;
    @ApiModelProperty(value = "description of the PremiumDto")
    private String description;

    @NotNull
    @Size(min = 2)
    @ApiModelProperty(value = "name ID of the PremiumDto")
    private String name;

    @NotNull
    @ApiModelProperty(value = "price ID of the PremiumDto")
    private Integer price;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(@Valid Integer price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(@Valid String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getAvailable() {
        return available;
    }

    public void setAvailable(Integer available) {
        this.available = available;
    }
}
