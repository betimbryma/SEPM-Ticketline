package at.ac.tuwien.inso.ticketline.dto;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.internal.constraintvalidators.hv.EmailValidator;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

@ApiModel(value = "CustomerDto", description = "Data transfer object for a customer")
public class CustomerDto {

    @NotNull
    @Size(min = 2)
    @ApiModelProperty(value = "Firstname of the customer", required = true)
    private String firstname;

    @NotNull
    @Size(min = 2)
    @ApiModelProperty(value = "Lastname of the customer", required = true)
    private String lastname;

    @ApiModelProperty(value = "Street where customer lives")
    private String street;
    @ApiModelProperty(value = "City where customer lives")
    private String city;

    @NotNull
    @Size(min = 2)
    @ApiModelProperty(value = "Postalcode of city where customer lives", required = true)
    private String postalcode;

    @ApiModelProperty(value = "Country where customer lives")
    private String country;
    @ApiModelProperty(value = "Gender of the customer")
    private String gender;
    @ApiModelProperty(value = "Date of the customers birth")
    private Date dateOfBirth;

    @Email
    @ApiModelProperty(value = "Email of the customer")
    private String email;

    @ApiModelProperty(value = "Phonenumber of the customer")
    private String phoneNumber;

    @ApiModelProperty(value = "Unique ID of the customer")
    private Integer id;


    @ApiModelProperty(value= "Premium number")
    private Integer premiumPoints;

    public Integer getPremiumPoints() {
        return premiumPoints;
    }

    public void setPremiumPoints(Integer premiumPoints) {
        this.premiumPoints = premiumPoints;
    }

    /**
     * @return the customers firstname
     */
    public String getFirstname() {
        return firstname;
    }

    /**
     * @param firstname the customers firstname
     */
    public void setFirstname(@Valid String firstname) {
        this.firstname = firstname;
    }

    /**
     * @return the customers lastname
     */
    public String getLastname() {
        return lastname;
    }

    /**
     * @param lastname the customers lastname
     */
    public void setLastname(@Valid String lastname) {
        this.lastname = lastname;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostalcode() {
        return postalcode;
    }

    public void setPostalcode(@Valid String postalcode) {
        this.postalcode = postalcode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(@Valid String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Integer getId() {
        return id;
    }

    public void setId( Integer id) {
        this.id = id;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "NewsDto [firstname=" + firstname + ", lastname=" + lastname + "]";
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null || that.getClass() != this.getClass()) {
            return false;
        }
        CustomerDto customerDto = (CustomerDto) that;

    return objectEquals(this.id, customerDto.id) && objectEquals(this.firstname, customerDto.firstname)
            && objectEquals(this.lastname, customerDto.lastname) && objectEquals(this.street, customerDto.street)
            && objectEquals(this.city, customerDto.city) && objectEquals(this.postalcode, customerDto.postalcode)
            && objectEquals(this.country, customerDto.country) && objectEquals(this.gender, customerDto.gender)
            && objectEquals(this.email, customerDto.email) && objectEquals(this.phoneNumber, customerDto.phoneNumber)
            && compareDate(this.dateOfBirth, customerDto.dateOfBirth);
    }

    // to avoid nullpointerexceptions while comparing
    private boolean objectEquals(Object o1, Object o2) {
        return (o1 == null && o2 == null) || o1.equals(o2);
    }

    private boolean compareDate(Date d1, Date d2) {
        if(d1 != null && d2 != null) {
           return d1.toInstant().atZone(ZoneId.systemDefault()).toLocalDate().equals(d2.toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
        } else {
            return d1 == null && d2 == null;
        }
    }

    /**
     * It is going to copy 'this''s variables into customerDto's variables
     *
     * @param customerDto that it is goign to be copied
     */
    public void copyCustomerDto(CustomerDto customerDto) {
        customerDto.id = this.id;
        customerDto.firstname = this.firstname;
        customerDto.lastname = this.lastname;
        customerDto.street = this.street;
        customerDto.city = this.city;
        customerDto.postalcode = this.postalcode;
        customerDto.country = this.country;
        customerDto.gender = this.gender;
        customerDto.email = this.email;
        customerDto.phoneNumber = this.phoneNumber;
        customerDto.dateOfBirth = new Date(this.dateOfBirth.getTime());
    }

}
