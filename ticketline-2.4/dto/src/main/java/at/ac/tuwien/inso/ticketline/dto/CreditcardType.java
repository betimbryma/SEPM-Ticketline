package at.ac.tuwien.inso.ticketline.dto;

import com.wordnik.swagger.annotations.ApiModel;

public enum CreditcardType {
    VISA,
    MASTERCARD,
    AMERICAN_EXPRESS,
}
