package at.ac.tuwien.inso.ticketline.dto;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.UUID;

@ApiModel(value = "TicketIdentifierDto", description = "DTO for TicketIdentifierDto")
public class TicketIdentifierDto {


    @ApiModelProperty(value = "ID of TicketIdentifier")
    private Integer id;

    @ApiModelProperty(value = "UUID of TicketIdentifier")
    private UUID uuid;
    @ApiModelProperty(value = "Validity of TicketIdentifier")
    private Boolean valid;
    @ApiModelProperty(value = "Cancellation reason of TicketIdentifier")
    private String cancellationReason;
    @ApiModelProperty(value = "TicketIdentifier voided by")
    private String voidedBy;
    @ApiModelProperty(value = "Date of voidation of TicketIdentifier")
    private Date voidationTime;
    @ApiModelProperty(value = "TicketIdentifier of reservation")
    private ReservationDto reservation;

    @NotNull
    @ApiModelProperty(value = "Ticket of TicketIdentifier")
    private TicketDto ticket;
    //private ReceiptEntry receiptEntry;

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId( Integer id) {
        this.id = id;
    }

    /**
     * Gets the uuid.
     *
     * @return the uuid
     */
    public UUID getUuid() {
        return uuid;
    }

    /**
     * Sets the uuid.
     *
     * @param uuid the new uuid
     */
    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    /**
     * Gets the valid.
     *
     * @return the valid
     */
    public Boolean getValid() {
        return valid;
    }

    /**
     * Sets the valid.
     *
     * @param valid the new valid
     */
    public void setValid(Boolean valid) {
        this.valid = valid;
    }

    /**
     * Gets the cancellation reason.
     *
     * @return the cancellation reason
     */
    public String getCancellationReason() {
        return cancellationReason;
    }

    /**
     * Sets the cancellation reason.
     *
     * @param cancellationReason the new cancellation reason
     */
    public void setCancellationReason(String cancellationReason) {
        this.cancellationReason = cancellationReason;
    }

    /**
     * Gets the voided by.
     *
     * @return the voided by
     */
    public String getVoidedBy() {
        return voidedBy;
    }

    /**
     * Sets the voided by.
     *
     * @param voidedBy the new voided by
     */
    public void setVoidedBy(String voidedBy) {
        this.voidedBy = voidedBy;
    }

    /**
     * Gets the voidation time.
     *
     * @return the voidation time
     */
    public Date getVoidationTime() {
        return voidationTime;
    }

    /**
     * Sets the voidation time.
     *
     * @param voidationTime the new voidation time
     */
    public void setVoidationTime(Date voidationTime) {
        this.voidationTime = voidationTime;
    }

    /**
     * Gets the reservation.
     *
     * @return the reservation
     */
    public ReservationDto getReservation() {
        return reservation;
    }

    /**
     * Sets the reservation.
     *
     * @param reservation the new reservation
     */
    public void setReservation(ReservationDto reservation) {
        this.reservation = reservation;
    }

    /**
     * Gets the ticket.
     *
     * @return the ticket
     */
    public TicketDto getTicket() {
        return ticket;
    }

    /**
     * Sets the ticket.
     *
     * @param ticket the new ticket
     */
    public void setTicket(@Valid TicketDto ticket) {
        this.ticket = ticket;
    }

    /**
     * Gets the receipt entry.
     *
     * @return the receipt entry

    public ReceiptEntry getReceiptEntry() {
        return receiptEntry;
    }
     */

    /**
     * Sets the receipt entry.
     *
     * @param receiptEntry the new receipt entry

    public void setReceiptEntry(ReceiptEntry receiptEntry) {
        this.receiptEntry = receiptEntry;
    }
     */
}
