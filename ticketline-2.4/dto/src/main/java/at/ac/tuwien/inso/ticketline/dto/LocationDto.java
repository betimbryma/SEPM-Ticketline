package at.ac.tuwien.inso.ticketline.dto;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@ApiModel(value = "LocationDto", description = "Data transfer object for a location")
public class LocationDto {


    @ApiModelProperty(value = "Unique id of location")
    private Integer id;

    @NotNull
    @Size(min = 2)
    @ApiModelProperty(value = "Name of location")
    private String name;

    @ApiModelProperty(value = "Description of location")
    private String description;
    @ApiModelProperty(value = "Owner of location")
    private String owner;
    @ApiModelProperty(value = "Street of location")
    private String street;
    @ApiModelProperty(value = "Postal code of location")
    private String postalCode;
    @ApiModelProperty(value = "City of location")
    private String city;
    @ApiModelProperty(value = "Country of location")
    private String country;
    @ApiModelProperty(value = "Rooms of location")
    private List<RoomDto> rooms;

    public Integer getId() {
        return id;
    }

    public void setId( Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(@Valid String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public List<RoomDto> getRooms() {
        return rooms;
    }

    public void setRooms(List<RoomDto> rooms) {
        this.rooms = rooms;
    }
}
