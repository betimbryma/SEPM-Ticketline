package at.ac.tuwien.inso.ticketline.dto;

public enum UserEvent {
    AUTH_SUCCESS,
    AUTH_FAILURE,
    AUTH_SUSPENDED,
    AUTH_NOW_SUSPENDED,
    ONE_ATTEMPT_LEFT,
    TWO_ATTEMPTS_LEFT,
    THREE_ATTEMPTS_LEFT,
    USER_INVALID,
    LOGOUT,
}
