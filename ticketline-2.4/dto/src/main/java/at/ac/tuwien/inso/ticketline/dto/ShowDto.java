package at.ac.tuwien.inso.ticketline.dto;


import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.text.SimpleDateFormat;
import java.util.Date;

@ApiModel(value = "ShowDto", description = "Data transfer object for a show")
public class ShowDto {

    @ApiModelProperty(value = "Unique id of show")
    private int id;

    @ApiModelProperty(value = "If the show was canceled")
    private boolean canceled;
    @ApiModelProperty(value = "Date of the Show")
    private Date dateOfPerformance;

    @NotNull
    @ApiModelProperty(value = "Room for the show")
    private RoomDto room;

    @NotNull
    @ApiModelProperty(value = "Performance for the show")
    private PerformanceDto performance;

    public int getId() {
        return id;
    }

    public void setId( int id) {
        this.id = id;
    }

    public boolean isCanceled() {
        return canceled;
    }

    public void setCanceled(boolean canceled) {
        this.canceled = canceled;
    }

    public Date getDateOfPerformance() {
        return dateOfPerformance;
    }

    public void setDateOfPerformance(Date dateOfPerformance) {
        this.dateOfPerformance = dateOfPerformance;
    }

    public RoomDto getRoom() {
        return room;
    }

    public void setRoom(@Valid RoomDto room) {
        this.room = room;
    }

    public PerformanceDto getPerformance() {
        return performance;
    }

    public void setPerformance(@Valid PerformanceDto performance) {
        this.performance = performance;
    }

    @Override
    public String toString(){
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.YYYY");
        return performance.getName()+":" + formatter.format(dateOfPerformance);
    }

    @Override
    public boolean equals(Object that){

        if (this == that) {
            return true;
        }
        if (that == null || that.getClass() != this.getClass()) {
            return false;
        }

        ShowDto showDto = (ShowDto) that;

        return showDto.id == this.id; //Right now we don't need to check in more details, it is sufficient like this rn
    }

    @Override
    public int hashCode(){
        return this.id;
    }

}
