package at.ac.tuwien.inso.ticketline.dto;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@ApiModel(value = "ParticipationDto", description = "Data transfer object for a participation")
public class ParticipationDto {

    @ApiModelProperty(value = "Unique id of participation")
    private int id;

    @ApiModelProperty(value = "Role of the artist")
    private String artistRole;
    @ApiModelProperty(value = "Description of the participation")
    private String description;

    @NotNull
    @ApiModelProperty(value = "Performance for the participation")
    private PerformanceDto performance;

    @NotNull
    @ApiModelProperty(value = "Artist that participates")
    private ArtistDto artist;

    public ArtistDto getArtist() {
        return artist;
    }

    public void setArtist(@Valid ArtistDto artist) {
        this.artist = artist;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getArtistRole() {
        return artistRole;
    }

    public void setArtistRole(String artistRole) {
        this.artistRole = artistRole;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public PerformanceDto getPerformance() {
        return performance;
    }

    public void setPerformance(@Valid PerformanceDto performance) {
        this.performance = performance;
    }

}