package at.ac.tuwien.inso.ticketline.dto;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@ApiModel(value = "ReceiptDto", description = "Data transfer object for a Receipt")
public class ReceiptDto {


    @ApiModelProperty(value = "Id of the receipt")
    private int id;

    @ApiModelProperty(value = "transactionDate of the receipt")
    private Date transactionDate;
    @ApiModelProperty(value = "Id of the receipt")
    private String transactionState;

    @NotNull
    @ApiModelProperty(value = "customer of the receipt")
    private CustomerDto customer;

    @ApiModelProperty(value = "employee of the receipt")
    private UserStatusDto employee;
    @ApiModelProperty(value = "Method of Payment for this reciept of the receipt")
    private CashDto cashDto;
    private BankAccountDto bankAccountDto;
    private CreditCardDto creditCardDto;
    @ApiModelProperty(value = "receiptentryDto of the receipt")
    private List<ReceiptentryDto> receiptentryDto;

    @ApiModelProperty(value = "Premium or sell")
    private boolean premium;

    public boolean isPremium() {
        return premium;
    }

    public void setPremium(boolean premium) {
        this.premium = premium;
    }
    public int getId() {
        return id;
    }

    public void setId( int id) {
        this.id = id;
    }

    public List<ReceiptentryDto> getReceiptentryDto() {
        return receiptentryDto;
    }

    public void setReceiptentryDto(List<ReceiptentryDto> receiptentryDto) {
        this.receiptentryDto = receiptentryDto;
    }

    public UserStatusDto getEmployee() {
        return employee;
    }

    public void setEmployee(UserStatusDto employee) {
        this.employee = employee;
    }

    public CustomerDto getCustomer() {
        return customer;
    }

    public void setCustomer(@Valid CustomerDto customer) {
        this.customer = customer;
    }

    public String getTransactionState() {
        return transactionState;
    }

    public void setTransactionState(String transactionState) {
        this.transactionState = transactionState;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }


    public CashDto getCashDto() {
        return cashDto;
    }

    public void setCashDto(CashDto cashDto) {
        this.cashDto = cashDto;
    }

    public BankAccountDto getBankAccountDto() {
        return bankAccountDto;
    }

    public void setBankAccountDto(BankAccountDto bankAccountDto) {
        this.bankAccountDto = bankAccountDto;
    }

    public CreditCardDto getCreditCardDto() {
        return creditCardDto;
    }

    public void setCreditCardDto(CreditCardDto creditCardDto) {
        this.creditCardDto = creditCardDto;
    }

    @Override
    public String toString() {
        return "ReceiptDto [name=" + transactionDate + ", transactionState=" + transactionState + "]";
    }
}

