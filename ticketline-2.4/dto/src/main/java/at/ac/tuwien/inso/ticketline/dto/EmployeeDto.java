package at.ac.tuwien.inso.ticketline.dto;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

import java.util.Date;

@ApiModel(value = "EmployeeDto", description = "Data transfer object for an employee")
public class EmployeeDto {


    @ApiModelProperty(value = "Unique ID of the employee")
    private Integer id;


    @ApiModelProperty(value = "Username")
    private String username;


    @ApiModelProperty(value = "First name of the employee")
    private String firstName;


    @ApiModelProperty(value = "Last name of the employee")
    private String lastName;

    @ApiModelProperty(value = "Date and Time of the employees last login")
    private Date lastlogin;
    @ApiModelProperty(value = "Permissions of the employee")
    private String permission;


    @ApiModelProperty(value = "Password hash of the employee")
    private String PasswordHash;

    public String getPasswordHash() {
        return PasswordHash;
    }

    public void setPasswordHash( String passwordHash) {
        PasswordHash = passwordHash;
    }

    public Integer getId() {
        return id;
    }

    public void setId( Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername( String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName( String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName( String lastName) {
        this.lastName = lastName;
    }

    public Date getLastlogin() {
        return lastlogin;
    }

    public void setLastlogin(Date lastlogin) {
        this.lastlogin = lastlogin;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }
}
