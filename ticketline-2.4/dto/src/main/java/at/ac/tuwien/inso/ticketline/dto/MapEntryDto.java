package at.ac.tuwien.inso.ticketline.dto;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@ApiModel(value = "MapEntryDto", description = "MapEntry data transfer objet")
public class MapEntryDto<K,V> {


    @ApiModelProperty(value = "Key")
    private K key;


    @ApiModelProperty(value = "Value")
    private V value;

    public MapEntryDto(){}

    public MapEntryDto(K key, V value){
        this.key=key;
        this.value=value;
    }

    public K getKey() {
        return key;
    }

    public void setKey(K key) {
        this.key = key;
    }

    public V getValue() {
        return value;
    }

    public void setValue(V value) {
        this.value = value;
    }
}
