package at.ac.tuwien.inso.ticketline.dto;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@ApiModel(value = "CategoryDto", description = "DTO for CategoryDto")
public class CategoryDto {

    @ApiModelProperty(value = "ID of Category")
    private Integer id;

    @NotNull
    @Size(min = 2)
    @ApiModelProperty(value = "Name of Category")
    private String name;

    @ApiModelProperty(value = "Description of Category")
    private String description;
    /*
    private List<Seat> seats;
*/
    /**
     * Gets the id.
     *
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name.
     *
     * @param name the new name
     */
    public void setName(@Valid String name) {
        this.name = name;
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the description.
     *
     * @param description the new description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets the seat.
     *
     * @return the seat

    public List<Seat> getSeat() {
        return seats;
    }

    /**
     * Sets the seat.
     *
     * @param seats the new seat

    public void setSeat(List<Seat> seats) {
        this.seats = seats;
    }
     */
}
