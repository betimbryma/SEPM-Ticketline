package at.ac.tuwien.inso.ticketline.dto;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

@ApiModel(value = "ReceiptentryDto", description = "Data transfer object for a Receiptentry")
public class ReceiptentryDto {

    @ApiModelProperty(value = "Id of the receiptentry")
    private Integer id;
    @ApiModelProperty(value = "position of the receiptentry")
    private Integer position;
    @ApiModelProperty(value = "amount of the receiptentry")
    private Integer amount;
    @ApiModelProperty(value = "unitPrice of the receiptentry")
    private Integer unitPrice;
    @ApiModelProperty(value = "receipt of the receiptentry")
    private MerchandiseDto receipt;
    @ApiModelProperty(value = "article of the receiptentry")
    private MerchandiseDto article;
    @ApiModelProperty(value = "Id of the receipt")
    private TicketIdentifierDto ticketIdentifierDto;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public MerchandiseDto getArticle() {
        return article;
    }

    public void setArticle(MerchandiseDto article) {
        this.article = article;
    }

    public MerchandiseDto getReceipt() {
        return receipt;
    }

    public void setReceipt(MerchandiseDto receipt) {
        this.receipt = receipt;
    }

    public Integer getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(Integer unitPrice) {
        this.unitPrice = unitPrice;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public TicketIdentifierDto getTicketIdentifierDto() {
        return ticketIdentifierDto;
    }

    public void setTicketIdentifierDto(TicketIdentifierDto ticketIdentifierDto) {
        this.ticketIdentifierDto = ticketIdentifierDto;
    }

    @Override
    public String toString() {
        return "ReceiptDto [name=" + position + ", transactionState=" + amount + "]";
    }
}
