package at.ac.tuwien.inso.ticketline.dto;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@ApiModel(value = "CreditCardDto", description = "Data transfer object for a Credit Card")
public class CreditCardDto {

    @ApiModelProperty(value = "owner of the receipt")
    private String owner;

    @NotNull
    @Size(min = 7)
    @ApiModelProperty(value = "creditcardNumber of the receipt",required = true)
    private String creditcardNumber;

    @ApiModelProperty(value = "validThru of the receipt")
    private Date validThru;
    @ApiModelProperty(value = "creditcardType of the receipt")
    private CreditcardType creditcardType;


    @ApiModelProperty(value = "id of the receipt")
    protected int id;

    @ApiModelProperty(value = "customer of the receipt",required = true)
    protected CustomerDto customer;
    @ApiModelProperty(value = "state of payment")
    protected Boolean deleted;

    public int getId() {
        return id;
    }

    public void setId( int id) {
        this.id = id;
    }

    public CustomerDto getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerDto customer) {
        this.customer = customer;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getCreditcardNumber() {
        return creditcardNumber;
    }

    public void setCreditcardNumber(@Valid String creditcardNumber) {
        this.creditcardNumber = creditcardNumber;
    }

    public Date getValidThru() {
        return validThru;
    }

    public void setValidThru(Date validThru) {
        this.validThru = validThru;
    }

    public CreditcardType getCreditcardType() {
        return creditcardType;
    }

    public void setCreditcardType(CreditcardType creditcardType) {
        this.creditcardType = creditcardType;
    }
}
