package at.ac.tuwien.inso.ticketline.dto;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@ApiModel(value = "CashDto", description = "Data transfer object for Cash")
public class CashDto {

    @ApiModelProperty(value = "id of the receipt")
    protected int id;

    @ApiModelProperty(value = "customer of the receipt",required = true)
    protected CustomerDto customer;

    @ApiModelProperty(value = "state of payment")
    protected Boolean deleted;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public CustomerDto getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerDto customer) {
        this.customer = customer;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}
