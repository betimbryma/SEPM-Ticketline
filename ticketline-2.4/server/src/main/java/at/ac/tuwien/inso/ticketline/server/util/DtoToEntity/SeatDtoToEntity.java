package at.ac.tuwien.inso.ticketline.server.util.DtoToEntity;

import at.ac.tuwien.inso.ticketline.dto.SeatDto;
import at.ac.tuwien.inso.ticketline.model.Seat;

public class SeatDtoToEntity {

    private static SeatDtoToEntity instance = null;
    private SeatDtoToEntity() {}
    public static SeatDtoToEntity getInstance() {
        if(instance == null) {
            instance = new SeatDtoToEntity();
        }
        return instance;
    }

    public static Seat convert(SeatDto seatDto){
        Seat seat = new Seat();
        seat.setRoom(RoomDtoToEntity.getInstance().convert(seatDto.getRoom()));
        seat.setId(seatDto.getId());
        seat.setRow(RowDtoToEntity.getInstance().convert(seatDto.getRow()));
        seat.setDescription(seatDto.getDescription());
        seat.setName(seatDto.getName());
        seat.setOrder(seatDto.getOrder());
        seat.setGallery(GalleryDtoToEntity.getInstance().convert(seatDto.getGallery()));
        seat.setCategory(CategoryDtoToEntity.getInstance().convert(seatDto.getCategory()));
        return seat;
    }
}
