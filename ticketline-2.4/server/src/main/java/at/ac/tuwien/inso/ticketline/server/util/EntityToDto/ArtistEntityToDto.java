package at.ac.tuwien.inso.ticketline.server.util.EntityToDto;

import at.ac.tuwien.inso.ticketline.dto.ArtistDto;
import at.ac.tuwien.inso.ticketline.model.Artist;

import java.util.ArrayList;
import java.util.List;

public class ArtistEntityToDto {

    private static ArtistEntityToDto instance = null;
    private ArtistEntityToDto() {}
    public static ArtistEntityToDto getInstance() {
        if(instance == null) {
            instance = new ArtistEntityToDto();
        }
        return instance;
    }

    /**
     * Converts an artist entity to an artist DTO.
     *
     * @param a the artist entity
     * @return the converted artist DTO
     */
    public static ArtistDto convertArtist(Artist a) {
        ArtistDto artistDto = new ArtistDto();
        artistDto.setId(a.getId());
        artistDto.setFirstName(a.getFirstname());
        artistDto.setLastName(a.getLastname());
        artistDto.setDescription(a.getDescription());
        return artistDto;
    }

    /**
     * Converts an artist entity list to an artist DTO list.
     *
     * @param artists the list of artist entities
     * @return the converted artist DTO list
     */
    public static List<ArtistDto> convertArtists(List<Artist> artists) {
        List<ArtistDto> result = new ArrayList<>();

        if (artists != null) {
            for (Artist a : artists) {
                result.add(convertArtist(a));
            }
        }

        return result;
    }
}
