package at.ac.tuwien.inso.ticketline.server.util.EntityToDto;

import at.ac.tuwien.inso.ticketline.dto.LocationDto;
import at.ac.tuwien.inso.ticketline.model.Location;

import java.util.ArrayList;
import java.util.List;

public class LocationEntityToDto {

    private static LocationEntityToDto instance = null;
    private LocationEntityToDto() {}
    public static LocationEntityToDto getInstance() {
        if(instance == null) {
            instance = new LocationEntityToDto();
        }
        return instance;
    }

    /**
     * Converts a location entity to an location DTO
     *
     * @param s the location entity
     * @return the converted location DTO
     */
    public static LocationDto convertLocation(Location s) {

        LocationDto locationDto = new LocationDto();
        locationDto.setId(s.getId());
        locationDto.setDescription(s.getDescription());
        locationDto.setName(s.getName());
        locationDto.setOwner(s.getOwner());
        locationDto.setStreet(s.getAddress().getStreet());
        locationDto.setPostalCode(s.getAddress().getPostalCode());
        locationDto.setCity(s.getAddress().getCity());
        locationDto.setCountry(s.getAddress().getCountry());

        return locationDto;
    }

    /**
     * Converts a location entity list to a location DTO list
     *
     * @param locations the list of location entities
     * @return the converted location DTO list
     */
    public static List<LocationDto> convertLocations(List<Location> locations) {
        List<LocationDto> result = new ArrayList<>();

        if (locations != null) {
            for (Location s : locations) {
                result.add(convertLocation(s));
            }
        }
        return result;
    }
}
