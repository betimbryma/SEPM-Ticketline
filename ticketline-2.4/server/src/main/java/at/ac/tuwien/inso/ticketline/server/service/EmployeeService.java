package at.ac.tuwien.inso.ticketline.server.service;

import at.ac.tuwien.inso.ticketline.dto.EmployeeDto;
import at.ac.tuwien.inso.ticketline.dto.UserStatusDto;
import at.ac.tuwien.inso.ticketline.model.Employee;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;

public interface EmployeeService {

    /**
     * Gets an Employe by User Name
     * @param userStatusDto
     * @return
     * @throws ServiceException
     */
    Employee searchEmployeByUsername(UserStatusDto userStatusDto) throws ServiceException;

    /**
     * Gets an Employee by User Name
     * @param employeeDto contains the username of the employee
     * @return the Employee with the given username
     * @throws ServiceException if no employee with the given username exists or the username is not unique or null
     */
    Employee searchEmployeeByUsername(EmployeeDto employeeDto) throws ServiceException;

    /**
     * Updates the date of an employees last login
     * @param employeeDto contains the id and last login date of the employee
     * @throws ServiceException if the last login date could not be updated
     */
    void updateLastLogin(EmployeeDto employeeDto) throws ServiceException;

}