package at.ac.tuwien.inso.ticketline.server.rest;

import at.ac.tuwien.inso.ticketline.dao.PerformanceDao;
import at.ac.tuwien.inso.ticketline.dto.PerformanceDto;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.server.service.PerformanceService;
import at.ac.tuwien.inso.ticketline.server.util.EntityToDto.PerformanceEntityToDto;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(value = "performance", description = "Performance REST service")
@RestController
@RequestMapping(value = "/performance")
public class PerformanceController {

    private static final Logger LOGGER = LoggerFactory.getLogger(PerformanceController.class);

    @Autowired
    private PerformanceService performanceService;

    /**
     * Get all the performances from the database
     *
     * @return a list of the performances
     * @throws ServiceException if there is any exceptions in the PerformanceService
     */
    @ApiOperation(value = "Gets all performances", response = PerformanceDao.class, responseContainer = "List")
    @RequestMapping(value = "/", method = RequestMethod.GET, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public List<PerformanceDto> getPerformances() throws ServiceException {

        LOGGER.info("getAll() called");

        return PerformanceEntityToDto.getInstance().convertPerformances(performanceService.getAll());
    }

}