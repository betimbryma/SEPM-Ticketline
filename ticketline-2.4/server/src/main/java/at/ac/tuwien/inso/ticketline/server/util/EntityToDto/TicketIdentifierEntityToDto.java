package at.ac.tuwien.inso.ticketline.server.util.EntityToDto;

import at.ac.tuwien.inso.ticketline.dto.TicketIdentifierDto;
import at.ac.tuwien.inso.ticketline.model.TicketIdentifier;

import java.util.ArrayList;
import java.util.List;

public class TicketIdentifierEntityToDto {

    private static TicketIdentifierEntityToDto instance = null;
    private TicketIdentifierEntityToDto() {}
    public static TicketIdentifierEntityToDto getInstance() {
        if(instance == null) {
            instance = new TicketIdentifierEntityToDto();
        }
        return instance;
    }

    public static TicketIdentifierDto convert(TicketIdentifier ticketIdentifier){
        TicketIdentifierDto ticketIdentifierDto = new TicketIdentifierDto();
        ticketIdentifierDto.setId(ticketIdentifier.getId());
        ticketIdentifierDto.setCancellationReason(ticketIdentifier.getCancellationReason());
        if(ticketIdentifier.getReservation() != null)
        ticketIdentifierDto.setReservation(ReservationEntityToDto.getInstance().convert(ticketIdentifier.getReservation()));
        else
        ticketIdentifierDto.setReservation(null);
        ticketIdentifierDto.setTicket(TicketEntityToDto.getInstance().convert(ticketIdentifier.getTicket()));
        ticketIdentifierDto.setUuid(ticketIdentifier.getUuid());
        ticketIdentifierDto.setValid(ticketIdentifier.getValid());
        ticketIdentifierDto.setVoidationTime(ticketIdentifier.getVoidationTime());
        ticketIdentifierDto.setVoidedBy(ticketIdentifier.getVoidedBy());
        return ticketIdentifierDto;
    }

    public static List<TicketIdentifierDto> convertTicketIdentifiers(List<TicketIdentifier> ticketIdentifierList){
        List<TicketIdentifierDto> list = new ArrayList<>();
        for(TicketIdentifier ticketIdentifier : ticketIdentifierList){
            list.add(convert(ticketIdentifier));
        }
        return list;
    }
}
