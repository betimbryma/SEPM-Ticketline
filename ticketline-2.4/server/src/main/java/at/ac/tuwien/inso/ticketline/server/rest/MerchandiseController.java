package at.ac.tuwien.inso.ticketline.server.rest;

import at.ac.tuwien.inso.ticketline.dto.PerformanceDto;
import at.ac.tuwien.inso.ticketline.dto.TicketIdentifierDto;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.server.service.MerchandiseService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@Api(value = "merchandise", description = "Customer REST service")
@RestController
@RequestMapping(value = "/merchandise")
public class MerchandiseController {

    private static final Logger LOGGER = LoggerFactory.getLogger(MerchandiseController.class);


    @Autowired
    private MerchandiseService merchandiseService;


    @ApiOperation(value = "Get Articles of This Performance", response = TicketIdentifierDto.class, responseContainer = "List")
    @RequestMapping(value = "/getArticlesOfThisPerformance", method = RequestMethod.POST, consumes = MimeTypeUtils.APPLICATION_JSON_VALUE, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public PerformanceDto reserveTickets(@ApiParam(name = "performance", value = "Tickets to reserve") @RequestBody PerformanceDto performanceDto) throws ServiceException {
        LOGGER.info("getArticleOfPerformance() called");
        return merchandiseService.getArticlesOfThisPerformance(performanceDto);
    }

}