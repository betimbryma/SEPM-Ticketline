package at.ac.tuwien.inso.ticketline.server.service.implementation;

import at.ac.tuwien.inso.ticketline.dao.ArtistDao;
import at.ac.tuwien.inso.ticketline.model.Artist;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.server.service.ArtistService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ArtistServiceImpl implements ArtistService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ArtistServiceImpl.class);

    @Autowired
    private ArtistDao artistDao;

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Artist> getAll() throws ServiceException {
        LOGGER.info("getAll() called");
        List<Artist> list = artistDao.findAll();
        return list;
    }

    // -------------------- For Testing purposes --------------------

    /**
     * Sets the customer dao.
     *
     * @param dao the new customer dao
     */
    public void setArtistDao(ArtistDao dao) {
        this.artistDao = dao;
    }

    // -------------------- For Testing purposes --------------------
}