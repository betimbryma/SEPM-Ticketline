package at.ac.tuwien.inso.ticketline.server.util.EntityToDto;

import at.ac.tuwien.inso.ticketline.dto.MerchandiseDto;
import at.ac.tuwien.inso.ticketline.model.Article;

public class ArticleEntityToDto {

    private static ArticleEntityToDto instance = null;
    private ArticleEntityToDto() {}
    public static ArticleEntityToDto getInstance() {
        if(instance == null) {
            instance = new ArticleEntityToDto();
        }
        return instance;
    }


    public static Article convertArticleDtoWithId(MerchandiseDto merchandiseDto) {
        Article article = new Article();
        article.setId(merchandiseDto.getId());
        article.setDescription(merchandiseDto.getDescription());
        article.setName(merchandiseDto.getName());
        article.setPrice(merchandiseDto.getPrice());
        article.setAvailable(merchandiseDto.getAvailable());
        return article;
    }
}
