package at.ac.tuwien.inso.ticketline.server.service;

import at.ac.tuwien.inso.ticketline.dto.ReservationDto;
import at.ac.tuwien.inso.ticketline.dto.TicketIdentifierDto;
import at.ac.tuwien.inso.ticketline.model.Customer;
import at.ac.tuwien.inso.ticketline.model.Reservation;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;

import java.util.List;

/**
 * Created by Betim on 5/16/2016.
 */
public interface ReservationService {

    Reservation save(Reservation reservation) throws ServiceException;

    ReservationDto saveeReservation(ReservationDto reservationDto) throws ServiceException;

    List<Reservation> getAll() throws ServiceException;

    List<Reservation> getForCustomer(Customer customer) throws ServiceException;

    public Integer cancelTickets(List<TicketIdentifierDto> list) throws ServiceException;

    }