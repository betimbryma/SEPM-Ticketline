package at.ac.tuwien.inso.ticketline.server.service;

import at.ac.tuwien.inso.ticketline.model.Location;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;

import java.util.List;

public interface LocationService {

    /**
     * Gets all the locations in the database
     *
     * @return a list of the locations
     * @throws ServiceException if there are any exception, they are going to be encapsulated in ServiceException
     */
    List<Location> getAll() throws ServiceException;
}