package at.ac.tuwien.inso.ticketline.server.rest;

import at.ac.tuwien.inso.ticketline.dao.RoomDao;
import at.ac.tuwien.inso.ticketline.dto.RoomDto;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.server.service.RoomService;
import at.ac.tuwien.inso.ticketline.server.util.EntityToDto.RoomEntityToDto;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(value = "room", description = "Room REST service")
@RestController
@RequestMapping(value = "/room")
public class RoomController {

    private static final Logger LOGGER = LoggerFactory.getLogger(RoomController.class);

    @Autowired
    private RoomService roomService;

    /**
     * Get all the rooms from the database
     * @return a list of the rooms
     * @throws ServiceException if there is any exceptions in the ShowsService
     */
    @ApiOperation(value = "Gets all rooms", response = RoomDao.class, responseContainer = "List")
    @RequestMapping(value = "/", method = RequestMethod.GET, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public List<RoomDto> getPerformances() throws ServiceException {

        LOGGER.info("getAll() called");

        return RoomEntityToDto.getInstance().convertRooms(roomService.getAll());
    }

    public void setRoomService(RoomService roomService) {
        this.roomService = roomService;
    }
}