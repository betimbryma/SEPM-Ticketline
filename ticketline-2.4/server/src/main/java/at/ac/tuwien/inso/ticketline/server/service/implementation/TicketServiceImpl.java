package at.ac.tuwien.inso.ticketline.server.service.implementation;

import at.ac.tuwien.inso.ticketline.dao.ShowDao;
import at.ac.tuwien.inso.ticketline.dao.TicketDao;
import at.ac.tuwien.inso.ticketline.dao.TicketIdentifierDao;
import at.ac.tuwien.inso.ticketline.dto.*;
import at.ac.tuwien.inso.ticketline.model.Reservation;
import at.ac.tuwien.inso.ticketline.model.Ticket;
import at.ac.tuwien.inso.ticketline.model.TicketIdentifier;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.server.service.TicketService;
import at.ac.tuwien.inso.ticketline.server.util.DtoToEntity.TicketDtoToEntity;
import at.ac.tuwien.inso.ticketline.server.util.DtoToEntity.TicketIdentifierDtoToEntity;
import at.ac.tuwien.inso.ticketline.server.util.EntityToDto.ShowEntityToDto;
import at.ac.tuwien.inso.ticketline.server.util.EntityToDto.TicketEntityToDto;
import at.ac.tuwien.inso.ticketline.server.util.EntityToDto.TicketIdentifierEntityToDto;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TicketServiceImpl implements TicketService {

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(TicketServiceImpl.class);
    @Autowired
    TicketDao ticketDao;

    @Autowired
    ShowDao showDao;

    @Autowired
    TicketIdentifierDao ticketIdentifierDao;

    @Override
    public List<Ticket> getTicketsByShow(Integer show) throws ServiceException {
        try {
            return ticketDao.findAllTicketsForShow(showDao.findOne(show));
        } catch (NullPointerException | DataIntegrityViolationException e) {
            throw new ServiceException(e);
        }
    }



    @Override
    public List<TicketIdentifier> getTicketIdentifier(Ticket ticket) throws ServiceException {
        try {
            return ticketIdentifierDao.findTicketIdentifierForTicket(ticket);
        } catch (NullPointerException | DataIntegrityViolationException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Integer getSoldTicketAmount(Integer show) throws ServiceException {
        try {
            Long count = ticketDao.countSoldByShow(showDao.findOne(show));
            ;
            return count != null ? count.intValue() : 0;
        } catch (NullPointerException | DataIntegrityViolationException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public List<TicketIdentifier> getAllTicketIdentifier() throws ServiceException {
        try {
            return ticketIdentifierDao.findAll();
        } catch (NullPointerException | DataIntegrityViolationException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public TicketIdentifier saveTicketIdentifier(TicketIdentifier ticketIdentifier) throws ServiceException {
        try {
            return ticketIdentifierDao.save(ticketIdentifier);
        } catch (NullPointerException | DataIntegrityViolationException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public List<MapEntryDto<ShowDto, Integer>> getMinPrices() throws ServiceException {
        try {
            List<MapEntryDto<ShowDto, Integer>> prices = showDao.findAll().stream().map(s ->
                    new MapEntryDto<>(ShowEntityToDto.getInstance().convertShow(s), ticketDao.getMinPrice(s))).collect(Collectors.toList());
            return prices;
        } catch (NullPointerException | DataIntegrityViolationException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public List<MapEntryDto<ShowDto, Integer>> getMaxPrices() throws ServiceException {
        try {
            List<MapEntryDto<ShowDto, Integer>> prices = showDao.findAll().stream().map(s ->
                    new MapEntryDto<>(ShowEntityToDto.getInstance().convertShow(s), ticketDao.getMaxPrice(s))).collect(Collectors.toList());
            return prices;
        } catch (NullPointerException | DataIntegrityViolationException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public TicketIdentifier getTicketIdentifierById(TicketIdentifier t) throws ServiceException {

        try {
            return ticketIdentifierDao.findOne(t.getId());
        } catch (NullPointerException | DataIntegrityViolationException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public List<TicketIdentifier> getTicketIdentifiersForReservation(Reservation reservation) throws ServiceException {
        try {
            return ticketIdentifierDao.getTicketIdentifiersForReservation(reservation);
        } catch (NullPointerException | DataIntegrityViolationException e) {
            throw new ServiceException(e);
        }

    }

    @Override
    public void updateThisTicketIdentifier(TicketIdentifier ticketIdentifier) throws ServiceException {

        try {
            ticketIdentifierDao.updateTicketIdentifiers(ticketIdentifier);
        } catch (NullPointerException | DataIntegrityViolationException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void cancelThisTicketIdentifier(TicketIdentifier ticketIdentifier) throws ServiceException {

        try {
            ticketIdentifierDao.cancelTicketIdentifiers(ticketIdentifier);
        } catch (NullPointerException | DataIntegrityViolationException e) {
            throw new ServiceException(e);
        }
    }


    public Ticket getTicketByID(int id) throws ServiceException {
        try {
            return ticketDao.getOne(id);
        } catch (NullPointerException | DataIntegrityViolationException e) {
            throw new ServiceException(e);
        }
    }

    public void updateTicketForCartAsUsed(Ticket t) throws ServiceException {
        try {


            java.util.Date d = new java.util.Date();
            Date newDate = new Date(d.getTime() + 2 * (3600 * 1000));
            /*
            t.setFlagdate(newDate);
            ticketDao.save(t);
            */
            ticketDao.updateDate(t, newDate);
            ticketDao.updateTicketForCartAsUsed(t);
        } catch (NullPointerException | DataIntegrityViolationException e) {
            throw new ServiceException(e);
        }

    }

    public void updateTicketForCartAsNotUsed(Ticket t) throws ServiceException {
        try {
            ticketDao.updateTicketForCartAsNotUsed(t);
        } catch (NullPointerException | DataIntegrityViolationException e) {
            throw new ServiceException(e);
        }
    }

    public void updateTicketForCartAsReserved(Ticket ticketE) throws ServiceException {

        try {
            ticketDao.updateTicketForCartAsReserved(ticketE);
        } catch (NullPointerException | DataIntegrityViolationException e) {
            throw new ServiceException(e);
        }
    }

    public void updateTicketForCartAsSold(Ticket ticketE) throws ServiceException {
        try {
            ticketDao.updateTicketForCartAsSold(ticketE);
        } catch (NullPointerException | DataIntegrityViolationException e) {
            throw new ServiceException(e);
        }

    }

    public Integer getFlag(Ticket t) throws ServiceException {
        try {
            return ticketDao.checkFlag(t);
        } catch (NullPointerException | DataIntegrityViolationException e) {
            throw new ServiceException(e);
        }
    }

    public List<TicketIdentifier> getTicketIdentifierForTicket(Ticket t) throws ServiceException {

        try {
            return ticketIdentifierDao.getTicketIdentifier(t);
        } catch (NullPointerException | DataIntegrityViolationException e) {
            throw new ServiceException(e);
        }
    }


    public MessageDto checkFreeTicketForSell(TicketDto ticketDto) throws ServiceException {
        Integer i = 0;
        Ticket t = TicketDtoToEntity.convert(ticketDto);
        t = getTicketByID(t.getId());
        i = returnIone(i, t);
        if (i == 0) {
            i = returnI(i, t);
            if (i == 0)
                updateTicketForCartAsUsed(t);
        }
        MessageDto msg = new MessageDto();
        msg.setType(MessageType.SUCCESS);
        msg.setText(i.toString());
        return msg;
    }


    public MessageDto checkReserveTicketForSell(TicketDto ticketDto) throws ServiceException {

        Integer i = 0;
        Ticket t = TicketDtoToEntity.convert(ticketDto);
        t = getTicketByID(t.getId());
        i = returnIone(i, t);
        if (i == 0) {
            i = returnI(i, t);
        }
        if (i == 2)
            updateTicketForCartAsUsed(t);

        MessageDto msg = new MessageDto();
        msg.setType(MessageType.SUCCESS);
        msg.setText(i.toString());
        return msg;
    }

    public MessageDto checkFreeTicketForReserve(TicketDto ticketDto) throws ServiceException {
        Integer i = 0;
        Ticket t = TicketDtoToEntity.convert(ticketDto);
        t = getTicketByID(t.getId());
        i = returnIone(i, t);
        if (i == 0) {
            i = returnI(i, t);
        }

        MessageDto msg = new MessageDto();
        msg.setType(MessageType.SUCCESS);
        msg.setText(i.toString());
        return msg;
    }

    @Override
    public List<MapEntryDto<TicketDto, TicketIdentifierDto>> getTicketsForShow(Integer show) throws ServiceException {
        List<TicketDto> list = TicketEntityToDto.getInstance().convertTickets(this.getTicketsByShow(show));
        List<MapEntryDto<TicketDto, TicketIdentifierDto>> lista = new ArrayList<>();

        for (int i = 0; i < list.size(); i++) {

            List<TicketIdentifierDto> ticketIdentifierDtoList = TicketIdentifierEntityToDto.getInstance().convertTicketIdentifiers(this.getTicketIdentifier(TicketDtoToEntity.getInstance().convert(list.get(i))));
            if (ticketIdentifierDtoList.size() <= 0) {
                lista.add(new MapEntryDto<TicketDto, TicketIdentifierDto>(list.get(i), null));
            }else {
                boolean gotIn = true;
                for (int j = 0; j < ticketIdentifierDtoList.size(); j++) {
                    if (ticketIdentifierDtoList.get(j).getValid()) {
                        gotIn = false;
                        lista.add(new MapEntryDto<TicketDto, TicketIdentifierDto>(list.get(i), ticketIdentifierDtoList.get(j)));
                        break;
                    }

                }
                if (gotIn)
                    lista.add(new MapEntryDto<TicketDto, TicketIdentifierDto>(list.get(i), null));
            }

        }

        /*
        ticketService.getTicketsByShow(show);
        ticketService.getAllTicketIdentifier();

        List<MapEntryDto<TicketDto, TicketIdentifierDto>> lista2 = new ArrayList<>();
        */

        return lista;
    }

    @Override
    public List<TicketIdentifierDto> reserveTickets(List<TicketIdentifierDto> ticketDtoList) throws ServiceException {
        List<TicketIdentifierDto> ticketIdentifierDtos = new ArrayList<>();
        for (int i = 0; i < ticketDtoList.size(); i++) {
            ticketIdentifierDtos.add(TicketIdentifierEntityToDto.getInstance().convert(this.saveTicketIdentifier(TicketIdentifierDtoToEntity.getInstance().convert(ticketDtoList.get(i)))));

        }
        return ticketIdentifierDtos;
    }


    public int returnI(int i, Ticket t) throws ServiceException {
        List<TicketIdentifier> ticketIdentifierForTicket = getTicketIdentifierForTicket(t);
        if (ticketIdentifierForTicket != null) {
            for (TicketIdentifier t1 : ticketIdentifierForTicket) {
                if (t1.getReservation() != null) {
                    i = 2;
                } else {
                    i = 3;
                }
            }
        }
        return i;
    }

    public int returnIone(int i, Ticket t) throws ServiceException {
        LOGGER.debug("returnIone");
        if (getFlag(t) == null) {
            i = 0;
        } else {
            i = getFlag(t);
            if (i == 1) {

                if (ticketDao.checkDate(t).before(new Date())) {
                    i = 0;
                }

            }
        }
        return i;
    }

    // -------------------- For Testing purposes --------------------

    /**
     * Sets the ticket dao.
     *
     * @param dao the new ticket dao
     */
    public void setTicketDao(TicketDao dao) {
        this.ticketDao = dao;
    }

    public void setShowDao(ShowDao dao) {
        this.showDao = dao;
    }

    public void setTicketIdentifierDao(TicketIdentifierDao dao) {
        this.ticketIdentifierDao = dao;
    }

    // -------------------- For Testing purposes --------------------
}