package at.ac.tuwien.inso.ticketline.server.util.EntityToDto;

import at.ac.tuwien.inso.ticketline.dto.ParticipationDto;
import at.ac.tuwien.inso.ticketline.model.Participation;

import java.util.ArrayList;
import java.util.List;

public class ParticipationEntityToDto {

    private static ParticipationEntityToDto instance = null;
    private ParticipationEntityToDto() {}
    public static ParticipationEntityToDto getInstance() {
        if(instance == null) {
            instance = new ParticipationEntityToDto();
        }
        return instance;
    }

    /**
     * Converts a participation entity to an participation DTO
     *
     * @param p the participation entity
     * @return the converted participation DTO
     */
    public static ParticipationDto convertParticipation(Participation p) {

        ParticipationDto participationDto = new ParticipationDto();
        participationDto.setPerformance(PerformanceEntityToDto.getInstance().convertPerformance(p.getPerformance()));
        participationDto.setId(p.getId());
        participationDto.setArtist(ArtistEntityToDto.getInstance().convertArtist(p.getArtist()));
        participationDto.setDescription(p.getDescription());
        participationDto.setArtistRole(p.getArtistRole());

        return participationDto;
    }

    /**
     * Converts a participation entity list to a participation DTO list
     *
     * @param participations the list of participation entities
     * @return the converted participation DTO list
     */
    public static List<ParticipationDto> convertParticipations(List<Participation> participations) {
        List<ParticipationDto> result = new ArrayList<>();

        if (participations != null) {
            for (Participation p : participations) {
                result.add(convertParticipation(p));
            }
        }
        return result;
    }
}
