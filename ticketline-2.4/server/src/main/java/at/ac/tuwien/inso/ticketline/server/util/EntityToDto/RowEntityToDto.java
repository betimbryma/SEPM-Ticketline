package at.ac.tuwien.inso.ticketline.server.util.EntityToDto;

import at.ac.tuwien.inso.ticketline.dto.RowDto;
import at.ac.tuwien.inso.ticketline.model.Row;

public class RowEntityToDto {

    private static RowEntityToDto instance = null;
    private RowEntityToDto() {}
    public static RowEntityToDto getInstance() {
        if(instance == null) {
            instance = new RowEntityToDto();
        }
        return instance;
    }

    public static RowDto convert(Row row){
        RowDto rowDto = new RowDto();
        rowDto.setDescription(row.getDescription());
        rowDto.setId(row.getId());
        rowDto.setOrder(row.getOrder());
        rowDto.setName(row.getName());
        return rowDto;
    }
}
