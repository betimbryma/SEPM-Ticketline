package at.ac.tuwien.inso.ticketline.server.service;

import at.ac.tuwien.inso.ticketline.dto.MerchandiseDto;
import at.ac.tuwien.inso.ticketline.dto.PerformanceDto;
import at.ac.tuwien.inso.ticketline.model.Article;
import at.ac.tuwien.inso.ticketline.model.Performance;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;

import java.util.List;

public interface MerchandiseService {

    /**
     * Get Articles for this Performance
     * @param performanceDto
     * @return
     * @throws ServiceException
     */

    PerformanceDto getArticlesOfThisPerformance(PerformanceDto performanceDto) throws ServiceException;


    /**
     * Articel Id suchen
     * @param id
     * @return
     * @throws ServiceException
     */
    Article searchArticleID(int id) throws  ServiceException;


    /**
     * Update this Article
     * @param article
     * @return
     * @throws ServiceException
     */
    Article updateArticle(Article article) throws  ServiceException;

    /**
     * increase an article by one
     *
     * @param article
     * @return
     * @throws ServiceException
     */
    public int increaseArticle(Article article) throws ServiceException;

    /**
     * decrease an article by one
     *
     * @param article
     * @return
     * @throws ServiceException
     */
    public int decreaseArticle2(Article article) throws ServiceException;

    /**
     * Get Articles of this Performace
     * @param performance
     * @return
     */
    List<Article> getByPerformance(Performance performance);


    /**
     * Release Articles as not used
     * @param merchandiseDtos
     * @throws ServiceException
     */
    void resetMerchandises(List<MerchandiseDto> merchandiseDtos)  throws ServiceException;
}