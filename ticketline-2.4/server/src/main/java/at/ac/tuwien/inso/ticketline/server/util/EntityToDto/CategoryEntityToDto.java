package at.ac.tuwien.inso.ticketline.server.util.EntityToDto;

import at.ac.tuwien.inso.ticketline.dto.CategoryDto;
import at.ac.tuwien.inso.ticketline.model.Category;

public class CategoryEntityToDto {

    private static CategoryEntityToDto instance = null;
    private CategoryEntityToDto() {}
    public static CategoryEntityToDto getInstance() {
        if(instance == null) {
            instance = new CategoryEntityToDto();
        }
        return instance;
    }

    public static CategoryDto convert(Category category){
        CategoryDto categoryDto = new CategoryDto();
        categoryDto.setId(category.getId());
        categoryDto.setName(category.getName());
        categoryDto.setDescription(category.getDescription());
        return categoryDto;
    }
}
