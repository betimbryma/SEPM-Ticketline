package at.ac.tuwien.inso.ticketline.server.rest;

import at.ac.tuwien.inso.ticketline.dto.MerchandiseDto;
import at.ac.tuwien.inso.ticketline.dto.MessageDto;
import at.ac.tuwien.inso.ticketline.dto.TicketDto;
import at.ac.tuwien.inso.ticketline.dto.TicketIdentifierDto;
import at.ac.tuwien.inso.ticketline.model.Article;
import at.ac.tuwien.inso.ticketline.model.Ticket;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.server.service.ConflictService;
import at.ac.tuwien.inso.ticketline.server.service.MerchandiseService;
import at.ac.tuwien.inso.ticketline.server.service.TicketService;
import at.ac.tuwien.inso.ticketline.server.util.DtoToEntity.MerchandiseDtoToEntity;
import at.ac.tuwien.inso.ticketline.server.util.DtoToEntity.TicketDtoToEntity;
import at.ac.tuwien.inso.ticketline.server.util.EntityToDto.MerchandiseEntityToDto;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.concurrent.Semaphore;

/**
 * Created by eni on 01.06.2016.
 */

@Api(value = "Conflict solver", description = "Conflict REST service")
@RestController
@RequestMapping(value = "/conflict")
public class ConflictController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConflictController.class);

    @Autowired
    private ConflictService conflictService;

    @ApiOperation(value = "Check if the merchandise is empty", response = MerchandiseDto.class, responseContainer = "MerchandiseDto")
    @RequestMapping(value = "/checkArticles", method = RequestMethod.POST, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public MerchandiseDto checkMerchandiseInServer(@ApiParam(name="articles",value="ArticlesTo Be Checked before puttinge to CArt") @RequestBody MerchandiseDto merchandiseDtos) throws ServiceException {
        LOGGER.info("checkArticles() is called");

        return conflictService.checkMerchandiseInServer(merchandiseDtos);
    }


    @ApiOperation(value = "Makes a reservation", response = MerchandiseDto.class, responseContainer = "List")
    @RequestMapping(value = "/releaseArticles", method = RequestMethod.POST, consumes = MimeTypeUtils.APPLICATION_JSON_VALUE, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public List<MerchandiseDto> releaseArticles(@ApiParam(name = "tickets", value = "Tickets to reserve") @RequestBody List<MerchandiseDto> merchandiseDtos) throws ServiceException {
        LOGGER.info("releaseArticles() called");

        return conflictService.releaseArticles(merchandiseDtos);
    }


    @ApiOperation(value = "check For the Ticket", response = TicketDto.class)
    @RequestMapping(value = "/checkFreeTicketForSell", method = RequestMethod.POST, consumes = MimeTypeUtils.APPLICATION_JSON_VALUE, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public MessageDto checkFreeTicketForSell(@ApiParam(name = "TicketDto", value = "TicketDto to check") @Valid @RequestBody TicketDto ticketDto) throws ServiceException {
        LOGGER.info("checkFreeTicketForSell() called");

        return conflictService.checkFreeTicketForSell(ticketDto);
    }

    @ApiOperation(value = "check For the Ticket", response = TicketDto.class)
    @RequestMapping(value = "/checkReserveTicketForSell", method = RequestMethod.POST, consumes = MimeTypeUtils.APPLICATION_JSON_VALUE, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public MessageDto checkReserveTicketForSell(@ApiParam(name = "TicketDto", value = "TicketDto to check") @Valid @RequestBody TicketDto ticketDto) throws ServiceException {
        LOGGER.info("checkReserveTicketForSell() called");

        return conflictService.checkReserveTicketForSell(ticketDto);
    }


    @ApiOperation(value = "check For the Ticket", response = TicketDto.class)
    @RequestMapping(value = "/checkFreeTicketForReserve", method = RequestMethod.POST, consumes = MimeTypeUtils.APPLICATION_JSON_VALUE, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public MessageDto checkFreeTicketForReserve(@ApiParam(name = "TicketDto", value = "TicketDto to check") @Valid @RequestBody TicketDto ticketDto) throws ServiceException {
        LOGGER.info("checkFreeTicketForReserve() called");

        return conflictService.checkFreeTicketForReserve(ticketDto);
    }


    @ApiOperation(value = "Release a Ticket", response = TicketIdentifierDto.class, responseContainer = "List")
    @RequestMapping(value = "/releaseTicket", method = RequestMethod.POST, consumes = MimeTypeUtils.APPLICATION_JSON_VALUE, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public List<TicketIdentifierDto> releaseTicket(@ApiParam(name = "tickets", value = "Tickets to reserve") @RequestBody List<TicketDto> ticktes) throws ServiceException {
        LOGGER.info("releaseTicket() called");

        return conflictService.releaseTicket(ticktes);
    }
}
