package at.ac.tuwien.inso.ticketline.server.service;

import at.ac.tuwien.inso.ticketline.model.Customer;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;

import java.util.List;

public interface CustomerService {

    /**
     * Saves the given customer object and returns the saved entity.
     *
     * @param customer object to persist
     * @return the saved entity
     * @throws ServiceException the service exception
     */
    public Customer save(Customer customer) throws ServiceException;

    /**
     * Saves the given customer object and returns the saved entity.
     *
     * @param id the ID of the customer
     * @return the customer with the specified id
     * @throws ServiceException the service exception
     */
    public Customer getByID(int id) throws ServiceException;

    /**
     * Gets all the customers in the database
     *
     * @return a list of the customers
     * @throws ServiceException if there are any exception, they are going to be encapsulated in ServiceException
     */
    List<Customer> getAll() throws ServiceException;


    /**
     * Edit a customer
     * @param customer that it is going to be edited
     * @return the edited customer
     * @throws ServiceException if there are exceptions they are going to be encapsulated in ServiceException
     */
    Customer edit(Customer customer) throws ServiceException;

    Customer getByID(Integer ID) throws ServiceException;
}