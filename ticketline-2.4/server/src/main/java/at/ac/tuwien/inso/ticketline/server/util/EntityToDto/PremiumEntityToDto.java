package at.ac.tuwien.inso.ticketline.server.util.EntityToDto;

import at.ac.tuwien.inso.ticketline.dto.PremiumDto;
import at.ac.tuwien.inso.ticketline.model.Premium;

/**
 * Created by eni on 04.06.2016.
 */
public class PremiumEntityToDto {

    public static PremiumDto convertPremium(Premium article) {
        if (article == null) {
            return null;
        }
        PremiumDto dto = new PremiumDto();
        dto.setId(article.getId());
        dto.setName(article.getName());
        dto.setAvailable(article.getAvailable());
        dto.setDescription(article.getDescription());
        dto.setPrice(article.getPrice());
        return dto;
    }
}
