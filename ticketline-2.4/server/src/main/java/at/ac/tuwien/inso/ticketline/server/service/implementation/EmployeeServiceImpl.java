package at.ac.tuwien.inso.ticketline.server.service.implementation;

import at.ac.tuwien.inso.ticketline.dao.EmployeeDao;
import at.ac.tuwien.inso.ticketline.dto.EmployeeDto;
import at.ac.tuwien.inso.ticketline.dto.UserStatusDto;
import at.ac.tuwien.inso.ticketline.model.Employee;
import at.ac.tuwien.inso.ticketline.model.Permission;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.server.service.EmployeeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmployeeServiceImpl.class);

    @Autowired
    private EmployeeDao employeeDao;

    public Employee searchEmployeByUsername(UserStatusDto userStatusDto) throws ServiceException {

        LOGGER.debug("Get the user for id");
        List<Employee> users = employeeDao.findByUsername(userStatusDto.getUsername());
        Employee employee;
        if (users.isEmpty()) {
            throw new ServiceException(String.format("user %s does not exists", userStatusDto.getUsername()));
        }
        if (1 == users.size()) {
            employee = users.get(0);
        } else {
            throw new ServiceException(String.format("Username %s is not unique", userStatusDto.getUsername()));
        }
        return employee;
    }

    public Employee searchEmployeeByUsername(EmployeeDto employeeDto) throws ServiceException {

        if(employeeDto == null) {
            LOGGER.error("called getEmployee with null");
            throw new ServiceException("called getEmployee with null as argument");
        }
        else if(employeeDto.getUsername() == null || employeeDto.getUsername().trim().isEmpty()) {
            LOGGER.error("called getEmployee without username");
            throw new ServiceException("called getEmployee without specifying a username");
        }

        LOGGER.debug("Get the user with username " + employeeDto.getUsername());

        List<Employee> users = employeeDao.findByUsername(employeeDto.getUsername());

        if (users.isEmpty()) {
            throw new ServiceException(String.format("user %s does not exists", employeeDto.getUsername()));
        }

        if (1 == users.size()) {
            return users.get(0);
        } else {
            throw new ServiceException(String.format("Username %s is not unique", employeeDto.getUsername()));
        }
    }

    @Override
    public void updateLastLogin(EmployeeDto employeeDto) throws ServiceException {

        if(employeeDto == null) {
            LOGGER.error("called updateLastLogin with null");
            throw new ServiceException("called updateLastLogin with null as argument");
        }
        else if(employeeDto.getUsername() == null || employeeDto.getUsername().trim().isEmpty()) {
            LOGGER.error("called updateLastLogin with empty nickname");
            throw new ServiceException("called updateLastLogin with empty nickname");
        }
        else if(employeeDto.getLastlogin() == null) {
            LOGGER.error("called updateLastLogin without a Date");
            throw new ServiceException("called updateLastLogin without a Date");
        }

        try {

            Employee employee = searchEmployeeByUsername(employeeDto);
            employee.setLastLogin(employeeDto.getLastlogin());
            employeeDao.save(employee);
        }
        catch(NullPointerException | DataIntegrityViolationException e) {
            throw new ServiceException("Last login date could not be updated");
        }

    }


    // -------------------- For Testing purposes --------------------

    /**
     * Sets the employee dao.
     *
     * @param dao the new employee dao
     */
    public void setEmployeeDao(EmployeeDao dao) {
        this.employeeDao = dao;
    }

    // -------------------- For Testing purposes --------------------
}