package at.ac.tuwien.inso.ticketline.server.util.DtoToEntity;

import at.ac.tuwien.inso.ticketline.dto.TicketDto;
import at.ac.tuwien.inso.ticketline.model.Ticket;

public class TicketDtoToEntity {

    private static TicketDtoToEntity instance = null;
    private TicketDtoToEntity() {}
    public static TicketDtoToEntity getInstance() {
        if(instance == null) {
            instance = new TicketDtoToEntity();
        }
        return instance;
    }

    public static Ticket convert(TicketDto ticketDto){
        if(ticketDto == null)
            return null;
        Ticket ticket = new Ticket();
        ticket.setDescription(ticketDto.getDescription());
        ticket.setId(ticketDto.getId());
        ticket.setPrice(ticketDto.getPrice());
        ticket.setShow(ShowDtoToEntity.getInstance().convert(ticketDto.getShow()));
        ticket.setSeat(SeatDtoToEntity.getInstance().convert(ticketDto.getSeat()));
        ticket.setFlag(ticketDto.getFlag());
        return ticket;
    }
}
