package at.ac.tuwien.inso.ticketline.server.security;

import at.ac.tuwien.inso.ticketline.dao.EmployeeDao;
import at.ac.tuwien.inso.ticketline.dto.EmployeeDto;
import at.ac.tuwien.inso.ticketline.dto.UserEvent;
import at.ac.tuwien.inso.ticketline.dto.UserStatusDto;
import at.ac.tuwien.inso.ticketline.model.Employee;
import at.ac.tuwien.inso.ticketline.model.Permission;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.server.service.EmployeeService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.util.MimeTypeUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Implementation of a Spring Security authentication handler.
 * Used by Spring Security
 */
public class AuthenticationHandler implements AuthenticationSuccessHandler, AuthenticationFailureHandler, LogoutSuccessHandler {


    private static final Logger LOGGER = LoggerFactory.getLogger(AuthenticationHandler.class);

    private ObjectMapper mapper;

    @Autowired
    EmployeeService employeeService;

    @Autowired
    EmployeeDao employeeDAO;

    /**
     * Instantiates a new authentication handler.
     */
    public AuthenticationHandler() {
        this.mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.INDENT_OUTPUT, true);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        UserStatusDto userStatusDto = AuthUtil.getUserStatusDto(authentication);

        if(userStatusDto.getPermission().equals("SUSPENDED")) {
            userStatusDto.setEvent(UserEvent.AUTH_SUSPENDED);
            LOGGER.info("Tried logging in with suspended account");
        }

        else {
            userStatusDto.setEvent(UserEvent.AUTH_SUCCESS);

            EmployeeDto empDto = new EmployeeDto();
            empDto.setUsername(userStatusDto.getUsername());

            try {
                Employee e = employeeService.searchEmployeeByUsername(empDto);

                if(e.getFailedLogins() > 0) {
                    e.setFailedLogins(0);
                    employeeDAO.save(e);
                }


            } catch (ServiceException e1) {
                LOGGER.error("Failed to retrieve or save employee");
            }

        }

        this.printUserStatusDto(userStatusDto, response);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
        UserStatusDto userStatusDto = new UserStatusDto();
        userStatusDto.setEvent(UserEvent.AUTH_FAILURE);

        // get the username from request
        String username = request.getParameterMap().get("user")[0];

        try {

            EmployeeDto empDto = new EmployeeDto();
            empDto.setUsername(username);
            Employee e = employeeService.searchEmployeeByUsername(empDto);

            //if user is in the database
            LOGGER.info("Username " + username + " belongs to " + e.getFirstname() + " " + e.getLastname());

            int failedLogins = e.getFailedLogins();

            if(failedLogins < 5) {
                failedLogins++;

                LOGGER.info("Failed logins: " + failedLogins);

                if(failedLogins == 5) {
                    e.setPermission(Permission.SUSPENDED);
                    userStatusDto.setEvent(UserEvent.AUTH_NOW_SUSPENDED);
                    LOGGER.info("Account has been suspended");
                }
                else if(failedLogins == 4) {
                    userStatusDto.setEvent(UserEvent.ONE_ATTEMPT_LEFT);
                    LOGGER.info("One login attempt left");
                }
                else if(failedLogins == 3) {
                    userStatusDto.setEvent(UserEvent.TWO_ATTEMPTS_LEFT);
                    LOGGER.info("Two login attempts left");
                }
                else if(failedLogins == 2) {
                    userStatusDto.setEvent(UserEvent.THREE_ATTEMPTS_LEFT);
                    LOGGER.info("Three login attempts left");
                }

                e.setFailedLogins(failedLogins);
                employeeDAO.save(e);

            }
            else if(failedLogins == 5) {
                userStatusDto.setEvent(UserEvent.AUTH_SUSPENDED);
                LOGGER.info("Tried logging in with suspended account");
            }


        } catch (ServiceException e) {
            LOGGER.error("User " + username + " is not in the database and could therefore not be logged in");
            userStatusDto.setEvent(UserEvent.USER_INVALID);
        }

        this.printUserStatusDto(userStatusDto, response);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        UserStatusDto userStatusDto = new UserStatusDto();
        userStatusDto.setEvent(UserEvent.LOGOUT);
        this.printUserStatusDto(userStatusDto, response);
    }

    /**
     * Prints the user status dto on the HTTP response stream.
     *
     * @param usd      the user status DTO
     * @param response the HTTP response
     * @throws IOException      Signals that an I/O exception has occurred.
     * @throws ServletException the servlet exception
     */
    private void printUserStatusDto(UserStatusDto usd, HttpServletResponse response) throws IOException, ServletException {
        response.setStatus(HttpServletResponse.SC_OK);
        response.setContentType(MimeTypeUtils.APPLICATION_JSON_VALUE);
        String result;
        try {
            result = this.mapper.writeValueAsString(usd);
        } catch (JsonProcessingException e) {
            throw new ServletException(e);
        }
        response.getOutputStream().print(result);
    }

}
