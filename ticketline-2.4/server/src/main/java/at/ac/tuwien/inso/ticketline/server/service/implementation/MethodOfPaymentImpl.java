package at.ac.tuwien.inso.ticketline.server.service.implementation;

import at.ac.tuwien.inso.ticketline.dao.BankAccountDao;
import at.ac.tuwien.inso.ticketline.dao.CashDao;
import at.ac.tuwien.inso.ticketline.dao.CreditCardDao;
import at.ac.tuwien.inso.ticketline.model.BankAccount;
import at.ac.tuwien.inso.ticketline.model.Cash;
import at.ac.tuwien.inso.ticketline.model.Creditcard;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.server.service.MethodOfPaymentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

@Service
public class MethodOfPaymentImpl implements MethodOfPaymentService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MethodOfPaymentImpl.class);

    @Autowired
    private CashDao cashDao;
    @Autowired
    private BankAccountDao bankAccountDao;
    @Autowired
    private CreditCardDao creditCardDao;

    @Override
    public Cash saveCash(Cash cash)throws ServiceException{

        LOGGER.info("save Payment");
        try {
            return cashDao.save(cash);
        } catch (NullPointerException | DataIntegrityViolationException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public BankAccount saveBankAccount(BankAccount bankAccount)throws ServiceException{

        LOGGER.info("save Payment");
        try {
            return bankAccountDao.save(bankAccount);
        } catch (Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Creditcard saveCreditcard(Creditcard creditcard)throws ServiceException{

        LOGGER.info("save Payment");
        try {
            return creditCardDao.save(creditcard);
        } catch (Exception e) {
            throw new ServiceException(e);
        }
    }
}