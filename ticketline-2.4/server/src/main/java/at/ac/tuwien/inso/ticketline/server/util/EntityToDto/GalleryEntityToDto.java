package at.ac.tuwien.inso.ticketline.server.util.EntityToDto;

import at.ac.tuwien.inso.ticketline.dto.GalleryDto;
import at.ac.tuwien.inso.ticketline.model.Gallery;

public class GalleryEntityToDto {

    private static GalleryEntityToDto instance = null;
    private GalleryEntityToDto() {}
    public static GalleryEntityToDto getInstance() {
        if(instance == null) {
            instance = new GalleryEntityToDto();
        }
        return instance;
    }

    public static GalleryDto convert(Gallery gallery){
        GalleryDto galleryDto = new GalleryDto();
        galleryDto.setDescription(gallery.getDescription());
        galleryDto.setId(gallery.getId());
        galleryDto.setOrder(gallery.getOrder());
        galleryDto.setName(gallery.getName());
        return galleryDto;
    }

}
