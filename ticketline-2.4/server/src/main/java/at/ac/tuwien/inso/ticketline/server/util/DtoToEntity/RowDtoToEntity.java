package at.ac.tuwien.inso.ticketline.server.util.DtoToEntity;

import at.ac.tuwien.inso.ticketline.dto.RowDto;
import at.ac.tuwien.inso.ticketline.model.Row;

public class RowDtoToEntity {

    private static RowDtoToEntity instance = null;
    private RowDtoToEntity() {}
    public static RowDtoToEntity getInstance() {
        if(instance == null) {
            instance = new RowDtoToEntity();
        }
        return instance;
    }

    public static Row convert(RowDto rowDto){
        Row row = new Row();
        row.setId(rowDto.getId());
        row.setOrder(rowDto.getOrder());
        row.setDescription(rowDto.getDescription());
        row.setName(rowDto.getName());
        return row;
    }
}
