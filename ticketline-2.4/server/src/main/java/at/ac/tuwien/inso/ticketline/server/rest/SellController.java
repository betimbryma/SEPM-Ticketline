package at.ac.tuwien.inso.ticketline.server.rest;

import at.ac.tuwien.inso.ticketline.dto.CustomerDto;
import at.ac.tuwien.inso.ticketline.dto.MessageDto;
import at.ac.tuwien.inso.ticketline.dto.ReceiptDto;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.server.service.SellService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Api(value = "receipt", description = "receipt service")
@RestController
@RequestMapping(value = "/receipt")
public class SellController {
    private static final Logger LOGGER = LoggerFactory.getLogger(SellController.class);

    @Autowired
    private SellService sellService;

    @ApiOperation(value = "Save receipt", response = CustomerDto.class)
    @RequestMapping(value = "/sellAll", method = RequestMethod.POST, consumes = MimeTypeUtils.APPLICATION_JSON_VALUE, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public MessageDto createReceipt(@ApiParam(name = "receipt", value = "Save receipt") @Valid @RequestBody ReceiptDto receiptDto) throws ServiceException {
        LOGGER.debug("Start Method create Receipt");
        return sellService.getReceipt(receiptDto);
    }
}
