package at.ac.tuwien.inso.ticketline.server.util.EntityToDto;

import at.ac.tuwien.inso.ticketline.dto.ReservationDto;
import at.ac.tuwien.inso.ticketline.model.Reservation;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Betim on 5/16/2016.
 */
public class ReservationEntityToDto {

    private static ReservationEntityToDto instance = null;
    private ReservationEntityToDto() {}
    public static ReservationEntityToDto getInstance() {
        if(instance == null) {
            instance = new ReservationEntityToDto();
        }
        return instance;
    }

    public static ReservationDto convert(Reservation reservation){
        ReservationDto reservationDto = new ReservationDto();
        reservationDto.setId(reservation.getId());
        reservationDto.setCustomer(CustomerEntityToDto.getInstance().convert(reservation.getCustomer()));
        reservationDto.setReservationNumber(reservation.getReservationNumber());
        reservationDto.setEmployee(EmployeeEntityToDto.getInstance().convert(reservation.getEmployee()));
        return reservationDto;
    }

    public static List<ReservationDto> convertReservations(List<Reservation> reservation) {
        List<ReservationDto> result = new ArrayList<>();

        if (reservation != null) {
            for (Reservation e : reservation) {
                result.add(convert(e));
            }
        }
        return result;
    }
}
