package at.ac.tuwien.inso.ticketline.server.util.EntityToDto;

import at.ac.tuwien.inso.ticketline.dto.CustomerDto;
import at.ac.tuwien.inso.ticketline.model.Address;
import at.ac.tuwien.inso.ticketline.model.Customer;
import at.ac.tuwien.inso.ticketline.model.Gender;

import java.util.ArrayList;
import java.util.List;

public class CustomerEntityToDto {

    private static CustomerEntityToDto instance = null;
    private CustomerEntityToDto() {}
    public static CustomerEntityToDto getInstance() {
        if(instance == null) {
            instance = new CustomerEntityToDto();
        }
        return instance;
    }

    /**
     * Converts a costumer entity to a customer DTO
     *
     * @param c the customer entity
     * @return the converted customer DTO
     */
    public static CustomerDto convert(Customer c) {
        CustomerDto dto = new CustomerDto();
        dto.setFirstname(c.getFirstname());
        dto.setLastname(c.getLastname());
        if (c.getGender().equals(Gender.MALE))
            dto.setGender("male");
        else if (c.getGender().equals(Gender.FEMALE))
            dto.setGender("female");
        dto.setDateOfBirth(c.getDateOfBirth());
        dto.setEmail(c.getEmail());
        dto.setPhoneNumber(c.getPhoneNumber());
        dto.setId(c.getId());
        dto.setPremiumPoints(c.getPremiumpoints());
        Address address = c.getAddress();

        if (address != null) {
            dto.setCountry(address.getCountry());
            dto.setCity(address.getCity());
            dto.setPostalcode(address.getPostalCode());
            dto.setStreet(address.getStreet());
        }

        return dto;
    }

    /**
     * Converts a costumer entity  list to a customer DTO list
     *
     * @param customers the list of costumer entities
     * @return the converted customer DTO list
     */
    public static List<CustomerDto> convertCustomers(List<Customer> customers) {
        List<CustomerDto> result = new ArrayList<>();

        if (customers != null) {
            for (Customer e : customers) {
                result.add(convert(e));
            }
        }
        return result;
    }
}
