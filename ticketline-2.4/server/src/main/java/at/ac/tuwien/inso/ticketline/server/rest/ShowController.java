package at.ac.tuwien.inso.ticketline.server.rest;

import at.ac.tuwien.inso.ticketline.dao.ShowDao;
import at.ac.tuwien.inso.ticketline.dto.*;
import at.ac.tuwien.inso.ticketline.model.*;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.server.service.ReservationService;
import at.ac.tuwien.inso.ticketline.server.service.SellService;
import at.ac.tuwien.inso.ticketline.server.service.ShowService;
import at.ac.tuwien.inso.ticketline.server.service.TicketService;
import at.ac.tuwien.inso.ticketline.server.util.DtoToEntity.CustomerDtoToEntity;
import at.ac.tuwien.inso.ticketline.server.util.DtoToEntity.TicketDtoToEntity;
import at.ac.tuwien.inso.ticketline.server.util.EntityToDto.ShowEntityToDto;
import at.ac.tuwien.inso.ticketline.server.util.EntityToDto.TicketIdentifierEntityToDto;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Api(value = "show", description = "Show REST service")
@RestController
@RequestMapping(value = "/show")
public class ShowController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ShowController.class);

    @Autowired
    private ShowService showService;


    /**
     * Get all the shows from the database
     *
     * @return a list of the shows
     * @throws ServiceException if there is any exceptions in the ShowsService
     */
    @ApiOperation(value = "Gets all shows", response = ShowDao.class, responseContainer = "List")
    @RequestMapping(value = "/", method = RequestMethod.GET, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public List<ShowDto> getPerformances() throws ServiceException {

        LOGGER.info("getAll() called");

        return ShowEntityToDto.getInstance().convertShows(showService.getAll());
    }


    @ApiOperation(value = "Gets the shows for which a certain customer has bought and/or reserved tickets", response = MapEntryDto.class, responseContainer = "List")
    @RequestMapping(value = "/customerShows", method = RequestMethod.POST, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public List<MapEntryDto<ShowDto,List<TicketIdentifierDto>>> getTicketsForShow(@ApiParam(name="customer",value="Shows for this customer") @RequestBody CustomerDto customerDto) throws ServiceException {
        LOGGER.info("getTicketsForShow() is called");

        return showService.getTicketsForShow(customerDto);
    }


    @ApiOperation(value = "Gets the ticketidentifiers  for which a certain customer has bought and/or reserved tickets", response = TicketIdentifierDto.class, responseContainer = "List")
    @RequestMapping(value = "/customerTickets", method = RequestMethod.POST, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public List<TicketIdentifierDto> getTicketsForShowAndCustomer(@ApiParam(name="tickets",value="TicketDtos for this show this customer") @RequestBody List<TicketDto> ticketDtos) throws ServiceException {
        LOGGER.info("getTicketsForShowAndCustomer() is called");

        return showService.getTicketsForShowAndCustomer(ticketDtos);
    }

    public void setShowService(ShowService showService) {
        this.showService = showService;
    }
}
