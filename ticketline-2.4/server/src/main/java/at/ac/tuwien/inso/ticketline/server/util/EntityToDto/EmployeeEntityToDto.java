package at.ac.tuwien.inso.ticketline.server.util.EntityToDto;

import at.ac.tuwien.inso.ticketline.dto.EmployeeDto;
import at.ac.tuwien.inso.ticketline.model.Employee;

public class EmployeeEntityToDto {

    private static EmployeeEntityToDto instance = null;
    private EmployeeEntityToDto() {}
    public static EmployeeEntityToDto getInstance() {
        if(instance == null) {
            instance = new EmployeeEntityToDto();
        }
        return instance;
    }

    public static EmployeeDto convert(Employee employee) {
        EmployeeDto employeeDto = new EmployeeDto();
        employeeDto.setId(employee.getId());
        employeeDto.setFirstName(employee.getFirstname());
        employeeDto.setLastName(employee.getLastname());
        employeeDto.setUsername(employee.getUsername());
        employeeDto.setLastlogin(employee.getLastLogin());
        employeeDto.setPasswordHash(employee.getPasswordHash());
        if(employee.getPermission()!=null)
        employeeDto.setPermission(employee.getPermission().toString());
        return employeeDto;
    }
}
