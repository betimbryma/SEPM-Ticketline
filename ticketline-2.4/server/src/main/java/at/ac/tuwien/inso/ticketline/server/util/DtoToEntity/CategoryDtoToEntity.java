package at.ac.tuwien.inso.ticketline.server.util.DtoToEntity;

import at.ac.tuwien.inso.ticketline.dto.CategoryDto;
import at.ac.tuwien.inso.ticketline.model.Category;

public class CategoryDtoToEntity {

    private static CategoryDtoToEntity instance = null;
    private CategoryDtoToEntity() {}
    public static CategoryDtoToEntity getInstance() {
        if(instance == null) {
            instance = new CategoryDtoToEntity();
        }
        return instance;
    }

    public static Category convert(CategoryDto categoryDto){
        Category category = new Category();
        category.setDescription(categoryDto.getDescription());
        category.setId(categoryDto.getId());
        category.setName(categoryDto.getName());
        return category;
    }
}
