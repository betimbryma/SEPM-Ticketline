package at.ac.tuwien.inso.ticketline.server.service;

import at.ac.tuwien.inso.ticketline.model.Room;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;

import java.util.List;

public interface RoomService {

    /**
     * Gets all the rooms in the database
     *
     * @return a list of the rooms
     * @throws ServiceException if there are any exception, they are going to be encapsulated in ServiceException
     */
    List<Room> getAll() throws ServiceException;
}