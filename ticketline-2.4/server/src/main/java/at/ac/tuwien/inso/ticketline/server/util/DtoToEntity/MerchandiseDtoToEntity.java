package at.ac.tuwien.inso.ticketline.server.util.DtoToEntity;

import at.ac.tuwien.inso.ticketline.dto.MerchandiseDto;
import at.ac.tuwien.inso.ticketline.model.Article;

public class MerchandiseDtoToEntity {

    public static Article convertArticleDtoWithId(MerchandiseDto merchandiseDto) {
        Article article = new Article();
        article.setId(merchandiseDto.getId());
        article.setDescription(merchandiseDto.getDescription());
        article.setName(merchandiseDto.getName());
        article.setPrice(merchandiseDto.getPrice());
        article.setAvailable(merchandiseDto.getAvailable());
        return article;
    }

}
