package at.ac.tuwien.inso.ticketline.server.util.DtoToEntity;

import at.ac.tuwien.inso.ticketline.dto.EmployeeDto;
import at.ac.tuwien.inso.ticketline.dto.UserStatusDto;
import at.ac.tuwien.inso.ticketline.model.Employee;
import at.ac.tuwien.inso.ticketline.model.Permission;

public class EmployeeDtoToEntity {

    private static EmployeeDtoToEntity instance = null;
    private EmployeeDtoToEntity() {}
    public static EmployeeDtoToEntity getInstance() {
        if(instance == null) {
            instance = new EmployeeDtoToEntity();
        }
        return instance;
    }

    /**
     * Convert UserStatusDto to Employee
     *
     * @param userStatusDto
     * @return
     */
    public static Employee convertUserStatusDto(UserStatusDto userStatusDto) {
        Employee employee = new Employee();
        employee.setId(1);
        return employee;
    }

    public static Employee convert(EmployeeDto employeeDto) {
        Employee employee = new Employee();
        employee.setId(employeeDto.getId());
        employee.setFirstname(employeeDto.getFirstName());
        employee.setLastname(employeeDto.getLastName());
        employee.setUsername(employeeDto.getUsername());
        employee.setLastLogin(employeeDto.getLastlogin());
        employee.setPasswordHash(employeeDto.getPasswordHash());
        if(employeeDto.getPermission()!=null) {
            switch (employeeDto.getPermission()) {
                case "SUSPENDED":
                    employee.setPermission(Permission.SUSPENDED);
                    break;
                case "USER":
                    employee.setPermission(Permission.USER);
                    break;
                case "GUEST":
                    employee.setPermission(Permission.GUEST);
                    break;
                default:
                    employee.setPermission(Permission.ADMINISTRATOR);
                    break;
            }
        }
        return employee;
    }

}
