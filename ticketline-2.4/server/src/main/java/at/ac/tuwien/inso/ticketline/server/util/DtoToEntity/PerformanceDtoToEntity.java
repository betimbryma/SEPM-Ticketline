package at.ac.tuwien.inso.ticketline.server.util.DtoToEntity;

import at.ac.tuwien.inso.ticketline.dto.PerformanceDto;
import at.ac.tuwien.inso.ticketline.model.Performance;

public class PerformanceDtoToEntity {

    private static PerformanceDtoToEntity instance = null;
    private PerformanceDtoToEntity() {}
    public static PerformanceDtoToEntity getInstance() {
        if(instance == null) {
            instance = new PerformanceDtoToEntity();
        }
        return instance;
    }

    public static Performance convertPerformance(PerformanceDto performanceDto) {
        Performance performance = new Performance();
        performance.setId(performanceDto.getId());
        performance.setDuration(performanceDto.getDuration());
        performance.setDescription(performanceDto.getDescription());
        performance.setName(performanceDto.getName());
        return performance;
    }
}
