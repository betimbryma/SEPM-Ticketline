package at.ac.tuwien.inso.ticketline.server.util.DtoToEntity;

import at.ac.tuwien.inso.ticketline.dto.TicketIdentifierDto;
import at.ac.tuwien.inso.ticketline.model.TicketIdentifier;

public class TicketIdentifierDtoToEntity {

    private static TicketIdentifierDtoToEntity instance = null;
    private TicketIdentifierDtoToEntity() {}
    public static TicketIdentifierDtoToEntity getInstance() {
        if(instance == null) {
            instance = new TicketIdentifierDtoToEntity();
        }
        return instance;
    }

    public static TicketIdentifier convert(TicketIdentifierDto ticketIdentifierDto){
        TicketIdentifier ticketIdentifier = new TicketIdentifier();
        ticketIdentifier.setId(ticketIdentifierDto.getId());
        ticketIdentifier.setCancellationReason(ticketIdentifierDto.getCancellationReason());
        if(ticketIdentifierDto.getReservation()!=null)
            ticketIdentifier.setReservation(ReservationDtoToEntity.getInstance().convert(ticketIdentifierDto.getReservation()));
        else
            ticketIdentifier.setReservation(null);
        ticketIdentifier.setTicket(TicketDtoToEntity.getInstance().convert(ticketIdentifierDto.getTicket()));
        ticketIdentifier.setUuid(ticketIdentifierDto.getUuid());
        ticketIdentifier.setValid(ticketIdentifierDto.getValid());
        ticketIdentifier.setVoidationTime(ticketIdentifierDto.getVoidationTime());
        ticketIdentifier.setVoidedBy(ticketIdentifierDto.getVoidedBy());
        return ticketIdentifier;
    }
}
