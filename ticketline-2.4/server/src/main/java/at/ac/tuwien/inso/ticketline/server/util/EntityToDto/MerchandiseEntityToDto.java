package at.ac.tuwien.inso.ticketline.server.util.EntityToDto;

import at.ac.tuwien.inso.ticketline.dto.MerchandiseDto;
import at.ac.tuwien.inso.ticketline.model.Article;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MerchandiseEntityToDto {

    private static final Logger LOGGER = LoggerFactory.getLogger(MerchandiseEntityToDto.class);

    private static MerchandiseEntityToDto instance = null;
    private MerchandiseEntityToDto() {}
    public static MerchandiseEntityToDto getInstance() {
        if(instance == null) {
            instance = new MerchandiseEntityToDto();
        }
        return instance;
    }

    /**
     * Converts a article entity to a article DTO.
     *
     * @param article the customer
     * @return the customer DTO
     */
    public static MerchandiseDto convertArticle(Article article) {
        if (article == null) {
            LOGGER.debug("Trying to convert a null article");
            return null;
        }
        MerchandiseDto dto = new MerchandiseDto();
        dto.setId(article.getId());
        dto.setName(article.getName());
        dto.setAvailable(article.getAvailable());
        dto.setDescription(article.getDescription());
        dto.setPrice(article.getPrice());
        dto.setPerf_id(article.getPerformance().getId());
        dto.setPerformanceDto(PerformanceEntityToDto.getInstance().convertPerformance(article.getPerformance()));
        return dto;
    }
}
