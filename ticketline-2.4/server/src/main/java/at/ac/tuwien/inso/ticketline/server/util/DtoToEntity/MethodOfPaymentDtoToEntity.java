package at.ac.tuwien.inso.ticketline.server.util.DtoToEntity;

import at.ac.tuwien.inso.ticketline.dto.BankAccountDto;
import at.ac.tuwien.inso.ticketline.dto.CashDto;
import at.ac.tuwien.inso.ticketline.dto.CreditCardDto;
import at.ac.tuwien.inso.ticketline.model.BankAccount;
import at.ac.tuwien.inso.ticketline.model.Cash;
import at.ac.tuwien.inso.ticketline.model.Creditcard;
import at.ac.tuwien.inso.ticketline.model.Customer;

public class MethodOfPaymentDtoToEntity {

    public static BankAccount convertBankAccountDto(BankAccountDto bankAccountDto) {
        BankAccount bankAccount = new BankAccount();
        bankAccount.setId(bankAccountDto.getId());
        bankAccount.setDeleted(bankAccountDto.getDeleted());

        /*Code too be changed*/
        Customer customer = new Customer();
        customer.setId(bankAccountDto.getCustomer().getId());


        bankAccount.setCustomer(customer);
        bankAccount.setBIC(bankAccountDto.getBIC());
        bankAccount.setIBAN(bankAccountDto.getIBAN());
        bankAccount.setBank(bankAccountDto.getBank());
        bankAccount.setAccountNumber(bankAccountDto.getAccountNumber());
        bankAccount.setBankCode(bankAccountDto.getBankCode());
        bankAccount.setOwner(bankAccountDto.getOwner());
        return bankAccount;
    }
    public static Creditcard convertCreditCardDto(CreditCardDto creditCardDto) {
        Creditcard creditcard = new Creditcard();
        creditcard.setId(creditCardDto.getId());
        creditcard.setDeleted(creditCardDto.getDeleted());

        /*Code too be changed*/
        Customer customer = new Customer();
        customer.setId(creditCardDto.getCustomer().getId());


        creditcard.setCustomer(customer);
        creditcard.setCreditcardNumber(creditCardDto.getCreditcardNumber());
        if(at.ac.tuwien.inso.ticketline.dto.CreditcardType.VISA==creditCardDto.getCreditcardType()){
            creditcard.setCreditcardType(at.ac.tuwien.inso.ticketline.model.CreditcardType.VISA);
        }else if(at.ac.tuwien.inso.ticketline.dto.CreditcardType.MASTERCARD==creditCardDto.getCreditcardType()){
            creditcard.setCreditcardType(at.ac.tuwien.inso.ticketline.model.CreditcardType.MASTERCARD);
        }else if(at.ac.tuwien.inso.ticketline.dto.CreditcardType.AMERICAN_EXPRESS==creditCardDto.getCreditcardType()){
            creditcard.setCreditcardType(at.ac.tuwien.inso.ticketline.model.CreditcardType.AMERICAN_EXPRESS);
        }
        creditcard.setOwner(creditCardDto.getOwner());
        creditcard.setValidThru(creditCardDto.getValidThru());
        return creditcard;

    }
    public static Cash convertCashDto(CashDto cashDto) {
        Cash cash = new Cash();
        cash.setId(cashDto.getId());
        cash.setDeleted(cashDto.getDeleted());

        /*Code too be changed*/
        Customer customer = new Customer();
        customer.setId(cashDto.getCustomer().getId());


        cash.setCustomer(customer);
        return cash;
    }
}
