package at.ac.tuwien.inso.ticketline.server.util.DtoToEntity;

import at.ac.tuwien.inso.ticketline.dto.ReceiptentryDto;
import at.ac.tuwien.inso.ticketline.model.ReceiptEntry;

public class ReceiptEntryDtoToEntity {

    public static ReceiptEntry convertReceiptEntryDto(ReceiptentryDto receiptentryDto) {
        if (receiptentryDto == null)
            return null;
        ReceiptEntry receiptEntry = new ReceiptEntry();
        receiptEntry.setId(receiptentryDto.getId());
        receiptEntry.setAmount(receiptentryDto.getAmount());
        receiptEntry.setPosition(receiptentryDto.getPosition());
        receiptEntry.setUnitPrice(receiptentryDto.getUnitPrice());

        return receiptEntry;
    }

}
