package at.ac.tuwien.inso.ticketline.server.service.implementation;

import at.ac.tuwien.inso.ticketline.dao.PremiumDao;
import at.ac.tuwien.inso.ticketline.dto.PremiumDto;
import at.ac.tuwien.inso.ticketline.model.Premium;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.server.service.PremiumService;
import at.ac.tuwien.inso.ticketline.server.util.DtoToEntity.PremiumDtoToEntity;
import at.ac.tuwien.inso.ticketline.server.util.EntityToDto.PremiumEntityToDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

/**
 * Created by eni on 04.06.2016.
 */
@Service
public class PremiumServiceImpl implements PremiumService {

    @Autowired
    private PremiumDao premiumDao;


    private Semaphore writeLockcheckArticles = new Semaphore(1);


    @Override
    public List<PremiumDto> getAll()throws ServiceException{

        List<PremiumDto> premiumDtos = new ArrayList<>();
        for (Premium p : premiumDao.findAll()){
            premiumDtos.add(PremiumEntityToDto.convertPremium(p));
        }

        return premiumDtos;

    }
    @Override
    public Premium searchPremiumID(int id) throws ServiceException {
        return premiumDao.findOne(id);
    }

    @Override
    public Premium updatePremium(Premium premium) throws ServiceException {
        return premiumDao.save(premium);
    }

    @Override
    public PremiumDto checkPremiumInServer(PremiumDto merchandiseDtos) throws ServiceException {
        if(merchandiseDtos ==null)
            return null;

        try {
            writeLockcheckArticles.acquire();
        } catch (InterruptedException e) {
            throw new ServiceException(e.getMessage());
        }
        Premium article = PremiumDtoToEntity.convertPremiumDtoWithId(merchandiseDtos);
        article = this.searchPremiumID(article.getId());
        if(article.getAvailable()>0) {
            article.setAvailable(article.getAvailable() - 1);
            article = this.updatePremium(article);
            writeLockcheckArticles.release();
            return PremiumEntityToDto.convertPremium(article);
        }else{
            merchandiseDtos.setAvailable(-1);
            writeLockcheckArticles.release();
            return merchandiseDtos;
        }
    }

    @Override
    public PremiumDto returnPremium(PremiumDto merchandiseDtos) throws ServiceException {

        if(merchandiseDtos ==null) {
            return null;
        }

        Premium article = PremiumDtoToEntity.convertPremiumDtoWithId(merchandiseDtos);
        article = this.searchPremiumID(article.getId());
        article.setAvailable(article.getAvailable() +1);
        article = this.updatePremium(article);
        return PremiumEntityToDto.convertPremium(article);
    }

    public void add(PremiumDao p ){premiumDao=p;}
}