package at.ac.tuwien.inso.ticketline.server.service.implementation;

import at.ac.tuwien.inso.ticketline.dao.ShowDao;
import at.ac.tuwien.inso.ticketline.dto.*;
import at.ac.tuwien.inso.ticketline.model.*;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.server.service.ReservationService;
import at.ac.tuwien.inso.ticketline.server.service.SellService;
import at.ac.tuwien.inso.ticketline.server.service.ShowService;
import at.ac.tuwien.inso.ticketline.server.service.TicketService;
import at.ac.tuwien.inso.ticketline.server.util.DtoToEntity.CustomerDtoToEntity;
import at.ac.tuwien.inso.ticketline.server.util.DtoToEntity.TicketDtoToEntity;
import at.ac.tuwien.inso.ticketline.server.util.EntityToDto.ShowEntityToDto;
import at.ac.tuwien.inso.ticketline.server.util.EntityToDto.TicketIdentifierEntityToDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ShowServiceImpl implements ShowService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ShowServiceImpl.class);

    @Autowired
    private ShowDao showDao;

    @Autowired
    private SellService sellService;

    @Autowired
    private ReservationService reservationService;

    @Autowired
    private TicketService ticketService;

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Show> getAll() throws ServiceException {
        LOGGER.info("getAll() called");
        List<Show> list = showDao.findAll();
        return list;
    }

    @Override
    public Show getById(Integer ID) throws ServiceException {
            return showDao.findOne(ID);
    }

    @Override
    public List<MapEntryDto<ShowDto, List<TicketIdentifierDto>>> getTicketsForShow(CustomerDto customerDto) throws ServiceException {
        Map<ShowDto,List<TicketIdentifierDto>> hashMap = new HashMap<>();
        List<Reservation> reservationList = reservationService.getForCustomer(CustomerDtoToEntity.getInstance().convert(customerDto));
        List<MapEntryDto<ShowDto,List<TicketIdentifierDto>>> list = new ArrayList<>();
        for(Reservation reservation : reservationList){
            List<TicketIdentifier> ticketIdentifiers = ticketService.getTicketIdentifiersForReservation(reservation);
            if(!ticketIdentifiers.isEmpty()){
                TicketIdentifier ticketIdentifier = ticketIdentifiers.get(0);
                Ticket ticket = ticketIdentifier.getTicket();
                ShowDto showDto = ShowEntityToDto.getInstance().convertShow(ticket.getShow());
                if(!hashMap.containsKey(showDto)) {
                    hashMap.put(showDto, TicketIdentifierEntityToDto.getInstance().convertTicketIdentifiers(ticketIdentifiers));
                } else {
                    List<TicketIdentifierDto> ticketIdentifiers1 = new ArrayList<>(hashMap.get(showDto));
                    ticketIdentifiers1.addAll(TicketIdentifierEntityToDto.getInstance().convertTicketIdentifiers(ticketIdentifiers));
                    hashMap.put(showDto,ticketIdentifiers1);
                }
            }
        }

        List<Receipt> receiptList = sellService.getForCustomer(CustomerDtoToEntity.getInstance().convert(customerDto));

        for(Receipt receipt : receiptList){
            List<ReceiptEntry> receiptEntryList = sellService.getForReceipt(receipt);
            for(ReceiptEntry receiptEntry : receiptEntryList){
                TicketIdentifier ticketIdentifier = receiptEntry.getTicketIdentifier();
                if(ticketIdentifier.getValid()) {
                    Ticket ticket = ticketIdentifier.getTicket();
                    ShowDto showDto = ShowEntityToDto.getInstance().convertShow(ticket.getShow());
                    if (!hashMap.containsKey(showDto)) {
                        List<TicketIdentifierDto> ticketIdentifierDtos = new ArrayList<>();
                        ticketIdentifierDtos.add(TicketIdentifierEntityToDto.getInstance().convert(ticketIdentifier));
                        hashMap.put(showDto, ticketIdentifierDtos);
                    } else {
                        List<TicketIdentifierDto> ticketIdentifiers1 = hashMap.get(showDto);
                        ticketIdentifiers1.add(TicketIdentifierEntityToDto.getInstance().convert(ticketIdentifier));
                        hashMap.put(showDto, ticketIdentifiers1);
                    }
                }
            }
        }

        for(Map.Entry<ShowDto,List<TicketIdentifierDto>> entry : hashMap.entrySet()){
            MapEntryDto<ShowDto,List<TicketIdentifierDto>> mapEntryDto = new MapEntryDto<>(entry.getKey(),entry.getValue());
            list.add(mapEntryDto);
        }

        return list;

    }

    @Override
    public List<TicketIdentifierDto> getTicketsForShowAndCustomer(List<TicketDto> ticketDtos) throws ServiceException {
        List<TicketIdentifier> ticketIdentifiers = new ArrayList<>();
        for(TicketDto ticketDto : ticketDtos){
            List<TicketIdentifier> ticketIdentifiers1 = ticketService.getTicketIdentifier(TicketDtoToEntity.getInstance().convert(ticketDto));
            if(!ticketIdentifiers1.isEmpty())
                ticketIdentifiers.add(ticketIdentifiers1.get(0));
        }
        return TicketIdentifierEntityToDto.getInstance().convertTicketIdentifiers(ticketIdentifiers);
    }

    // -------------------- For Testing purposes --------------------

    /**
     * Sets the customer dao.
     *
     * @param dao the new customer dao
     */
    public void setShowDao(ShowDao dao) {
        this.showDao = dao;
    }

    // -------------------- For Testing purposes --------------------
}