package at.ac.tuwien.inso.ticketline.server.service;

import at.ac.tuwien.inso.ticketline.dto.*;
import at.ac.tuwien.inso.ticketline.model.Reservation;
import at.ac.tuwien.inso.ticketline.model.Ticket;
import at.ac.tuwien.inso.ticketline.model.TicketIdentifier;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;

import java.util.List;

public interface TicketService {

    List<Ticket> getTicketsByShow(Integer show) throws ServiceException;

    List<TicketIdentifier> getTicketIdentifier(Ticket ticket) throws ServiceException;

    Integer getSoldTicketAmount(Integer show) throws ServiceException;

    List<TicketIdentifier> getAllTicketIdentifier() throws ServiceException;

    TicketIdentifier saveTicketIdentifier(TicketIdentifier ticketIdentifier) throws ServiceException;

    List<MapEntryDto<ShowDto, Integer>> getMinPrices() throws ServiceException;

    List<MapEntryDto<ShowDto, Integer>> getMaxPrices() throws ServiceException;

    TicketIdentifier getTicketIdentifierById(TicketIdentifier t) throws ServiceException;

    List<TicketIdentifier> getTicketIdentifiersForReservation(Reservation reservation) throws ServiceException;

    void updateThisTicketIdentifier(TicketIdentifier ticketIdentifier) throws ServiceException;

    void cancelThisTicketIdentifier(TicketIdentifier ticketIdentifier) throws ServiceException;

    Ticket getTicketByID(int id) throws  ServiceException;

    void updateTicketForCartAsUsed(Ticket t)throws ServiceException;

    void updateTicketForCartAsNotUsed(Ticket t) throws ServiceException;

    void updateTicketForCartAsReserved( Ticket ticketE) throws ServiceException;

    void updateTicketForCartAsSold(Ticket ticketE) throws ServiceException;

    Integer getFlag(Ticket t) throws ServiceException;

    List<TicketIdentifier> getTicketIdentifierForTicket(Ticket t) throws ServiceException;



    /**
     * check Free Ticket For Sell
     * @param ticketDto
     * @return
     * @throws ServiceException
     */
    MessageDto checkFreeTicketForSell(TicketDto ticketDto) throws ServiceException;

    /**
     * check Reserve Ticket For Sell
     *
     * @param ticketDto
     * @return
     * @throws ServiceException
     */
    MessageDto checkReserveTicketForSell(TicketDto ticketDto) throws ServiceException;

    /**
     *check Free Ticket For Reserve
     * @param ticketDto
     * @return
     * @throws ServiceException
     */
    MessageDto checkFreeTicketForReserve(TicketDto ticketDto) throws ServiceException;

    public List<MapEntryDto<TicketDto, TicketIdentifierDto>> getTicketsForShow(Integer show) throws ServiceException;

    public List<TicketIdentifierDto> reserveTickets(List<TicketIdentifierDto> ticketDtoList) throws ServiceException;


    }
