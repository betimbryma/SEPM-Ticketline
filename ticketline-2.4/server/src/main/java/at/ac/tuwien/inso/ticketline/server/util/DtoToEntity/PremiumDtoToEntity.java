package at.ac.tuwien.inso.ticketline.server.util.DtoToEntity;

import at.ac.tuwien.inso.ticketline.dto.PremiumDto;
import at.ac.tuwien.inso.ticketline.model.Premium;

/**
 * Created by eni on 04.06.2016.
 */
public class PremiumDtoToEntity {
    public static Premium convertPremiumDtoWithId(PremiumDto merchandiseDto) {
        Premium article = new Premium();
        article.setId(merchandiseDto.getId());
        article.setDescription(merchandiseDto.getDescription());
        article.setName(merchandiseDto.getName());
        article.setPrice(merchandiseDto.getPrice());
        article.setAvailable(merchandiseDto.getAvailable());
        return article;
    }

}
