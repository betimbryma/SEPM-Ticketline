package at.ac.tuwien.inso.ticketline.server.rest;

import at.ac.tuwien.inso.ticketline.dto.PremiumDto;
import at.ac.tuwien.inso.ticketline.model.Premium;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.server.service.PremiumService;
import at.ac.tuwien.inso.ticketline.server.util.DtoToEntity.PremiumDtoToEntity;
import at.ac.tuwien.inso.ticketline.server.util.EntityToDto.PremiumEntityToDto;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

/**
 * Created by eni on 04.06.2016.
 */
@Api(value = "premium", description = "Premium Rest service")
@RestController
@RequestMapping(value = "/premium")
public class PremiumController {

    private static final Logger LOGGER = LoggerFactory.getLogger(PremiumController.class);

    @Autowired
    private PremiumService premiumService;

    @ApiOperation(value = "Check if the Premium is empty", response = PremiumDto.class, responseContainer = "PremiumDto")
    @RequestMapping(value = "/checkPremium", method = RequestMethod.POST, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public PremiumDto checkPremiumInServer(@ApiParam(name="articles",value="ArticlesTo Be Checked before puttinge to CArt") @RequestBody PremiumDto merchandiseDtos) throws ServiceException {
        LOGGER.info("checkArticles() is called");

        return premiumService.checkPremiumInServer(merchandiseDtos);
    }

    @ApiOperation(value = "Check if the Premium is empty", response = PremiumDto.class, responseContainer = "PremiumDto")
    @RequestMapping(value = "/ReturnPremium", method = RequestMethod.POST, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public PremiumDto returnPremium(@ApiParam(name="premium",value="returned premium") @RequestBody PremiumDto merchandiseDtos) throws ServiceException {
        LOGGER.info("checkArticles() is called");

        return premiumService.returnPremium(merchandiseDtos);
    }

    /**
     * Gets the all news items.
     *
     * @return list of all news items
     * @throws ServiceException the service exception
     */
    @ApiOperation(value = "Gets all premiums", response = PremiumDto.class, responseContainer = "List")
    @RequestMapping(value = "/premiums", method = RequestMethod.GET, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public List<PremiumDto> getAll() throws ServiceException {
        LOGGER.info("getAll() called");

        return premiumService.getAll();
    }
}
