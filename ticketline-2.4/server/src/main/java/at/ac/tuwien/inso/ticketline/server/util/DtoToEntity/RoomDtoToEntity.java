package at.ac.tuwien.inso.ticketline.server.util.DtoToEntity;

import at.ac.tuwien.inso.ticketline.dto.RoomDto;
import at.ac.tuwien.inso.ticketline.model.Room;

public class RoomDtoToEntity {

    private static RoomDtoToEntity instance = null;
    private RoomDtoToEntity() {}
    public static RoomDtoToEntity getInstance() {
        if(instance == null) {
            instance = new RoomDtoToEntity();
        }
        return instance;
    }

    public static Room convert(RoomDto roomDto){
        Room room = new Room();
        room.setDescription(roomDto.getDescription());
        room.setId(roomDto.getId());
        room.setName(roomDto.getName());
        room.setLocation(LocationDtoToEntity.getInstance().convert(roomDto.getLocation()));
        return room;
    }
}
