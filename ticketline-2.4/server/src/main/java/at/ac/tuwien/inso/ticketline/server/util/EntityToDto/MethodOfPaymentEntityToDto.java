package at.ac.tuwien.inso.ticketline.server.util.EntityToDto;

import at.ac.tuwien.inso.ticketline.model.Cash;
import at.ac.tuwien.inso.ticketline.model.MethodOfPayment;

public class MethodOfPaymentEntityToDto {

    private static MethodOfPaymentEntityToDto instance = null;
    private MethodOfPaymentEntityToDto() {}
    public static MethodOfPaymentEntityToDto getInstance() {
        if(instance == null) {
            instance = new MethodOfPaymentEntityToDto();
        }
        return instance;
    }

    public static MethodOfPayment convertMethodOfPaymentDto() {
        MethodOfPayment methodOfPayment = new Cash();
        methodOfPayment.setId(1);
        return methodOfPayment;
    }
}
