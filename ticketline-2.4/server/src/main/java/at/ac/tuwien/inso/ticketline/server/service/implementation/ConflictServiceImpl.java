package at.ac.tuwien.inso.ticketline.server.service.implementation;

import at.ac.tuwien.inso.ticketline.dto.MerchandiseDto;
import at.ac.tuwien.inso.ticketline.dto.MessageDto;
import at.ac.tuwien.inso.ticketline.dto.TicketDto;
import at.ac.tuwien.inso.ticketline.dto.TicketIdentifierDto;
import at.ac.tuwien.inso.ticketline.model.Article;
import at.ac.tuwien.inso.ticketline.model.Ticket;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.server.service.ConflictService;
import at.ac.tuwien.inso.ticketline.server.service.MerchandiseService;
import at.ac.tuwien.inso.ticketline.server.service.TicketService;
import at.ac.tuwien.inso.ticketline.server.util.DtoToEntity.MerchandiseDtoToEntity;
import at.ac.tuwien.inso.ticketline.server.util.DtoToEntity.TicketDtoToEntity;
import at.ac.tuwien.inso.ticketline.server.util.EntityToDto.MerchandiseEntityToDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.Semaphore;

/**
 * Created by Jakob on 18.06.2016.
 */
@Service
public class ConflictServiceImpl implements ConflictService {

    @Autowired
    private MerchandiseService merchandiseService;

    @Autowired
    private TicketService ticketService;

    private Semaphore writeLockcheckArticles = new Semaphore(1);
    private Semaphore writeLockreleaseArticles = new Semaphore(1);
    private Semaphore writeLockcheckFreeTicketForSell = new Semaphore(1);
    private Semaphore writeLockcheckReserveTicketForSell = new Semaphore(1);
    private Semaphore writeLockcheckFreeTicketForReserve = new Semaphore(1);

    @Override
    public MerchandiseDto checkMerchandiseInServer(MerchandiseDto merchandiseDtos) throws ServiceException {
        if(merchandiseDtos ==null)
            return null;

        try {
            writeLockcheckArticles.acquire();
        } catch (InterruptedException e) {
            throw new ServiceException(e.getMessage());
        }
        Article article = MerchandiseDtoToEntity.convertArticleDtoWithId(merchandiseDtos);
        article = merchandiseService.searchArticleID(article.getId());
        if(article.getAvailable()>0) {
            article.setAvailable(article.getAvailable() - 1);
            merchandiseService.decreaseArticle2(article);
            article = merchandiseService.searchArticleID(article.getId());
            writeLockcheckArticles.release();
            return MerchandiseEntityToDto.convertArticle(article);
        }else{
            merchandiseDtos.setAvailable(-1);
            writeLockcheckArticles.release();
            return merchandiseDtos;
        }
    }

    @Override
    public List<MerchandiseDto> releaseArticles(List<MerchandiseDto> merchandiseDtos) throws ServiceException {
        if(merchandiseDtos ==null)
            return null;

        try {
            writeLockreleaseArticles.acquire();
        } catch (InterruptedException e) {
            throw new ServiceException(e.getMessage());
        }

        merchandiseService.resetMerchandises( merchandiseDtos);
        writeLockreleaseArticles.release();
        return null;
    }

    @Override
    public MessageDto checkFreeTicketForSell(TicketDto ticketDto) throws ServiceException {
        try {
            writeLockcheckFreeTicketForSell.acquire();
        } catch (InterruptedException e) {
            throw new ServiceException(e.getMessage());
        }


        MessageDto msg = ticketService.checkFreeTicketForSell(ticketDto);
        writeLockcheckFreeTicketForSell.release();
        return msg;
    }

    @Override
    public MessageDto checkReserveTicketForSell(TicketDto ticketDto) throws ServiceException {
        try {
            writeLockcheckReserveTicketForSell.acquire();
        } catch (InterruptedException e) {
            throw new ServiceException(e.getMessage());
        }

        MessageDto msg = ticketService.checkReserveTicketForSell(ticketDto);
        writeLockcheckReserveTicketForSell.release();
        return msg;
    }

    @Override
    public MessageDto checkFreeTicketForReserve(TicketDto ticketDto) throws ServiceException {
        try {
            writeLockcheckFreeTicketForReserve.acquire();
        } catch (InterruptedException e) {
            throw new ServiceException(e.getMessage());
        }
        MessageDto msg = ticketService.checkFreeTicketForReserve(ticketDto);
        writeLockcheckFreeTicketForReserve.release();
        return msg;
    }

    @Override
    public List<TicketIdentifierDto> releaseTicket(List<TicketDto> ticktes) throws ServiceException {
        for(TicketDto t: ticktes){
            Ticket ticket = TicketDtoToEntity.convert(t);
            ticket = ticketService.getTicketByID(t.getId());
            if(ticket!=null) {
                if (3 != ticketService.getFlag(ticket))
                    ticketService.updateTicketForCartAsNotUsed(ticket);

            }
        }

        return null;
    }

}
