package at.ac.tuwien.inso.ticketline.server.util.EntityToDto;

import at.ac.tuwien.inso.ticketline.dto.RoomDto;
import at.ac.tuwien.inso.ticketline.model.Room;

import java.util.ArrayList;
import java.util.List;

public class RoomEntityToDto {

    private static RoomEntityToDto instance = null;
    private RoomEntityToDto() {}
    public static RoomEntityToDto getInstance() {
        if(instance == null) {
            instance = new RoomEntityToDto();
        }
        return instance;
    }

    /**
     * Converts a room entity to an room DTO
     *
     * @param s the room entity
     * @return the converted room DTO
     */
    public static RoomDto convertRoom(Room s) {

        RoomDto roomDto = new RoomDto();
        roomDto.setId(s.getId());
        roomDto.setName(s.getName());
        roomDto.setDescription(s.getDescription());
        roomDto.setLocation(LocationEntityToDto.getInstance().convertLocation(s.getLocation()));

        return roomDto;
    }

    /**
     * Converts a room entity list to a room DTO list
     *
     * @param rooms the list of room entities
     * @return the converted room DTO list
     */
    public static List<RoomDto> convertRooms(List<Room> rooms) {
        List<RoomDto> result = new ArrayList<>();

        if (rooms != null) {
            for (Room s : rooms) {
                result.add(convertRoom(s));
            }
        }
        return result;
    }
}
