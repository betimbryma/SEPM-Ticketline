package at.ac.tuwien.inso.ticketline.server.rest;

import at.ac.tuwien.inso.ticketline.dao.ParticipationDao;
import at.ac.tuwien.inso.ticketline.dto.ParticipationDto;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.server.service.ParticipationService;
import at.ac.tuwien.inso.ticketline.server.util.EntityToDto.ParticipationEntityToDto;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(value = "participation", description = "Participation REST service")
@RestController
@RequestMapping(value = "/participation")
public class ParticipationController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ParticipationController.class);

    @Autowired
    private ParticipationService participationService;

    /**
     * Get all the participations from the database
     *
     * @return a list of the participations
     * @throws ServiceException if there is any exceptions in the ParticipationService
     */
    @ApiOperation(value = "Gets all participations", response = ParticipationDao.class, responseContainer = "List")
    @RequestMapping(value = "/", method = RequestMethod.GET, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public List<ParticipationDto> getPerformances() throws ServiceException {

        LOGGER.info("getAll() called");

        return ParticipationEntityToDto.getInstance().convertParticipations(participationService.getAll());
    }

}
