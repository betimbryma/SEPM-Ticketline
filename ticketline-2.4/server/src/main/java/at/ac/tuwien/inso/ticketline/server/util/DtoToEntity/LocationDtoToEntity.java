package at.ac.tuwien.inso.ticketline.server.util.DtoToEntity;

import at.ac.tuwien.inso.ticketline.dto.LocationDto;
import at.ac.tuwien.inso.ticketline.model.Address;
import at.ac.tuwien.inso.ticketline.model.Location;

public class LocationDtoToEntity {

    private static LocationDtoToEntity instance = null;
    private LocationDtoToEntity() {}
    public static LocationDtoToEntity getInstance() {
        if(instance == null) {
            instance = new LocationDtoToEntity();
        }
        return instance;
    }

    public static Location convert(LocationDto locationDto){
        Location location = new Location();
        location.setId(locationDto.getId());
        location.setDescription(locationDto.getDescription());
        location.setName(locationDto.getName());
        location.setOwner(locationDto.getOwner());
        Address address = new Address();
        address.setCity(locationDto.getCity());
        address.setStreet(locationDto.getStreet());
        address.setCountry(locationDto.getCountry());
        address.setPostalCode(locationDto.getPostalCode());
        location.setAddress(address);
        return location;
    }
}
