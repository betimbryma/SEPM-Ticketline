package at.ac.tuwien.inso.ticketline.server.util.EntityToDto;

import at.ac.tuwien.inso.ticketline.dto.NewsDto;
import at.ac.tuwien.inso.ticketline.model.News;

import java.util.ArrayList;
import java.util.List;

public class NewsEntityToDto {

    private static NewsEntityToDto instance = null;
    private NewsEntityToDto() {}
    public static NewsEntityToDto getInstance() {
        if(instance == null) {
            instance = new NewsEntityToDto();
        }
        return instance;
    }

    /**
     * Converts a list of news entities to news DTOs
     *
     * @param news the list of news entities
     * @return the list of news DTOs
     */
    public static List<NewsDto> convertNews(List<News> news) {
        List<NewsDto> ret = new ArrayList<>();
        if (null != news) {
            for (News n : news) {
                NewsDto dto = convert(n);
                ret.add(dto);
            }
        }
        return ret;
    }

    /**
     * Converts a news entity to a news DTO.
     *
     * @param news the news
     * @return the news DTO
     */
    public static NewsDto convert(News news) {
        NewsDto dto = new NewsDto();
        dto.setTitle(news.getTitle());
        dto.setNewsText(news.getNewsText());
        dto.setSubmittedOn(news.getSubmittedOn());
        return dto;
    }
}
