package at.ac.tuwien.inso.ticketline.server.service;

import at.ac.tuwien.inso.ticketline.model.BankAccount;
import at.ac.tuwien.inso.ticketline.model.Cash;
import at.ac.tuwien.inso.ticketline.model.Creditcard;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;

public interface MethodOfPaymentService {
    /**
     * Save cash
     * @param cash
     * @return Cash
     * @throws ServiceException
     */
    Cash saveCash(Cash cash)throws ServiceException;
    /**
     * Save bankAccount
     * @param bankAccount
     * @return BankAccount
     * @throws ServiceException
     */
    BankAccount saveBankAccount(BankAccount bankAccount)throws ServiceException;
    /**
     * Save creditcard
     * @param creditcard
     * @return Creditcard
     * @throws ServiceException
     */
    Creditcard saveCreditcard(Creditcard creditcard)throws ServiceException;
}