package at.ac.tuwien.inso.ticketline.server.util.EntityToDto;

import at.ac.tuwien.inso.ticketline.dto.ShowDto;
import at.ac.tuwien.inso.ticketline.model.Show;

import java.util.ArrayList;
import java.util.List;

public class ShowEntityToDto {

    private static ShowEntityToDto instance = null;
    private ShowEntityToDto() {}
    public static ShowEntityToDto getInstance() {
        if(instance == null) {
            instance = new ShowEntityToDto();
        }
        return instance;
    }

    /**
     * Converts a show entity to a show DTO
     *
     * @param s the show entity
     * @return the converted show DTO
     */
    public static ShowDto convertShow(Show s) {

        ShowDto showDto = new ShowDto();
        showDto.setId(s.getId());
        showDto.setPerformance(PerformanceEntityToDto.getInstance().convertPerformance(s.getPerformance()));
        showDto.setDateOfPerformance(s.getDateOfPerformance());
        showDto.setCanceled(s.getCanceled());
        showDto.setRoom(RoomEntityToDto.getInstance().convertRoom(s.getRoom()));

        return showDto;
    }

    /**
     * Converts a show entity list to a show DTO list
     *
     * @param shows the list of show entities
     * @return the converted show DTO list
     */
    public static List<ShowDto> convertShows(List<Show> shows) {
        List<ShowDto> result = new ArrayList<>();

        if (shows != null) {
            for (Show s : shows) {
                result.add(convertShow(s));
            }
        }
        return result;
    }
}
