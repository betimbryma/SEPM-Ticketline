package at.ac.tuwien.inso.ticketline.server.service;

import at.ac.tuwien.inso.ticketline.dto.*;
import at.ac.tuwien.inso.ticketline.model.Show;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;

import java.util.List;

public interface ShowService {

    /**
     * Gets all the shows in the database
     *
     * @return a list of the shows
     * @throws ServiceException if there are any exception, they are going to be encapsulated in ServiceException
     */
    List<Show> getAll() throws ServiceException;

    Show getById(Integer ID) throws ServiceException;

    public List<MapEntryDto<ShowDto,List<TicketIdentifierDto>>> getTicketsForShow(CustomerDto customerDto) throws ServiceException;

    public List<TicketIdentifierDto> getTicketsForShowAndCustomer(List<TicketDto> ticketDtos) throws ServiceException;


    }