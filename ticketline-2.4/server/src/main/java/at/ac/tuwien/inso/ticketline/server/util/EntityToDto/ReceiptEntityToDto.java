package at.ac.tuwien.inso.ticketline.server.util.EntityToDto;

public class ReceiptEntityToDto {

    private static ReceiptEntityToDto instance = null;
    private ReceiptEntityToDto() {}
    public static ReceiptEntityToDto getInstance() {
        if(instance == null) {
            instance = new ReceiptEntityToDto();
        }
        return instance;
    }

}
