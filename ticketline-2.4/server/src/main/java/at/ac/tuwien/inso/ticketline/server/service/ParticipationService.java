package at.ac.tuwien.inso.ticketline.server.service;

import at.ac.tuwien.inso.ticketline.model.Participation;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;

import java.util.List;

public interface ParticipationService {

    /**
     * Gets all the participations in the database
     *
     * @return a list of the participations
     * @throws ServiceException if there are any exception, they are going to be encapsulated in ServiceException
     */
    List<Participation> getAll() throws ServiceException;
}