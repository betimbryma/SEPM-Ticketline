package at.ac.tuwien.inso.ticketline.server.service;

import at.ac.tuwien.inso.ticketline.dto.MerchandiseDto;
import at.ac.tuwien.inso.ticketline.dto.MessageDto;
import at.ac.tuwien.inso.ticketline.dto.TicketDto;
import at.ac.tuwien.inso.ticketline.dto.TicketIdentifierDto;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;

import java.util.List;

/**
 * Created by Jakob on 18.06.2016.
 */
public interface ConflictService {

    public MerchandiseDto checkMerchandiseInServer(MerchandiseDto merchandiseDtos) throws ServiceException;

    public List<MerchandiseDto> releaseArticles(List<MerchandiseDto> merchandiseDtos) throws ServiceException;

    public MessageDto checkFreeTicketForSell(TicketDto ticketDto) throws ServiceException;

    public MessageDto checkReserveTicketForSell(TicketDto ticketDto) throws ServiceException;

    public MessageDto checkFreeTicketForReserve(TicketDto ticketDto) throws ServiceException;

    public List<TicketIdentifierDto> releaseTicket(List<TicketDto> ticktes) throws ServiceException;


    }
