package at.ac.tuwien.inso.ticketline.server.rest;

import at.ac.tuwien.inso.ticketline.dto.EmployeeDto;
import at.ac.tuwien.inso.ticketline.dto.MessageDto;
import at.ac.tuwien.inso.ticketline.dto.MessageType;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.server.service.EmployeeService;
import at.ac.tuwien.inso.ticketline.server.util.DtoToEntity.EmployeeDtoToEntity;
import at.ac.tuwien.inso.ticketline.server.util.EntityToDto.EmployeeEntityToDto;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(value = "employee", description = "Employee REST service")
@RestController
@RequestMapping(value = "/employee")
public class EmployeeController {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmployeeController.class);

    @Autowired
    private EmployeeService employeeService;


    /**
     * Get a certain employee from the database
     *
     * @return an employee with the specified username
     * @throws ServiceException if no employee has the username or if it is not unique or null
     */
    @ApiOperation(value = "gets a certain employee by username", response = EmployeeDto.class)
    @RequestMapping(value = "/{username}", method = RequestMethod.GET, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public EmployeeDto getEmployee(@ApiParam(name = "username", value = "username of the employee") @PathVariable("username") String username) throws ServiceException {

        LOGGER.info("get Employee by Username called");

        EmployeeDto employeeDto = new EmployeeDto();
        employeeDto.setUsername(username);
        return EmployeeEntityToDto.getInstance().convert(employeeService.searchEmployeeByUsername(employeeDto));
    }

    /**
     * Updates the last login date of an employee in the database
     *
     * @return the employee whose last login date should be updated
     * @throws ServiceException if the date could not be updated
     */
    @ApiOperation(value = "updates the date of an employees last login", response = EmployeeDto.class)
    @RequestMapping(value = "/updateLastLogin", method = RequestMethod.PUT, consumes = MimeTypeUtils.APPLICATION_JSON_VALUE, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public MessageDto updateLastLogin(@ApiParam(name = "employee", value = "The Employee to update") @Valid @RequestBody EmployeeDto employee) throws ServiceException {

        LOGGER.info("update last login date called");

        MessageDto msg = new MessageDto();

        employeeService.updateLastLogin(employee);

        msg.setType(MessageType.SUCCESS);
        return msg;
    }

    public void setEmployeeService(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }
}
