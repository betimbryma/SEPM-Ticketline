package at.ac.tuwien.inso.ticketline.server.util.DtoToEntity;

import at.ac.tuwien.inso.ticketline.dao.EmployeeDao;
import at.ac.tuwien.inso.ticketline.dto.ReservationDto;
import at.ac.tuwien.inso.ticketline.model.Reservation;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by Betim on 5/16/2016.
 */
public class ReservationDtoToEntity {

    @Autowired
    EmployeeDao employeeDao;

    private static ReservationDtoToEntity instance = null;
    private ReservationDtoToEntity() {}
    public static ReservationDtoToEntity getInstance() {
        if(instance == null) {
            instance = new ReservationDtoToEntity();
        }
        return instance;
    }

    public Reservation convert(ReservationDto reservationDto){
        Reservation reservation = new Reservation();
        reservation.setId(reservationDto.getId());
        reservation.setReservationNumber(reservationDto.getReservationNumber());
        reservation.setCustomer(CustomerDtoToEntity.getInstance().convert(reservationDto.getCustomer()));
        reservation.setEmployee(EmployeeDtoToEntity.getInstance().convert(reservationDto.getEmployee()));
        return reservation;
    }

    public static Reservation convertReservationDto(Reservation reservationDto) {
        Reservation reservation = new Reservation();
        reservation.setId(1); // muss unbedingt ein ID haben !!!
        return reservation;
    }
}
