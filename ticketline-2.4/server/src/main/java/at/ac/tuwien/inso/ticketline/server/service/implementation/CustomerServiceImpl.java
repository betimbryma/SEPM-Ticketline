package at.ac.tuwien.inso.ticketline.server.service.implementation;

import at.ac.tuwien.inso.ticketline.dao.CustomerDao;
import at.ac.tuwien.inso.ticketline.model.Customer;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.server.service.CustomerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerServiceImpl implements CustomerService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomerServiceImpl.class);

    @Autowired
    private CustomerDao customerDao;

    /**
     * {@inheritDoc}
     */
    @Override
    public Customer save(Customer customer) throws ServiceException {

        if (customer == null) {
            LOGGER.error("Tried to save null");
            throw new ServiceException("Customer must not be null");
        }

        try {
            Customer saved = customerDao.save(customer);
            LOGGER.debug("ID of new Customer: " + saved.getId());
            return saved;
        } catch (NullPointerException | DataIntegrityViolationException e) {
//            e.printStackTrace();
            throw new ServiceException(e);
        }
    }

    @Override
    public Customer getByID(int id) throws ServiceException {

        if (id <= 0) {
            throw new ServiceException("ID must be bigger than 0");
        }

        try {
            return customerDao.findOne(id);
        } catch (NullPointerException | DataIntegrityViolationException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Customer> getAll() throws ServiceException {
        LOGGER.info("getAll() called");
        List<Customer> list = customerDao.findAll();

        for (Customer c : list) {
            if(c.getId() == 1 && c.getFirstname().equals("Anonymus") && c.getLastname().equals("Anonymus")) {
                list.remove(c);
                break;
            } else {
                System.err.println(c);
            }
        }

        return list;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Customer edit(Customer customer) throws ServiceException {

        if (customer == null) {
            LOGGER.error("Null argument reference");
            throw new ServiceException("Null argument reference");
        }

        try {
            Customer customerI = customerDao.findOne(customer.getId());
            System.out.println(customerI == null);
            System.out.println(customer.getId().toString());
            if(customerI!=null && customer.getPremiumpoints()!=null && customerI.getPremiumpoints()!=null && customer.getPremiumpoints()<=0) {
                if (customerI.getPremiumpoints() + customer.getPremiumpoints() < 0) {
                    customer.setPremiumpoints(customerI.getPremiumpoints());
                    throw new ServiceException("Not Enough Points to get an Premium or to return a ticket");
                }else
                    customer.setPremiumpoints(customerI.getPremiumpoints() + customer.getPremiumpoints());
            }

            return customerDao.save(customer);
        } catch (NullPointerException | DataIntegrityViolationException ex) {
            throw new ServiceException(ex);
        }
    }

    @Override
    public Customer getByID(Integer ID) throws ServiceException {
        try{
            return customerDao.findOne(ID);
        } catch (NullPointerException | DataIntegrityViolationException ex) {
            throw new ServiceException(ex);
        }
    }

    // -------------------- For Testing purposes --------------------

    /**
     * Sets the customer dao.
     *
     * @param dao the new customer dao
     */
    public void setCustomerDao(CustomerDao dao) {
        this.customerDao = dao;
    }

    // -------------------- For Testing purposes --------------------
}