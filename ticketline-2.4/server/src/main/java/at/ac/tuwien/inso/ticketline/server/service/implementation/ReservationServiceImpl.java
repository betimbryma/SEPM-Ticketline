package at.ac.tuwien.inso.ticketline.server.service.implementation;

import at.ac.tuwien.inso.ticketline.dao.ReservationDao;
import at.ac.tuwien.inso.ticketline.dto.ReservationDto;
import at.ac.tuwien.inso.ticketline.dto.TicketIdentifierDto;
import at.ac.tuwien.inso.ticketline.model.Customer;
import at.ac.tuwien.inso.ticketline.model.Reservation;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.server.service.ReservationService;
import at.ac.tuwien.inso.ticketline.server.service.TicketService;
import at.ac.tuwien.inso.ticketline.server.util.DtoToEntity.ReservationDtoToEntity;
import at.ac.tuwien.inso.ticketline.server.util.DtoToEntity.TicketIdentifierDtoToEntity;
import at.ac.tuwien.inso.ticketline.server.util.EntityToDto.ReservationEntityToDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Betim on 5/16/2016.
 */
@Service
public class ReservationServiceImpl implements ReservationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReservationServiceImpl.class);

    @Autowired
    ReservationDao reservationDao;

    @Autowired
    private TicketService ticketService;

    @Override
    public Reservation save(Reservation reservation) throws ServiceException {
        try {
            return reservationDao.save(reservation);
        } catch (NullPointerException | DataIntegrityViolationException e) {
            throw new ServiceException(e);
        }
    }


    @Override
    public ReservationDto saveeReservation(ReservationDto reservationDto) throws ServiceException {

        ReservationDto reservationDto1 = ReservationEntityToDto.getInstance().convert(this.save(ReservationDtoToEntity.getInstance().convert(reservationDto)));
        Reservation reservation = new Reservation();
        return ReservationEntityToDto.getInstance().convert(this.save(ReservationDtoToEntity.getInstance().convert(reservationDto1)));

    }

    @Override
    public List<Reservation> getAll() throws ServiceException {
        LOGGER.info("getAll() called");
        List<Reservation> list = reservationDao.findAll();
        return list;

    }

    @Override
    public List<Reservation> getForCustomer(Customer customer) throws ServiceException {
        LOGGER.info("getForCustomer() called with ID:" + customer.getId());

        try{
            return reservationDao.findReservationsForCustomer(customer);
        } catch (NullPointerException | DataIntegrityViolationException e){
            throw new ServiceException(e);
        }
    }

    @Override
    public Integer cancelTickets(List<TicketIdentifierDto> list) throws ServiceException {

        for(TicketIdentifierDto ticketIdentifierDto : list){
            ticketIdentifierDto.setValid(false);
            ticketService.cancelThisTicketIdentifier(TicketIdentifierDtoToEntity.getInstance().convert(ticketIdentifierDto));
        }

        return 0;
    }
}