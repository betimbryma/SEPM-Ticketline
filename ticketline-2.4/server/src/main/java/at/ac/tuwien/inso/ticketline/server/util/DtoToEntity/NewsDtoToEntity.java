package at.ac.tuwien.inso.ticketline.server.util.DtoToEntity;

import at.ac.tuwien.inso.ticketline.dto.NewsDto;
import at.ac.tuwien.inso.ticketline.model.News;

public class NewsDtoToEntity {

    private static NewsDtoToEntity instance = null;
    private NewsDtoToEntity() {}
    public static NewsDtoToEntity getInstance() {
        if(instance == null) {
            instance = new NewsDtoToEntity();
        }
        return instance;
    }

    /**
     * Converts a news DTO to a news entity.
     *
     * @param newsDto the news DTO
     * @return the news
     */
    public static News convert(NewsDto newsDto) {
        News news = new News();
        news.setTitle(newsDto.getTitle());
        news.setNewsText(newsDto.getNewsText());
        return news;
    }
}
