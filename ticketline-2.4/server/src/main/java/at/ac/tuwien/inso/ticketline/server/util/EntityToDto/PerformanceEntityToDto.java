package at.ac.tuwien.inso.ticketline.server.util.EntityToDto;

import at.ac.tuwien.inso.ticketline.dto.PerformanceDto;
import at.ac.tuwien.inso.ticketline.model.Performance;

import java.util.ArrayList;
import java.util.List;

public class PerformanceEntityToDto {

    private static PerformanceEntityToDto instance = null;
    private PerformanceEntityToDto() {}
    public static PerformanceEntityToDto getInstance() {
        if(instance == null) {
            instance = new PerformanceEntityToDto();
        }
        return instance;
    }

    /**
     * Converts a performance entity to a performance DTO
     *
     * @param performance the performance entity
     * @return the converted performance DTO
     */
    public static PerformanceDto convertPerformance(Performance performance) {
        PerformanceDto performanceDto = new PerformanceDto();
        performanceDto.setName(performance.getName());
        performanceDto.setDescription(performance.getDescription());
        performanceDto.setId(performance.getId());
        if(performance.getDuration() != null)
        performanceDto.setDuration(performance.getDuration());
        else
        performanceDto.setDuration(0);
        if(performance.getPerformanceType() != null){
            performanceDto.setPerformancetype(performance.getPerformanceType().toString());
        }else{
            performanceDto.setPerformancetype("No type defined.");
        }
        //  performanceDto.setArticles(MerchandiseEntityToDto.getInstance().convertArticles(performance.getArticles()));
        return performanceDto;
    }

    /**
     * Converts an performance entity list to an performance DTO list
     *
     * @param performances the list of performance entities
     * @return the converted performance DTO list
     */
    public static List<PerformanceDto> convertPerformances(List<Performance> performances) {
        List<PerformanceDto> result = new ArrayList<>();

        if (performances != null) {
            for (Performance p : performances) {
                result.add(convertPerformance(p));
            }
        }
        return result;
    }
}
