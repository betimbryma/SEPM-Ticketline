package at.ac.tuwien.inso.ticketline.server.util.DtoToEntity;

import at.ac.tuwien.inso.ticketline.dto.ReceiptDto;
import at.ac.tuwien.inso.ticketline.model.Customer;
import at.ac.tuwien.inso.ticketline.model.Receipt;
import at.ac.tuwien.inso.ticketline.model.TransactionState;

public class ReceiptDtotoEntity {

    public static Receipt convertReceiptDto(ReceiptDto receiptDto) {
        Receipt receipt = new Receipt();
        if (receiptDto.getReceiptentryDto() != null) {

        }
        receipt.setId(receiptDto.getId());
        receipt.setTransactionDate(receiptDto.getTransactionDate());

        /*Code too be changed*/
        Customer customer = new Customer();
        customer.setId(receiptDto.getCustomer().getId());

        receipt.setCustomer(customer);
        receipt.setTransactionState(TransactionState.PAID);
        if (receiptDto.getBankAccountDto()!=null) {
            receipt.setMethodOfPayment(MethodOfPaymentDtoToEntity.convertBankAccountDto(receiptDto.getBankAccountDto()));
        }else if(receiptDto.getCreditCardDto()!=null) {
            receipt.setMethodOfPayment(MethodOfPaymentDtoToEntity.convertCreditCardDto(receiptDto.getCreditCardDto()));
        }else if(receiptDto.getCashDto()!=null) {
            receipt.setMethodOfPayment(MethodOfPaymentDtoToEntity.convertCashDto(receiptDto.getCashDto()));
        }


        return receipt;

    }
}
