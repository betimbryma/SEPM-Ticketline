package at.ac.tuwien.inso.ticketline.server.service;

import at.ac.tuwien.inso.ticketline.dto.PremiumDto;
import at.ac.tuwien.inso.ticketline.model.Premium;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;

import java.util.List;

/**
 * Created by eni on 04.06.2016.
 */
public interface PremiumService {

    /**
     * getAll premium
     * @return
     * @throws ServiceException
     */
    List<PremiumDto> getAll() throws ServiceException;


    /**
     * Search Premium Id
     * @param id
     * @return
     * @throws ServiceException
     */
    Premium searchPremiumID(int id) throws  ServiceException;


    /**
     * Update this Premium
     * @param premium
     * @return
     * @throws ServiceException
     */
    Premium updatePremium(Premium premium) throws  ServiceException;

    public PremiumDto checkPremiumInServer(PremiumDto merchandiseDtos) throws ServiceException;

    public PremiumDto returnPremium(PremiumDto merchandiseDtos) throws ServiceException;


    }