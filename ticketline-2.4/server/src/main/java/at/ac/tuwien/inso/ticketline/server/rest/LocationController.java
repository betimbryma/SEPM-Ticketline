package at.ac.tuwien.inso.ticketline.server.rest;

import at.ac.tuwien.inso.ticketline.dao.LocationDao;
import at.ac.tuwien.inso.ticketline.dto.LocationDto;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.server.service.LocationService;
import at.ac.tuwien.inso.ticketline.server.util.EntityToDto.LocationEntityToDto;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(value = "location", description = "Location REST service")
@RestController
@RequestMapping(value = "/location")
public class LocationController {

    private static final Logger LOGGER = LoggerFactory.getLogger(LocationController.class);

    @Autowired
    private LocationService locationService;

    /**
     * Get all the locations from the database
     * @return a list of the locations
     * @throws ServiceException if there is any exceptions in the ShowsService
     */
    @ApiOperation(value = "Gets all locations", response = LocationDao.class, responseContainer = "List")
    @RequestMapping(value = "/", method = RequestMethod.GET, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public List<LocationDto> getPerformances() throws ServiceException {

        LOGGER.info("getAll() called");

        return LocationEntityToDto.getInstance().convertLocations(locationService.getAll());
    }

    public void setLocationService(LocationService locationService) {
        this.locationService = locationService;
    }
}
