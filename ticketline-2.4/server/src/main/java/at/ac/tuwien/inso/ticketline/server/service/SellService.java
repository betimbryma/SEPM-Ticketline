package at.ac.tuwien.inso.ticketline.server.service;

import at.ac.tuwien.inso.ticketline.dto.MessageDto;
import at.ac.tuwien.inso.ticketline.dto.ReceiptDto;
import at.ac.tuwien.inso.ticketline.model.Customer;
import at.ac.tuwien.inso.ticketline.model.Receipt;
import at.ac.tuwien.inso.ticketline.model.ReceiptEntry;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;

import java.util.List;

public interface SellService {

    /**
     * get and save the receipt
     * @param receiptDto
     * @return
     * @throws ServiceException
     */
    MessageDto getReceipt(ReceiptDto receiptDto) throws ServiceException;
    /**
     * Receipt speichern
     * @param receipt
     * @return
     * @throws ServiceException
     */
    Receipt saveReceipt(Receipt receipt)throws ServiceException;

    /**
     * Receipt Entrys Speichern
     * @param receiptEntry
     * @return
     * @throws ServiceException
     */
    ReceiptEntry saveReceiptEntrys(ReceiptEntry receiptEntry) throws ServiceException;

    List<ReceiptEntry> getForReceipt(Receipt receipt) throws ServiceException;

    List<Receipt> getForCustomer(Customer customer) throws ServiceException;

}