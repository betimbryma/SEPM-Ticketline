package at.ac.tuwien.inso.ticketline.server.rest;

import at.ac.tuwien.inso.ticketline.dto.MessageDto;
import at.ac.tuwien.inso.ticketline.dto.MessageType;
import at.ac.tuwien.inso.ticketline.dto.NewsDto;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.server.service.NewsService;
import at.ac.tuwien.inso.ticketline.server.util.DtoToEntity.NewsDtoToEntity;
import at.ac.tuwien.inso.ticketline.server.util.EntityToDto.NewsEntityToDto;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;

@Api(value = "news", description = "News REST service")
@RestController
@RequestMapping(value = "/news")
public class NewsController {

    private static final Logger LOGGER = LoggerFactory.getLogger(NewsController.class);

    @Autowired
    private NewsService newsService;

    /**
     * Gets the news by id.
     *
     * @param id the id
     * @return the news by id
     * @throws ServiceException the service exception
     */
    @ApiOperation(value = "Gets the news by id", response = NewsDto.class)
    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public NewsDto getNewsById(@ApiParam(name = "id", value = "ID of the news") @PathVariable("id") Integer id) throws ServiceException {
        LOGGER.info("getNewsById() called");
        return NewsEntityToDto.getInstance().convert(newsService.getNews(id));
    }

    /**
     * Gets the all news items.
     *
     * @return list of all news items
     * @throws ServiceException the service exception
     */
    @ApiOperation(value = "Gets all news", response = NewsDto.class, responseContainer = "List")
    @RequestMapping(value = "/", method = RequestMethod.GET, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public List<NewsDto> getAll() throws ServiceException {
        LOGGER.info("getAll() called");
        return NewsEntityToDto.getInstance().convertNews(newsService.getAllNews());
    }

    /**
     * Publishes a new news.
     *
     * @param news the news
     * @return the message dto
     * @throws ServiceException the service exception
     */
    @ApiOperation(value = "Pubilishes a new news", response = NewsDto.class)
    @RequestMapping(value = "/publish", method = RequestMethod.POST, consumes = MimeTypeUtils.APPLICATION_JSON_VALUE, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public MessageDto publishNews(@ApiParam(name = "news", value = "News to publish") @Valid @RequestBody NewsDto news) throws ServiceException {
        LOGGER.info("publishNews() called");
        Integer id = this.newsService.save(NewsDtoToEntity.getInstance().convert(news)).getId();
        MessageDto msg = new MessageDto();
        msg.setType(MessageType.SUCCESS);
        msg.setText(id.toString());
        return msg;
    }

    @ApiOperation(value = "Gets the news by date", response = NewsDto.class, responseContainer = "List")
    @RequestMapping(value = "/getBefore", method = RequestMethod.POST, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public List<NewsDto> getBeforeDate(@ApiParam(name="date",value="Date to filter") @RequestBody Date date) throws ServiceException {
        LOGGER.info("getBeforeDate() is called");
        return NewsEntityToDto.convertNews(newsService.getBefore(date));
    }

}
