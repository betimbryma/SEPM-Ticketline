package at.ac.tuwien.inso.ticketline.server.util.DtoToEntity;

import at.ac.tuwien.inso.ticketline.dto.GalleryDto;
import at.ac.tuwien.inso.ticketline.model.Gallery;

public class GalleryDtoToEntity {

    private static GalleryDtoToEntity instance = null;
    private GalleryDtoToEntity() {}
    public static GalleryDtoToEntity getInstance() {
        if(instance == null) {
            instance = new GalleryDtoToEntity();
        }
        return instance;
    }

    public Gallery convert(GalleryDto galleryDto){
        Gallery gallery = new Gallery();
        gallery.setId(galleryDto.getId());
        gallery.setDescription(galleryDto.getDescription());
        gallery.setName(galleryDto.getName());
        gallery.setOrder(galleryDto.getOrder());
        return gallery;
    }
}
