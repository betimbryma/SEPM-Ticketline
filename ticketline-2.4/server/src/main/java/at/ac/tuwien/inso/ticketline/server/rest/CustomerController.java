package at.ac.tuwien.inso.ticketline.server.rest;

import at.ac.tuwien.inso.ticketline.dto.CustomerDto;
import at.ac.tuwien.inso.ticketline.dto.MessageDto;
import at.ac.tuwien.inso.ticketline.dto.MessageType;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.server.service.CustomerService;
import at.ac.tuwien.inso.ticketline.server.util.DtoToEntity.CustomerDtoToEntity;
import at.ac.tuwien.inso.ticketline.server.util.EntityToDto.CustomerEntityToDto;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import javafx.fxml.Initializable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

@Api(value = "customer", description = "Customer REST service")
@RestController
@RequestMapping(value = "/customer")
public class CustomerController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomerController.class);

    @Autowired
    private CustomerService customerService;

    /**
     * Creates a new customer.
     *
     * @param customer the customer
     * @return the message dto
     * @throws ServiceException the service exception
     */
    @ApiOperation(value = "Creates a new customer", response = CustomerDto.class)
    @RequestMapping(value = "/", method = RequestMethod.POST, consumes = MimeTypeUtils.APPLICATION_JSON_VALUE, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public MessageDto createCustomer(@ApiParam(name = "customer", value = "Customer to create") @Valid @RequestBody CustomerDto customer) throws ServiceException {

        LOGGER.info("createCustomer() called");

        MessageDto msg = new MessageDto();

        Integer id = this.customerService.save(CustomerDtoToEntity.getInstance().convert(customer)).getId();

        msg.setType(MessageType.SUCCESS);
        msg.setText(id.toString());

        return msg;
    }

    /**
     * Get all the customers from the database
     *
     * @return a list of the customers
     * @throws ServiceException if there is any exceptions in the CustomersService
     */
    @ApiOperation(value = "Gets all customers", response = CustomerDto.class, responseContainer = "List")
    @RequestMapping(value = "/", method = RequestMethod.GET, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public List<CustomerDto> getCustomers() throws ServiceException {

        LOGGER.info("getAll() called");

        return CustomerEntityToDto.getInstance().convertCustomers(customerService.getAll());
    }


    /**
     * Edit a customer from the database
     *
     * @param customerDto that is going to be edited
     * @return the messageDTO
     */
    @ApiOperation(value = "Edit a customer", response = CustomerDto.class)
    @RequestMapping(value = "/{id}", method = RequestMethod.POST, consumes = MimeTypeUtils.APPLICATION_JSON_VALUE, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public MessageDto editCustomer(@ApiParam(name = "customer", value = "customer to edit") @Valid @RequestBody CustomerDto customerDto) throws ServiceException {

        Integer ID = this.customerService.edit(CustomerDtoToEntity.getInstance().convert(customerDto)).getPremiumpoints();

        MessageDto messageDto = new MessageDto();
        messageDto.setType(MessageType.SUCCESS);
        messageDto.setText(ID==null?new Integer(0).toString():ID.toString());

        return messageDto;

    }

    @ApiOperation(value = "Gets the customer with ID", response = CustomerDto.class)
    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public CustomerDto getCustomerWithID(@ApiParam(name="id",value="ID of customer") @PathVariable("id") @RequestBody Integer id) throws ServiceException {
        LOGGER.info("getCustomerWithID() is called");

        return CustomerEntityToDto.getInstance().convert(customerService.getByID(id));
    }


    //--------------------------- for testing purposes ------------------------
    public void setCustomerService(CustomerService c) {
        this.customerService = c;
    }
}
