package at.ac.tuwien.inso.ticketline.server.service.implementation;

import at.ac.tuwien.inso.ticketline.dao.ParticipationDao;
import at.ac.tuwien.inso.ticketline.model.Participation;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.server.service.ParticipationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ParticipationServiceImpl implements ParticipationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ParticipationServiceImpl.class);

    @Autowired
    private ParticipationDao participationDao;

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Participation> getAll() throws ServiceException {
        LOGGER.info("getAll() called");
        List<Participation> list = participationDao.findAll();
        return list;
    }

    // -------------------- For Testing purposes --------------------

    /**
     * Sets the customer dao.
     *
     * @param dao the new customer dao
     */
    public void setParticipationDao(ParticipationDao dao) {
        this.participationDao = dao;
    }

    // -------------------- For Testing purposes --------------------

}