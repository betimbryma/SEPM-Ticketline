package at.ac.tuwien.inso.ticketline.server.rest;

import at.ac.tuwien.inso.ticketline.dto.*;
import at.ac.tuwien.inso.ticketline.model.Ticket;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.server.service.ShowService;
import at.ac.tuwien.inso.ticketline.server.service.TicketService;
import at.ac.tuwien.inso.ticketline.server.util.DtoToEntity.TicketDtoToEntity;
import at.ac.tuwien.inso.ticketline.server.util.DtoToEntity.TicketIdentifierDtoToEntity;
import at.ac.tuwien.inso.ticketline.server.util.EntityToDto.TicketEntityToDto;
import at.ac.tuwien.inso.ticketline.server.util.EntityToDto.TicketIdentifierEntityToDto;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Api(value = "ticket", description = "Ticket REST service")
@RestController
@RequestMapping(value = "/tickets")
public class TicketController {

    private static final Logger LOGGER = LoggerFactory.getLogger(TicketController.class);

    @Autowired
    private TicketService ticketService;

    @ApiOperation(value = "Gets the tickets of a certain show", response = MapEntryDto.class, responseContainer = "List")
    @RequestMapping(value = "/getByShow", method = RequestMethod.POST, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public List<MapEntryDto<TicketDto, TicketIdentifierDto>> getTicketsForShow(@ApiParam(name = "show", value = "Tickets for this show") @RequestBody Integer show) throws ServiceException {
        LOGGER.info("getTicketsForShow() is called");

        return ticketService.getTicketsForShow(show);
    }



    @ApiOperation(value = "Get number of sold tickets for a certain show", response = MapEntryDto.class, responseContainer = "Integer")
    @RequestMapping(value = "/getSoldByShow", method = RequestMethod.POST, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public Integer getSoldTicketsForShow(@ApiParam(name = "show", value = "Sold tickets for this show") @RequestBody Integer show) throws ServiceException {
        LOGGER.info("getSoldTicketsForShow() is called");
        return ticketService.getSoldTicketAmount(show);
    }

    @ApiOperation(value = "Get minimum amount of ticket price", response = MapEntryDto.class, responseContainer = "List")
    @RequestMapping(value = "/getMinPrices", method = RequestMethod.POST, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public List<MapEntryDto<ShowDto, Integer>> getMinPrices() throws ServiceException {
        LOGGER.info("getMinPrices() is called");
        return ticketService.getMinPrices();
    }

    @ApiOperation(value = "Get maximum amount of ticket price", response = MapEntryDto.class, responseContainer = "List")
    @RequestMapping(value = "/getMaxPrices", method = RequestMethod.POST, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public List<MapEntryDto<ShowDto, Integer>> getMaxPrices() throws ServiceException {
        LOGGER.info("getMaxPrices() is called");
        return ticketService.getMaxPrices();
    }

    @ApiOperation(value = "Gets all TicketIdentifiers", response = TicketIdentifierDto.class, responseContainer = "List")
    @RequestMapping(value = "/getAll", method = RequestMethod.GET, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public List<TicketIdentifierDto> getAll() throws ServiceException {
        LOGGER.info("getAll() called");
        return TicketIdentifierEntityToDto.getInstance().convertTicketIdentifiers(ticketService.getAllTicketIdentifier());
    }

    @ApiOperation(value = "Gets all Tickets", response = TicketDto.class, responseContainer = "List")
    @RequestMapping(value = "/getAllTickets", method = RequestMethod.POST, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public List<TicketDto> getAllTickets(@ApiParam(name = "show", value = "Tickets for this show") @RequestBody Integer show) throws ServiceException {
        LOGGER.info("getAll() called");
        return TicketEntityToDto.getInstance().convertTickets(ticketService.getTicketsByShow(show));
    }

    @ApiOperation(value = "Makes a reservation", response = TicketIdentifierDto.class, responseContainer = "List")
    @RequestMapping(value = "/reserve", method = RequestMethod.POST, consumes = MimeTypeUtils.APPLICATION_JSON_VALUE, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public List<TicketIdentifierDto> reserveTickets(@ApiParam(name = "tickets", value = "Tickets to reserve") @RequestBody List<TicketIdentifierDto> ticketDtoList) throws ServiceException {
        LOGGER.info("reserveTickets() called");

        return ticketService.reserveTickets(ticketDtoList);
    }

    /**
     * Change the reservation from the database
     *
     * @param ticketIdentifierDto that is going to be edited
     * @return the messageDTO
     */
    @ApiOperation(value = "Change the Reservation", response = TicketIdentifierDto.class)
    @RequestMapping(value = "/changereserve", method = RequestMethod.POST, consumes = MimeTypeUtils.APPLICATION_JSON_VALUE, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public MessageDto changeReservation(@ApiParam(name = "reservation", value = "reservation to change") @Valid @RequestBody TicketIdentifierDto ticketIdentifierDto) throws ServiceException {

        MessageDto messageError = new MessageDto();
        messageError.setType(MessageType.ERROR);


        Ticket ticket = TicketDtoToEntity.convert(ticketIdentifierDto.getTicket());
        ticketService.updateTicketForCartAsNotUsed(ticket);


        this.ticketService.cancelThisTicketIdentifier(TicketIdentifierDtoToEntity.getInstance().convert(ticketIdentifierDto));
        Integer ID = new Integer(1);
        MessageDto messageDto = new MessageDto();
        messageDto.setType(MessageType.SUCCESS);
        messageDto.setText(ID.toString());

        return messageDto;

    }


}
