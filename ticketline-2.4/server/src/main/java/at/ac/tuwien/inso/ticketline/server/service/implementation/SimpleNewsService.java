package at.ac.tuwien.inso.ticketline.server.service.implementation;

import at.ac.tuwien.inso.ticketline.dao.NewsDao;
import at.ac.tuwien.inso.ticketline.model.News;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.server.service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import javax.validation.constraints.Null;
import java.util.Date;
import java.util.List;

@Service
public class SimpleNewsService implements NewsService {

    @Autowired
    private NewsDao newsDao;

    /**
     * {@inheritDoc}
     */
    @Override
    public News getNews(Integer id) throws ServiceException {

        if (id < 1) {
            throw new ServiceException("Invalid ID");
        }

        try {
            return newsDao.findOne(id);
        } catch (Exception e) {
            throw new ServiceException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public News save(News news) throws ServiceException {
        try {
            news.setSubmittedOn(new Date());
            return newsDao.save(news);
        } catch (NullPointerException | DataIntegrityViolationException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<News> getAllNews() throws ServiceException {
        try {
            return newsDao.findAllOrderBySubmittedOnAsc();
        } catch (RuntimeException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<News> getBefore(Date date) throws ServiceException {

        try{
            return newsDao.findBeforeOrderBySubmittedOn(date);
        } catch (NullPointerException | DataIntegrityViolationException e){
            throw new ServiceException(e);
        }
    }

    // -------------------- For Testing purposes --------------------

    /**
     * Sets the news dao.
     *
     * @param dao the new news dao
     */
    public void setNewsDao(NewsDao dao) {
        this.newsDao = dao;
    }

    // -------------------- For Testing purposes --------------------
}