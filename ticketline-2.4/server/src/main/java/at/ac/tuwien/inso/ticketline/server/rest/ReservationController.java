package at.ac.tuwien.inso.ticketline.server.rest;

import at.ac.tuwien.inso.ticketline.dto.ReservationDto;
import at.ac.tuwien.inso.ticketline.dto.TicketIdentifierDto;
import at.ac.tuwien.inso.ticketline.model.Reservation;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.server.service.ReservationService;
import at.ac.tuwien.inso.ticketline.server.service.TicketService;
import at.ac.tuwien.inso.ticketline.server.util.DtoToEntity.ReservationDtoToEntity;
import at.ac.tuwien.inso.ticketline.server.util.DtoToEntity.TicketIdentifierDtoToEntity;
import at.ac.tuwien.inso.ticketline.server.util.EntityToDto.ReservationEntityToDto;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by Betim on 5/16/2016.
 */
@Api(value = "reservation", description = "Reservation REST service")
@RestController
@RequestMapping(value = "/reservation")
public class ReservationController {

    @Autowired
    private ReservationService reservationService;

    private static final Logger LOGGER = LoggerFactory.getLogger(ReservationController.class);

    @ApiOperation(value = "Makes a reservation", response = ReservationDto.class)
    @RequestMapping(value = "/", method = RequestMethod.POST, consumes = MimeTypeUtils.APPLICATION_JSON_VALUE, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public ReservationDto saveeReservation(@ApiParam(name = "reservation", value = "Reservation to save") @Valid @RequestBody ReservationDto reservationDto) throws ServiceException {
        LOGGER.info("saveReservation() called");

        return reservationService.saveeReservation(reservationDto);

        }

    @ApiOperation(value = "Gets all reservations", response = ReservationDto.class, responseContainer = "List")
    @RequestMapping(value = "/", method = RequestMethod.GET, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public List<ReservationDto> getReservation() throws ServiceException {

        LOGGER.info("getAll() called");

        return ReservationEntityToDto.getInstance().convertReservations(reservationService.getAll());
    }

    @ApiOperation(value = "Cancel the tickets of a certain show", responseContainer = "Integer")
    @RequestMapping(value = "/cancel", method = RequestMethod.POST, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public Integer cancelTickets(@ApiParam(name="ticketIdentifiers",value="TicketIdentifiers taht are going to be set invalid for this show") @RequestBody List<TicketIdentifierDto> list) throws ServiceException {
        LOGGER.info("cancelTickets() is called");

        return reservationService.cancelTickets(list);
    }
}
