package at.ac.tuwien.inso.ticketline.server.service.implementation;

import at.ac.tuwien.inso.ticketline.dao.MerchandiseDao;
import at.ac.tuwien.inso.ticketline.dto.MerchandiseDto;
import at.ac.tuwien.inso.ticketline.dto.PerformanceDto;
import at.ac.tuwien.inso.ticketline.model.Article;
import at.ac.tuwien.inso.ticketline.model.Performance;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.server.service.MerchandiseService;
import at.ac.tuwien.inso.ticketline.server.util.DtoToEntity.MerchandiseDtoToEntity;
import at.ac.tuwien.inso.ticketline.server.util.DtoToEntity.PerformanceDtoToEntity;
import at.ac.tuwien.inso.ticketline.server.util.EntityToDto.MerchandiseEntityToDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MerchandiseServiceImpl implements MerchandiseService {


    private static final Logger LOGGER = LoggerFactory.getLogger(MerchandiseServiceImpl.class);


    @Autowired
    private MerchandiseDao merchandiseDao;

    public PerformanceDto getArticlesOfThisPerformance(PerformanceDto performanceDto) throws ServiceException{
        if(performanceDto==null){
            return null;
        }
        Performance performance = PerformanceDtoToEntity.convertPerformance(performanceDto);
        List<Article> articles = merchandiseDao.getByPerformance(performance);
        List<MerchandiseDto> merchandiseDtos = new ArrayList<>();

        for(Article a : articles){
            merchandiseDtos.add(MerchandiseEntityToDto.getInstance().convertArticle(a));
        }
        performanceDto.setArticles(merchandiseDtos);
        return performanceDto;
    }

    @Override
    public Article searchArticleID(int id) throws ServiceException {
        try {
            return merchandiseDao.findOne(id);
        } catch (NullPointerException | DataIntegrityViolationException e) {
            throw new ServiceException(e);
        }
    }


    @Override
    public int increaseArticle(Article article) throws ServiceException {
        return merchandiseDao.increaseArticle(article);
    }

    @Override
    public int decreaseArticle2(Article article) throws ServiceException {
        return merchandiseDao.decreaseArticle(article);
    }
    @Override
    public Article updateArticle(Article article) throws ServiceException {
        return merchandiseDao.save(article);
    }

    @Override
    public List<Article> getByPerformance(Performance performance){
        return merchandiseDao.getByPerformance(performance);
    }

    public void addDao(MerchandiseDao dao){
        this.merchandiseDao=dao;
    }

    @Override
    public void resetMerchandises(List<MerchandiseDto> merchandiseDtos) throws ServiceException{

        for(int i=0; i<merchandiseDtos.size();i++) {

            Article m = MerchandiseDtoToEntity.convertArticleDtoWithId(merchandiseDtos.get(i));
            m=searchArticleID(m.getId());
            m.setAvailable(m.getAvailable()+1);
            updateArticle(m);
        }
    }
}