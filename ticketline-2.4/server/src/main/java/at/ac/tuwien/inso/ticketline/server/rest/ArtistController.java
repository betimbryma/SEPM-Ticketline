package at.ac.tuwien.inso.ticketline.server.rest;

import at.ac.tuwien.inso.ticketline.dao.ArtistDao;
import at.ac.tuwien.inso.ticketline.dto.ArtistDto;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.server.service.ArtistService;
import at.ac.tuwien.inso.ticketline.server.util.EntityToDto.ArtistEntityToDto;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(value = "artist", description = "Arist REST service")
@RestController
@RequestMapping(value = "/artist")
public class ArtistController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ArtistController.class);
    @Autowired
    private ArtistService artistService;

    /**
     * Get all the artists from the database.
     *
     * @return a list of the artists
     * @throws ServiceException if there is any exceptions in the CustomersService
     */
    @ApiOperation(value = "Gets all artists", response = ArtistDao.class, responseContainer = "List")
    @RequestMapping(value = "/", method = RequestMethod.GET, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public List<ArtistDto> getArtists() throws ServiceException {
        LOGGER.info("getAll() called");
        return ArtistEntityToDto.getInstance().convertArtists(artistService.getAll());
    }

    public void setArtistService(ArtistService artistService) {
        this.artistService = artistService;
    }
}
