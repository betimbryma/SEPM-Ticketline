package at.ac.tuwien.inso.ticketline.server.service;

import at.ac.tuwien.inso.ticketline.model.Performance;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;

import java.util.List;

public interface PerformanceService {

    /**
     * Gets all the performances in the database
     *
     * @return a list of the performances
     * @throws ServiceException if there are any exception, they are going to be encapsulated in ServiceException
     */
    List<Performance> getAll() throws ServiceException;
}