package at.ac.tuwien.inso.ticketline.server.service.implementation;

import at.ac.tuwien.inso.ticketline.dao.*;
import at.ac.tuwien.inso.ticketline.dto.GalleryDto;
import at.ac.tuwien.inso.ticketline.dto.RoomDto;
import at.ac.tuwien.inso.ticketline.model.*;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.server.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

/**
 * Created by Betim on 6/4/2016.
 */
@Service
public class TestServiceImpl implements TestService {

    @Autowired
    private TicketDao ticketDao;
    @Autowired
    private TicketIdentifierDao ticketIdentifierDao;
    @Autowired
    private RowDao rowDao;
    @Autowired
    private LocationDao locationDao;
    @Autowired
    private SeatDao seatDao;
    @Autowired
    private ShowDao showDao;
    @Autowired
    private PerformanceDao performanceDao;
    @Autowired
    private GalleryDao galleryDao;
    @Autowired
    private CategoryDao categoryDao;
    @Autowired
    private RoomDao roomDao;
    @Autowired EmployeeDao employeeDao;
    @Autowired CashDao cashDao;

    @Override
    public Ticket ticketSave(Ticket ticket) throws ServiceException {
        try{
            return ticketDao.save(ticket);
        } catch (NullPointerException | DataIntegrityViolationException e){
            throw new ServiceException(e);
        }
    }

    @Override
    public TicketIdentifier ticketIdentifierSave(TicketIdentifier ticket) throws ServiceException {
        try{
            return ticketIdentifierDao.save(ticket);
        } catch (NullPointerException | DataIntegrityViolationException e){
            throw new ServiceException(e);
        }
    }

    @Override
    public Row rowSave(Row ticket) throws ServiceException {
        try{
            return rowDao.save(ticket);
        } catch (NullPointerException | DataIntegrityViolationException e){
            throw new ServiceException(e);
        }
    }

    @Override
    public Location locationSave(Location ticket) throws ServiceException {
        try{
            return locationDao.save(ticket);
        } catch (NullPointerException | DataIntegrityViolationException e){
            throw new ServiceException(e);
        }
    }

    @Override
    public Seat seatSave(Seat ticket) throws ServiceException {
        try{
            return seatDao.save(ticket);
        } catch (NullPointerException | DataIntegrityViolationException e){
            throw new ServiceException(e);
        }
    }

    @Override
    public Show showSave(Show ticket) throws ServiceException {
        try{
            return showDao.save(ticket);
        } catch (NullPointerException | DataIntegrityViolationException e){
            throw new ServiceException(e);
        }
    }

    @Override
    public Performance performanceSave(Performance ticket) throws ServiceException {
        try{
            return performanceDao.save(ticket);
        } catch (NullPointerException | DataIntegrityViolationException e){
            throw new ServiceException(e);
        }
    }

    @Override
    public Gallery gallerySave(Gallery ticket) throws ServiceException {
        try{
            return galleryDao.save(ticket);
        } catch (NullPointerException | DataIntegrityViolationException e){
            throw new ServiceException(e);
        }
    }

    @Override
    public Category categorySave(Category ticket) throws ServiceException {
        try{

            return categoryDao.save(ticket);
        } catch (NullPointerException | DataIntegrityViolationException e){
            throw new ServiceException(e);
        }
    }

    @Override
    public Room roomSave(Room ticket) throws ServiceException {
        try{

            return roomDao.save(ticket);
        } catch (NullPointerException | DataIntegrityViolationException e){
            throw new ServiceException(e);
        }
    }
    @Override
    public Employee saveEmployee(Employee employee) throws ServiceException {
        try{

            return employeeDao.save(employee);
        } catch (NullPointerException | DataIntegrityViolationException e){
            throw new ServiceException(e);
        }
    }
    @Override
    public MethodOfPayment saveMethodOfPayment(Cash methodOfPayment) throws ServiceException {
        try{

            return cashDao.save(methodOfPayment);
        } catch (NullPointerException | DataIntegrityViolationException e){
            throw new ServiceException(e);
        }
    }


}