package at.ac.tuwien.inso.ticketline.server.util.DtoToEntity;

import at.ac.tuwien.inso.ticketline.dto.CustomerDto;
import at.ac.tuwien.inso.ticketline.model.Address;
import at.ac.tuwien.inso.ticketline.model.Customer;
import at.ac.tuwien.inso.ticketline.model.Gender;

public class CustomerDtoToEntity {

    private static CustomerDtoToEntity instance = null;
    private CustomerDtoToEntity() {}
    public static CustomerDtoToEntity getInstance() {
        if(instance == null) {
            instance = new CustomerDtoToEntity();
        }
        return instance;
    }

    public static Customer convert(CustomerDto customerDto) {

        Customer customer = new Customer();

        customer.setId(customerDto.getId());

        customer.setFirstname(customerDto.getFirstname());
        customer.setLastname(customerDto.getLastname());

        if (customerDto.getGender().equals("male")) {
            customer.setGender(Gender.MALE);
        } else if (customerDto.getGender().equals("female")) {
            customer.setGender(Gender.FEMALE);
        }

        customer.setPremiumpoints(customerDto.getPremiumPoints());
        customer.setDateOfBirth(customerDto.getDateOfBirth());

        Address address = new Address();
        address.setStreet(customerDto.getStreet());
        address.setPostalCode(customerDto.getPostalcode());
        address.setCity(customerDto.getCity());
        address.setCountry(customerDto.getCountry());
        customer.setAddress(address);

        customer.setEmail(customerDto.getEmail());
        customer.setPhoneNumber(customerDto.getPhoneNumber());

        return customer;
    }

    /**
     * convert Customer Dto to Customer
     *
     * @param customerDto
     * @return
     */
    public static Customer convertCustomerDto(CustomerDto customerDto) {
        Customer customer = new Customer();
        customer.setId(1); // muss unbedingt ein ID haben !!!
        return customer;
    }
}
