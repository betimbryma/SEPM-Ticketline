package at.ac.tuwien.inso.ticketline.server.util.EntityToDto;

public class ReceiptEntryEntityToDto {

    private static ReceiptEntryEntityToDto instance = null;
    private ReceiptEntryEntityToDto() {}
    public static ReceiptEntryEntityToDto getInstance() {
        if(instance == null) {
            instance = new ReceiptEntryEntityToDto();
        }
        return instance;
    }


}
