package at.ac.tuwien.inso.ticketline.server.service.implementation;

import at.ac.tuwien.inso.ticketline.dao.*;
import at.ac.tuwien.inso.ticketline.dto.*;
import at.ac.tuwien.inso.ticketline.model.*;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.server.service.*;
import at.ac.tuwien.inso.ticketline.server.util.DtoToEntity.MethodOfPaymentDtoToEntity;
import at.ac.tuwien.inso.ticketline.server.util.DtoToEntity.ReceiptDtotoEntity;
import at.ac.tuwien.inso.ticketline.server.util.DtoToEntity.ReceiptEntryDtoToEntity;
import at.ac.tuwien.inso.ticketline.server.util.DtoToEntity.TicketIdentifierDtoToEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SellServiceImpl implements SellService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SellServiceImpl.class);
    @Autowired
    public ReceiptDao receiptDao;
    @Autowired
    public ReceiptEntryDao receiptEntryDao;
    @Autowired
    public EmployeeDao employeeDao;
    @Autowired
    public CustomerDao customerDao;
    @Autowired
    public MerchandiseDao merchandiseDao;

    @Autowired
    private MerchandiseService merchandiseService;
    @Autowired
    private CustomerService customerService;
    @Autowired
    private EmployeeService employeeService;
    @Autowired
    private MethodOfPaymentService methodOfPaymentService;
    @Autowired
    private TicketService ticketService;

    public MessageDto getReceipt(ReceiptDto receiptDto) throws ServiceException{
        MessageDto msg = new MessageDto();
        int total = 0;

        Receipt receiptold = ReceiptDtotoEntity.convertReceiptDto(receiptDto);
        //Client search
        if (receiptold.getCustomer() == null) {
            msg.setType(MessageType.ERROR);
            return msg;
        }
        if(checkIfTicketAreSold(receiptDto)){
            throw new ServiceException("One of the ticket is sold! Please check the cart again");
        }

        //Employe search
        Employee employee;
        employee = employeeService.searchEmployeByUsername(receiptDto.getEmployee());
        receiptold.setEmployee(employee);

        // Saving the Method of Payment
        MethodOfPayment methodOfPayment = new Cash();
        if(receiptDto.getCashDto()!=null){
            methodOfPayment= methodOfPaymentService.saveCash(MethodOfPaymentDtoToEntity.convertCashDto(receiptDto.getCashDto()));
        }else if(receiptDto.getBankAccountDto()!=null){
            methodOfPayment=methodOfPaymentService.saveBankAccount(MethodOfPaymentDtoToEntity.convertBankAccountDto(receiptDto.getBankAccountDto()));
        }else if(receiptDto.getCreditCardDto()!=null){
            methodOfPayment= methodOfPaymentService.saveCreditcard(MethodOfPaymentDtoToEntity.convertCreditCardDto(receiptDto.getCreditCardDto()));
        }
        receiptold.setMethodOfPayment(methodOfPayment);
        /*Saving the receipt*/
        Receipt receiptnew = saveReceipt(receiptold);

        /*Updating every Article Avaiability and Saving every Receiptentrys*/
        List<ReceiptentryDto> receiptentryDtos = receiptDto.getReceiptentryDto();
        for (ReceiptentryDto r : receiptentryDtos) {
            LOGGER.debug("sSS" + r.getAmount());

            ReceiptEntry receiptEntry = ReceiptEntryDtoToEntity.convertReceiptEntryDto(r);


            if(r.getArticle()!=null){
                MerchandiseDto mercha = r.getArticle();

                Article article = merchandiseService.searchArticleID(mercha.getId());
                /*
                article.setAvailable(article.getAvailable()-r.getAmount());
                merchandiseService.updateArticle(article);
                */
                receiptEntry.setArticle(article);

            }else if(r.getTicketIdentifierDto()!=null){
                TicketIdentifier ti = TicketIdentifierDtoToEntity.convert(r.getTicketIdentifierDto());
                TicketDto ticketDto = r.getTicketIdentifierDto().getTicket();
                Ticket tick = ticketService.getTicketByID(ticketDto.getId());
                ticketService.updateTicketForCartAsSold(tick);
                if(ticketDto.getOldTE()!=null){

                    TicketIdentifierDto oldTEDto = ticketDto.getOldTE();
                    ticketDto.setOldTE(null);
                    oldTEDto.setTicket(ticketDto);

                    TicketIdentifier oldTE = TicketIdentifierDtoToEntity.convert(oldTEDto);
                    oldTE=ticketService.getTicketIdentifierById(oldTE);
                    oldTE.setValid(false);
                    oldTE.setCancellationReason("SOLD");
                    ticketService.updateThisTicketIdentifier(oldTE);

                }
                ti= ticketService.saveTicketIdentifier(ti);
                receiptEntry.setTicketIdentifier(ti);
            }
            total+=receiptEntry.getUnitPrice();


            if(receiptDto.isPremium()){
                receiptEntry.setUnitPrice(0);
            }

            receiptEntry.setReceipt(receiptnew);
            saveReceiptEntrys(receiptEntry);
        }

        Customer c = receiptnew.getCustomer();
        if(c.getId()!=1){
            if(receiptDto.isPremium()){
                c.setPremiumpoints(c.getPremiumpoints()-(total));
                customerService.save(c);
            }else{
                int premiumPoint=0;

                try {
                    premiumPoint =c.getPremiumpoints();
                }catch (NullPointerException e){

                }
                c.setPremiumpoints(premiumPoint+(total/10));
                customerService.save(c);
            }
        }

        Integer id = 0;
        msg.setType(MessageType.SUCCESS);
        msg.setText(id.toString());
        return msg;
    }
    private boolean checkIfTicketAreSold(ReceiptDto receiptDto) throws ServiceException{
        boolean sold=false;
        List<ReceiptentryDto> receiptentryDtos = receiptDto.getReceiptentryDto();
        for(ReceiptentryDto r:receiptentryDtos){
            if(r.getTicketIdentifierDto()!=null){
                if(r.getTicketIdentifierDto().getTicket()!=null){
                    Ticket t = ticketService.getTicketByID(r.getTicketIdentifierDto().getTicket().getId());
                    int flag = ticketService.getFlag(t);
                    if(flag == 3){
                        LOGGER.debug("Ticket sold");
                        return true;
                    }
                }else{
                    return true;
                }
            }
        }
        return sold;
    }

    @Override
    public Receipt saveReceipt(Receipt receipt) throws ServiceException {
        LOGGER.debug("Start saving the new Receipt");
        try {
            LOGGER.debug("save the receipt in db");
            return receiptDao.save(receipt);
        } catch (NullPointerException | DataIntegrityViolationException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public ReceiptEntry saveReceiptEntrys(ReceiptEntry receiptEntry) throws ServiceException {
        return receiptEntryDao.save(receiptEntry);
    }

    @Override
    public List<ReceiptEntry> getForReceipt(Receipt receipt) throws ServiceException {
        try{
            return receiptDao.getForTicketsCustomer(receipt);
        } catch (NullPointerException | DataIntegrityViolationException e){
            throw new ServiceException(e);
        }
    }

    @Override
    public List<Receipt> getForCustomer(Customer customer) throws ServiceException {
        LOGGER.debug("Getting receipts for customer");

        try{
            return receiptDao.getForCustomer(customer);
        } catch (NullPointerException | DataIntegrityViolationException e){
            throw new ServiceException(e);
        }
    }

    // -------------------- For Testing purposes --------------------

    /**
     * Sets the ticket dao.
     *
     * @param dao the new ticket dao
     */
    public void setReceiptDao(ReceiptDao dao) {
        this.receiptDao = dao;
    }
    public void setReceiptEntryDao(ReceiptEntryDao dao) {
        this.receiptEntryDao = dao;
    }

    public void setEmployeeDao(EmployeeDao dao) {
        this.employeeDao = dao;
    }

    public void setCustomerDao(CustomerDao dao) {
        this.customerDao = dao;
    }
    public void setMerchandiseDao(MerchandiseDao dao ){this.merchandiseDao = dao;}

    // -------------------- For Testing purposes --------------------
}