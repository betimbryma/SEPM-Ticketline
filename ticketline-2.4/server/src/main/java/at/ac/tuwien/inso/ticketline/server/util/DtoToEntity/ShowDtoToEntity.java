package at.ac.tuwien.inso.ticketline.server.util.DtoToEntity;

import at.ac.tuwien.inso.ticketline.dto.ShowDto;
import at.ac.tuwien.inso.ticketline.model.Show;

public class ShowDtoToEntity {

    private static ShowDtoToEntity instance = null;
    private ShowDtoToEntity() {}
    public static ShowDtoToEntity getInstance() {
        if(instance == null) {
            instance = new ShowDtoToEntity();
        }
        return instance;
    }

    public static Show convert(ShowDto showDto){
        Show show = new Show();
        show.setId(showDto.getId());
        show.setCanceled(showDto.isCanceled());
        show.setDateOfPerformance(showDto.getDateOfPerformance());
        show.setRoom(RoomDtoToEntity.getInstance().convert(showDto.getRoom()));
        show.setPerformance(PerformanceDtoToEntity.getInstance().convertPerformance(showDto.getPerformance()));

        return show;
    }
}
