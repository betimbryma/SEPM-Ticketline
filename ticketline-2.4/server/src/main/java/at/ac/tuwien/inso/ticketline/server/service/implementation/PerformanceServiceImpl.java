package at.ac.tuwien.inso.ticketline.server.service.implementation;

import at.ac.tuwien.inso.ticketline.dao.PerformanceDao;
import at.ac.tuwien.inso.ticketline.model.Performance;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.server.service.PerformanceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PerformanceServiceImpl implements PerformanceService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PerformanceServiceImpl.class);

    @Autowired
    private PerformanceDao performanceDao;

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Performance> getAll() throws ServiceException {
        LOGGER.info("getAll() called");
        List<Performance> list = performanceDao.findAll();
        return list;
    }

    public void addDao(PerformanceDao m){
        this.performanceDao=m;
    }

    // -------------------- For Testing purposes --------------------

    /**
     * Sets the customer dao.
     *
     * @param dao the new customer dao
     */
    public void setCustomerDao(PerformanceDao dao) {
        this.performanceDao = dao;
    }

    // -------------------- For Testing purposes --------------------
}
