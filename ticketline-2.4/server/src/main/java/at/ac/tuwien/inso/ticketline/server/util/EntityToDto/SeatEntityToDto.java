package at.ac.tuwien.inso.ticketline.server.util.EntityToDto;

import at.ac.tuwien.inso.ticketline.dto.SeatDto;
import at.ac.tuwien.inso.ticketline.model.Seat;

public class SeatEntityToDto {

    private static SeatEntityToDto instance = null;
    private SeatEntityToDto() {}
    public static SeatEntityToDto getInstance() {
        if(instance == null) {
            instance = new SeatEntityToDto();
        }
        return instance;
    }

    public static SeatDto convert(Seat seat){
        SeatDto seatDto = new SeatDto();
        seatDto.setId(seat.getId());
        seatDto.setDescription(seat.getDescription());
        seatDto.setName(seat.getName());
        seatDto.setOrder(seat.getOrder());
        seatDto.setRoom(RoomEntityToDto.getInstance().convertRoom(seat.getRoom()));
        seatDto.setRow(RowEntityToDto.getInstance().convert(seat.getRow()));
        seatDto.setCategory(CategoryEntityToDto.getInstance().convert(seat.getCategory()));
        seatDto.setGallery(GalleryEntityToDto.getInstance().convert(seat.getGallery()));

        return seatDto;
    }
}
