package at.ac.tuwien.inso.ticketline.server.service;

import at.ac.tuwien.inso.ticketline.model.*;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;

/**
 * For testing purposes
 */
public interface TestService {

    Ticket ticketSave(Ticket ticket) throws ServiceException;
    TicketIdentifier ticketIdentifierSave(TicketIdentifier ticket) throws ServiceException;
    Row rowSave(Row ticket) throws ServiceException;
    Location locationSave(Location ticket) throws ServiceException;
    Seat seatSave(Seat ticket) throws ServiceException;
    Show showSave(Show ticket) throws ServiceException;
    Performance performanceSave(Performance ticket) throws ServiceException;
    Gallery gallerySave(Gallery ticket) throws ServiceException;
    Category categorySave(Category ticket) throws ServiceException;
    Room roomSave(Room ticket) throws ServiceException;
    Employee saveEmployee(Employee employee) throws ServiceException;
    MethodOfPayment saveMethodOfPayment(Cash methodOfPayment) throws ServiceException;
}