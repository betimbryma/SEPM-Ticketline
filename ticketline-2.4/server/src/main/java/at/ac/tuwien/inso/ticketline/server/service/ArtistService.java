package at.ac.tuwien.inso.ticketline.server.service;

import at.ac.tuwien.inso.ticketline.model.Artist;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;

import java.util.List;

public interface ArtistService {

    /**
     * Gets all the artists in the database.
     *
     * @return a list of the artists
     * @throws ServiceException if there are any exception, they are going to be encapsulated in ServiceException
     */
    List<Artist> getAll() throws ServiceException;
}