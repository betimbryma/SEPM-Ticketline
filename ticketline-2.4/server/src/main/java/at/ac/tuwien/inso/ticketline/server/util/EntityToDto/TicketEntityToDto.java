package at.ac.tuwien.inso.ticketline.server.util.EntityToDto;

import at.ac.tuwien.inso.ticketline.dto.TicketDto;
import at.ac.tuwien.inso.ticketline.model.Ticket;

import java.util.ArrayList;
import java.util.List;

public class TicketEntityToDto {

    private static TicketEntityToDto instance = null;
    private TicketEntityToDto() {}
    public static TicketEntityToDto getInstance() {
        if(instance == null) {
            instance = new TicketEntityToDto();
        }
        return instance;
    }

    public static TicketDto convert(Ticket ticket){
        TicketDto ticketDto = new TicketDto();
        ticketDto.setDescription(ticket.getDescription());
        ticketDto.setId(ticket.getId());
        ticketDto.setPrice(ticket.getPrice());
        ticketDto.setShow(ShowEntityToDto.getInstance().convertShow(ticket.getShow()));
        ticketDto.setSeat(SeatEntityToDto.getInstance().convert(ticket.getSeat()));

        if(ticket.getFlag()!=null){
            ticketDto.setFlag(ticket.getFlag());
        }else{
            ticketDto.setFlag(0);
        }
        return ticketDto;
    }

    public static List<TicketDto> convertTickets(List<Ticket> tickets){
        List<TicketDto> ticketDtoList = new ArrayList<>();

        for (Ticket ticket : tickets){
            ticketDtoList.add(convert(ticket));
        }
        return ticketDtoList;
    }
}
