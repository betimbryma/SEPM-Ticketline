package at.ac.tuwien.inso.ticketline.server.integrationtest.service;

import at.ac.tuwien.inso.ticketline.model.Participation;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.server.service.ParticipationService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * @author Michael Oppitz
 * @date 09.05.2016
 */
public class ParticipationServiceIntegrationTest extends AbstractServiceIntegrationTest {

    @Autowired
    private ParticipationService participationService;

    @Test
    public void testGetAll() {
        try {
            List<Participation> participation = participationService.getAll();
            assertEquals(0, participation.size());
        } catch (ServiceException e) {
            fail("ServiceException thrown");
        }
    }
}
