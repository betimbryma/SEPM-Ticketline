package at.ac.tuwien.inso.ticketline.server.integrationtest.service;

import at.ac.tuwien.inso.ticketline.model.Artist;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.server.service.ArtistService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * @author Michael Oppitz
 * @date 09.05.2016
 */
public class ArtistServiceIntegrationTest extends AbstractServiceIntegrationTest {

    @Autowired
    private ArtistService service;

    @Test
    public void testGetAll() {
        try {
            List<Artist> artist = service.getAll();
            assertEquals(0, artist.size());
        } catch (ServiceException e) {
            fail("ServiceException thrown");
        }
    }
}
