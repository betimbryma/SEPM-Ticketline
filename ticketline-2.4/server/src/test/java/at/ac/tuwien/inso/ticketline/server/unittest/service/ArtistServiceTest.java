package at.ac.tuwien.inso.ticketline.server.unittest.service;

import at.ac.tuwien.inso.ticketline.dao.ArtistDao;
import at.ac.tuwien.inso.ticketline.model.Artist;
import at.ac.tuwien.inso.ticketline.server.TestUtils;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.server.service.implementation.ArtistServiceImpl;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class ArtistServiceTest {

    private static final int amount = 9;
    private static ArtistServiceImpl artistService;
    private static List<Artist> artists;

    @BeforeClass
    public static void setUp() {
        artistService = new ArtistServiceImpl();
        artists = new ArrayList<>();

        for (int i = 0; i < amount; i++) {
            artists.add(TestUtils.generateArtist(i));
        }
    }

    @Test
    public void getAllTest() {
        ArtistDao dao = Mockito.mock(ArtistDao.class);
        Mockito.when(dao.findAll()).thenReturn(artists);
        artistService.setArtistDao(dao);

        try {
            List<Artist> allArtists = artistService.getAll();
            assertEquals(amount, allArtists.size());

            for(Artist a : allArtists) {
                Artist a2 = artists.get(a.getId());

                assertEquals(a2.getFirstname(), a.getFirstname());
                assertEquals(a2.getLastname(), a.getLastname());
                assertEquals(a2.getDescription(), a.getDescription());
            }

        } catch (ServiceException e) {
            fail("A service exception was thrown");

        }
    }
}
