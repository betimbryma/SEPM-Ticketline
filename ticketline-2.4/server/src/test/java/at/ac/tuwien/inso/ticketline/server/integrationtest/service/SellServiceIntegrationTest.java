package at.ac.tuwien.inso.ticketline.server.integrationtest.service;

import at.ac.tuwien.inso.ticketline.model.*;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.server.service.CustomerService;
import at.ac.tuwien.inso.ticketline.server.service.SellService;
import at.ac.tuwien.inso.ticketline.server.service.TestService;
import at.ac.tuwien.inso.ticketline.server.service.implementation.TicketServiceImpl;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * Created by eni on 05.06.2016.
 */
public class SellServiceIntegrationTest extends  AbstractServiceIntegrationTest{
    @Autowired
    private TicketServiceImpl ticketService;
    private List<Ticket> tickets;
    private Row row1,row2;
    private Category vip;
    private Gallery gallery1,gallery2,gallery3,gallery4;
    private Show show;

    @Autowired
    private TestService testService;
    @Autowired
    private CustomerService customerService;
    @Autowired
    private SellService sellService;


    @Test
    public void setUp() throws ServiceException {


        Category vip = new Category();
        vip.setDescription("This category of tickets is meant for VIP");
        vip.setName("VIP");
        if(vip == null)
            System.out.println("null");
        vip = testService.categorySave(vip);


        Row row1 = new Row();
        row1.setDescription("The first row");
        row1.setName("#1Row");
        row1.setOrder(1);

        Row row2 = new Row();
        row2.setDescription("The second row");
        row2.setName("#2Row");
        row2.setOrder(2);

        row1 = testService.rowSave(row1);
        row2 = testService.rowSave(row2);
        List<Row> rows = new ArrayList<>();
        rows.add(row1);
        rows.add(row2);
        Gallery gallery1 = new Gallery();
        gallery1.setName("#1");
        gallery1.setDescription("First column");
        gallery1.setOrder(1);

        Gallery gallery2 = new Gallery();
        gallery2.setName("#2");
        gallery2.setDescription("Second column");
        gallery2.setOrder(2);

        Gallery gallery3 = new Gallery();
        gallery3.setName("#3");
        gallery3.setDescription("Third column");
        gallery3.setOrder(3);

        Gallery gallery4 = new Gallery();
        gallery4.setName("#4");
        gallery4.setDescription("Fourth column");
        gallery4.setOrder(4);
        gallery1 = testService.gallerySave(gallery1);
        gallery2 = testService.gallerySave(gallery2);
        gallery3 = testService.gallerySave(gallery3);
        gallery4 = testService.gallerySave(gallery4);
        List<Gallery> gallerys = new ArrayList<>();
        gallerys.add(gallery1);
        gallerys.add(gallery2);
        gallerys.add(gallery3);
        gallerys.add(gallery4);

        Location l = new Location();

        l.setName("Tokyo");
        l.setDescription("Capital city of Japan");
        l.setAddress(new Address("1-5-2 Higashi-Shimbashi ", "105-7123", "Tokyo", "Japan"));
        l.setOwner("Yoichi Masuzoe");
        l=testService.locationSave(l);
        Room room = new Room("Saitama Super Arena", "multi-purpose indoor arena located in Chūō-ku", l);
        room = testService.roomSave(room);
        Performance performance = new Performance("Epson Aqua Park Shinagawa", "Great place to go on a date, with friends or family.", 5, PerformanceType.OPER);
        performance = testService.performanceSave(performance);
        Show show = new Show(false, new Date(500), room, performance);
        show = testService.showSave(show);


        Customer c = new Customer();
        c.setFirstname("Testfirstname");
        c.setLastname("Testlastname");

        Address a = new Address();
        a.setStreet("Teststreet");
        a.setPostalCode("Testpostalcode");
        a.setCity("Testcity");
        a.setCountry("Testcountry");

        c.setAddress(a);
        c.setGender(Gender.MALE);

        c = customerService.save(c);

        Employee cc = new Employee();
        cc.setFirstname("Testfirstname");
        cc.setLastname("Testlastname");
        Address aa = new Address();
        aa.setStreet("Teststreet");
        aa.setPostalCode("Testpostalcode");
        aa.setCity("Testcity");
        aa.setCountry("Testcountry");
        cc.setAddress(a);
        cc.setGender(Gender.MALE);
        cc.setUsername("admin");
        cc.setPasswordHash("sdafadfasdf");
        cc = testService.saveEmployee(cc);

        Cash methodOfPayment = new Cash();
        methodOfPayment.setDeleted(false);
        methodOfPayment.setCustomer(c);
        testService.saveMethodOfPayment(methodOfPayment);


        Receipt receipt = new Receipt();
        receipt.setCustomer(c);
        receipt.setEmployee(cc);
        receipt.setMethodOfPayment(methodOfPayment);
        receipt.setTransactionState(TransactionState.PAID);
        Receipt receiptNew =  sellService.saveReceipt(receipt);
        assertTrue(receipt.getCustomer().getId()==receiptNew.getCustomer().getId());

        int totalTicket = 0;
        int totalReceiptEntry = 0;

        for(int i = 0; i<8;i++){
            Seat seat = new Seat("po", "jo", i, room, vip, rows.get(i%2), gallerys.get(i%4));
            seat = testService.seatSave(seat);
            Ticket ticket = new Ticket("Shinagawa", 70, show, seat);
            int b = (int) Math.random()*30;
            totalTicket +=b;
            ticket.setPrice(b);
            ticket = testService.ticketSave(ticket);

            ticketService.updateTicketForCartAsNotUsed(ticket);
            assertTrue("ticket flag should be 0",ticketService.getFlag(ticket)==0);
            ticketService.updateTicketForCartAsUsed(ticket);
            assertTrue("ticket flag should be 1",ticketService.getFlag(ticket)==1);
            ticketService.updateTicketForCartAsReserved(ticket);
            assertTrue("ticket flag should be 2",ticketService.getFlag(ticket)==2);
            ticketService.updateTicketForCartAsSold(ticket);
            assertTrue("ticket flag should be 3",ticketService.getFlag(ticket)==3);

            TicketIdentifier ticketIdentifier = new TicketIdentifier();
            ticketIdentifier.setTicket(ticket);
            ticketIdentifier = ticketService.saveTicketIdentifier(ticketIdentifier);
            assertTrue(ticketIdentifier.getTicket().getId()==ticket.getId());


            ReceiptEntry receiptEntry = new ReceiptEntry();
            receiptEntry.setPosition(10);
            receiptEntry.setTicketIdentifier(ticketIdentifier);
            receiptEntry.setReceipt(receipt);
            receiptEntry.setAmount(1);
            receiptEntry.setUnitPrice(ticket.getPrice());
            receiptEntry = sellService.saveReceiptEntrys(receiptEntry);
            totalReceiptEntry += receiptEntry.getUnitPrice();
            assertTrue(receiptEntry.getTicketIdentifier().getId()==ticketIdentifier.getId());
            assertTrue(receiptEntry.getReceipt().getId()==receipt.getId());


 //           receiptEntry.setTicketIdentifier();
        }

        assertTrue("Receipt entry cost is sam as the cost of its tickets",totalReceiptEntry==totalTicket);
        assertTrue("The customer has only one Receipt",sellService.getForCustomer(c).size()==1);
        assertTrue("The receipt should have 8 Receipt entrys",sellService.getForReceipt(receipt).size()==8);




    }


}
