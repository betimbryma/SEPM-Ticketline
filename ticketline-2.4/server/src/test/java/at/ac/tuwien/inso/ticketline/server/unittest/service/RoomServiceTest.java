package at.ac.tuwien.inso.ticketline.server.unittest.service;

import at.ac.tuwien.inso.ticketline.dao.RoomDao;
import at.ac.tuwien.inso.ticketline.model.Room;
import at.ac.tuwien.inso.ticketline.server.TestUtils;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.server.service.implementation.RoomServiceImpl;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class RoomServiceTest {

    private static final int amount = 9;
    private static RoomServiceImpl roomService;
    private static List<Room> rooms;

    @BeforeClass
    public static void setUp() {
        roomService = new RoomServiceImpl();
        rooms = new ArrayList<>();

        for (int i = 0; i < amount; i++) {
            rooms.add(TestUtils.generateRoom(i));
        }
    }

    @Test
    public void getAllTest() {
        RoomDao dao = Mockito.mock(RoomDao.class);
        Mockito.when(dao.findAll()).thenReturn(rooms);
        roomService.setRoomDao(dao);

        try {
            List<Room> allRooms = roomService.getAll();
            assertEquals(amount, allRooms.size());

            for(Room r : allRooms) {
                Room r2 = rooms.get(r.getId());

                assertEquals(r2.getName(), r.getName());
                assertEquals(r2.getDescription(), r.getDescription());
            }

        } catch (ServiceException e) {
            fail("A service exception was thrown");
        }


    }

}
