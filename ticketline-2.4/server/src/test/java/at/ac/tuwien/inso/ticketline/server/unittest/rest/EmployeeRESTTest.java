package at.ac.tuwien.inso.ticketline.server.unittest.rest;

import at.ac.tuwien.inso.ticketline.dto.EmployeeDto;
import at.ac.tuwien.inso.ticketline.model.Employee;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.server.rest.EmployeeController;
import at.ac.tuwien.inso.ticketline.server.service.EmployeeService;
import at.ac.tuwien.inso.ticketline.server.service.implementation.EmployeeServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.io.IOException;
import java.util.Date;

import static junit.framework.TestCase.fail;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Created by Jakob on 18.06.2016.
 */
public class EmployeeRESTTest {

    private MockMvc mockMvc;

    @Autowired
    private EmployeeService employeeServiceMock;

    @Before
    public void setUp() {
        employeeServiceMock = Mockito.mock(EmployeeServiceImpl.class);

        EmployeeController emplContr = new EmployeeController();
        emplContr.setEmployeeService(employeeServiceMock);

        this.mockMvc = MockMvcBuilders.standaloneSetup(emplContr).build();
    }

    @Test
    public void getEmployeeByUsername() throws ServiceException {

        Employee e = new Employee();
        e.setUsername("Testemployee");
        e.setFirstname("Testfirstname1");

        when(employeeServiceMock.searchEmployeeByUsername(Matchers.any(EmployeeDto.class))).thenReturn(e);

        try {
            mockMvc.perform(get("/employee/Testemployee"))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType("application/json"))
                    .andExpect(jsonPath("firstName", is("Testfirstname1")));
        } catch (Exception ex) {
            ex.printStackTrace();
            fail("An Exception has been thrown while executing getAll");
        }


    }


    @Test
    public void updateEmployeeLogin() throws ServiceException {

        EmployeeDto emplDto = new EmployeeDto();
        emplDto.setUsername("testy");
        emplDto.setFirstName("Testfirstname");
        emplDto.setLastName("Testlastname");
        emplDto.setLastlogin(new Date(100));

        try {
            mockMvc.perform(put("/employee/updateLastLogin")
                    .contentType(TestUtils.APPLICATION_JSON_UTF8)
                    .content(TestUtils.convertObjectToJsonBytes(emplDto))
            )
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$type", is("SUCCESS")));
        }
        catch (IOException ex) {
            fail("IOException has been thrown when converting DTO to JSON");
        }
        catch (Exception ex) {
            ex.printStackTrace();
            fail();
        }

    }
}
