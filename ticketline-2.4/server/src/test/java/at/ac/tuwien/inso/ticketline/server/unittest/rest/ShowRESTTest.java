package at.ac.tuwien.inso.ticketline.server.unittest.rest;


import at.ac.tuwien.inso.ticketline.model.*;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.server.rest.ShowController;
import at.ac.tuwien.inso.ticketline.server.service.ShowService;
import at.ac.tuwien.inso.ticketline.server.service.implementation.ShowServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;

import static junit.framework.TestCase.fail;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Created by Jakob on 18.06.2016.
 */


public class ShowRESTTest {

    private MockMvc mockMvc;

    @Autowired
    private ShowService showServiceMock;



    @Before
    public void setUp() {
        showServiceMock = Mockito.mock(ShowServiceImpl.class);

        ShowController showContr = new ShowController();
        showContr.setShowService(showServiceMock);

        this.mockMvc = MockMvcBuilders.standaloneSetup(showContr).build();

    }

    @Test
    public void findAllShows() throws ServiceException {

        Address a = new Address();
        a.setStreet("Somestreet");
        a.setPostalCode("1234");
        a.setCity("Somecity");
        a.setCountry("Somecountry");

        Location loc1 = new Location();
        loc1.setId(1);
        loc1.setName("Testlocation1");
        loc1.setAddress(a);

        Room r = new Room();
        r.setId(1);
        r.setName("Testroom1");
        r.setLocation(loc1);

        Show s1 = new Show();
        s1.setId(1);
        s1.setCanceled(false);
        Performance p = new Performance();
        p.setName("Testperformance1");
        s1.setPerformance(p);
        s1.setRoom(r);

        Show s2 = new Show();
        s2.setId(2);
        s2.setCanceled(true);
        Performance p2 = new Performance();
        p2.setName("Testperformance2");
        s2.setPerformance(p2);
        s2.setRoom(r);

        when(showServiceMock.getAll()).thenReturn(Arrays.asList(s1, s2));

        try {
            mockMvc.perform(get("/show/"))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType("application/json"))
                    .andExpect(jsonPath("$", hasSize(2)))
                    .andExpect(jsonPath("$[0].performance.name", is("Testperformance1")))
                    .andExpect(jsonPath("$[0].room.name", is("Testroom1")))
                    .andExpect(jsonPath("$[1].performance.name", is("Testperformance2")));
        } catch (Exception e) {
            e.printStackTrace();
            fail("An Exception has been thrown while executing getAll");
        }

    }
}