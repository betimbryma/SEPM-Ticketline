package at.ac.tuwien.inso.ticketline.server.unittest.service;

import at.ac.tuwien.inso.ticketline.dao.EmployeeDao;
import at.ac.tuwien.inso.ticketline.dto.EmployeeDto;
import at.ac.tuwien.inso.ticketline.model.Employee;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.server.service.implementation.EmployeeServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.ArrayList;
import java.util.Date;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doAnswer;

/**
 * Created by Jakob on 01.06.2016.
 */
public class EmployeeServiceTest {

    private EmployeeServiceImpl emplService = null;
    private ArrayList<Employee> employees;

    @Before
    public void setUp() {
        emplService = new EmployeeServiceImpl();

        employees = new ArrayList<Employee>();

        Employee employee = new Employee();
        employee.setUsername("Testemployee");
        employee.setId(1);
        employee.setLastLogin(new Date(500));
        employees.add(employee);
    }

    @Test
    public void testGetByUsername() {
        EmployeeDao dao = Mockito.mock(EmployeeDao.class);

        Mockito.when(dao.findByUsername("Testemployee")).thenReturn(employees);

        emplService.setEmployeeDao(dao);

        try {

            EmployeeDto edto = new EmployeeDto();
            edto.setUsername("Testemployee");

            assertEquals(emplService.searchEmployeeByUsername(edto).getUsername(), "Testemployee");
        } catch (ServiceException e) {
            fail("ServiceException thrown");
        }
    }

    @Test(expected = ServiceException.class)
    public void testGetByUsernameNull() throws ServiceException {
        EmployeeDao dao = Mockito.mock(EmployeeDao.class);
        Mockito.when(dao.findByUsername("Testemployee")).thenReturn(employees);

        emplService.setEmployeeDao(dao);
        emplService.searchEmployeeByUsername(null);
    }

    @Test
    public void testChangeLastLogin() {
        EmployeeDao dao = Mockito.mock(EmployeeDao.class);

        Mockito.when(dao.findByUsername("Testemployee")).thenReturn(employees);

        doAnswer(new Answer<Employee>() {
            @Override
            public Employee answer(InvocationOnMock invocation) {
                Employee employee = (Employee) invocation.getArguments()[0];

                for(Employee e : employees) {
                    if(e.getUsername().equals(employee.getUsername())) {
                        e.setLastLogin(employee.getLastLogin());
                        return e;
                    }
                }

                return null;
            }
        }).when(dao).save(any(Employee.class));

        emplService.setEmployeeDao(dao);

        try {

            EmployeeDto edto = new EmployeeDto();
            edto.setUsername("Testemployee");
            Employee e = emplService.searchEmployeeByUsername(edto);

            assertTrue(e.getLastLogin().before(new Date(System.currentTimeMillis())));

            Date previousLogin = e.getLastLogin();
            edto.setLastlogin(new Date(System.currentTimeMillis()));

            emplService.updateLastLogin(edto);
            e = emplService.searchEmployeeByUsername(edto);

            assertTrue(e.getLastLogin().after(previousLogin));

        } catch (ServiceException e) {
            fail("ServiceException thrown");
        }
    }


    @Test(expected = ServiceException.class)
    public void testChangeLastLoginNull() throws ServiceException {
        emplService.updateLastLogin(null);
    }
}
