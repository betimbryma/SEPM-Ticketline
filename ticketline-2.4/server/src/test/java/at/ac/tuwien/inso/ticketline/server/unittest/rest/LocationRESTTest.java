package at.ac.tuwien.inso.ticketline.server.unittest.rest;


import at.ac.tuwien.inso.ticketline.model.Address;
import at.ac.tuwien.inso.ticketline.model.Location;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.server.rest.LocationController;
import at.ac.tuwien.inso.ticketline.server.service.LocationService;
import at.ac.tuwien.inso.ticketline.server.service.implementation.LocationServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;

import static junit.framework.TestCase.fail;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Created by Jakob on 18.06.2016.
 */


public class LocationRESTTest {

    private MockMvc mockMvc;

    @Autowired
    private LocationService locationServiceMock;



    @Before
    public void setUp() {
        locationServiceMock = Mockito.mock(LocationServiceImpl.class);

        LocationController loContr = new LocationController();
        loContr.setLocationService(locationServiceMock);

        this.mockMvc = MockMvcBuilders.standaloneSetup(loContr).build();

    }

    @Test
    public void findAllLocations() throws ServiceException {

        Address a = new Address();
        a.setStreet("Somestreet");
        a.setPostalCode("1234");
        a.setCity("Somecity");
        a.setCountry("Somecountry");

        Location loc1 = new Location();
        loc1.setId(1);
        loc1.setName("Testlocation1");
        loc1.setAddress(a);

        Location loc2 = new Location();
        loc2.setId(2);
        loc2.setName("Testlocation2");
        loc2.setAddress(a);

        when(locationServiceMock.getAll()).thenReturn(Arrays.asList(loc1, loc2));

        try {
            mockMvc.perform(get("/location/"))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType("application/json"))
                    .andExpect(jsonPath("$", hasSize(2)))
                    .andExpect(jsonPath("$[0].name", is("Testlocation1")))
                    .andExpect(jsonPath("$[1].name", is("Testlocation2")));
        } catch (Exception e) {
            e.printStackTrace();
            fail("An Exception has been thrown while executing getAll");
        }

    }
}