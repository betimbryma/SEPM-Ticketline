package at.ac.tuwien.inso.ticketline.server.integrationtest.service;

import at.ac.tuwien.inso.ticketline.model.Performance;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.server.service.PerformanceService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * @author Michael Oppitz
 * @date 09.05.2016
 */
public class PerformanceServiceIntegrationTest extends AbstractServiceIntegrationTest {

    @Autowired
    private PerformanceService performanceService;

    @Test
    public void testGetAll() {
        try {
            List<Performance> performance = performanceService.getAll();
            assertEquals(0, performance.size());
        } catch (ServiceException e) {
            fail("ServiceException thrown");
        }
    }

}
