package at.ac.tuwien.inso.ticketline.server.unittest.rest;


import at.ac.tuwien.inso.ticketline.model.Artist;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.server.rest.ArtistController;
import at.ac.tuwien.inso.ticketline.server.service.ArtistService;
import at.ac.tuwien.inso.ticketline.server.service.implementation.ArtistServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;

import static junit.framework.TestCase.fail;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Created by Jakob on 18.06.2016.
 */


public class ArtistRESTTest {

    private MockMvc mockMvc;

    @Autowired
    private ArtistService artistServiceMock;



    @Before
    public void setUp() {
        artistServiceMock = Mockito.mock(ArtistServiceImpl.class);

        ArtistController artContr = new ArtistController();
        artContr.setArtistService(artistServiceMock);

        this.mockMvc = MockMvcBuilders.standaloneSetup(artContr).build();

    }

    @Test
    public void findAllCustomers() throws ServiceException {

        Artist art1 = new Artist();
        art1.setFirstname("Testartist1");
        art1.setLastname("Testinger");
        art1.setId(1);

        Artist art2 = new Artist();
        art2.setFirstname("Testartist2");
        art2.setLastname("Testinger");
        art2.setId(2);

        when(artistServiceMock.getAll()).thenReturn(Arrays.asList(art1, art2));

        try {
            mockMvc.perform(get("/artist/"))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType("application/json"))
                    .andExpect(jsonPath("$", hasSize(2)))
                    .andExpect(jsonPath("$[0].firstName", is("Testartist1")))
                    .andExpect(jsonPath("$[1].firstName", is("Testartist2")));
        } catch (Exception e) {
            e.printStackTrace();
            fail("An Exception has been thrown while executing getAll");
        }


    }
}