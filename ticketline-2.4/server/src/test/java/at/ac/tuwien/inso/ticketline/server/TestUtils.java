package at.ac.tuwien.inso.ticketline.server;

import at.ac.tuwien.inso.ticketline.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class TestUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(TestUtils.class);

    private static Date generateDate(int year, int month, int day) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
        if (year <= 0 || month <= 0 || day <= 0 || month > 12 || day > 31) {
            LOGGER.debug("Invalid date: " + day + "." + month + "." + year);
            return null;
        }
        try {
            return formatter.parse(year + "/" + month + "/" + day);
        } catch (ParseException e) {
            LOGGER.debug("Invalid date: " + day + "." + month + "." + year);
            return null;
        }
    }

    private static Date generateDate(int year, int month, int day, int hour, int minute, int second) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");

        if (year <= 0 || month <= 0 || day <= 0 || hour < 0 || minute < 0 || second < 0
                || month > 12 || day > 31 || hour > 23 || minute > 59 || second > 59) {
            LOGGER.debug("Invalid date: " + day + "." + month + "." + year + " - " + hour + ":" + minute + ":" + second);
            return null;
        }
        try {
//            LOGGER.debug("Valid date: " + day + "." + month + "." + year + " - " + hour + ":" + minute + ":" + second);
            return formatter.parse(year + "/" + month + "/" + day + " " + hour + ":" + minute + ":" + second);
        } catch (ParseException e) {
            LOGGER.debug("Invalid date: " + day + "." + month + "." + year + " - " + hour + ":" + minute + ":" + second);
            return null;
        }
    }

    private static Date generateRandomDate() {
        Random r = new Random();
        return generateDate(r.nextInt(2) + 2016, r.nextInt(12) + 1, r.nextInt(28) + 1, r.nextInt(24), r.nextInt(60), r.nextInt(60));
    }

    public static Show generateShow(int id) {
        Show s = new Show();
        s.setId(id);

        s.setDateOfPerformance(generateRandomDate());
        s.setCanceled(Math.random() < 0.5);


        return s;
    }

    public static Artist generateArtist(int id) {
        Artist a = new Artist();

        a.setId(id);
        a.setFirstname("Artist_fn_" + id);
        a.setLastname("Artist_ln_" + id);
        a.setDescription("Cool Artist #" + id);

        return a;
    }

    public static Room generateRoom(int id) {
        Room r = new Room();
        r.setId(id);
        r.setDescription("Room #" + id);
        r.setName("Room #" + id);

        return r;
    }

    public static Location generateLocation(int id) {
        Location l = new Location();
        l.setId(id);
        l.setDescription("Location #" + id);
        l.setAddress(generateAddress(id));
        l.setName("Name #" + id);
        l.setOwner("Owner #" + id);

        return l;
    }

    private static Address generateAddress(int id) {
        Address a = new Address();
        a.setCity("City #" + id);
        a.setCountry("Country #" + id);
        a.setPostalCode("PostalCode #" + id);
        a.setStreet("Street #" + id);

        return a;
    }

    public static Performance generatePerformance(int id) {
        Performance p = new Performance();
        p.setId(id);
        p.setName("Performance #" + id);
        p.setDescription("Performance #" + id);
        p.setPerformanceType(getRandom(PerformanceType.values()));

        return p;
    }

    private static <T> T getRandom(T[] array) {
        if (array == null || array.length == 0) {
            return null;
        }
        Random r = new Random();
        return array[r.nextInt(array.length)];
    }

}
