package at.ac.tuwien.inso.ticketline.server;

import at.ac.tuwien.inso.ticketline.server.integrationtest.service.MerchandiseServiceIntegrationTest;
import at.ac.tuwien.inso.ticketline.server.integrationtest.service.NewsServiceIntegrationTest;
import at.ac.tuwien.inso.ticketline.server.integrationtest.service.PremiumServiceIntegrationTest;
import at.ac.tuwien.inso.ticketline.server.integrationtest.service.SellServiceIntegrationTest;
import at.ac.tuwien.inso.ticketline.server.unittest.service.MerchandiseServiceTest;
import at.ac.tuwien.inso.ticketline.server.unittest.service.NewsServiceTest;
import at.ac.tuwien.inso.ticketline.server.unittest.service.PremiumServiceTest;
import at.ac.tuwien.inso.ticketline.server.unittest.service.SellServiceTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(value = Suite.class)
@SuiteClasses(value = {NewsServiceTest.class, NewsServiceIntegrationTest.class, SellServiceTest.class, SellServiceIntegrationTest.class,MerchandiseServiceTest.class, MerchandiseServiceIntegrationTest.class,
        PremiumServiceTest.class, PremiumServiceIntegrationTest.class})
public class AppTestSuite {

}
