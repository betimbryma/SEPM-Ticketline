package at.ac.tuwien.inso.ticketline.server.unittest.service;


import at.ac.tuwien.inso.ticketline.dao.ShowDao;
import at.ac.tuwien.inso.ticketline.model.Show;
import at.ac.tuwien.inso.ticketline.server.TestUtils;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.server.service.implementation.ShowServiceImpl;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class ShowServiceTest {
    private static final int amount = 9;
    private static ShowServiceImpl showService;
    private static List<Show> shows;

    @BeforeClass
    public static void setUp() {
        showService = new ShowServiceImpl();
        shows = new ArrayList<>();

        for (int i = 0; i < amount; i++) {
            shows.add(TestUtils.generateShow(i));
        }
    }

    @Test
    public void getAllTest() {
        ShowDao dao = Mockito.mock(ShowDao.class);
        Mockito.when(dao.findAll()).thenReturn(shows);
        showService.setShowDao(dao);

        try {
            List<Show> allShows = showService.getAll();
            assertEquals(amount, allShows.size());

            for(Show s : allShows) {
                Show s2 = shows.get(s.getId());

                assertEquals(s2.getCanceled(), s.getCanceled());
                assertEquals(s2.getDateOfPerformance(), s.getDateOfPerformance());
            }

        } catch (ServiceException e) {
            fail("A service exception was thrown");
        }
    }

}
