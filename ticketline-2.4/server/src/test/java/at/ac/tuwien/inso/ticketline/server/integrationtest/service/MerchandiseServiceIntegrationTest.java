package at.ac.tuwien.inso.ticketline.server.integrationtest.service;

import at.ac.tuwien.inso.ticketline.model.Article;
import at.ac.tuwien.inso.ticketline.model.Performance;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.server.service.MerchandiseService;
import at.ac.tuwien.inso.ticketline.server.service.PerformanceService;
import at.ac.tuwien.inso.ticketline.server.service.TestService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Created by eni on 09.05.2016.
 */
public class MerchandiseServiceIntegrationTest  extends AbstractServiceIntegrationTest {

    @Autowired
    private PerformanceService performanceService;
    @Autowired
    private MerchandiseService merchandiseService;
    @Autowired
    private TestService testService;


    @Test
    public void testSearchArticleID(){

        Performance p = new Performance();
        p.setName("sss");
        try {
            p= testService.performanceSave(p);
        } catch (ServiceException e) {
            fail("Exception");
        }


        for (int i = 0 ; i<6;i++){
            Article merchandiseDto = new Article();
            merchandiseDto.setAvailable(5);
            merchandiseDto.setPrice(3);
            merchandiseDto.setName("sd");
            merchandiseDto.setDescription("s");
            merchandiseDto.setPerformance(p);

            try {
              Article a=  merchandiseService.updateArticle(merchandiseDto);
                a.setAvailable(35);
                assertTrue(merchandiseService.updateArticle(a).getAvailable()==merchandiseService.searchArticleID(a.getId()).getAvailable());
            } catch (ServiceException e) {
                fail(" ");
            }
        }
    }


    @Test
    public void testSearchArticleByPerformance(){

        Performance p = new Performance();
        p.setName("sss");
        try {
            p= testService.performanceSave(p);
            for (int i = 0 ; i<6;i++){
                Article merchandiseDto = new Article();
                merchandiseDto.setAvailable(5);
                merchandiseDto.setPrice(3);
                merchandiseDto.setName("sd");
                merchandiseDto.setDescription("s");
                merchandiseDto.setPerformance(p);
                merchandiseService.updateArticle(merchandiseDto);

            }

        } catch (ServiceException e) {
            fail("Exception");
        }


        assertTrue(6==merchandiseService.getByPerformance(p).size());

    }

 /*

    @Override
    public Article searchArticleID(int id) throws ServiceException {
        try {
            return merchandiseDao.findOne(id);
        } catch (Exception e) {
            throw new ServiceException(e);
        }
    }


    @Override
    public Article updateArticle(Article article) throws ServiceException {
        return merchandiseDao.save(article);
    }

    @Override
    public List<Article> getByPerformance(Performance performance){
        return merchandiseDao.getByPerformance(performance);
    }

    public void addDao(MerchandiseDao m){
        this.merchandiseDao=m;
    }
    */
}
