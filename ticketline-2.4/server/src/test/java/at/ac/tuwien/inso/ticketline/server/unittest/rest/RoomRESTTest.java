package at.ac.tuwien.inso.ticketline.server.unittest.rest;


import at.ac.tuwien.inso.ticketline.model.*;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.server.rest.RoomController;
import at.ac.tuwien.inso.ticketline.server.service.RoomService;
import at.ac.tuwien.inso.ticketline.server.service.implementation.RoomServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;

import static junit.framework.TestCase.fail;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Created by Jakob on 18.06.2016.
 */


public class RoomRESTTest {

    private MockMvc mockMvc;

    @Autowired
    private RoomService roomServiceMock;



    @Before
    public void setUp() {
        roomServiceMock = Mockito.mock(RoomServiceImpl.class);

        RoomController roomContr = new RoomController();
        roomContr.setRoomService(roomServiceMock);

        this.mockMvc = MockMvcBuilders.standaloneSetup(roomContr).build();

    }

    @Test
    public void findAllRooms() throws ServiceException {

        Address a = new Address();
        a.setStreet("Somestreet");
        a.setPostalCode("1234");
        a.setCity("Somecity");
        a.setCountry("Somecountry");

        Location loc1 = new Location();
        loc1.setId(1);
        loc1.setName("Testlocation1");
        loc1.setAddress(a);

        Room r1 = new Room();
        r1.setId(1);
        r1.setName("Testroom1");
        r1.setLocation(loc1);

        Room r2 = new Room();
        r2.setId(2);
        r2.setName("Testroom2");
        r2.setLocation(loc1);


        when(roomServiceMock.getAll()).thenReturn(Arrays.asList(r1, r2));

        try {
            mockMvc.perform(get("/room/"))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType("application/json"))
                    .andExpect(jsonPath("$", hasSize(2)))
                    .andExpect(jsonPath("$[0].name", is("Testroom1")))
                    .andExpect(jsonPath("$[1].name", is("Testroom2")))
                    .andExpect(jsonPath("$[1].location.city", is("Somecity")));
        } catch (Exception e) {
            e.printStackTrace();
            fail("An Exception has been thrown while executing getAll");
        }

    }
}