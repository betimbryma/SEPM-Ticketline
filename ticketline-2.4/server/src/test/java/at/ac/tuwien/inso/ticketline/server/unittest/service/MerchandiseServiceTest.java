package at.ac.tuwien.inso.ticketline.server.unittest.service;

import at.ac.tuwien.inso.ticketline.dao.MerchandiseDao;
import at.ac.tuwien.inso.ticketline.model.Article;
import at.ac.tuwien.inso.ticketline.model.Performance;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.server.service.implementation.MerchandiseServiceImpl;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import static org.aspectj.bridge.MessageUtil.fail;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by eni on 04.06.2016.
 */
public class MerchandiseServiceTest {


    private MerchandiseServiceImpl service = null;

    private List<Article> articles = null;
    private List<Performance> performances= null;

    @org.junit.Before
    public void setUp() {

        service = new MerchandiseServiceImpl();
        performances = new ArrayList<>();
        articles = new ArrayList<>();

        Performance p = new Performance();
        p.setName("sdf");
        p.setId(1);
        performances.add(p);

        for (int i = 0 ; i<6;i++){
            Article merchandiseDto = new Article();
            merchandiseDto.setId(i+1);
            merchandiseDto.setAvailable(5);
            merchandiseDto.setPrice(3);
            merchandiseDto.setName("sd");
            merchandiseDto.setDescription("s");
            merchandiseDto.setPerformance(p);
            articles.add(merchandiseDto);
        }


    }

    @Test
    public void testGetByPerformance(){
        Performance p = performances.get(0);
        MerchandiseDao daoP = Mockito.mock(MerchandiseDao.class);
        Mockito.when(daoP.getByPerformance(p)).thenReturn(articles);
        service.addDao(daoP);
        assertEquals(6, service.getByPerformance(p).size());
    }

    @Test
    public void testSave(){

        Performance p = performances.get(0);
        MerchandiseDao daoP = Mockito.mock(MerchandiseDao.class);
        articles.get(0).setAvailable(10);
        Mockito.when(daoP.save(articles.get(0))).thenReturn(articles.get(0));
        service.addDao(daoP);

        articles.get(0).setAvailable(9);
        try {
            assertTrue("article  save",service.updateArticle(articles.get(0)).getAvailable()==9);

            assertTrue("article update",service.updateArticle(articles.get(0)).getId()==articles.get(0).getId());
        } catch (ServiceException e) {
            assertTrue(false);
        }



    }

    @Test
    public void testSearchArticleID() {

        Performance p = performances.get(0);
        MerchandiseDao daoP = Mockito.mock(MerchandiseDao.class);
        articles.get(0).setAvailable(10);
        Mockito.when(daoP.save(articles.get(0))).thenReturn(articles.get(0));
        service.addDao(daoP);


        Mockito.when(daoP.findOne(1)).thenReturn(articles.get(0));
        service.addDao(daoP);

        try {
           assertTrue(service.searchArticleID(1).getAvailable()==10);
        } catch (ServiceException e) {
            fail("Exception");
        }


    }

}
