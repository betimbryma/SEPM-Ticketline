package at.ac.tuwien.inso.ticketline.server.unittest.rest;

import at.ac.tuwien.inso.ticketline.dto.CustomerDto;
import at.ac.tuwien.inso.ticketline.model.Address;
import at.ac.tuwien.inso.ticketline.model.Customer;
import at.ac.tuwien.inso.ticketline.model.Gender;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.server.rest.CustomerController;
import at.ac.tuwien.inso.ticketline.server.service.CustomerService;
import at.ac.tuwien.inso.ticketline.server.service.implementation.CustomerServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.io.IOException;
import java.util.Arrays;

import static junit.framework.TestCase.fail;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Created by Jakob on 18.06.2016.
 */

public class CustomerRESTTest {

    private MockMvc mockMvc;

    @Autowired
    private CustomerService customerServiceMock;

    @Before
    public void setUp() {
        customerServiceMock = Mockito.mock(CustomerServiceImpl.class);

        CustomerController custContr = new CustomerController();
        custContr.setCustomerService(customerServiceMock);

        this.mockMvc = MockMvcBuilders.standaloneSetup(custContr).build();
    }

    @Test
    public void findAllCustomers() throws ServiceException {

        Customer cust1 = new Customer();
        cust1.setId(1);
        cust1.setFirstname("Testfirstname1");
        cust1.setGender(Gender.FEMALE);

        Customer cust2 = new Customer();
        cust2.setId(2);
        cust2.setFirstname("Testfirstname2");
        cust2.setGender(Gender.MALE);

        when(customerServiceMock.getAll()).thenReturn(Arrays.asList(cust1, cust2));

        try {
            mockMvc.perform(get("/customer/"))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType("application/json"))
                    .andExpect(jsonPath("$", hasSize(2)))
                    .andExpect(jsonPath("$[0].firstname", is("Testfirstname1")))
                    .andExpect(jsonPath("$[1].firstname", is("Testfirstname2")));
        } catch (Exception e) {
            e.printStackTrace();
            fail("An Exception has been thrown while executing getAll");
        }


    }

    @Test
    public void findCustomerByID() throws ServiceException {

        Customer cust1 = new Customer();
        cust1.setId(1);
        cust1.setFirstname("Testfirstname1");
        cust1.setGender(Gender.FEMALE);

        when(customerServiceMock.getByID(new Integer(1))).thenReturn(cust1);

        try {
            mockMvc.perform(get("/customer/{id}", 1)
                    .contentType(TestUtils.APPLICATION_JSON_UTF8))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType("application/json"))
                    .andExpect(jsonPath("firstname", is("Testfirstname1")))
                    .andExpect(jsonPath("id", is(1)));
        } catch (Exception e) {
            e.printStackTrace();
            fail("An Exception has been thrown while executing getAll");
        }


    }

    @Test
    public void saveCustomer() throws ServiceException {

        CustomerDto custDto = new CustomerDto();
        custDto.setFirstname("Testfirstname");
        custDto.setLastname("Testlastname");
        custDto.setPostalcode("666");
        custDto.setGender("female");

        Customer cust = new Customer();
        cust.setId(1);
        cust.setFirstname("Testfirstname");
        cust.setLastname("Testlastname");
        cust.setGender(Gender.FEMALE);
        Address a = new Address();
        a.setPostalCode("666");
        cust.setAddress(a);

        when(customerServiceMock.save(Matchers.any(Customer.class))).thenReturn(cust);

        try {
            mockMvc.perform(post("/customer/")
                    .contentType(TestUtils.APPLICATION_JSON_UTF8)
                    .content(TestUtils.convertObjectToJsonBytes(custDto))
            )
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$type", is("SUCCESS")))
                    .andExpect(jsonPath("$text", is("1")));
        }
        catch (IOException e) {
            fail("IOException has been thrown when converting DTO to JSON");
        }
        catch (Exception e) {
            e.printStackTrace();
            fail();
        }

    }

    @Test
    public void saveInvalidCustomer() throws ServiceException {

        CustomerDto custDto = new CustomerDto();

        try {
            mockMvc.perform(post("/customer/")
                    .contentType(TestUtils.APPLICATION_JSON_UTF8)
                    .content(TestUtils.convertObjectToJsonBytes(custDto))
            )
                    .andExpect(status().is4xxClientError());

        }
        catch (IOException e) {
            fail("IOException has been thrown when converting DTO to JSON");
        }
        catch (Exception e) {
            e.printStackTrace();
            fail();
        }

    }

    @Test
    public void editCustomer() throws ServiceException {

        CustomerDto custDto = new CustomerDto();
        custDto.setId(1);
        custDto.setFirstname("Testfirstname");
        custDto.setLastname("Testlastname");
        custDto.setPostalcode("666");
        custDto.setGender("female");

        Customer cust = new Customer();
        cust.setId(1);
        cust.setFirstname("Testfirstname");
        cust.setLastname("Testlastname");
        cust.setGender(Gender.FEMALE);
        Address a = new Address();
        a.setPostalCode("666");
        cust.setAddress(a);

        when(customerServiceMock.edit(Matchers.any(Customer.class))).thenReturn(cust);

        try {
            mockMvc.perform(post("/customer/{id}", 1)
                    .contentType(TestUtils.APPLICATION_JSON_UTF8)
                    .content(TestUtils.convertObjectToJsonBytes(custDto))
            )
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$type", is("SUCCESS")));
        }
        catch (IOException e) {
            fail("IOException has been thrown when converting DTO to JSON");
        }
        catch (Exception e) {
            e.printStackTrace();
            fail();
        }

    }

    @Test
    public void editInvalidCustomer() throws ServiceException {

        CustomerDto custDto = new CustomerDto();

        // no first name => invalid!
        //custDto.setFirstname("Testfirstname");

        custDto.setLastname("Testlastname");
        custDto.setPostalcode("666");
        custDto.setGender("female");


        try {
            mockMvc.perform(post("/customer/{id}", 1)
                    .contentType(TestUtils.APPLICATION_JSON_UTF8)
                    .content(TestUtils.convertObjectToJsonBytes(custDto))
            )
                    .andExpect(status().is4xxClientError());
        }
        catch (IOException e) {
            fail("IOException has been thrown when converting DTO to JSON");
        }
        catch (Exception e) {
            e.printStackTrace();
            fail();
        }

    }

}
