package at.ac.tuwien.inso.ticketline.server.integrationtest.service;

import at.ac.tuwien.inso.ticketline.model.*;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.server.service.TestService;
import at.ac.tuwien.inso.ticketline.server.service.implementation.TicketServiceImpl;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TicketServiceIntegrationTest extends AbstractServiceIntegrationTest{
    @Autowired
    private TicketServiceImpl ticketService;
    private List<Ticket> tickets;
    private Row row1,row2;
    private Category vip;
    private Gallery gallery1,gallery2,gallery3,gallery4;
    private Show show;

    @Autowired
    private TestService testService;


    @Test
    public void setUp() throws ServiceException {


            Category vip = new Category();
            vip.setDescription("This category of tickets is meant for VIP");
            vip.setName("VIP");
        if(vip == null)
            System.out.println("null");
            vip = testService.categorySave(vip);


            Row row1 = new Row();
            row1.setDescription("The first row");
            row1.setName("#1Row");
            row1.setOrder(1);

            Row row2 = new Row();
            row2.setDescription("The second row");
            row2.setName("#2Row");
            row2.setOrder(2);

            row1 = testService.rowSave(row1);
            row2 = testService.rowSave(row2);
            List<Row> rows = new ArrayList<>();
            rows.add(row1);
            rows.add(row2);
            Gallery gallery1 = new Gallery();
            gallery1.setName("#1");
            gallery1.setDescription("First column");
            gallery1.setOrder(1);

            Gallery gallery2 = new Gallery();
            gallery2.setName("#2");
            gallery2.setDescription("Second column");
            gallery2.setOrder(2);

            Gallery gallery3 = new Gallery();
            gallery3.setName("#3");
            gallery3.setDescription("Third column");
            gallery3.setOrder(3);

            Gallery gallery4 = new Gallery();
            gallery4.setName("#4");
            gallery4.setDescription("Fourth column");
            gallery4.setOrder(4);
            gallery1 = testService.gallerySave(gallery1);
            gallery2 = testService.gallerySave(gallery2);
            gallery3 = testService.gallerySave(gallery3);
            gallery4 = testService.gallerySave(gallery4);
            List<Gallery> gallerys = new ArrayList<>();
            gallerys.add(gallery1);
            gallerys.add(gallery2);
            gallerys.add(gallery3);
            gallerys.add(gallery4);

            Location l = new Location();

            l.setName("Tokyo");
            l.setDescription("Capital city of Japan");
            l.setAddress(new Address("1-5-2 Higashi-Shimbashi ", "105-7123", "Tokyo", "Japan"));
            l.setOwner("Yoichi Masuzoe");
            l=testService.locationSave(l);
            Room room = new Room("Saitama Super Arena", "multi-purpose indoor arena located in Chūō-ku", l);
            room = testService.roomSave(room);
            Performance performance = new Performance("Epson Aqua Park Shinagawa", "Great place to go on a date, with friends or family.", 5, PerformanceType.OPER);
            performance = testService.performanceSave(performance);
            Show show = new Show(false, new Date(500), room, performance);
            show = testService.showSave(show);

            for(int i = 0; i<8;i++){
                Seat seat = new Seat("po", "jo", i, room, vip, rows.get(i%2), gallerys.get(i%4));
                seat = testService.seatSave(seat);
                Ticket ticket = new Ticket("Shinagawa", 70, show, seat);
                ticket = testService.ticketSave(ticket);
            }

            List<Ticket> lista = ticketService.getTicketsByShow(1);
          //  assertThat(lista.size(),is(8));

    }



}
