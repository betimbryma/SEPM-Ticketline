package at.ac.tuwien.inso.ticketline.server.unittest.service;

import at.ac.tuwien.inso.ticketline.dao.LocationDao;
import at.ac.tuwien.inso.ticketline.model.Location;
import at.ac.tuwien.inso.ticketline.server.TestUtils;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.server.service.implementation.LocationServiceImpl;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * Created by Florian on 06.06.2016.
 */
public class LocationServiceTest {
    private static final int amount = 9;
    private static LocationServiceImpl locationService;
    private static List<Location> locations;

    @BeforeClass
    public static void setUp() {
        locationService = new LocationServiceImpl();
        locations = new ArrayList<>();

        for (int i = 0; i < amount; i++) {
            locations.add(TestUtils.generateLocation(i));
        }
    }

    @Test
    public void getAllTest() {
        LocationDao dao = Mockito.mock(LocationDao.class);
        Mockito.when(dao.findAll()).thenReturn(locations);
        locationService.setLocationDao(dao);

        try {
            List<Location> allLocations = locationService.getAll();
            assertEquals(amount, allLocations.size());

            for(Location l : allLocations) {
                Location l2 = locations.get(l.getId());

                assertEquals(l2.getDescription(), l.getDescription());
                assertEquals(l2.getName(), l.getName());
                assertEquals(l2.getAddress(), l.getAddress());
                assertEquals(l2.getOwner(), l.getOwner());
            }

        } catch (ServiceException e) {
            fail("A service exception was thrown");
        }


    }
}
