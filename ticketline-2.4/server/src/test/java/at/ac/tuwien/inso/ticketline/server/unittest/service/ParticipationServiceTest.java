package at.ac.tuwien.inso.ticketline.server.unittest.service;

import at.ac.tuwien.inso.ticketline.dao.ParticipationDao;
import at.ac.tuwien.inso.ticketline.model.Artist;
import at.ac.tuwien.inso.ticketline.model.Participation;
import at.ac.tuwien.inso.ticketline.model.Performance;
import at.ac.tuwien.inso.ticketline.server.TestUtils;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.server.service.implementation.ParticipationServiceImpl;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class ParticipationServiceTest {

    private static final int amount = 9;
    private static ParticipationServiceImpl participationService;
    private static List<Participation> participationList;


    @BeforeClass
    public static void setUp() {
        participationService = new ParticipationServiceImpl();
        participationList = new ArrayList<>();

        Artist[] artists = new Artist[amount];
        Performance[] performances = new Performance[amount];

        for (int i = 0; i < amount; i++) {
            artists[i] = TestUtils.generateArtist(i);
            performances[i] = TestUtils.generatePerformance(i);
        }


        for (int i = 0; i < amount; i++) {
            Participation p = new Participation();
            p.setId(i);
            p.setDescription("Participation #" + i );
            p.setArtist(artists[i]);
            p.setArtistRole("Artist_Role #" + i );

            p.setPerformance(performances[i]);

            participationList.add(p);
        }

    }

    @Test
    public void getAllTest_shouldGetAllParticipations() {
        ParticipationDao dao = Mockito.mock(ParticipationDao.class);

        Mockito.when(dao.findAll()).thenReturn(participationList);
        participationService.setParticipationDao(dao);

        try {
            List<Participation> allParticipationList = participationService.getAll();
            assertEquals(amount, allParticipationList.size());

            for (Participation p : allParticipationList) {
                Participation p2 = participationList.get(p.getId());
                assertEquals(p2.getArtist(), p.getArtist());
            }

        } catch (ServiceException e) {
            fail("A service exception was thrown");
        }
    }
}