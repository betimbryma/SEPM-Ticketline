package at.ac.tuwien.inso.ticketline.server.integrationtest.service;

import at.ac.tuwien.inso.ticketline.model.Show;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.server.service.ShowService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * @author Michael Oppitz
 * @date 09.05.2016
 */
public class ShowServiceIntergrationTest extends AbstractServiceIntegrationTest {

    @Autowired
    private ShowService showService;

    @Test
    public void testGetAll() {
        try {
            List<Show> show = showService.getAll();
            assertEquals(0, show.size());
        } catch (ServiceException e) {
            fail("ServiceException thrown");
        }
    }
}
