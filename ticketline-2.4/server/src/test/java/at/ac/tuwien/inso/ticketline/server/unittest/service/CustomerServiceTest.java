package at.ac.tuwien.inso.ticketline.server.unittest.service;

import at.ac.tuwien.inso.ticketline.dao.CustomerDao;
import at.ac.tuwien.inso.ticketline.model.Customer;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.server.service.implementation.CustomerServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.ArrayList;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doAnswer;

public class CustomerServiceTest {

    private CustomerServiceImpl custService = null;
    private ArrayList<Customer> customers;

    @Before
    public void setUp() {
        custService = new CustomerServiceImpl();
        customers = new ArrayList<>();

        Customer customer = new Customer();
        customer.setFirstname("Testcustomer1");
        customer.setId(1);
        customers.add(customer);

        customer = new Customer();
        customer.setFirstname("Testcustomer2");
        customer.setId(2);
        customers.add(customer);

        customer = new Customer();
        customer.setFirstname("Testcustomer3");
        customer.setId(3);
        customers.add(customer);
    }

    @Test
    public void testGetAll() {
        CustomerDao dao = Mockito.mock(CustomerDao.class);
        Mockito.when(dao.findAll()).thenReturn(customers);
        custService.setCustomerDao(dao);

        try {
            assertEquals(3, custService.getAll().size());
        } catch (ServiceException e) {
            fail("ServiceException thrown");
        }
    }

    @Test
    public void testGetByID() {
        CustomerDao dao = Mockito.mock(CustomerDao.class);

        for(int i = 1; i < 4; i++) {

        }

        Mockito.when(dao.findOne(1)).thenReturn(customers.get(0));
        Mockito.when(dao.findOne(2)).thenReturn(customers.get(1));
        Mockito.when(dao.findOne(3)).thenReturn(customers.get(2));

        custService.setCustomerDao(dao);

        try {
            assertEquals(custService.getByID(1).getFirstname(), "Testcustomer1");
            assertEquals(custService.getByID(2).getFirstname(), "Testcustomer2");
            assertEquals(custService.getByID(3).getFirstname(), "Testcustomer3");
        } catch (ServiceException e) {
            fail("ServiceException thrown");
        }
    }

    @Test(expected = ServiceException.class)
    public void testGetByIDNegative() throws ServiceException{
        CustomerDao dao = Mockito.mock(CustomerDao.class);

        custService.setCustomerDao(dao);
        custService.getByID(-1);
    }

    @Test
    public void testSave() {
        CustomerDao dao = Mockito.mock(CustomerDao.class);

        doAnswer(new Answer<Customer>() {
            @Override
            public Customer answer(InvocationOnMock invocation) {
                Customer customer = (Customer) invocation.getArguments()[0];
                customer.setId(customers.size()+1);
                customers.add(customer);
                return customer;
            }
        }).when(dao).save(any(Customer.class));

        custService.setCustomerDao(dao);

        try {

            Customer new1 = new Customer();
            Customer new2 = new Customer();

            assertTrue(custService.save(new1).getId() == 4);
            assertTrue(custService.save(new2).getId() == 5);
            assertTrue(custService.save(new2).getId() == 6);
        } catch (ServiceException e) {
            fail("Service Exception thrown");
        }
    }

    @Test(expected = ServiceException.class)
    public void testSaveNull() throws ServiceException {
        CustomerDao dao = Mockito.mock(CustomerDao.class);
        custService.setCustomerDao(dao);
        custService.save(null);
    }

    @Test
    public void testEdit() {
        CustomerDao dao = Mockito.mock(CustomerDao.class);

        doAnswer(new Answer<Customer>() {
            @Override
            public Customer answer(InvocationOnMock invocation) {
                Customer customer = (Customer) invocation.getArguments()[0];
                customers.set(customer.getId()-1, customer);
                return customer;
            }
        }).when(dao).save(any(Customer.class));

        Mockito.when(dao.findAll()).thenReturn(customers);

        custService.setCustomerDao(dao);

        try {

            Customer toEdit = custService.getAll().get(0);

            assertTrue(toEdit.getId() == 1);
            assertTrue(toEdit.getFirstname() == "Testcustomer1");

            toEdit.setFirstname("ChangedFirstname");
            custService.edit(toEdit);

            Customer edited = custService.getAll().get(0);

            assertTrue(edited.getId() == 1);
            assertTrue(edited.getFirstname() == "ChangedFirstname");
        } catch (ServiceException e) {
            fail("Service Exception thrown");
        }
    }

    @Test(expected = ServiceException.class)
    public void testEditNull() throws ServiceException {
        CustomerDao dao = Mockito.mock(CustomerDao.class);
        custService.setCustomerDao(dao);
        custService.edit(null);
    }

}
