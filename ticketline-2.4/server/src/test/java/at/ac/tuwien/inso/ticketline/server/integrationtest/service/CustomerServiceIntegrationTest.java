package at.ac.tuwien.inso.ticketline.server.integrationtest.service;

import at.ac.tuwien.inso.ticketline.model.Address;
import at.ac.tuwien.inso.ticketline.model.Customer;
import at.ac.tuwien.inso.ticketline.model.Gender;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.server.service.CustomerService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.Assert.*;

public class CustomerServiceIntegrationTest extends AbstractServiceIntegrationTest {

    @Autowired
    private CustomerService service;


    @Test
    public void testSaveCustomer() {
        try {

            Customer c = new Customer();
            c.setFirstname("Testfirstname");
            c.setLastname("Testlastname");

            Address a = new Address();
            a.setStreet("Teststreet");
            a.setPostalCode("Testpostalcode");
            a.setCity("Testcity");
            a.setCountry("Testcountry");

            c.setAddress(a);
            c.setGender(Gender.MALE);

            c = service.save(c);
            assertTrue(c.getId() >= 1);

            Customer c2 = service.getByID(c.getId());

            assertTrue(c.getFirstname().equals(c2.getFirstname()));
            assertTrue(c.getLastname().equals(c2.getLastname()));
            assertTrue(c.getAddress().equals(c2.getAddress()));
            assertTrue(c.getGender() == c2.getGender());

        } catch (ServiceException e) {
            fail("ServiceException thrown");
        }
    }

    @Test(expected = ServiceException.class)
    public void testSaveCustomerNull() throws ServiceException {
        service.save(null);
    }

    @Test(expected = ServiceException.class)
    public void testSaveCustomerNoName() throws ServiceException {
        service.save(new Customer());
    }

    @Test
    public void editCustomerFirstName() throws ServiceException {

        Customer customerToEdit;
        List<Customer> customers = service.getAll();
        if(customers.size()>0)
            customerToEdit=customers.get(0);
        else {
            customerToEdit = new Customer();
            customerToEdit.setFirstname("Editfirstname");
            customerToEdit.setLastname("Editlastname");

            Address a = new Address();
            a.setStreet("Editstreet");
            a.setPostalCode("Editpostalcode");
            a.setCity("Editcity");
            a.setCountry("Editcountry");

            customerToEdit.setAddress(a);
            customerToEdit.setGender(Gender.MALE);

            customerToEdit = service.save(customerToEdit);

        }
        Integer ID = customerToEdit.getId();
        Customer customer = service.getByID(ID);
        customer.setFirstname("editCustomerFirstName");
        service.edit(customer);
        customer = service.getByID(ID);
        assertEquals("editCustomerFirstName",customer.getFirstname());
    }

    @Test
    public void editCustomerLastName() throws ServiceException {

        Customer customerToEdit;
        List<Customer> customers = service.getAll();
        if(customers.size()>0)
            customerToEdit=customers.get(0);
        else {
            customerToEdit = new Customer();
            customerToEdit.setFirstname("Editfirstname");
            customerToEdit.setLastname("Editlastname");

            Address a = new Address();
            a.setStreet("Editstreet");
            a.setPostalCode("Editpostalcode");
            a.setCity("Editcity");
            a.setCountry("Editcountry");

            customerToEdit.setAddress(a);
            customerToEdit.setGender(Gender.MALE);

            customerToEdit = service.save(customerToEdit);

        }
        Integer ID = customerToEdit.getId();
        Customer customer = service.getByID(ID);
        customer.setLastname("editCustomerLastName");
        service.edit(customer);
        customer = service.getByID(ID);
        assertEquals("editCustomerLastName",customer.getLastname());
    }

    @Test
    public void editCustomerStreet() throws ServiceException {

        Customer customerToEdit;
        List<Customer> customers = service.getAll();
        if(customers.size()>0)
            customerToEdit=customers.get(0);
        else {
            customerToEdit = new Customer();
            customerToEdit.setFirstname("Editfirstname");
            customerToEdit.setLastname("Editlastname");

            Address a = new Address();
            a.setStreet("Editstreet");
            a.setPostalCode("Editpostalcode");
            a.setCity("Editcity");
            a.setCountry("Editcountry");

            customerToEdit.setAddress(a);
            customerToEdit.setGender(Gender.MALE);

            customerToEdit = service.save(customerToEdit);

        }
        Integer ID = customerToEdit.getId();
        Customer customer = service.getByID(ID);
        Address address = customer.getAddress();
        address.setStreet("editCustomerStreet");
        customer.setAddress(address);
        service.edit(customer);
        customer = service.getByID(ID);
        address = customer.getAddress();
        assertEquals("editCustomerStreet",address.getStreet());
}

    @Test
    public void editCustomerCity() throws ServiceException {

        Customer customerToEdit;
        List<Customer> customers = service.getAll();
        if(customers.size()>0)
            customerToEdit=customers.get(0);
        else {
            customerToEdit = new Customer();
            customerToEdit.setFirstname("Editfirstname");
            customerToEdit.setLastname("Editlastname");

            Address a = new Address();
            a.setStreet("Editstreet");
            a.setPostalCode("Editpostalcode");
            a.setCity("Editcity");
            a.setCountry("Editcountry");

            customerToEdit.setAddress(a);
            customerToEdit.setGender(Gender.MALE);

            customerToEdit = service.save(customerToEdit);

        }
        Integer ID = customerToEdit.getId();
        Customer customer = service.getByID(ID);
        Address address = customer.getAddress();
        address.setCity("editCustomerCity");
        customer.setAddress(address);
        service.edit(customer);
        customer = service.getByID(ID);
        address = customer.getAddress();
        assertEquals("editCustomerCity",address.getCity());
    }

    @Test(expected = ServiceException.class)
    public void editNullCustomer() throws ServiceException {
        service.edit(null);
    }

}
