package at.ac.tuwien.inso.ticketline.server.integrationtest.service;

import at.ac.tuwien.inso.ticketline.model.Premium;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.server.service.PremiumService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Created by eni on 04.06.2016.
 */
public class PremiumServiceIntegrationTest  extends AbstractServiceIntegrationTest  {

    @Autowired
    private PremiumService premiumService;

    @Before
    public void before(){
        for (int i = 0;i<5; i++ ){
            Premium p = new Premium();
            p.setPrice(234);
            p.setAvailable(4);
            p.setDescription("sdfgsdfg");
            p.setName("safgaf");
            try {
                premiumService.updatePremium(p);
            } catch (ServiceException e) {
                fail("");
            }
        }
    }


    @Test
    public void testGetAll(){



        try {
            assertTrue("Thera are 55 poremiums", 5==premiumService.getAll().size());
        } catch (ServiceException e) {
            fail("");
        }
    }
    @Test
     public  void testSearchPremiumById(){

        Premium p = new Premium();
        p.setPrice(666);
        p.setAvailable(4);
        p.setDescription("sdfgsdfg");
        p.setName("safgaf");
        try {
            Premium a = premiumService.updatePremium(p);
            assertTrue(a.getAvailable()==premiumService.searchPremiumID(a.getId()).getAvailable());
        } catch (ServiceException e) {
           fail("fail exception");
        }
    }






    /**
     * getAll premium
     * @return
     * @throws ServiceException
     *

    List<Premium> getAll() throws ServiceException;
    /**
     * Search Premium Id
     * @param id
     * @return
     * @throws ServiceException

    Premium searchPremiumID(int id) throws  ServiceException;


    /**
     * Update this Premium
     * @param premium
     * @return
     * @throws ServiceException

    Premium updatePremium(Premium premium) throws  ServiceException;
*/
}
