package at.ac.tuwien.inso.ticketline.server.unittest.service;

import at.ac.tuwien.inso.ticketline.dao.PerformanceDao;
import at.ac.tuwien.inso.ticketline.model.Performance;
import at.ac.tuwien.inso.ticketline.server.TestUtils;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.server.service.implementation.PerformanceServiceImpl;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class PerformanceServiceTest {

    private static final int amount = 6;
    private static PerformanceServiceImpl performanceService;
    private static List<Performance> performances;

    @BeforeClass
    public static void setUp() {
        performanceService = new PerformanceServiceImpl();
        performances = new ArrayList<>();

        for (int i = 0; i < amount; i++) {
           performances.add(TestUtils.generatePerformance(i));
        }

    }

    @Test
    public void getAllTest() {
        PerformanceDao dao = Mockito.mock(PerformanceDao.class);
        Mockito.when(dao.findAll()).thenReturn(performances);
        performanceService.setCustomerDao(dao);

        try {
            List<Performance> allList = performanceService.getAll();
            assertEquals(amount, allList.size());

            for(Performance p : allList) {
                assertEquals(performances.get(p.getId()).getName(), p.getName());
            }

        } catch (ServiceException e) {
            fail("Service Exception was thrown");
        }

    }
}