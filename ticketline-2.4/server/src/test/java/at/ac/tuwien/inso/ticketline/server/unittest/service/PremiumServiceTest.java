package at.ac.tuwien.inso.ticketline.server.unittest.service;

import at.ac.tuwien.inso.ticketline.dao.PremiumDao;
import at.ac.tuwien.inso.ticketline.model.Premium;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.server.service.implementation.PremiumServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Created by eni on 04.06.2016.
 */
public class PremiumServiceTest {


    List<Premium> premiumList = null;

    private PremiumServiceImpl premiumService;

    @Before
    public void setUp(){

        premiumService= new PremiumServiceImpl();
        premiumList = new ArrayList<>();
        for (int i = 0;i<5; i++ ){
            Premium p = new Premium();
            p.setPrice(234);
            p.setAvailable(4);
            p.setDescription("sdfgsdfg");
            p.setName("safgaf");
            premiumList.add(p);
        }
    }
    @Test
    public void testGetAll(){
        PremiumDao dao = Mockito.mock(PremiumDao.class);
        Mockito.when(dao.findAll()).thenReturn(premiumList);
        premiumService.add(dao);

        try {
            assertTrue(premiumService.getAll().size()==5);
        } catch (ServiceException e) {
            fail("fail Exception");
        }
    }

    @Test //test updatePremium and searchPremium
    public void testSearchPremiumID(){
        PremiumDao dao = Mockito.mock(PremiumDao.class);
        Mockito.when(dao.findOne(1)).thenReturn(premiumList.get(0));
        Mockito.when(dao.save(premiumList.get(0))).thenReturn(premiumList.get(0));
        premiumService.add(dao);

        try {
            premiumList.get(0).setName("Test");
            premiumService.updatePremium(premiumList.get(0));
            assertTrue(premiumService.searchPremiumID(1).getName().equals(premiumList.get(0).getName()));
        } catch (ServiceException e) {
            fail("fail Exception");
        }
    }
}
