package at.ac.tuwien.inso.ticketline.server.unittest.service;

import at.ac.tuwien.inso.ticketline.dao.*;
import at.ac.tuwien.inso.ticketline.model.*;
import at.ac.tuwien.inso.ticketline.server.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.server.service.implementation.CustomerServiceImpl;
import at.ac.tuwien.inso.ticketline.server.service.implementation.SellServiceImpl;
import at.ac.tuwien.inso.ticketline.server.service.implementation.TicketServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Created by eni on 04.06.2016.
 */
public class SellServiceTest {

    private TicketServiceImpl ticketService;
    private List<Ticket> tickets;
    private List<Ticket> tickets1;
    private List<TicketIdentifier> ticketIdentifiers;
    private List<TicketIdentifier> ticketIdentifiers2;
    private Row row1,row2,row3,row4,row5,row6,row7,row8,row9,row10;
    private Category vip, specialGuest, normal;
    private Gallery gallery1,gallery2,gallery3,gallery4,gallery5,gallery6,gallery7;
    private Show show, show1;
    @Mock
    private TicketDao ticketDao;
    @Mock
    private ShowDao showDao;
    @Mock
    private TicketIdentifierDao ticketIdentifierDao;



    @Mock
    private EmployeeDao employeeDao;
    @Mock
    private MerchandiseDao merchandiseDao;
    @Mock
    private CustomerDao customerDao;

    private ReceiptDao receiptDao;
    @Mock
    private ReceiptEntryDao receiptEntryDao;
    @Mock
    private CustomerServiceImpl customerServiceImpl;


    private SellServiceImpl sellServiceImpl;


    @Before
    public void setUp(){



        MockitoAnnotations.initMocks(this);
        ticketService = new TicketServiceImpl();
        ticketIdentifiers = new ArrayList<>();
        ticketIdentifiers2=new ArrayList<>();
        ticketService.setTicketDao(ticketDao);
        ticketService.setShowDao(showDao);
        ticketService.setTicketIdentifierDao(ticketIdentifierDao);
        sellServiceImpl = new SellServiceImpl();
        sellServiceImpl.setEmployeeDao(employeeDao);
        sellServiceImpl.setMerchandiseDao(merchandiseDao);
        sellServiceImpl.setReceiptDao(receiptDao);
        customerServiceImpl.setCustomerDao(customerDao);
        sellServiceImpl.setReceiptEntryDao(receiptEntryDao);
        tickets = new ArrayList<>();
        tickets1 = new ArrayList<>();
        vip = new Category();
        vip.setDescription("This category of tickets is meant for VIP");
        vip.setName("VIP");


        specialGuest = new Category();
        specialGuest.setDescription("This category of tickets is meant for special guests");
        specialGuest.setName("Special guest");


        normal = new Category();
        normal.setDescription("This category of tickets is meant for normal attendees");
        normal.setName("Normal");

        row1 = new Row();
        row1.setDescription("The first row");
        row1.setName("#1Row");
        row1.setOrder(1);

        row2 = new Row();
        row2.setDescription("The second row");
        row2.setName("#2Row");
        row2.setOrder(2);

        row3 = new Row();
        row3.setDescription("The third row");
        row3.setName("#3Row");
        row3.setOrder(3);

        row4 = new Row();
        row4.setDescription("The fourth row");
        row4.setName("#4Row");
        row4.setOrder(4);

        row5 = new Row();
        row5.setDescription("The fifth row");
        row5.setName("#5Row");
        row5.setOrder(5);

        row6 = new Row();
        row6.setDescription("The sixth row");
        row6.setName("#6Row");
        row6.setOrder(6);

        row7 = new Row();
        row7.setDescription("The seventh row");
        row7.setName("#7Row");
        row7.setOrder(7);

        row8 = new Row();
        row8.setDescription("The eighth row");
        row8.setName("#8Row");
        row8.setOrder(8);

        row9 = new Row();
        row9.setDescription("The ninth row");
        row9.setName("#9Row");
        row9.setOrder(9);

        row10 = new Row();
        row10.setDescription("The tenth row");
        row10.setName("#10Row");
        row10.setOrder(10);

        gallery1 = new Gallery();
        gallery1.setName("#1");
        gallery1.setDescription("First column");
        gallery1.setOrder(1);

        gallery2 = new Gallery();
        gallery2.setName("#2");
        gallery2.setDescription("Second column");
        gallery2.setOrder(2);

        gallery3 = new Gallery();
        gallery3.setName("#3");
        gallery3.setDescription("Third column");
        gallery3.setOrder(3);

        gallery4 = new Gallery();
        gallery4.setName("#4");
        gallery4.setDescription("Fourth column");
        gallery4.setOrder(4);

        gallery5 = new Gallery();
        gallery5.setName("#5");
        gallery5.setDescription("Fifth column");
        gallery5.setOrder(5);

        gallery6 = new Gallery();
        gallery6.setName("#6");
        gallery6.setDescription("Sixth column");
        gallery6.setOrder(6);

        gallery7 = new Gallery();
        gallery7.setName("#7");
        gallery7.setDescription("Seventh column");
        gallery7.setOrder(7);

        Location l = new Location();

        l.setName("Tokyo");
        l.setDescription("Capital city of Japan");
        l.setAddress(new Address("1-5-2 Higashi-Shimbashi ", "105-7123", "Tokyo", "Japan"));
        l.setOwner("Yoichi Masuzoe");

        Location l1 = new Location();

        l.setName("Seoul");
        l.setDescription("Capital city of South Korea");
        l.setAddress(new Address("30 Eulji-ro, Jung-gu", "04533", "Seoul", "South Korea"));

        l.setOwner("Park Won-soon");

        Room room = new Room("Saitama Super Arena", "multi-purpose indoor arena located in Chūō-ku", l);
        Room room1 = new Room("Olympic Gymnastics Arena", "an indoor sports arena, located within the Olympic Park, in Bangi-dong", l1);

        Performance performance = new Performance("Epson Aqua Park Shinagawa", "Great place to go on a date, with friends or family.", 5, PerformanceType.OPER);
        Performance performance1 = new Performance("Yesung", "Yesung", 2, PerformanceType.CONCERT);

        show = new Show(false, new Date(500), room, performance);
        show.setId(1);
        show1 = new Show(false, new Date(500), room1, performance1);
        show.setId(2);
        List<Seat> seats = generateSeats(room);
        List<Seat> seats1 = generateSeats(room1);
        int id = 0;
        for(int i =0; i<seats.size();i++){
            Seat seat = seats.get(i);
            Ticket ticket = new Ticket("Epson Aqua Park Shinagawa", 80, show, seat);
            ticket.setId(++id);
            tickets.add(ticket);

            TicketIdentifier ticketIdentifier = new TicketIdentifier();
            ticketIdentifier.setValid(true);
            ticketIdentifier.setTicket(ticket);
            ticketIdentifier.setId(i+1);
            ticketIdentifiers.add(ticketIdentifier);
        }

        for(int i =0; i<seats1.size();i++){
            Seat seat = seats1.get(i);
            Ticket ticket = new Ticket("Yesung", 90, show1, seat);
            ticket.setId(++id);
            tickets1.add(ticket);

            TicketIdentifier ticketIdentifier = new TicketIdentifier();
            ticketIdentifier.setValid(true);
            ticketIdentifier.setTicket(ticket);
            ticketIdentifier.setId(i+1);
            ticketIdentifiers2.add(ticketIdentifier);
        }


    }

    private List<Seat> generateSeats(Room room){
        List<Seat> seatList = new ArrayList<>();
        int coulumn = 0;
        for (int i = 1; i <= 70; i++) {
            coulumn++;
            if (coulumn == 8)
                coulumn = 1;
            Seat seat = new Seat();
            seat.setName("Seat #" + i);
            seat.setDescription("Seat nr." + i + " located in" + room.getName());
            if (i <= 28) {
                seat.setCategory(specialGuest);
                if (i <= 7)
                    seat.setRow(row1);
                else if (i > 7 && i <= 14)
                    seat.setRow(row2);
                else if (i > 14 && i <= 21)
                    seat.setRow(row3);
                else
                    seat.setRow(row4);
            } else if ((28 < i) && (i <= 49)) {
                seat.setCategory(vip);
                if (i <= 35)
                    seat.setRow(row5);
                else if (i > 35 && i <= 42)
                    seat.setRow(row6);
                else
                    seat.setRow(row7);
            } else {
                seat.setCategory(normal);
                if (i > 49 && i <= 56)
                    seat.setRow(row8);
                else if (i > 56 && i <= 63)
                    seat.setRow(row9);
                else
                    seat.setRow(row10);
            }
            seat.setRoom(room);

            switch (coulumn) {
                case 1:
                    seat.setGallery(gallery1);
                    break;
                case 2:
                    seat.setGallery(gallery2);
                    break;
                case 3:
                    seat.setGallery(gallery3);
                    break;
                case 4:
                    seat.setGallery(gallery4);
                    break;
                case 5:
                    seat.setGallery(gallery5);
                    break;
                case 6:
                    seat.setGallery(gallery6);
                    break;
                default:
                    seat.setGallery(gallery7);
            }
            seat.setOrder(i);
            seatList.add(seat);
        }
        return seatList;
    }



    @Test
    public void testSearchArticleID() {

        Employee employee = new Employee();
        employee.setId(1);
        employee.setFirstname("Testfirstname");
        employee.setLastname("Testlastname");
        Address aa = new Address();
        aa.setStreet("Teststreet");
        aa.setPostalCode("Testpostalcode");
        aa.setCity("Testcity");
        aa.setCountry("Testcountry");
        employee.setAddress(aa);
        employee.setGender(Gender.MALE);
        employee.setUsername("admin");
        employee.setPasswordHash("sdafadfasdf");

        Customer c = new Customer();
        c.setId(1);
        c.setFirstname("Testfirstname");
        c.setLastname("Testlastname");

        Address a = new Address();
        a.setStreet("Teststreet");
        a.setPostalCode("Testpostalcode");
        a.setCity("Testcity");
        a.setCountry("Testcountry");

        c.setAddress(a);
        c.setGender(Gender.MALE);


        Cash methodOfPayment = new Cash();
        methodOfPayment.setId(1);
        methodOfPayment.setDeleted(false);
        methodOfPayment.setCustomer(c);


        Receipt receipt = new Receipt();
        receipt.setId(1);
        receipt.setCustomer(c);
        receipt.setEmployee(employee);
        receipt.setMethodOfPayment(methodOfPayment);
        receipt.setTransactionState(TransactionState.PAID);

        Receipt receiptNew = new Receipt();
        receiptNew.setId(1);

        ReceiptDao daoP = Mockito.mock(ReceiptDao.class);
        Mockito.when(daoP.save(receipt)).thenReturn(receiptNew);
        sellServiceImpl.setReceiptDao(daoP);

        try {
            assertTrue(sellServiceImpl.saveReceipt(receipt).getId()==receipt.getId());
        } catch (ServiceException e) {
            fail("fail Exception");
        }



        /*
        MerchandiseDao daoP = Mockito.mock(MerchandiseDao.class);
        articles.get(0).setAvailable(10);
        Mockito.when(daoP.save(articles.get(0))).thenReturn(articles.get(0));
        service.addDao(daoP);


        Mockito.when(daoP.findOne(1)).thenReturn(articles.get(0));
        service.addDao(daoP);

        try {
            assertTrue(service.searchArticleID(1).getAvailable()==10);
        } catch (ServiceException e) {
            fail("Exception");
        }

        */
    }

}
