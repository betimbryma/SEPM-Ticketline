package at.ac.tuwien.inso.ticketline.client.gui.controller.tabs.search;

import at.ac.tuwien.inso.ticketline.client.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.client.gui.controller.MainController;
import at.ac.tuwien.inso.ticketline.client.gui.controller.tabs.search.artists.ArtistsController;
import at.ac.tuwien.inso.ticketline.client.gui.controller.tabs.search.events.EventsController;
import at.ac.tuwien.inso.ticketline.client.gui.controller.tabs.search.shows.ShowsController;
import at.ac.tuwien.inso.ticketline.client.gui.controller.tabs.search.topEvents.TopEventsController;
import at.ac.tuwien.inso.ticketline.client.gui.controller.tabs.search.venues.VenuesController;
import at.ac.tuwien.inso.ticketline.client.gui.controller.tabs.search.views.ArtistViewController;
import at.ac.tuwien.inso.ticketline.client.gui.controller.tabs.search.views.EventViewController;
import at.ac.tuwien.inso.ticketline.client.gui.controller.tabs.search.views.ShowViewController;
import at.ac.tuwien.inso.ticketline.client.gui.controller.tabs.search.views.VenueViewController;
import at.ac.tuwien.inso.ticketline.client.service.*;
import at.ac.tuwien.inso.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.ticketline.client.util.DialogUtil;
import at.ac.tuwien.inso.ticketline.client.util.SpringFxmlLoader;
import at.ac.tuwien.inso.ticketline.dto.*;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Accordion;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.net.URL;
import java.util.Calendar;
import java.util.Locale;
import java.util.ResourceBundle;

@Component
public class SearchController implements Initializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(SearchController.class);

    private TopEventsController topEventsController;
    private ArtistsController artistsController;
    private EventsController eventsController;
    private ShowsController showsController;
    private VenuesController venuesController;
    private ArtistViewController artistViewController;
    private ShowViewController showViewController;
    private EventViewController eventViewController;
    private VenueViewController venueViewController;
    @Autowired
    private SpringFxmlLoader springFxmlLoader;
    @Autowired
    private MainController mainController;
    @Autowired
    private ShowService showService;
    @Autowired
    private ParticipationService participationService;
    @Autowired
    private PerformanceService performanceService;
    @Autowired
    private RoomService roomService;
    @Autowired
    private LocationService locationService;
    @Autowired
    private ArtistService artistService;
    @Autowired
    private TicketService ticketService;
    @FXML
    private Accordion accordion;
    @FXML
    private TitledPane topEventsPane;
    @FXML
    private TitledPane artistsPane;
    @FXML
    private TitledPane venuesPane;
    @FXML
    private TitledPane eventsPane;
    @FXML
    private TitledPane showsPane;
    @FXML
    private AnchorPane artistViewPane;
    @FXML
    private AnchorPane showViewPane;
    @FXML
    private AnchorPane eventViewPane;
    @FXML
    private AnchorPane venueViewPane;
    @FXML
    private StackPane stackPane;
    private ObservableList<PerformanceDto> allPerformances;
    private ObservableList<ParticipationDto> allParticipations;
    private ObservableList<ShowDto> allShows;
    private ObservableList<RoomDto> allRooms;
    private ObservableList<LocationDto> allLocations;
    private ObservableList<ArtistDto> allArtists;
    private ObservableMap<ShowDto, Integer> allSoldTickets;
    private ObservableMap<ShowDto, Integer> allTicketsMinPrice;
    private ObservableList<MapEntryDto<ShowDto, Integer>> tempAllTicketsMinPrice;
    private ObservableMap<ShowDto, Integer> allTicketsMaxPrice;
    private ObservableList<MapEntryDto<ShowDto, Integer>> tempAllTicketsMaxPrice;
    private boolean stillLoading = true;
    private boolean initialized = false;

    /**
     * This method initializes the SearchController.
     *
     * @param location  location of the fxml file
     * @param resources resources
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        accordion.setExpandedPane(topEventsPane);
        initAccordion();
        getStackPane().getChildren().addListener(new ListChangeListener<Node>() {
            @Override
            public void onChanged(Change<? extends Node> c) {
                if (getStackPane().getChildren().contains(getAccordion())) {
                    if(!isStillLoading()) {
                        if (getAccordion().getExpandedPane() == topEventsPane) {
                            reloadTickets(topEventsController.getMonth(), topEventsController.getYear());
                            topEventsController.initChart();
                        }
                    }
                }
            }
        });
        reinitLocale(MainController.currentLocale);
    }

    /**
     * Starts the thread for loading all the data for searches.
     */
    public void startLoadingThread(){
        Task task = new Task() {
            @Override
            protected Object call() throws Exception {
                try {
                    allShows = FXCollections.observableList(showService.getAll());
                    allParticipations = FXCollections.observableList(participationService.getAll());
                    allPerformances = FXCollections.observableList(performanceService.getAll());
                    allRooms = FXCollections.observableList(roomService.getAll());
                    allLocations = FXCollections.observableList(locationService.getAll());
                    allArtists = FXCollections.observableList(artistService.getAll());
                    allSoldTickets = FXCollections.observableHashMap();
                    allTicketsMinPrice = FXCollections.observableHashMap();
                    allTicketsMaxPrice = FXCollections.observableHashMap();
                    tempAllTicketsMinPrice = FXCollections.observableList(ticketService.getMinPrices());
                    tempAllTicketsMaxPrice = FXCollections.observableList(ticketService.getMaxPrices());
                    for(int i = 0; i < tempAllTicketsMinPrice.size(); i++){
                        allTicketsMinPrice.put(tempAllTicketsMinPrice.get(i).getKey(), tempAllTicketsMinPrice.get(i).getValue());
                        allTicketsMaxPrice.put(tempAllTicketsMaxPrice.get(i).getKey(), tempAllTicketsMaxPrice.get(i).getValue());
                    }
                    setStillLoading(false);
                    mainController.getLoadingSymbol().setFill(Color.GREEN);
                    LOGGER.info("Finished loading resources for search tab.");
                } catch (ServiceException se) {
                    DialogUtil.showErrorDialog(BundleManager.getBundle().getString("search.ErrTitle"), BundleManager.getBundle().getString("search.ErrHeader"), BundleManager.getBundle().getString("search.ErrContent"));
                    return null;
                }
                return null;
            }
        };

        Thread thread = new Thread(task);
        thread.setPriority(Thread.MIN_PRIORITY);
        thread.start();
    }

    /**
     * Reinitializes the localization.
     *
     * @param newValue the new localization value
     */
    public void reinitLocale(Locale newValue) {
        showsController.reinitLocale(MainController.currentLocale);
        eventsController.reinitLocale(MainController.currentLocale);
        venuesController.reinitLocale(MainController.currentLocale);
        artistsController.reinitLocale(MainController.currentLocale);
        topEventsController.reinitLocale(MainController.currentLocale);
        LocaleContextHolder.setLocale(newValue);
        BundleManager.changeLocale(newValue);
        topEventsPane.setText(BundleManager.getBundle().getString("search.topevents"));
        artistsPane.setText(BundleManager.getBundle().getString("search.artists"));
        venuesPane.setText(BundleManager.getBundle().getString("search.venues"));
        eventsPane.setText(BundleManager.getBundle().getString("search.events"));
        showsPane.setText(BundleManager.getBundle().getString("search.shows"));
    }

    /**
     * Initializes the controllers after loading the data.
     */
    public void init(){
        artistsController.init();
        eventsController.init();
        showsController.init();
        topEventsController.init();
        venuesController.init();
    }

    /**
     * This method initializes all accordion entries.
     */
    private void initAccordion() {
        SpringFxmlLoader.LoadWrapper loadWrapper = springFxmlLoader.loadAndWrap("/gui/fxml/tabs/search/views/artist_shows_prototype.fxml");
        artistViewController = ((ArtistViewController) loadWrapper.getController());
        artistViewPane.getChildren().clear();
        artistViewPane = ((AnchorPane) loadWrapper.getLoadedObject());
        loadWrapper = springFxmlLoader.loadAndWrap("/gui/fxml/tabs/search/views/show_prototype.fxml");
        showViewController = ((ShowViewController) loadWrapper.getController());
        showViewPane = ((AnchorPane) loadWrapper.getLoadedObject());
        loadWrapper = springFxmlLoader.loadAndWrap("/gui/fxml/tabs/search/views/event_prototype.fxml");
        eventViewController = ((EventViewController) loadWrapper.getController());
        eventViewPane = ((AnchorPane) loadWrapper.getLoadedObject());
        loadWrapper = springFxmlLoader.loadAndWrap("/gui/fxml/tabs/search/views/venue_prototype.fxml");
        venueViewController = ((VenueViewController) loadWrapper.getController());
        venueViewPane = ((AnchorPane) loadWrapper.getLoadedObject());
        loadWrapper = springFxmlLoader.loadAndWrap("/gui/fxml/tabs/search/artists/artists.fxml");
        artistsController = ((ArtistsController) loadWrapper.getController());
        artistsPane.setContent((Node) loadWrapper.getLoadedObject());
        loadWrapper = springFxmlLoader.loadAndWrap("/gui/fxml/tabs/search/topEvents/topEvents.fxml");
        topEventsController = ((TopEventsController) loadWrapper.getController());
        topEventsPane.setContent((Node) loadWrapper.getLoadedObject());
        loadWrapper = springFxmlLoader.loadAndWrap("/gui/fxml/tabs/search/venues/venues.fxml");
        venuesController = ((VenuesController) loadWrapper.getController());
        venuesPane.setContent((Node) loadWrapper.getLoadedObject());
        loadWrapper = springFxmlLoader.loadAndWrap("/gui/fxml/tabs/search/events/events.fxml");
        eventsController = ((EventsController) loadWrapper.getController());
        eventsPane.setContent((Node) loadWrapper.getLoadedObject());
        loadWrapper = springFxmlLoader.loadAndWrap("/gui/fxml/tabs/search/shows/shows.fxml");
        showsController = ((ShowsController) loadWrapper.getController());
        showsPane.setContent((Node) loadWrapper.getLoadedObject());
        accordion.expandedPaneProperty().addListener(new ChangeListener<TitledPane>() {
            @Override
            public void changed(ObservableValue<? extends TitledPane> observable, TitledPane oldValue, TitledPane newValue) {
                if (newValue == topEventsPane && newValue != oldValue) {
                    reloadTickets(Calendar.getInstance().get(Calendar.MONTH)+1, Calendar.getInstance().get(Calendar.YEAR));
                    topEventsController.reset();
                }
                if (newValue == artistsPane && newValue != oldValue) {
                    artistsController.reset();
                }
                if (newValue == venuesPane && newValue != oldValue) {
                    venuesController.reset();
                }
                if (newValue == eventsPane && newValue != oldValue) {
                    eventsController.reset();
                }
                if (newValue == showsPane && newValue != oldValue) {
                    showsController.reset();
                }
            }
        });
    }

    /**
     * Reloads the ticket list.
     */
    public void reloadTickets(int month, int year) {
        try {
            allSoldTickets = FXCollections.observableHashMap();
            for (ShowDto s : allShows) {
                Calendar cal = Calendar.getInstance();
                cal.setTime(s.getDateOfPerformance());
                if(month == cal.get(Calendar.MONTH)+1 && year == cal.get(Calendar.YEAR)) {
                    allSoldTickets.put(s, ticketService.getSoldByShow(s.getId()));
                }
            }
        } catch (ServiceException se) {
            DialogUtil.showErrorDialog(BundleManager.getBundle().getString("search.ErrTitle"), BundleManager.getBundle().getString("search.ErrHeader"), BundleManager.getBundle().getString("search.ErrContent"));
            return;
        }
    }

    /**
     * This methods converts the performance type to the selected language.
     *
     * @param p performance
     * @return String with performance type
     */
    public String convertPerformanceType(PerformanceDto p) {
        String type = "";
        if (p.getPerformancetype().equals("MOVIE")) {
            type = BundleManager.getBundle().getString("eventsearch.typeMovie");
        } else if (p.getPerformancetype().equals("FESTIVAL")) {
            type = BundleManager.getBundle().getString("eventsearch.typeFestival");
        } else if (p.getPerformancetype().equals("CONCERT")) {
            type = BundleManager.getBundle().getString("eventsearch.typeConcert");
        } else if (p.getPerformancetype().equals("MUSICAL")) {
            type = BundleManager.getBundle().getString("eventsearch.typeMusical");
        } else if (p.getPerformancetype().equals("OPER")) {
            type = BundleManager.getBundle().getString("eventsearch.typeOper");
        } else if (p.getPerformancetype().equals("THEATER")) {
            type = BundleManager.getBundle().getString("eventsearch.typeTheater");
        }
        return type;
    }

    /**
     * This method resets the accordion and updates it's entries.
     */
    public void reset() {
        if(accordion.getExpandedPane() == topEventsPane) {
            reloadTickets(Calendar.getInstance().get(Calendar.MONTH) + 1, Calendar.getInstance().get(Calendar.YEAR));
        }
        stackPane.getChildren().clear();
        stackPane.getChildren().add(accordion);
        resetData();
    }

    /**
     * This method resets the data in all controllers.
     */
    public void resetData() {
        artistsController.reset();
        eventsController.reset();
        showsController.reset();
        venuesController.reset();
        topEventsController.reset();
        artistViewController.reset();
        eventViewController.reset();
        //showViewController.reset();
        venueViewController.reset();
    }

    public TopEventsController getTopEventsController() {
        return topEventsController;
    }

    public void setTopEventsController(TopEventsController topEventsController) {
        this.topEventsController = topEventsController;
    }

    public VenuesController getVenuesController() {
        return venuesController;
    }

    public void setVenuesController(VenuesController venuesController) {
        this.venuesController = venuesController;
    }

    public ArtistsController getArtistsController() {
        return artistsController;
    }

    public void setArtistsController(ArtistsController artistsController) {
        this.artistsController = artistsController;
    }

    public EventsController getEventsController() {
        return eventsController;
    }

    public void setEventsController(EventsController eventsController) {
        this.eventsController = eventsController;
    }

    public ShowsController getShowsController() {
        return showsController;
    }

    public void setShowsController(ShowsController showsController) {
        this.showsController = showsController;
    }

    public AnchorPane getArtistViewPane() {
        return artistViewPane;
    }

    public void setArtistViewPane(AnchorPane artistViewPane) {
        this.artistViewPane = artistViewPane;
    }

    public StackPane getStackPane() {
        return stackPane;
    }

    public void setStackPane(StackPane stackPane) {
        this.stackPane = stackPane;
    }

    public Accordion getAccordion() {
        return accordion;
    }

    public void setAccordion(Accordion accordion) {
        this.accordion = accordion;
    }

    public AnchorPane getShowViewPane() {
        return showViewPane;
    }

    public void setShowViewPane(AnchorPane showViewPane) {
        this.showViewPane = showViewPane;
    }

    public AnchorPane getEventViewPane() {
        return eventViewPane;
    }

    public void setEventViewPane(AnchorPane eventViewPane) {
        this.eventViewPane = eventViewPane;
    }

    public AnchorPane getVenueViewPane() {
        return venueViewPane;
    }

    public void setVenueViewPane(AnchorPane venueViewPane) {
        this.venueViewPane = venueViewPane;
    }

    public ObservableList<ShowDto> getAllShows() {
        return allShows;
    }

    public void setAllShows(ObservableList<ShowDto> allShows) {
        this.allShows = allShows;
    }

    public ObservableList<ParticipationDto> getAllParticipations() {
        return allParticipations;
    }

    public void setAllParticipations(ObservableList<ParticipationDto> allParticipations) {
        this.allParticipations = allParticipations;
    }

    public ObservableList<PerformanceDto> getAllPerformances() {
        return allPerformances;
    }

    public void setAllPerformances(ObservableList<PerformanceDto> allPerformances) {
        this.allPerformances = allPerformances;
    }

    public ObservableList<LocationDto> getAllLocations() {
        return allLocations;
    }

    public void setAllLocations(ObservableList<LocationDto> allLocations) {
        this.allLocations = allLocations;
    }

    public ObservableList<RoomDto> getAllRooms() {
        return allRooms;
    }

    public void setAllRooms(ObservableList<RoomDto> allRooms) {
        this.allRooms = allRooms;
    }

    public ObservableList<ArtistDto> getAllArtists() {
        return allArtists;
    }

    public void setAllArtists(ObservableList<ArtistDto> allArtists) {
        this.allArtists = allArtists;
    }

    public ObservableMap<ShowDto, Integer> getAllSoldTickets() {
        return allSoldTickets;
    }

    public void setAllSoldTickets(ObservableMap<ShowDto, Integer> allSoldTickets) {
        this.allSoldTickets = allSoldTickets;
    }

    public ObservableMap<ShowDto, Integer> getAllTicketsMinPrice() {
        return allTicketsMinPrice;
    }

    public void setAllTicketsMinPrice(ObservableMap<ShowDto, Integer> allTicketsMinPrice) {
        this.allTicketsMinPrice = allTicketsMinPrice;
    }

    public ObservableMap<ShowDto, Integer> getAllTicketsMaxPrice() {
        return allTicketsMaxPrice;
    }

    public void setAllTicketsMaxPrice(ObservableMap<ShowDto, Integer> allTicketsMaxPrice) {
        this.allTicketsMaxPrice = allTicketsMaxPrice;
    }

    public boolean isStillLoading() {
        return stillLoading;
    }

    public void setStillLoading(boolean stillLoading) {
        this.stillLoading = stillLoading;
    }

    public boolean isInitialized() {
        return initialized;
    }

    public void setInitialized(boolean initialized) {
        this.initialized = initialized;
    }
}
