package at.ac.tuwien.inso.ticketline.client.gui.controller.tabs.customer;

import at.ac.tuwien.inso.ticketline.client.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.client.gui.controller.LoginController;
import at.ac.tuwien.inso.ticketline.client.gui.controller.MainController;
import at.ac.tuwien.inso.ticketline.client.service.CustomerService;
import at.ac.tuwien.inso.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.ticketline.client.util.DateUtil;
import at.ac.tuwien.inso.ticketline.client.util.DialogUtil;
import at.ac.tuwien.inso.ticketline.client.util.SpringFxmlLoader;
import at.ac.tuwien.inso.ticketline.dto.CustomerDto;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.net.URL;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.function.BiPredicate;
import java.util.function.Function;

@Component
public class CustomersController implements Initializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomersController.class);
    private Service<Void> service;
    @FXML
    private TableView<CustomerDto> customerTable;

    @FXML
    private TableColumn<CustomerDto, String> firstname, lastname, city, country, postalcode, street, email, gender, phonenumber, birthday;

    // @FXML
    //  private TableColumn<CustomerDto, Date> birthday;

    @Autowired
    private SpringFxmlLoader springFxmlLoader;

    @Autowired
    private CustomerService customerService;

    private MainController mainController;

    @FXML
    private TextField firstNameTF, lastNameTF, postalCodeTF;

    @FXML
    private RadioButton maleRB, femaleRB;

    @FXML
    private StackPane stackPane;

    @FXML
    private AnchorPane mainCustomerAnchorPane, editCustomerAnchorPane;

    @FXML
    private Button btnCreate;

    @FXML
    private Label fname, lname, postalCode, genderLabel;


    private EditCustomerController editCustomerController;
    private CustomerViewController customerViewController;
    private List<CustomerDto> allCustomers;
    private ObservableList<CustomerDto> filteredCustomers = FXCollections.observableArrayList();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        firstname.setCellValueFactory(new PropertyValueFactory<>("firstname"));
        lastname.setCellValueFactory(new PropertyValueFactory<>("lastname"));
        city.setCellValueFactory(new PropertyValueFactory<>("city"));
        country.setCellValueFactory(new PropertyValueFactory<>("country"));
        postalcode.setCellValueFactory(new PropertyValueFactory<>("postalcode"));
        street.setCellValueFactory(new PropertyValueFactory<>("street"));
        email.setCellValueFactory(new PropertyValueFactory<>("email"));
        //gender.setCellValueFactory(new PropertyValueFactory<>("gender"));
        gender.setCellValueFactory(param -> {
            String genderString = param.getValue().getGender();
            return new SimpleStringProperty(genderString == null ? "-" : (genderString.toLowerCase().equals("male") ? "M" : "F"));
        });
        phonenumber.setCellValueFactory(new PropertyValueFactory<>("phoneNumber"));
        //birthday.setCellValueFactory(new PropertyValueFactory<>("dateOfBirth"));
        birthday.setCellValueFactory(param -> new SimpleStringProperty(DateUtil.getEuropeanDateString(param.getValue().getDateOfBirth())));

        customerTable.setRowFactory(ev -> {
            final TableRow<CustomerDto> row = new TableRow<>();
            final ContextMenu contextMenu = new ContextMenu();
            final MenuItem menuItem = new MenuItem("Edit");
            menuItem.setOnAction(context -> onEdit());
            contextMenu.getItems().add(menuItem);
            row.contextMenuProperty().bind(Bindings.when(row.emptyProperty()).then((ContextMenu) null).otherwise(contextMenu));
            row.setOnMouseClicked(mouse -> {
                if (mouse.getClickCount() == 2 && (!row.isEmpty()))
                    onView();
            });
            return row;
        });
        reinitLocale(MainController.currentLocale);


        // EventHandler for Gender RadioButtons
        EventHandler<ActionEvent> genderRadioButtonEventHandler = event -> {
            if (event.getSource() == maleRB || event.getSource() == femaleRB) {
                RadioButton sourceButton = (RadioButton) event.getSource();
                RadioButton otherButton = sourceButton == maleRB ? femaleRB : maleRB;

                boolean liveSearchPossible = sourceButton.isSelected() && !otherButton.isSelected();
                if (sourceButton.isSelected()) {
                    otherButton.setSelected(false);
                }
                if (liveSearchPossible) {
                    liveCustomerSearch("", maleRB.isSelected() ? "Male" : "Female", e -> e.getGender(), (e, f) -> e.equals(f));
                } else {
                    filterCustomers();
                }
            }
        };

        maleRB.setOnAction(genderRadioButtonEventHandler);
        femaleRB.setOnAction(genderRadioButtonEventHandler);

        firstNameTF.textProperty().addListener((observable, oldValue, newValue) -> {
            liveCustomerSearch(oldValue, newValue, e -> e.getFirstname(), (e, f) -> e.contains(f));
        });

        lastNameTF.textProperty().addListener((observable, oldValue, newValue) -> {
            liveCustomerSearch(oldValue, newValue, e -> e.getLastname(), (e, f) -> e.contains(f));
        });


        postalCodeTF.textProperty().addListener((observable, oldValue, newValue) -> {
            liveCustomerSearch(oldValue, newValue, e -> e.getPostalcode(), (e, f) -> e.contains(f));
        });

        refreshAllCustomersList();
        filteredCustomers.clear();
        filteredCustomers.addAll(allCustomers);
        customerTable.setItems(filteredCustomers);
    }

    public void reinitLocale(Locale newValue) {
        LocaleContextHolder.setLocale(newValue);
        BundleManager.changeLocale(newValue);
        btnCreate.setText(BundleManager.getBundle().getString("customer.newCostumerButton"));

        fname.setText(BundleManager.getBundle().getString("customer.fnLable"));
        lname.setText(BundleManager.getBundle().getString("customer.lnLable"));
        postalCode.setText(BundleManager.getBundle().getString("customer.postalCodeLable"));
        genderLabel.setText(BundleManager.getBundle().getString("customer.genderLable"));
        maleRB.setText(BundleManager.getBundle().getString("customer.genderM"));
        femaleRB.setText(BundleManager.getBundle().getString("customer.genderF"));

        firstname.setText(BundleManager.getBundle().getString("customer.fn"));
        lastname.setText(BundleManager.getBundle().getString("customer.ln"));
        city.setText(BundleManager.getBundle().getString("customer.city"));
        country.setText(BundleManager.getBundle().getString("customer.country"));
        postalcode.setText(BundleManager.getBundle().getString("customer.pc"));
        street.setText(BundleManager.getBundle().getString("customer.street"));
        email.setText(BundleManager.getBundle().getString("customer.email"));
        gender.setText(BundleManager.getBundle().getString("customer.gender"));
        phonenumber.setText(BundleManager.getBundle().getString("customer.phone"));
        birthday.setText(BundleManager.getBundle().getString("customer.birth"));

        if (editCustomerController != null) {
            editCustomerController.reinitLocale(newValue);
        }
        if(customerViewController != null) {
            customerViewController.reinitLocale(newValue);
        }
    }


    public void setMainController(MainController mainController) {
        this.mainController = mainController;
    }

    /**
     * Opens create customer window.
     *
     * @param event the event
     */
    @FXML
    private void onCreateCustomerButtonPressed(ActionEvent event) {

        //TODO: open Create Customer Dialog

        LOGGER.debug("create customer button pressed");

        SpringFxmlLoader.LoadWrapper loadWrapper = springFxmlLoader.loadAndWrap("/gui/fxml/tabs/customers/create_customer_prototype.fxml");
        ((Node) event.getSource()).setCursor(Cursor.WAIT);
        Stage newCustomerStage = new Stage();
        newCustomerStage.setScene(new Scene((Parent) loadWrapper.getLoadedObject()));
        newCustomerStage.setResizable(false);
        newCustomerStage.setTitle(BundleManager.getBundle().getString("app.name"));
        newCustomerStage.getIcons().add(new Image(LoginController.class.getResourceAsStream("/image/ticketlineLogo.png")));


        try {
            ((CreateCustomerController) loadWrapper.getController()).setCustomersController(this);
        } catch(ClassCastException e) {}

        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();

        newCustomerStage.initModality(Modality.WINDOW_MODAL);
        newCustomerStage.initOwner(stage);

        //asks user for confirmation before window is closed
        newCustomerStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                if (MainController.currentLocale.equals(Locale.ENGLISH)) {

                    if (DialogUtil.showConfirmationDialog("Closing customer creator",
                            "Do you really wish to quit the customer creation?") != ButtonType.OK) {
                        event.consume();
                    }
                } else {
                    if (DialogUtil.showConfirmationDialog("Fenster schließen",
                            "Möchten Sie die Erstellung eines neuen Kunden wirklich abbrechen?") != ButtonType.OK) {
                        event.consume();
                    }
                }
            }
        });

        ((Node) event.getSource()).setCursor(Cursor.DEFAULT);
        newCustomerStage.show();
    }

//    @FXML
//    private void onGetAllButtonPressed(ActionEvent actionEvent) {
//            customerTable.setItems(filteredCustomers);
//    }


    public void refreshCustomerList() {
        refreshAllCustomersList();
        filterCustomers();
    }

    private void refreshAllCustomersList() {
        try {
            allCustomers = customerService.getCustomers();
        } catch (ServiceException e) {
            LOGGER.error("Could not retrieve all customers!");
        }
    }


    /**
     * Filter the filteredCustomer List to match the user input of a changed textfield or radiobutton
     *
     * @param oldUserInput             the old value of the changed textfield
     * @param newUserInput             the new value of the changed textfield
     * @param getCustomerValueFunction the function to get the value to filter from the CustomerDTO value (e.g. getFirstname())
     * @param compareFunction          the function to test if a CustomerDTO value is matching the newuUerInput-Value
     */
    private void liveCustomerSearch(String oldUserInput, String newUserInput, Function<CustomerDto, String> getCustomerValueFunction, BiPredicate<String, String> compareFunction) {
        if (newUserInput.toLowerCase().contains(oldUserInput.toLowerCase())) {
            for (int i = 0; i < filteredCustomers.size(); i++) {
                CustomerDto c = filteredCustomers.get(i);
                if (!compareFunction.test(getCustomerValueFunction.apply(c).toLowerCase(), newUserInput.toLowerCase())) {
                    filteredCustomers.remove(i);
                    i--;
                }
            }
        } else { // some valid customers may not be in the filtered list anymore ==> refilter complete list
            filterCustomers();
        }
    }

    /**
     * Filters the filteredCustomer List to match the user input
     */
    private void filterCustomers() {
        String forename = firstNameTF.getText().trim();
        String surname = lastNameTF.getText().trim();
        String postalCode = postalCodeTF.getText().trim();
        String gender = maleRB.isSelected() ? "male" : (femaleRB.isSelected() ? "female" : "");

        filteredCustomers.clear();

        for (CustomerDto c : allCustomers) {
            if (isMatch(forename, c.getFirstname()) && isMatch(surname, c.getLastname())
                    && isMatch(postalCode, c.getPostalcode()) && (gender.equals("") || gender.equals(c.getGender().toLowerCase()))) {
                filteredCustomers.add(c);
            }
        }
    }

    /**
     * Looks if a customer value contains a string value typed in by a user
     *
     * @param userInputString the input from the user
     * @param customerValue   the customer value to look if it matches the user input
     * @return true if the customer value contains the user input, false otherwise
     */
    public boolean isMatch(String userInputString, String customerValue) {
        if (userInputString == null || userInputString.length() == 0) { // no restrictions made
            return true;
        }

        if (customerValue == null) {
            return false;
        }
        return customerValue.toLowerCase().contains(userInputString.toLowerCase());
    }

    private void onEdit() {
        LOGGER.info("Edit context menu item clicked");
        CustomerDto customerDto = customerTable.getSelectionModel().getSelectedItem();
        if (customerDto != null) {
            SpringFxmlLoader.LoadWrapper loadWrapper = springFxmlLoader.loadAndWrap("/gui/fxml/tabs/customers/edit_customer.fxml");
            editCustomerController = (EditCustomerController) loadWrapper.getController();
            editCustomerController.setCustomerDto(customerDto, customerTable);
            editCustomerController.setStackPane(stackPane);
            editCustomerController.setCustomersAnchorPane(mainCustomerAnchorPane);
            editCustomerController.setCustomersController(this);
            stackPane.getChildren().clear();
            stackPane.getChildren().add((Node) loadWrapper.getLoadedObject());

        }
    }

    private void onView(){

        LOGGER.info("Double clicked on a customer");
        CustomerDto customerDto = customerTable.getSelectionModel().getSelectedItem();
        if (customerDto != null) {
            SpringFxmlLoader.LoadWrapper loadWrapper = springFxmlLoader.loadAndWrap("/gui/fxml/tabs/customers/customer_view.fxml");
            customerViewController = (CustomerViewController) loadWrapper.getController();
            customerViewController.setCustomerDto(customerDto);
            customerViewController.setMainController(mainController);
            customerViewController.setStackPane(stackPane);
            customerViewController.setAnchorPane(mainCustomerAnchorPane);
            customerViewController.setCustomerController(this);
            stackPane.getChildren().clear();
            stackPane.getChildren().add((Node) loadWrapper.getLoadedObject());

            service = new Service<Void>() {
                @Override
                protected Task<Void> createTask() {
                    return new Task<Void>() {
                        @Override
                        protected Void call() throws Exception {
                            customerViewController.getShows();
                            return null;
                        }
                    };
                }
            };
            service.restart();
        }
    }

    public void returned(){
        customerTable.getColumns().get(0).setVisible(false);
        customerTable.getColumns().get(0).setVisible(true);
    }
}