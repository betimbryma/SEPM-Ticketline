package at.ac.tuwien.inso.ticketline.client.gui;

import at.ac.tuwien.inso.ticketline.client.util.DateUtil;
import com.google.common.base.Throwables;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

import java.time.LocalDate;
import java.util.function.Function;

import static at.ac.tuwien.inso.ticketline.client.util.BundleManager.getBundle;
import static at.ac.tuwien.inso.ticketline.client.util.BundleManager.getExceptionBundle;

/**
 * JavaFXUtils
 * This class provides helper methods for JavaFX
 */
public class JavaFXUtils {

    /**
     * Creates a dialog for an exception
     * Based on {@link http://code.makery.ch/blog/javafx-dialogs-official/}
     *
     * @param exception the exception
     * @return the dialog which shows the exception
     */
    public static Alert createAlert(Exception e) {
        Alert alert = new Alert(AlertType.ERROR);
        alert.setTitle(getBundle().getString("app.name") + " - " + getExceptionBundle().getString("error"));
        alert.setHeaderText(getExceptionBundle().getString("exception.unexpected"));
        alert.setContentText(e.getMessage());

        String exceptionText = Throwables.getStackTraceAsString(e);

        Label label = new Label(getExceptionBundle().getString("exception.stacktrace"));

        TextArea textArea = new TextArea(exceptionText);
        textArea.setEditable(false);
        textArea.setWrapText(true);

        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);

        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(label, 0, 0);
        expContent.add(textArea, 0, 1);

        alert.getDialogPane().setExpandableContent(expContent);

        return alert;
    }

    /**
     * Generates a Table Row that has a tooltip
     *
     * @param function the function to get the string of the tooltiü
     * @param <T>      the type the TableRow contains
     * @return The created TableRow
     */
    public static <T> TableRow<T> getTableRowWithTooltip(Function<T, String> function) {
        return new TableRow<T>() {
            private Tooltip tooltip = new Tooltip();

            @Override
            public void updateItem(T dto, boolean empty) {
                super.updateItem(dto, empty);
                if (dto == null) {
                    setTooltip(null);
                } else {
                    tooltip.setText(function.apply(dto));
                    setTooltip(tooltip);
                }
            }
        };
    }

    public static void setChangeListenerToDatePicker(DatePicker dp) {
        dp.focusedProperty().addListener(observable -> {
            LocalDate date = DateUtil.getDateFromString(dp.getEditor().getText());
            if (date == null) {
                dp.getEditor().clear();
            }
            dp.setValue(date);
        });
    }
}
