package at.ac.tuwien.inso.ticketline.client.gui.controller.shoppingCart;


import at.ac.tuwien.inso.ticketline.client.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.client.exception.ValidationException;
import at.ac.tuwien.inso.ticketline.client.gui.JavaFXUtils;
import at.ac.tuwien.inso.ticketline.client.gui.controller.MainController;
import at.ac.tuwien.inso.ticketline.client.gui.controller.tabs.search.views.ReservationSelection;
import at.ac.tuwien.inso.ticketline.client.service.CustomerService;
import at.ac.tuwien.inso.ticketline.client.util.*;
import at.ac.tuwien.inso.ticketline.dto.CustomerDto;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.stage.Stage;
import org.controlsfx.validation.ValidationResult;
import org.controlsfx.validation.ValidationSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.net.URL;
import java.time.LocalDate;
import java.util.Locale;
import java.util.ResourceBundle;

@Component
public class CreateCustomerShoppingCartController implements Initializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(CreateCustomerShoppingCartController.class);

    @Autowired
    private SpringFxmlLoader springFxmlLoader;

    @FXML
    private TextField tf_customerFirstname;
    @FXML
    private TextField tf_customerLastname;
    @FXML
    private TextField tf_customerPostalcode;
    @FXML
    private TextField tf_customerStreet;
    @FXML
    private TextField tf_customerCity;
    @FXML
    private TextField tf_customerCountry;
    @FXML
    private TextField tf_customerEmail;
    @FXML
    private TextField tf_customerPhonenumber;

    @FXML
    private RadioButton button_male;
    @FXML
    private RadioButton button_female;

    @FXML
    private DatePicker dp_birthdate;

    @FXML
    private Label fname, lname, birth, Email, phone, city, str, pc, country;

    @FXML
    private Button create, back;

    @FXML
    private TitledPane title;

    @Autowired
    private CustomerService customerService;

    SellController sellController;
    ReservationSelection reservationController;

    Stage previousStage;
    private CustomerDto customerDto;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        new ValidationSupport().registerValidator(tf_customerFirstname, true, (Control c, String s) ->
            ValidationResult.fromErrorIf(c, BundleManager.getBundle().getString("crcustomer.fnValError"), !UserInputValidator.validatePersonName(s, false)));

        new ValidationSupport().registerValidator(tf_customerLastname, true, (Control c, String s) ->
                ValidationResult.fromErrorIf(c, BundleManager.getBundle().getString("crcustomer.lnValError"), !UserInputValidator.validatePersonName(s, false)));

        new ValidationSupport().registerValidator(tf_customerEmail, false, (Control c, String s) ->
                ValidationResult.fromErrorIf(c, BundleManager.getBundle().getString("crcustomer.emailValError"), !UserInputValidator.validateEmail(s, true)));

        new ValidationSupport().registerValidator(tf_customerCity, true, (Control c, String s) ->
                ValidationResult.fromErrorIf(c, BundleManager.getBundle().getString("crcustomer.cityValError"), !UserInputValidator.validateNoNumbersString(s, false)));

        new ValidationSupport().registerValidator(tf_customerCountry, true, (Control c, String s) ->
                ValidationResult.fromErrorIf(c, BundleManager.getBundle().getString("crcustomer.countryValError"), !UserInputValidator.validateNoNumbersString(s, false)));

        new ValidationSupport().registerValidator(tf_customerStreet, true, (Control c, String s) ->
                ValidationResult.fromErrorIf(c, BundleManager.getBundle().getString("crcustomer.streetValError"), !UserInputValidator.validateNonEmptyString(s)));

        new ValidationSupport().registerValidator(tf_customerPostalcode, true, (Control c, String s) ->
                ValidationResult.fromErrorIf(c, BundleManager.getBundle().getString("crcustomer.postalCodeValError"), !UserInputValidator.validateNonEmptyString(s)));

        new ValidationSupport().registerValidator(tf_customerPhonenumber, false, (Control c, String s) ->
                ValidationResult.fromErrorIf(c, BundleManager.getBundle().getString("crcustomer.phoneValError"), !UserInputValidator.validatePhoneNumber(s, true)));

        new ValidationSupport().registerValidator(dp_birthdate, false, (Control c, LocalDate d) ->
                ValidationResult.fromErrorIf(c, BundleManager.getBundle().getString("crcustomer.bdValError"), !UserInputValidator.validateBirthday(d, true)));

        dp_birthdate.setConverter(DateUtil.getEuropeanDateStringConverter());

        JavaFXUtils.setChangeListenerToDatePicker(dp_birthdate);


        reinitLocale(MainController.currentLocale);

    }
    public void reinitLocale(Locale newValue) {
        LocaleContextHolder.setLocale(newValue);
        BundleManager.changeLocale(newValue);
        button_male.setText(BundleManager.getBundle().getString("shoppingcartCreate.male"));
        button_female.setText(BundleManager.getBundle().getString("shoppingcartCreate.female"));

        fname.setText(BundleManager.getBundle().getString("shoppingcartCreate.fn"));
        lname.setText(BundleManager.getBundle().getString("shoppingcartCreate.ln"));
        birth.setText(BundleManager.getBundle().getString("shoppingcartCreate.birth"));
        Email.setText(BundleManager.getBundle().getString("shoppingcartCreate.email"));
        phone.setText(BundleManager.getBundle().getString("shoppingcartCreate.phone"));
        city.setText(BundleManager.getBundle().getString("shoppingcartCreate.city"));
        str.setText(BundleManager.getBundle().getString("shoppingcartCreate.street"));
        pc.setText(BundleManager.getBundle().getString("shoppingcartCreate.pc"));
        country.setText(BundleManager.getBundle().getString("shoppingcartCreate.country"));

        create.setText(BundleManager.getBundle().getString("shoppingcartCreate.createB"));
        back.setText(BundleManager.getBundle().getString("shoppingcartCreate.backB"));

        title.setText(BundleManager.getBundle().getString("shoppingcartCreate.title"));
    }
    /**
     * Reads Data from Textfields and creates Customer if Data is valid.
     * If Data is not valid, a Dialogue is shown.
     *
     * @param event the event
     */
    @FXML
    private void onCreateButtonPressed(ActionEvent event) {

        LOGGER.debug("create Button pressed");

        CustomerDto customer;

        String gender = "";
        if (button_male.isSelected()) {
            gender = "male";
        } else if (button_female.isSelected()) {
            gender = "female";
        }


        try {

            customer = CustomerDataValidator.validateAndCreate(tf_customerFirstname.getText(),
                    tf_customerLastname.getText(), tf_customerStreet.getText(), tf_customerPostalcode.getText(),
                    tf_customerCity.getText(), tf_customerCountry.getText(), gender, dp_birthdate.getValue(),
                    tf_customerEmail.getText(), tf_customerPhonenumber.getText());

            customer.setId(this.customerService.saveCustomer(customer));
            if(MainController.currentLocale.equals(Locale.ENGLISH)) {

                DialogUtil.showSuccessDialog("Customer created!",
                        "The customer was successfully created!",
                        customer.getFirstname() + " " + customer.getLastname() + " is now stored in the database");
            }else{
                DialogUtil.showSuccessDialog("Kunde wurde erstellt!",
                        "Der neue Kunde wurde erfolgreich erstellt!",
                        customer.getFirstname() + " " + customer.getLastname() + " befindet sich jetzt in der Datenbank.");
            }
            Node source = (Node) event.getSource();
            Stage stage = (Stage) source.getScene().getWindow();
            stage.close();
            customerDto=customer;
            if(sellController!=null && previousStage!=null) {
                sellController.setCustomer(customer);
                previousStage.show();
            }
            else if(reservationController!=null && previousStage!=null) {
                reservationController.setCustomer(customer);
                previousStage.show();
            }
        }
        catch (ValidationException e) {
            LOGGER.error("customer data is not valid");
            return;
        }
        catch (ServiceException e) {
            if(MainController.currentLocale.equals(Locale.ENGLISH)) {

                DialogUtil.showErrorDialog("Error creating customer",
                        "Customer could not be created",
                        "An error in the application occured");
            }else{
                DialogUtil.showErrorDialog("Fehler beim Erstellen eines neuen Kunden",
                        "Kunde konnte nicht erstellt werden.",
                        "Es ist ein Fehler im Programm aufgetaucht");
            }
        }
    }

    @FXML
    private void handleBack(ActionEvent event) {
        if(MainController.currentLocale.equals(Locale.ENGLISH)) {

            if (DialogUtil.showConfirmationDialog("Closing customer creator",
                    "Do you really wish to quit the customer creation?") == ButtonType.OK) {
                Node source = (Node) event.getSource();
                Stage stage = (Stage) source.getScene().getWindow();
                stage.close();
                if(previousStage!=null)
                    previousStage.show();
            }
        }else{
            if (DialogUtil.showConfirmationDialog("Customer Creator schließen",
                    "Wollen Sie wirklich die Erstellung eines neuen Kunden abbrechen?") == ButtonType.OK) {
                Node source = (Node) event.getSource();
                Stage stage = (Stage) source.getScene().getWindow();
                stage.close();
                if(previousStage!=null)
                    previousStage.show();
            }
        }
    }

    public void setSellController(SellController sellController) {

        this.sellController = sellController;
        this.reservationController = null;
    }

    public void setReservationController(ReservationSelection reservationController) {
        this.reservationController = reservationController;
        this.sellController = null;
    }

    public void setPreviousStage(Stage previousStage) {
        this.previousStage = previousStage;
    }

    public CustomerDto getCustomerDto(){
        return this.customerDto;
    }
}
