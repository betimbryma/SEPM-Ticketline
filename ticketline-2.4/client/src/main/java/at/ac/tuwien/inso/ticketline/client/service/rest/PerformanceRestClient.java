package at.ac.tuwien.inso.ticketline.client.service.rest;

import at.ac.tuwien.inso.ticketline.client.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.client.service.PerformanceService;
import at.ac.tuwien.inso.ticketline.dto.PerformanceDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.List;

@Component
public class PerformanceRestClient implements PerformanceService {

    public static final String GET_ALL_PERFORMANCES = "/service/performance/";
    private static final Logger LOGGER = LoggerFactory.getLogger(PerformanceRestClient.class);

    @Autowired
    private RestClient restClient;

    /**
     * {@inheritDoc}
     */
    @Override
    public List<PerformanceDto> getAll() throws ServiceException {
        RestTemplate restTemplate = this.restClient.getRestTemplate();
        String url = this.restClient.createServiceUrl(GET_ALL_PERFORMANCES);
        HttpEntity<String> entity = new HttpEntity<>(this.restClient.getHttpHeaders());
        LOGGER.info("Retrieving performances from {}", url);
        List<PerformanceDto> performances;

        try {
            ParameterizedTypeReference<List<PerformanceDto>> ref = new ParameterizedTypeReference<List<PerformanceDto>>() {
            };
            ResponseEntity<List<PerformanceDto>> response = restTemplate.exchange(URI.create(url), HttpMethod.GET, entity, ref);
            performances = response.getBody();

        } catch (RestClientException e) {
            throw new ServiceException("Could not retrieve performances: " + e.getMessage(), e);
        }

        return performances;
    }
}
