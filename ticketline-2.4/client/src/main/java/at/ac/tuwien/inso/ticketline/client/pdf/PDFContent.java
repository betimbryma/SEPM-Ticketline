package at.ac.tuwien.inso.ticketline.client.pdf;

import org.apache.pdfbox.pdmodel.PDPageContentStream;

import java.io.IOException;

public interface PDFContent {

    void print(PDPageContentStream cos) throws IOException;
}
