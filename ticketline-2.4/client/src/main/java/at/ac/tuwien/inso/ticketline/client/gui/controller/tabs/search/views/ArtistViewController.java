package at.ac.tuwien.inso.ticketline.client.gui.controller.tabs.search.views;

import at.ac.tuwien.inso.ticketline.client.gui.JavaFXUtils;
import at.ac.tuwien.inso.ticketline.client.gui.controller.MainController;
import at.ac.tuwien.inso.ticketline.client.gui.controller.tabs.search.SearchController;
import at.ac.tuwien.inso.ticketline.client.gui.controller.tabs.search.artists.ArtistsController;
import at.ac.tuwien.inso.ticketline.client.service.PerformanceService;
import at.ac.tuwien.inso.ticketline.client.service.TicketService;
import at.ac.tuwien.inso.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.ticketline.dto.ArtistDto;
import at.ac.tuwien.inso.ticketline.dto.ParticipationDto;
import at.ac.tuwien.inso.ticketline.dto.PerformanceDto;
import at.ac.tuwien.inso.ticketline.dto.ShowDto;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;
import java.util.ResourceBundle;

@Component
public class ArtistViewController implements Initializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(ArtistViewController.class);

    @Autowired
    private MainController mainController;
    @Autowired
    private SearchController searchController;
    @Autowired
    private ShowViewController showViewController;
    @Autowired
    private EventViewController eventViewController;
    @Autowired
    private ArtistsController artistsController;
    @Autowired
    private PerformanceService performanceService;
    @Autowired
    private TicketService ticketService;
    @FXML
    private Button backButton;
    @FXML
    private Label titleText, mainLabel, headerLabel;
    @FXML
    private TableView<PerformanceDto> performanceTable;
    @FXML
    private TableView<ShowDto> showTable;
    @FXML
    private TableColumn<ShowDto, String> showDateColumn, showTimeColumn, showVenueColumn, showRoomColumn, showPriceMinColumn, showPriceMaxColumn;
    @FXML
    private TableColumn<PerformanceDto, String> performanceNameColumn, performanceDescriptionColumn, performanceDurationColumn, performanceTypeColumn;
    private ArtistDto artist;
    private PerformanceDto performance;
    private ObservableList<PerformanceDto> filteredPerformances;
    private ObservableList<ShowDto> filteredShows;

    /**
     * This method initializes the ArtistViewController.
     *
     * @param location  location of the fxml file
     * @param resources resources
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        filteredPerformances = FXCollections.observableList(new ArrayList<>());
        filteredShows = FXCollections.observableList(new ArrayList<>());
        initEventTable();
        initShowTable();
        reinitLocale(MainController.currentLocale);
    }

    /**
     * Reinitializes the localization.
     * @param newValue the new localization value
     */
    public void reinitLocale(Locale newValue) {
        initEventTable();
        initShowTable();
        LocaleContextHolder.setLocale(newValue);
        BundleManager.changeLocale(newValue);
        backButton.setText(BundleManager.getBundle().getString("artView.backB"));
        if(artist != null) {
            titleText.setText(BundleManager.getBundle().getString("artView.title") + " " +  artist.getFirstName() + " " + artist.getLastName());
        }
        mainLabel.setText(BundleManager.getBundle().getString("artView.main"));
        performanceNameColumn.setText(BundleManager.getBundle().getString("artView.nameCol"));
        performanceTypeColumn.setText(BundleManager.getBundle().getString("artView.typeCol"));
        performanceDurationColumn.setText(BundleManager.getBundle().getString("artView.lengthCol"));
        performanceDescriptionColumn.setText(BundleManager.getBundle().getString("artView.descriptionCol"));
        showDateColumn.setText(BundleManager.getBundle().getString("artView.dateCol"));
        showTimeColumn.setText(BundleManager.getBundle().getString("artView.timeCol"));
        showVenueColumn.setText(BundleManager.getBundle().getString("artView.venueCol"));
        showRoomColumn.setText(BundleManager.getBundle().getString("artView.roomCol"));
        showPriceMinColumn.setText(BundleManager.getBundle().getString("artView.priceMinCol"));
        showPriceMaxColumn.setText(BundleManager.getBundle().getString("artView.priceMaxCol"));
    }

    /**
     * Initializes the event table.
     */
    public void initEventTable() {
        performanceNameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        performanceTypeColumn.setCellValueFactory(param -> {
            ObservableValue<String> type = new ReadOnlyObjectWrapper<>(searchController.convertPerformanceType(param.getValue()));
            return type;
        });
        performanceDurationColumn.setCellValueFactory(param -> {
            ObservableValue<String> duration = new ReadOnlyObjectWrapper<>(param.getValue().getDuration() + "min");
            return duration;
        });
        performanceDescriptionColumn.setCellValueFactory(new PropertyValueFactory<>("description"));
        performanceTable.setRowFactory(tv -> {
            TableRow<PerformanceDto> row = JavaFXUtils.getTableRowWithTooltip(e -> e.getDescription());
            row.setOnMouseClicked(event -> {
                if (!row.isEmpty()) {
                    if (event.getClickCount() == 2) {
                        eventViewController.reset();
                        eventViewController.setPerformance(row.getItem());
                        searchController.getStackPane().getChildren().clear();
                        searchController.getStackPane().getChildren().add(searchController.getEventViewPane());
                    } else {
                        setPerformance(row.getItem());
                    }
                }
            });
            return row;
        });
        performanceTable.setItems(filteredPerformances);
    }

    /**
     * Initializes the show table.
     */
    public void initShowTable() {
        showDateColumn.setCellValueFactory(param -> {
            SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
            ObservableValue<String> date = new ReadOnlyObjectWrapper<>(format.format(param.getValue().getDateOfPerformance()));
            return date;
        });
        showTimeColumn.setCellValueFactory(param -> {
            SimpleDateFormat format = new SimpleDateFormat("HH:mm");
            ObservableValue<String> time = new ReadOnlyObjectWrapper<>(format.format(param.getValue().getDateOfPerformance()));
            return time;
        });
        showVenueColumn.setCellValueFactory(param -> {
            ObservableValue<String> venue = new ReadOnlyObjectWrapper<>(param.getValue().getRoom().getLocation().getName());
            return venue;
        });
        showRoomColumn.setCellValueFactory(param -> {
            ObservableValue<String> room = new ReadOnlyObjectWrapper<>(param.getValue().getRoom().getName());
            return room;
        });
        showPriceMinColumn.setCellValueFactory(param -> {
            int newPrice = searchController.getAllTicketsMinPrice().get(param.getValue());
            ObservableValue<String> price = new ReadOnlyObjectWrapper<>(Integer.toString(newPrice));
            return price;
        });
        showPriceMaxColumn.setCellValueFactory(param -> {
            int newPrice = searchController.getAllTicketsMaxPrice().get(param.getValue());
            ObservableValue<String> price = new ReadOnlyObjectWrapper<>(Integer.toString(newPrice));
            return price;
        });
        showTable.setRowFactory(tv -> {
            TableRow<ShowDto> row = JavaFXUtils.getTableRowWithTooltip(e -> e.getRoom().getLocation().getDescription());
            row.setOnMouseClicked(event -> {
                if ((!row.isEmpty())) {
                    //showViewController.reset();
                    showViewController.setShow(row.getItem());
                    searchController.getStackPane().getChildren().clear();
                    searchController.getStackPane().getChildren().add(searchController.getShowViewPane());
                }
            });
            return row;
        });
        showTable.setItems(filteredShows);
    }

    /**
     * Sets the artist and the according information in this view.
     * This method has to be called when this pane is set in front.
     *
     * @param artist the selected artist
     */
    public void setArtist(ArtistDto artist) {
        this.artist = artist;
        titleText.setText(BundleManager.getBundle().getString("artView.title") + " " +  artist.getFirstName() + " " + artist.getLastName());
        headerLabel.setText("-> " + artist.getFirstName() + " " + artist.getLastName());
        filteredPerformances.clear();

        for (ParticipationDto p : searchController.getAllParticipations()) {
            if (artist.getId() == p.getArtist().getId()) {
                filteredPerformances.add(p.getPerformance());
            }
        }

        performanceTable.setItems(filteredPerformances);
    }

    /**
     * This method sets the selected performance and creates the list of shows for the performance.
     *
     * @param performance selected performance
     */
    public void setPerformance(PerformanceDto performance) {
        this.performance = performance;
        filteredShows.clear();
        for (ShowDto s : searchController.getAllShows()) {
            if (s.getPerformance().getId().equals(performance.getId())) {
                filteredShows.add(s);
            }
        }
    }

    /**
     * This method puts the accordion in the foreground.
     *
     * @param event the back button event
     */
    @FXML
    public void handleBack(ActionEvent event) {
        searchController.getStackPane().getChildren().clear();
        searchController.getStackPane().getChildren().add(searchController.getAccordion());
    }

    /**
     * Resets the show list.
     */
    public void reset() {
        filteredShows.clear();
    }
}
