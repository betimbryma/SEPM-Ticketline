package at.ac.tuwien.inso.ticketline.client.service;

import at.ac.tuwien.inso.ticketline.client.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.dto.LocationDto;

import java.util.List;

public interface LocationService {

    /**
     * Gets all saved locations
     *
     * @return A list with all saved locations
     * @throws ServiceException if an error occurs
     */
    List<LocationDto> getAll() throws ServiceException;

}