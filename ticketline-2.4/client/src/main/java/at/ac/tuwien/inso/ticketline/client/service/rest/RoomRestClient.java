package at.ac.tuwien.inso.ticketline.client.service.rest;

import at.ac.tuwien.inso.ticketline.client.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.client.service.RoomService;
import at.ac.tuwien.inso.ticketline.dto.RoomDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.List;

@Component
public class RoomRestClient implements RoomService {

    public static final String GET_ALL_ROOMS = "/service/room/";
    private static final Logger LOGGER = LoggerFactory.getLogger(RoomRestClient.class);

    @Autowired
    private RestClient restClient;

    /**
     * {@inheritDoc}
     */
    @Override
    public List<RoomDto> getAll() throws ServiceException {
        RestTemplate restTemplate = this.restClient.getRestTemplate();
        String url = this.restClient.createServiceUrl(GET_ALL_ROOMS);
        HttpEntity<String> entity = new HttpEntity<>(this.restClient.getHttpHeaders());
        LOGGER.info("Retrieving rooms from {}", url);
        List<RoomDto> rooms;

        try {
            ParameterizedTypeReference<List<RoomDto>> ref = new ParameterizedTypeReference<List<RoomDto>>() {
            };
            ResponseEntity<List<RoomDto>> response = restTemplate.exchange(URI.create(url), HttpMethod.GET, entity, ref);
            rooms = response.getBody();

        } catch (RestClientException e) {
            throw new ServiceException("Could not retrieve rooms: " + e.getMessage(), e);
        }

        return rooms;
    }
}