package at.ac.tuwien.inso.ticketline.client.gui.controller.tabs.search.views;

import at.ac.tuwien.inso.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.ticketline.dto.CategoryDto;
import at.ac.tuwien.inso.ticketline.dto.TicketDto;
import at.ac.tuwien.inso.ticketline.dto.TicketIdentifierDto;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

/**
 * Created by Ahmed on 22.05.2016.
 */
public class TicketCircle extends Circle {

    private int seat;
    private int row;
    private int column;
    private int price;
    private String status;
    private CategoryDto category;
    private TicketIdentifierDto ticketIdentifierDto;
    private TicketDto ticketDto;
    private Color originalColor;
    private Color currentColor;


    public int getSeat() {
        return this.seat;
    }

    public void setSeat(int seat) {
        this.seat = seat;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public CategoryDto getCategory() {
        return category;
    }

    public void setCategory(CategoryDto category) {
        this.category = category;
    }

    public String toString() {

        String result = BundleManager.getBundle().getString("ticketCircle.seatNumber") + ": " + seat
                + "\n" + BundleManager.getBundle().getString("ticketCircle.row") + ": " + row
                + "\n" + BundleManager.getBundle().getString("ticketCircle.gallery") + ": " + column
                + "\n" + BundleManager.getBundle().getString("ticketCircle.price") + ": " + price
                + "\n" + BundleManager.getBundle().getString("ticketCircle.category") + ": " + category.getName()
                + "\n" + BundleManager.getBundle().getString("ticketCircle.status") + ": " + status; // TODO localize status e.g. "frei" instead of "free"

        return result;
//        return "Seat Number: " + seat + " | Row: " + row + " | Gallery: " + column + " | Price: " + price + " | Category: " + category.getName() + " | Status: " + status;
    }

    public TicketIdentifierDto getTicketIdentifierDto() {
        return ticketIdentifierDto;
    }

    public void setTicketIdentifierDto(TicketIdentifierDto ticketIdentifierDto) {
        this.ticketIdentifierDto = ticketIdentifierDto;
    }

    public TicketDto getTicketDto() {
        return ticketDto;
    }

    public void setTicketDto(TicketDto ticketDto) {
        this.ticketDto = ticketDto;
    }

    public Color getOriginalColor() {
        return originalColor;
    }

    public void setOriginalColor(Color originalColor) {
        this.originalColor = originalColor;
    }

    public Color getCurrentColor() {
        return currentColor;
    }

    public void setCurrentColor(Color currentColor) {
        this.currentColor = currentColor;
    }
}
