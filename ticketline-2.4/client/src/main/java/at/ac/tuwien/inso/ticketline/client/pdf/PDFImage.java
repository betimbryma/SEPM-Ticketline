package at.ac.tuwien.inso.ticketline.client.pdf;

import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;

import java.io.IOException;

public class PDFImage implements PDFContent {

    private PDImageXObject image;
    private int posX;
    private int posY;
    private float width;
    private float height;

    public PDFImage(PDImageXObject image, int posX, int posY, float width, float height) {
        this.image = image;
        this.posX = posX;
        this.posY = posY;
        this.width = width;
        this.height = height;
    }


    @Override
    public void print(PDPageContentStream cos) throws IOException {
        cos.drawImage(image, posX, posY, width, height);
    }
}
