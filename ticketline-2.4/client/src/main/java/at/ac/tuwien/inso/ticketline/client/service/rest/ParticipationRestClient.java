package at.ac.tuwien.inso.ticketline.client.service.rest;

import at.ac.tuwien.inso.ticketline.client.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.client.service.ParticipationService;
import at.ac.tuwien.inso.ticketline.dto.ParticipationDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.List;

@Component
public class ParticipationRestClient implements ParticipationService {

    public static final String GET_ALL_PARTICIPATIONS = "/service/participation/";
    private static final Logger LOGGER = LoggerFactory.getLogger(ParticipationRestClient.class);

    @Autowired
    private RestClient restClient;

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ParticipationDto> getAll() throws ServiceException {
        RestTemplate restTemplate = this.restClient.getRestTemplate();
        String url = this.restClient.createServiceUrl(GET_ALL_PARTICIPATIONS);
        HttpEntity<String> entity = new HttpEntity<>(this.restClient.getHttpHeaders());
        LOGGER.info("Retrieving participations from {}", url);
        List<ParticipationDto> participations;

        try {
            ParameterizedTypeReference<List<ParticipationDto>> ref = new ParameterizedTypeReference<List<ParticipationDto>>() {
            };
            ResponseEntity<List<ParticipationDto>> response = restTemplate.exchange(URI.create(url), HttpMethod.GET, entity, ref);
            participations = response.getBody();

        } catch (RestClientException e) {
            throw new ServiceException("Could not retrieve participations: " + e.getMessage(), e);
        }

        return participations;
    }
}
