package at.ac.tuwien.inso.ticketline.client.gui.controller.tabs;

import at.ac.tuwien.inso.ticketline.client.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.client.gui.controller.LoginController;
import at.ac.tuwien.inso.ticketline.client.gui.controller.MainController;
import at.ac.tuwien.inso.ticketline.client.gui.controller.shoppingCart.SelectCustomerController;
import at.ac.tuwien.inso.ticketline.client.service.CustomerService;
import at.ac.tuwien.inso.ticketline.client.service.PremiumService;
import at.ac.tuwien.inso.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.ticketline.client.util.DialogUtil;
import at.ac.tuwien.inso.ticketline.client.util.SpringFxmlLoader;
import at.ac.tuwien.inso.ticketline.dto.CustomerDto;
import at.ac.tuwien.inso.ticketline.dto.PremiumDto;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;

@Component
public class PremiumsController implements Initializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(PremiumsController.class);
    @Autowired
    private MainController mainController;

    @Autowired
    private SpringFxmlLoader springFxmlLoader;
    @Autowired
    private CustomerService customerService;

    @FXML
    private Button find;

    @FXML
    private Label points;

    @FXML
    private Label pointsL;

    /**
     * Initializes the controller.
     *
     * @param location  location of the fxml file
     * @param resources resources
     */
    @Autowired
    private PremiumService premiumService;
    int i = 0;
    PremiumDto joker = new PremiumDto();

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        try {
            premiumDtos = FXCollections.observableArrayList(premiumService.getPremiums());
            fillTablePremium(FXCollections.observableArrayList(premiumDtos));
        } catch (ServiceException e) {

            DialogUtil.showExceptionDialog("Can not send initialize", "", e);
        }
        reinitLocale(MainController.currentLocale);
    }

    public void reinitLocale(Locale newValue) {
        LocaleContextHolder.setLocale(newValue);
        BundleManager.changeLocale(newValue);


        name.setText(BundleManager.getBundle().getString("premium.name"));
        cost.setText(BundleManager.getBundle().getString("premium.cost"));
        description.setText(BundleManager.getBundle().getString("premium.description"));
        available.setText(BundleManager.getBundle().getString("premium.avaiable"));

        fnameL.setText(BundleManager.getBundle().getString("shoppingcartSell.fn"));
        lnameL.setText(BundleManager.getBundle().getString("shoppingcartSell.ln"));
        pcL.setText(BundleManager.getBundle().getString("shoppingcartSell.pc"));
        genderL.setText(BundleManager.getBundle().getString("shoppingcartSell.gender"));
        birthL.setText(BundleManager.getBundle().getString("shoppingcartSell.birth"));
        emailL.setText(BundleManager.getBundle().getString("shoppingcartSell.email"));
        phoneL.setText(BundleManager.getBundle().getString("shoppingcartSell.phone"));

        pointsL.setText(BundleManager.getBundle().getString("premium.points"));

        selectCustomerBtn.setText(BundleManager.getBundle().getString("premium.selectCustomer"));
        searchT.setPromptText(BundleManager.getBundle().getString("premium.searchTF"));
    }


    @FXML
    private Button selectCustomerBtn;

    @FXML
    Label label_forename;
    @FXML
    Label label_lastname;
    @FXML
    Label label_postalcode;
    @FXML
    Label label_gender;
    @FXML
    Label label_birthdate;
    @FXML
    Label label_email;
    @FXML
    Label label_phone;
    @FXML
    Label label_premium;
    @FXML
    private Label fnameL, lnameL, streetL, pcL, cityL, countryL, genderL, birthL, emailL, phoneL;
    private CustomerDto customerDto;
    private ObservableList<PremiumDto> premiumDtos;

    @FXML
    public void handleSelectCustomer(ActionEvent event) {

        ((Node) event.getSource()).setCursor(Cursor.WAIT);

        Stage nextStage = new Stage();
        SpringFxmlLoader.LoadWrapper loadWrapper = springFxmlLoader.loadAndWrap("/gui/fxml/shoppingCart/select_customer_prototype.fxml");
        nextStage.setScene(new Scene((Parent) loadWrapper.getLoadedObject()));

        SelectCustomerController controller = (SelectCustomerController) loadWrapper.getController();
        controller.setPremiumController(this);

        ((Node) event.getSource()).setCursor(Cursor.DEFAULT);
        Node source = (Node) event.getSource();
        Stage thisStage = (Stage) source.getScene().getWindow();
        controller.setPreviousStage(thisStage);

        nextStage.setResizable(false);
        nextStage.setTitle(BundleManager.getBundle().getString("app.name"));
        nextStage.getIcons().add(new Image(LoginController.class.getResourceAsStream("/image/ticketlineLogo.png")));

        nextStage.initModality(Modality.WINDOW_MODAL);
        nextStage.initOwner(thisStage);

        controller.setThisStage(nextStage);
        nextStage.showAndWait();
    }


    /**
     * Sets the customer, who should be on the receipt, and displays his/her data on the labels
     *
     * @param customer The customer, who should be on the receipt
     */
    public void setCustomer(CustomerDto customer) {

        try {

            this.customerDto = customer;
            label_forename.setText(customerDto.getFirstname());
        } catch (NullPointerException e) {
            //not initialized!
            return;
        }
        label_lastname.setText(customerDto.getLastname());
        label_gender.setText(customerDto.getGender());

        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.YYYY");
        Date birthday = customerDto.getDateOfBirth();
        label_birthdate.setText(birthday == null ? "" : formatter.format(birthday));

        String email = customerDto.getEmail();
        label_email.setText(email == null || email.trim().isEmpty() ? "-" : email);

        String phone = customerDto.getPhoneNumber();
        label_phone.setText(phone == null || phone.trim().isEmpty() ? "-" : phone);

        label_postalcode.setText(customerDto.getPostalcode());


        label_premium.setText(String.valueOf(customerDto.getPremiumPoints()));

        LOGGER.info("set customer for receipt - ID: " + customer.getId()
                + " Name: " + customer.getFirstname() + " " + customer.getLastname());
    }


    @FXML
    private TableView<PremiumDto> tablePremium;

    @FXML
    private TableColumn<CustomerDto, String> name, cost, description, available;

    private void fillTablePremium(ObservableList<PremiumDto> obs) {
        name.setCellValueFactory(new PropertyValueFactory<>("name"));
        cost.setCellValueFactory(new PropertyValueFactory<>("price"));
        description.setCellValueFactory(new PropertyValueFactory<>("description"));
        available.setCellValueFactory(new PropertyValueFactory<>("available"));
        name.setSortType(TableColumn.SortType.ASCENDING);
        tablePremium.setItems(obs);
    }

    @FXML
    private void sellPremium() {
        PremiumDto a = tablePremium.getSelectionModel().getSelectedItem();
        tablePremium.getSelectionModel().clearSelection();
        if (a == null)
            return;
        if (customerDto == null) {
            DialogUtil.showErrorDialog(BundleManager.getBundle().getString("premium.nocustomerTitle"),
                    BundleManager.getBundle().getString("premium.nocustomerHeading"),
                    BundleManager.getBundle().getString("premium.nocustomerText"));
        } else {
            if (!a.getDescription().equals("") && !a.getName().equals("")) {

                if (a.getPrice() <= customerDto.getPremiumPoints()) {
                    try {
                        PremiumDto p = premiumService.searchPremium(a);
                        a.setAvailable(p.getAvailable());
                        if (a.getAvailable() > -1) {
                            if (DialogUtil.showConfirmationDialog(BundleManager.getBundle().getString("premium.sellTitle"),
                                    BundleManager.getBundle().getString("premium.sellHeading")) == ButtonType.OK) {
                                customerDto.setPremiumPoints(-1 * a.getPrice());
                                try {
                                    customerDto.setPremiumPoints(customerService.editCustomer(customerDto));
                                } catch (ServiceException e) {
                                    customerDto.setPremiumPoints(0);
                                    DialogUtil.showErrorDialog(BundleManager.getBundle().getString("premium.nopointsTitle"),
                                            BundleManager.getBundle().getString("premium.nopointsHeading"),
                                            BundleManager.getBundle().getString("premium.nopointsText"));
                                    PremiumDto pp = premiumService.returnPremium(a);
                                }
                                label_premium.setText(String.valueOf(customerDto.getPremiumPoints()));
                            } else {
                                PremiumDto pp = premiumService.returnPremium(a);
                                a.setAvailable(pp.getAvailable());
                            }
                        } else {
                            DialogUtil.showErrorDialog(BundleManager.getBundle().getString("premium.noarticleTitle"),
                                    BundleManager.getBundle().getString("premium.noarticleHeading"),
                                    BundleManager.getBundle().getString("premium.noarticleText"));
                            a.setAvailable(0);
                        }
                        if (premiumDtos.contains(joker)) {
                            premiumDtos.remove(joker);
                        } else {
                            joker.setDescription("");
                            joker.setName("");
                            premiumDtos.add(joker);
                        }

                        fillTablePremium(premiumDtos);
                    } catch (ServiceException e) {
                        DialogUtil.showExceptionDialog("Exception from Server", "Exception from Server!", e);
                    }
                } else {
                    DialogUtil.showErrorDialog(BundleManager.getBundle().getString("premium.nopointsTitle"),
                            BundleManager.getBundle().getString("premium.nopointsHeading"),
                            BundleManager.getBundle().getString("premium.nopointsText"));
                }
            }
        }
    }

    @FXML
    private TextField searchT;

    @FXML
    private void searchPremium() {
        ObservableList<PremiumDto> perf = FXCollections.observableArrayList();
        if (searchT.getText().equals("")) {
            try {
                premiumDtos = FXCollections.observableArrayList(premiumService.getPremiums());
                fillTablePremium(FXCollections.observableArrayList(premiumDtos));
            } catch (ServiceException e) {
                DialogUtil.showExceptionDialog("Can not send initialize", "", e);
            }
            return;
        } else {
            int i = -666;
            try {
                i = Integer.valueOf(searchT.getText());

            } catch (NullPointerException | NumberFormatException e) {
            }
            for (PremiumDto performanceDto : premiumDtos) {
                try {
                    if (performanceDto.getId().equals(i)) {
                        perf.add(performanceDto);
                    }

                } catch (NullPointerException e) {
                }
                if (performanceDto.getName().toLowerCase().contains(searchT.getText().toLowerCase())) {
                    perf.add(performanceDto);
                }
            }
        }
        fillTablePremium(perf);

    }
}
