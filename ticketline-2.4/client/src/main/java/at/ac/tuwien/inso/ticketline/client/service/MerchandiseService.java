package at.ac.tuwien.inso.ticketline.client.service;

import at.ac.tuwien.inso.ticketline.client.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.dto.MerchandiseDto;
import at.ac.tuwien.inso.ticketline.dto.PerformanceDto;

import java.util.List;

public interface MerchandiseService {



    /**
     * Get Performances of this article
     * @param reservationDto
     * @return
     * @throws ServiceException
     */
    PerformanceDto getArticlesOfPerformance(PerformanceDto reservationDto) throws ServiceException;
    MerchandiseDto checkMerchandise(MerchandiseDto merchandiseDto) throws ServiceException;
    List<MerchandiseDto> releaseArticle(List<MerchandiseDto> merchandiseDtos) throws ServiceException;

}
