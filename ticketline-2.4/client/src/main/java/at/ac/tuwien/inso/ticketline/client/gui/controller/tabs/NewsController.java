package at.ac.tuwien.inso.ticketline.client.gui.controller.tabs;

import at.ac.tuwien.inso.ticketline.client.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.client.gui.controller.MainController;
import at.ac.tuwien.inso.ticketline.client.gui.controller.MessageElementController;
import at.ac.tuwien.inso.ticketline.client.gui.controller.NewsElementController;
import at.ac.tuwien.inso.ticketline.client.service.NewsService;
import at.ac.tuwien.inso.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.ticketline.client.util.DateUtil;
import at.ac.tuwien.inso.ticketline.client.util.DialogUtil;
import at.ac.tuwien.inso.ticketline.client.util.SpringFxmlLoader;
import at.ac.tuwien.inso.ticketline.dto.NewsDto;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.layout.VBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;

@Component
public class NewsController implements Initializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(NewsController.class);

    @Autowired
    private MainController mainController;

    @Autowired
    private NewsService newsService;

    @Autowired
    private SpringFxmlLoader springFxmlLoader;

    @FXML
    private VBox vBox;

    @FXML
    private Label showNews;

    @FXML
    private DatePicker dp_newsdate;

    @FXML
    private Button showBtn;

    MessageElementController messageElementController;
    private String lastLoginDate;

    /**
     * Initializes the controller.
     *
     * @param location  location of the fxml file
     * @param resources resources
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        dp_newsdate.setConverter(DateUtil.getEuropeanDateStringConverter());

        reinitLocale(MainController.currentLocale);
    }

    public void reinitLocale(Locale newValue) {
        LocaleContextHolder.setLocale(newValue);
        BundleManager.changeLocale(newValue);
        showBtn.setText(BundleManager.getBundle().getString("news.showBtn"));
        showNews.setText(BundleManager.getBundle().getString("news.showLable"));

        if(messageElementController != null && lastLoginDate != null) {
            messageElementController.initializeMessage(BundleManager.getBundle().getString("news.noNewsSinceLogin") + " (" + this.lastLoginDate + ")");

        }

    }

    public void initializeNews(Date lastLogin) {
        List<NewsDto> news;

        try{
            news = newsService.getNews();

            if(news != null && !news.isEmpty()) {
                Collections.reverse(news);
            }

        } catch (ServiceException se){
            DialogUtil.showErrorDialog("News","Couldn't retrieve the news",se.getMessage());
            return;
        }

        Iterator<NewsDto> newsDtoIterator = news.iterator();
        ObservableList<Node> observableList = vBox.getChildren();

        int newNews = 0;

        while(newsDtoIterator.hasNext()) {

            NewsDto newsDto = newsDtoIterator.next();

            if (newsDto.getSubmittedOn().after(lastLogin)) {

                observableList.add(generateNews(newsDto));
                newNews++;

                if (newsDtoIterator.hasNext()) {
                    observableList.add(new Separator());
                }
            }
        }

        if(newNews == 0) {
            SpringFxmlLoader.LoadWrapper loader = springFxmlLoader.loadAndWrap("/gui/fxml/news/noNewsSinceLastLogin.fxml");
            MessageElementController messageElementController = (MessageElementController)loader.getController();

            DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm");
            String loginDate = dateFormat.format(lastLogin);

            this.lastLoginDate = loginDate;

            messageElementController.initializeMessage(BundleManager.getBundle().getString("news.noNewsSinceLogin") + " (" + loginDate + ")");
            this.messageElementController = messageElementController;

            observableList.add((Node) loader.getLoadedObject());
        }

    }

    @FXML
    private void onShowNewsButtonPressed() {

        LocalDate date = dp_newsdate.getValue();
        Date newsdate = null;

        if(date == null) {
            DialogUtil.showErrorDialog(BundleManager.getBundle().getString("news.error"),
                    BundleManager.getBundle().getString("news.cause"),
                    BundleManager.getBundle().getString("news.solution"));
            return;
        }
        else if(date.isAfter(LocalDate.now())) {
            DialogUtil.showErrorDialog(BundleManager.getBundle().getString("news.error"),
                    BundleManager.getBundle().getString("news.cause"),
                    BundleManager.getBundle().getString("news.solutionAfterToday"));
            return;
        }
        else {
            String year = "" + date.getYear();
            String month = "" + date.getMonthValue();
            String day = "" + date.getDayOfMonth();

            if (date.getMonthValue() < 10) {
                month = "0" + month;
            }
            if (date.getDayOfMonth() < 10) {
                day = "0" + day;
            }

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
            try {
                newsdate = formatter.parse(year + "/" + month + "/" + day);
            } catch (ParseException e) {
                LOGGER.error("Could not parse Newsdate");
                DialogUtil.showErrorDialog(BundleManager.getBundle().getString("news.error"),
                        BundleManager.getBundle().getString("news.cause"),
                        BundleManager.getBundle().getString("news.solution"));
                return;
            }
        }

        List<NewsDto> news;

        try {
            news = newsService.getBefore(newsdate);

            if(news != null && !news.isEmpty()) {
                Collections.reverse(news);
            }

        } catch (ServiceException e) {
            DialogUtil.showErrorDialog("News","Couldn't retrieve the news", e.getMessage());
            return;
        }

        if(news.isEmpty()) {
            LOGGER.error("no news since " + date);
            DialogUtil.showErrorDialog(BundleManager.getBundle().getString("news.error"),
                    BundleManager.getBundle().getString("news.causeNothingPublished"),
                    BundleManager.getBundle().getString("news.solutionEarlier"));
            return;
        }

        Iterator<NewsDto> newsDtoIterator = news.iterator();
        ObservableList<Node> observableList = vBox.getChildren();
        observableList.clear();

        while(newsDtoIterator.hasNext()) {
            NewsDto newsDto = newsDtoIterator.next();

            Node n = generateNews(newsDto);
            n.autosize();

            observableList.add(n);
            if (newsDtoIterator.hasNext()) {
                observableList.add(new Separator());
            }
        }
    }

    private Node generateNews(NewsDto newsDto){
        SpringFxmlLoader.LoadWrapper loader = springFxmlLoader.loadAndWrap("/gui/fxml/news/newsElement.fxml");
        NewsElementController newsElementController = (NewsElementController)loader.getController();
        newsElementController.initializeData(newsDto);
        return (Node) loader.getLoadedObject();
    }
}
