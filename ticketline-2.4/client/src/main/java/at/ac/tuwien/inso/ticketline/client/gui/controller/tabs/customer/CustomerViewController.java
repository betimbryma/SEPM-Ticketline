package at.ac.tuwien.inso.ticketline.client.gui.controller.tabs.customer;

import at.ac.tuwien.inso.ticketline.client.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.client.gui.controller.LoginController;
import at.ac.tuwien.inso.ticketline.client.gui.controller.MainController;
import at.ac.tuwien.inso.ticketline.client.gui.controller.shoppingCart.SellController;
import at.ac.tuwien.inso.ticketline.client.gui.controller.tabs.search.SearchController;
import at.ac.tuwien.inso.ticketline.client.gui.controller.tabs.search.views.TicketCircle;
import at.ac.tuwien.inso.ticketline.client.pdf.PDFCreator;
import at.ac.tuwien.inso.ticketline.client.service.*;
import at.ac.tuwien.inso.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.ticketline.client.util.DialogUtil;
import at.ac.tuwien.inso.ticketline.client.util.SpringFxmlLoader;
import at.ac.tuwien.inso.ticketline.dto.*;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Created by Betim on 5/29/2016.
 */
@Component
public class CustomerViewController {

    private Sequence sequence;
    private ArrayList<String> sekuencat;
    private ArrayList<String> numberSequence;

    @Autowired
    SearchController searchController;
    @Autowired
    CustomerService customerService;
    @Autowired
    SpringFxmlLoader springFxmlLoader;
    @FXML
    private Label localBirthday, localPhone, localStreet, localPostal, localCity, localCountry, localGender,
            birthday, email, phone, street, postal, city, country, gender, customerView, mainLabel, pageLabel, name, localPoints, poenat,
            soldSeats, reservedSeats, inCartSeats;

    @FXML
    private Button cancel, back, sell;

    @FXML
    private ComboBox<ShowDto> menu;

    private MainController mainController;

    private CustomerDto customerDto;
    @FXML
    private ScrollPane scrollPane;
    @FXML
    private BorderPane seatingBorderPane;
    private StackPane stackPane;
    private AnchorPane mainAnchorPane;
    private CustomersController customerController;
    private Map<ShowDto, List<TicketIdentifierDto>> showList;
    private List<TicketCircle> selected;
    private List<TicketCircle> inCart;
    private ShowDto showDto;
    private GridPane gridPane;
    @Autowired
    private TicketService ticketService;

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomerViewController.class);
    @Autowired
    ShowService showService;
    @Autowired
    ReservationService reservationService;
    @Autowired
    ReceiptService receiptService;
    @Autowired
    SellController sellController;

    public void initialize() {
        menu.valueProperty().addListener(e -> {
            ShowDto showDto = menu.getSelectionModel().getSelectedItem();
            if (showDto != null)
                setShow(showDto);
        });
        sekuencat = new ArrayList<>();
        numberSequence = new ArrayList<>();
        sequence = new Sequence();
        for (int i = 0; i < 702; i++)
            sekuencat.add(sequence.next());
        for (int i = 0; i < 650; i++)
            numberSequence.add(String.format("%03d", i));
    }


    public void setMainController(MainController mainController) {
        this.mainController = mainController;
    }

    public void setCustomerDto(CustomerDto customerDto) {
        this.customerDto = customerDto;
        reinitLocale(MainController.currentLocale);
        name.setText(customerDto.getFirstname() + " " + customerDto.getLastname());
        street.setText(customerDto.getStreet());
        postal.setText(customerDto.getPostalcode());
        city.setText(customerDto.getCity());
        country.setText(customerDto.getCountry());

        if (customerDto.getDateOfBirth() != null) {
            SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.YYYY");
            birthday.setText(formatter.format(customerDto.getDateOfBirth()));
        }

        if (customerDto.getEmail() != null)
            email.setText(customerDto.getEmail());
        if (customerDto.getPhoneNumber() != null)
            phone.setText(customerDto.getPhoneNumber());
        if (customerDto.getGender() != null)
            gender.setText(customerDto.getGender());
        try {
            CustomerDto customerDto1 = customerService.getCustomerByID(customerDto.getId());
            if (customerDto.getPremiumPoints() != null)
                poenat.setText(customerDto1.getPremiumPoints().toString());
        } catch (ServiceException se) {
            poenat.setText("0");
        }
    }

    public void reinitLocale(Locale newValue) {
        LocaleContextHolder.setLocale(newValue);
        BundleManager.changeLocale(newValue);

        back.setText(BundleManager.getBundle().getString("viewCustomer.backB"));
        cancel.setText(BundleManager.getBundle().getString("viewCustomer.cancel"));
        sell.setText(BundleManager.getBundle().getString("viewCustomer.sell"));

        localBirthday.setText(BundleManager.getBundle().getString("viewCustomer.birthday"));
        localPhone.setText(BundleManager.getBundle().getString("viewCustomer.phone"));
        localStreet.setText(BundleManager.getBundle().getString("viewCustomer.street"));

        localPostal.setText(BundleManager.getBundle().getString("viewCustomer.postal"));
        localCity.setText(BundleManager.getBundle().getString("viewCustomer.city"));
        localCountry.setText(BundleManager.getBundle().getString("viewCustomer.country"));
        localGender.setText(BundleManager.getBundle().getString("viewCustomer.gender"));
        customerView.setText(BundleManager.getBundle().getString("viewCustomer.title"));
        pageLabel.setText(BundleManager.getBundle().getString("viewCustomer.customer"));
        mainLabel.setText(BundleManager.getBundle().getString("viewCustomer.main"));
        menu.setPromptText(BundleManager.getBundle().getString("viewCustomer.menu"));
        localPoints.setText(BundleManager.getBundle().getString("viewCustomer.points"));
        soldSeats.setText(BundleManager.getBundle().getString("viewCustomer.sold"));
        reservedSeats.setText(BundleManager.getBundle().getString("viewCustomer.reserved"));
        inCartSeats.setText(BundleManager.getBundle().getString("viewCustomer.inCart"));

    }

    public void setStackPane(StackPane stackPane) {
        this.stackPane = stackPane;
    }

    public void setAnchorPane(AnchorPane mainAnchorPane) {
        this.mainAnchorPane = mainAnchorPane;
    }

    public void setCustomerController(CustomersController customerController) {
        this.customerController = customerController;
    }

    @FXML
    private void onBackButtonPressed(ActionEvent event) {
        LOGGER.info("User clikced the back button, going back to the main-customers page");
        stackPane.getChildren().clear();
        stackPane.getChildren().add(mainAnchorPane);
    }

    @FXML
    public void onCancelPressed(ActionEvent event) {
        int premiumPointsDifference = 0;
        if (!selected.isEmpty()) {
            List<TicketIdentifierDto> tiketat = new ArrayList<>();
            List<ReceiptentryDto> boughtTickets = new LinkedList<>();

            for (TicketCircle circle : selected) {
                if (circle.getOriginalColor() == Color.GOLD) {
                    premiumPointsDifference += circle.getTicketIdentifierDto().getTicket().getPrice() / 10;
                    ReceiptentryDto r = new ReceiptentryDto();
                    r.setTicketIdentifierDto(circle.getTicketIdentifierDto());
                    boughtTickets.add(r);
                }
                tiketat.add(circle.getTicketIdentifierDto());

            }

            int oldPoints;

            try {
                oldPoints = customerDto.getPremiumPoints();
            } catch (NullPointerException e) {
                oldPoints = 0;
            }
            try {
                customerDto.setPremiumPoints(premiumPointsDifference * (-1));

                int newPoints = customerService.editCustomer(customerDto);
                poenat.setText(newPoints + "");
                customerDto = customerService.getCustomerByID(customerDto.getId());

                try {
                    ReceiptDto receipt = new ReceiptDto();
                    receipt.setCustomer(customerDto);

                    UserStatusDto employee;
                    try {
                        EmployeeDto e = mainController.getCurrentEmployee();
                        employee = new UserStatusDto();
                        employee.setAnonymous(false);
                        employee.setFirstName(e.getFirstName());
                        employee.setLastName(e.getLastName());
                    } catch (NullPointerException e) {
                        employee = null;
                    }

                    receipt.setEmployee(employee);
                    receipt.setReceiptentryDto(boughtTickets);

                    Node source = (Node) event.getSource();
                    Stage thisStage = (Stage) source.getScene().getWindow();
                    new PDFCreator(thisStage, true).createInvoicePDF(receipt);
                } catch (IOException e) {
                    DialogUtil.showErrorDialog(BundleManager.getExceptionBundle().getString("custView.pdfCreationErrTitle"),
                            BundleManager.getExceptionBundle().getString("custView.pdfCreationErrHeader"), "");
                }

            } catch (ServiceException e) {

                poenat.setText(oldPoints + "");
                DialogUtil.showInformationDialog(BundleManager.getBundle().getString("cancel.notEnoughPointsTitle"),
                        BundleManager.getBundle().getString("cancel.notEnoughPointsHeader"),
                        BundleManager.getBundle().getString("cancel.notEnoughPointsText"));
                return;

            }

            for (TicketCircle circle : selected) {

                circle.setFill(Color.DARKGRAY);
                circle.setOnMouseClicked(e -> {
                });

            }

            Service<Void> service = new Service<Void>() {
                @Override
                protected Task<Void> createTask() {
                    return new Task<Void>() {
                        @Override
                        protected Void call() throws Exception {
                            try {
                                reservationService.cancelReservation(tiketat);
                            } catch (ServiceException se) {
                                DialogUtil.showExceptionDialog("Ticket selling", "Sorry something went wrong with the server", se);
                            }
                            return null;
                        }
                    };
                }
            };
            service.restart();


        } else {
            DialogUtil.showInformationDialog(BundleManager.getBundle().getString("cancel.noSeatsSelectedTitle"),
                    BundleManager.getBundle().getString("cancel.noSeatsSelectedHeader"),
                    BundleManager.getBundle().getString("cancel.noSeatsSelectedText"));
        }
        selected = new ArrayList<>();
    }

    public void getShows() {
        if (customerDto != null) {
            try {
                menu.valueProperty().set(null);
                List<MapEntryDto<ShowDto, List<TicketIdentifierDto>>> lista = showService.getShowsForCustomer(customerDto);

                showList = new HashMap<>();
                selected = new ArrayList<>();
                inCart = new ArrayList<>();

                for (MapEntryDto<ShowDto, List<TicketIdentifierDto>> map : lista) {
                    ShowDto showDto = map.getKey();
                    List<TicketIdentifierDto> ticketIdentifierDtos = map.getValue();
                    menu.getItems().add(showDto);
                    showList.put(showDto, ticketIdentifierDtos);
                }
                menu.valueProperty().addListener(e -> {
                    ShowDto showDto = menu.getSelectionModel().getSelectedItem();
                    if (showDto != null)
                        setShow(showDto);
                });

            } catch (ServiceException se) {
                System.out.println("Something went wrong"); //TODO implement real exception handling
            }
        }
    }

    private void setShow(ShowDto showDto) {


        this.showDto = showDto;
        selected = new ArrayList<>();

        ImageView image = new ImageView();
        Image i2;
        if (showDto.getRoom().getDescription().contains("Screen")) {
            i2 = new Image("image/Screen.PNG");
        } else {
            i2 = new Image("image/Stage.PNG");
        }
        image.setImage(i2);

        BorderPane borderPane = new BorderPane();
        gridPane = new GridPane();
        gridPane.setHgap(4);
        gridPane.setVgap(10);


        gridPane.setPadding(new Insets(10, 10, 10, 10));

        BorderPane.setAlignment(image, Pos.CENTER);
        borderPane.setTop(image);

        ArrayList<Integer> dim = new ArrayList<>();
        ArrayList<Integer> HS = new ArrayList<>();
        ArrayList<Integer> VS = new ArrayList<>();

        String description = showDto.getRoom().getDescription();
        Pattern dimensions = Pattern.compile("[\\{]\\d+\\D\\d+");
        Pattern hs = Pattern.compile("[\\H\\S](\\|*\\d+\\|)+[,]");
        Pattern vs = Pattern.compile("[\\V\\S](\\|*\\d+\\|)+[)]");
        Matcher m = dimensions.matcher(description);

        if (m.find()) {
            Pattern p = Pattern.compile("\\d+");
            Matcher m1 = p.matcher(m.group());
            while (m1.find()) {
                dim.add(Integer.parseInt(m1.group()));
            }
        }

        m = hs.matcher(description);
        if (m.find()) {
            Pattern p = Pattern.compile("\\d+");
            Matcher m1 = p.matcher(m.group());
            while (m1.find()) {
                int sum = 0;
                if (!HS.isEmpty())
                    sum = HS.get(HS.size() - 1);
                HS.add(Integer.parseInt(m1.group()) + sum);
            }
        }

        m = vs.matcher(description);
        if (m.find()) {
            Pattern p = Pattern.compile("\\d+");
            Matcher m1 = p.matcher(m.group());
            while (m1.find()) {
                int sum = 0;
                if (!VS.isEmpty())
                    sum = VS.get(VS.size() - 1);

                VS.add(Integer.parseInt(m1.group()) + sum);
            }
        }


        for (int x = 0; x <= dim.get(0); x++) {
            RowConstraints rowConstraints = new RowConstraints();
            if (VS.contains(x))
                rowConstraints.setPercentHeight(40);
            else
                rowConstraints.setPercentHeight(20);
            gridPane.getRowConstraints().add(rowConstraints);
        }
        for (int y = 0; y <= dim.get(1); y++) {
            ColumnConstraints columnConstraints = new ColumnConstraints();
            if (HS.contains(y))
                columnConstraints.setPercentWidth(40);
            else
                columnConstraints.setPercentWidth(20);
            gridPane.getColumnConstraints().add(columnConstraints);
        }

        for (int i = 1; i <= dim.get(0); i++) {
            Label label = new Label(sekuencat.get(25 + i));
            GridPane.setConstraints(label, 0, i, 1, 1, HPos.LEFT, VPos.TOP);
            gridPane.getChildren().add(label);
        }

        for (int i = 1; i <= dim.get(1); i++) {

            Text label = new Text(numberSequence.get(i - 1));
            GridPane.setConstraints(label, i, 0, 1, 1, HPos.LEFT, VPos.TOP);
            gridPane.getChildren().add(label);
        }


        for (int i = 1; i <= dim.get(1); i++) {

            for (int y = 1; y <= dim.get(0); y++) {

                Circle circle = new Circle();
                circle.setFill(Color.DARKGRAY);
                circle.setStroke(Color.BLACK);
                circle.setRadius(10.0);
                GridPane.setConstraints(circle, i, y, 1, 1, HPos.LEFT, VPos.TOP);


                gridPane.getChildren().add(circle);
            }

        }

        for (TicketIdentifierDto ticketIdentifierDto : showList.get(showDto)) {
            TicketDto ticketDto = ticketIdentifierDto.getTicket();
            SeatDto seat = ticketDto.getSeat();
            TicketCircle circle = new TicketCircle();
            circle.setStroke(Color.BLACK);
            circle.setRadius(10.0);
            circle.setOnMouseClicked(e -> {
                seatClicked(circle);
            });
            circle.setTicketIdentifierDto(ticketIdentifierDto);
            if (ticketIdentifierDto.getReservation() != null) {
                circle.setFill(Color.LIGHTSEAGREEN);
                circle.setOriginalColor(Color.LIGHTSEAGREEN);

            } else {
                circle.setFill(Color.GOLD);
                circle.setOriginalColor(Color.GOLD);

            }
            GridPane.setConstraints(circle, seat.getGallery().getOrder(), seat.getRow().getOrder(), 1, 1, HPos.LEFT, VPos.TOP);
            gridPane.getChildren().add(circle);

        }


        borderPane.setCenter(gridPane);
        scrollPane.setContent(borderPane);
        seatingBorderPane.setCenter(scrollPane);


    }

    private void seatClicked(TicketCircle circle) {

        if (circle.getFill() == Color.BLUEVIOLET) {
            selected.remove(circle);
            circle.setFill(circle.getOriginalColor());
        } else if (circle.getFill() == Color.LIGHTSEAGREEN || circle.getFill() == Color.GOLD) {
            circle.setFill(Color.BLUEVIOLET);
            selected.add(circle);
        }
    }

    @FXML
    public void onSellPressed() {

        if (selected.isEmpty()) {
            DialogUtil.showInformationDialog(BundleManager.getBundle().getString("cancel.sell.noSeatsSelectedTitle"),
                    BundleManager.getBundle().getString("cancel.sell.noSeatsSelectedHeader"),
                    BundleManager.getBundle().getString("cancel.sell.noSeatsSelectedText"));
            return;
        }

        for (int i = 0; i < selected.size(); i++) {
            TicketCircle ticketCircle = selected.get(i);
            if (ticketCircle.getTicketIdentifierDto().getReservation() == null) {

                DialogUtil.showErrorDialog(BundleManager.getBundle().getString("cancel.sell.soldSelectedTitle"),
                        BundleManager.getBundle().getString("cancel.sell.soldSelectedHeader"),
                        BundleManager.getBundle().getString("cancel.sell.soldSelectedText"));
                return;
            }
        }

        try {
            sellController.setCustomer(selected.get(0).getTicketIdentifierDto().getReservation().getCustomer());
        } catch (NullPointerException e) {
            DialogUtil.showExceptionDialog("Ticket selling", "Sorry something went wrong with the server", e);
        }


        boolean added = true;
        int b = 0;
        for (int i = 0; i < selected.size(); i++) {
            TicketCircle ticketCircle = selected.get(i);


            try {

                if (!receiptService.addReservedTicketToCart(ticketCircle.getTicketIdentifierDto().getTicket())) {

                    b++;
                    added = false;
                    ticketCircle.setFill(ticketCircle.getOriginalColor());
                } else {
                    ticketCircle.setFill(Color.DARKORANGE);
                    ticketCircle.setStatus("Sold");
                    inCart.add(ticketCircle);
                }
            } catch (ServiceException e) {
                LOGGER.debug("Error in database");
                DialogUtil.showErrorDialog("Error Dialog", "Error in Server", e.getMessage());
            }


            TicketIdentifierDto oldTE = new TicketIdentifierDto();
            oldTE.setId(ticketCircle.getTicketIdentifierDto().getId());

            oldTE.setReservation(ticketCircle.getTicketIdentifierDto().getReservation());
            oldTE.setVoidationTime(ticketCircle.getTicketIdentifierDto().getVoidationTime());
            oldTE.setVoidedBy(ticketCircle.getTicketIdentifierDto().getVoidedBy());
            oldTE.setReservation(ticketCircle.getTicketIdentifierDto().getReservation());
            ticketCircle.getTicketIdentifierDto().getTicket().setOldTE(oldTE);
        }
        selected = new ArrayList<>();
        receiptService.isCartEmpty();
        if (!added) {
            if (DialogUtil.showConfirmationDialog("Ticket selling", b + " tickets are being used in another system. Do you want to add the rest of them?") != ButtonType.OK) {
                List<TicketDto> ticket = new ArrayList<>();
                for (int e = 0; e < inCart.size(); e++) {
                    ticket.add(inCart.get(e).getTicketIdentifierDto().getTicket());
                    receiptService.getTicketCart().remove(inCart.get(e).getTicketIdentifierDto().getTicket());
                }
                try {
                    ticketService.releaseTicket(ticket);
                } catch (ServiceException e) {
                    DialogUtil.showExceptionDialog("Error in Database", "Error in Database", e);
                }
                for (int e = 0; e < inCart.size(); e++) {
                    TicketCircle ticketCircle = inCart.get(e);
                    ticketCircle.setFill(ticketCircle.getOriginalColor());
                }
                inCart = new ArrayList<>();
            }
        }

    }

    public void ticketRemoved(TicketDto ticketDto) {

        if (inCart != null) {
            for (TicketCircle ticketCircle : inCart) {
                TicketDto ticketDto1 = ticketCircle.getTicketIdentifierDto().getTicket();
                if (ticketDto1.equals(ticketDto)) {
                    ticketCircle.setFill(ticketCircle.getOriginalColor());
                    inCart.remove(ticketCircle);
                    return;
                }

            }
        }
    }

    public void resetCurrentShow(CustomerDto customerDto) {
        if (showDto != null && customerDto != null && this.customerDto.equals(customerDto)) {
            try {
                List<TicketDto> tickets = new ArrayList<>();
                for (TicketCircle ticketCircle : inCart) {
                    TicketDto ticketDto = ticketCircle.getTicketIdentifierDto().getTicket();
                    tickets.add(ticketDto);
                }
                List<TicketIdentifierDto> ticketIdentifierDtos = showService.getTicketsForShowAndCustomer(tickets);
                for (TicketIdentifierDto ticketIdentifierDto : ticketIdentifierDtos) {
                    TicketDto ticketDto = ticketIdentifierDto.getTicket();
                    SeatDto seat = ticketDto.getSeat();
                    TicketCircle circle = new TicketCircle();
                    circle.setStroke(Color.BLACK);
                    circle.setRadius(10.0);
                    circle.setOnMouseClicked(e -> {
                        seatClicked(circle);
                    });
                    circle.setTicketIdentifierDto(ticketIdentifierDto);
                    if (ticketIdentifierDto.getReservation() != null) {
                        circle.setFill(Color.LIGHTSEAGREEN);
                        circle.setOriginalColor(Color.LIGHTSEAGREEN);

                    } else {
                        circle.setFill(Color.GOLD);
                        circle.setOriginalColor(Color.GOLD);

                    }
                    GridPane.setConstraints(circle, seat.getGallery().getOrder(), seat.getRow().getOrder(), 1, 1, HPos.LEFT, VPos.TOP);
                    gridPane.getChildren().add(circle);
                }
                inCart = new ArrayList<>();


            } catch (ServiceException se) {
                DialogUtil.showExceptionDialog("Ticket selling", "Sorry something went wrong with the server", se);
            }
        } else {
            if (inCart != null) {
                try {
                    List<TicketDto> tickets = new ArrayList<>();
                    for (TicketCircle ticketCircle : inCart) {
                        TicketDto ticketDto = ticketCircle.getTicketIdentifierDto().getTicket();
                        tickets.add(ticketDto);
                    }
                    List<TicketIdentifierDto> ticketIdentifierDtos = showService.getTicketsForShowAndCustomer(tickets);
                    for (TicketCircle ticketCircle : inCart) {
                        if (ticketIdentifierDtos.contains(ticketCircle.getTicketIdentifierDto()))
                            ticketCircle.setFill(ticketCircle.getOriginalColor());
                        else {
                            ticketCircle.setFill(Color.DARKGRAY);
                            ticketCircle.setOnMouseClicked(e -> {
                            });
                        }
                    }
                } catch (ServiceException se) {
                    DialogUtil.showExceptionDialog("Ticket selling", "Sorry something went wrong with the server", se);
                }
            }
            inCart = new ArrayList<>();
        }
    }

    @FXML
    private void popUp(ActionEvent event) {
        if (showDto != null) {
            CustomerViewPopUp customerViewPopUp;
            SpringFxmlLoader.LoadWrapper loadWrapper = springFxmlLoader.loadAndWrap("/gui/fxml/tabs/customers/customer_view_popup.fxml");
            customerViewPopUp = (CustomerViewPopUp) loadWrapper.getController();
            Stage newStage = new Stage();
            newStage.setScene(new Scene((Parent) loadWrapper.getLoadedObject()));
            newStage.setTitle(BundleManager.getBundle().getString("app.name"));
            newStage.getIcons().add(new Image(LoginController.class.getResourceAsStream("/image/ticketlineLogo.png")));
            seatingBorderPane.getChildren().clear();
            this.seatingBorderPane.setCenter(new ScrollPane());
            customerViewPopUp.setSold(soldSeats.getText());
            customerViewPopUp.setReserved(reservedSeats.getText());
            customerViewPopUp.setInCart(inCartSeats.getText());
            customerViewPopUp.setStage(newStage);
            customerViewPopUp.setCustomerViewController(this);
            customerViewPopUp.setScrollPane(this.scrollPane);
            //seatingBorderPane.requestLayout();

            Node source = (Node) event.getSource();
            Stage thisStage = (Stage) source.getScene().getWindow();

            newStage.initOwner(thisStage);
            newStage.initModality(Modality.WINDOW_MODAL);

            newStage.setMaximized(true);

            newStage.show();

        }

    }

    public void setScrollPane(ScrollPane scrollPane) {
        this.scrollPane = scrollPane;
        this.seatingBorderPane.setCenter(scrollPane);
    }


}
