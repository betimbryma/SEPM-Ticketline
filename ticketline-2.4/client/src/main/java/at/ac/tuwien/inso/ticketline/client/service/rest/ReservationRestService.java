package at.ac.tuwien.inso.ticketline.client.service.rest;

import at.ac.tuwien.inso.ticketline.client.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.client.exception.ValidationException;
import at.ac.tuwien.inso.ticketline.client.service.ReservationService;
import at.ac.tuwien.inso.ticketline.dto.MessageDto;
import at.ac.tuwien.inso.ticketline.dto.ReservationDto;
import at.ac.tuwien.inso.ticketline.dto.TicketIdentifierDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.Collections;
import java.util.List;

/**
 * Created by Betim on 5/16/2016.
 */
@Component
public class ReservationRestService implements ReservationService {

    public static final String SAVE_RESERVATION ="/service/reservation/";
    public static final String GET_ALL_RESERVATIONS = "/service/reservation/";
    public static final String CANCEL_RESERVATIONS = "/service/reservation/cancel";

    private static final Logger LOGGER = LoggerFactory.getLogger(ReservationRestService.class);

    @Autowired
    private RestClient restClient;

    @Override
    public ReservationDto saveReservation(ReservationDto reservationDto) throws ServiceException {

        String url = this.restClient.createServiceUrl(SAVE_RESERVATION);
        LOGGER.info("Saving reservation at {}", url);

        RestTemplate restTemplate = this.restClient.getRestTemplate();
        HttpHeaders httpHeaders = restClient.getHttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        httpHeaders.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        HttpEntity<ReservationDto> entity = new HttpEntity<>(reservationDto, httpHeaders);

        ReservationDto reservationDto1;

        try {
            reservationDto1 = restTemplate.postForObject(url, entity, ReservationDto.class);
        } catch (HttpStatusCodeException e) {
            MessageDto errorMsg = this.restClient.mapExceptionToMessage(e);
            if (errorMsg.hasFieldErrors()) {
                throw new ValidationException(errorMsg.getFieldErrors());
            } else {
                throw new ServiceException(errorMsg.getText());
            }
        } catch (RestClientException e) {
            throw new ServiceException("Could not save reservation: " + e.getMessage(), e);
        }

        return reservationDto1;

    }

    @Override
    public List<ReservationDto> getReservations() throws ServiceException {
        RestTemplate restTemplate = this.restClient.getRestTemplate();
        String url = this.restClient.createServiceUrl(GET_ALL_RESERVATIONS);
        HttpEntity<String> entity = new HttpEntity<>(this.restClient.getHttpHeaders());
        LOGGER.info("Retrieving reservations from {}", url);
        List<ReservationDto> reservations;
        try {
            ParameterizedTypeReference<List<ReservationDto>> ref = new ParameterizedTypeReference<List<ReservationDto>>() {
            };
            ResponseEntity<List<ReservationDto>> response = restTemplate.exchange(URI.create(url), HttpMethod.GET, entity, ref);
            reservations = response.getBody();

        } catch (RestClientException e) {
            throw new ServiceException("Could not retrieve reservations: " + e.getMessage(), e);
        }
        return reservations;
    }

    @Override
    public Integer cancelReservation(List<TicketIdentifierDto> ticketIdentifierDtos) throws ServiceException {

        String url = this.restClient.createServiceUrl(CANCEL_RESERVATIONS);
        LOGGER.info("Canceling reservations/soldTickets at {}", url);

        RestTemplate restTemplate = this.restClient.getRestTemplate();
        int rez = 0;
        HttpHeaders headers = this.restClient.getHttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        HttpEntity<List<TicketIdentifierDto>> entity = new HttpEntity<>(ticketIdentifierDtos, headers);


        try {
            ParameterizedTypeReference<Integer> parameterizedTypeReference = new ParameterizedTypeReference<Integer>() {};
            ResponseEntity<Integer> responseEntity = restTemplate.exchange(url, HttpMethod.POST, entity, parameterizedTypeReference);

            rez=responseEntity.getBody();
        } catch (RestClientException se) {
            throw new ServiceException("Could not cancel the tickets "+se.getMessage());
        } if(rez == 1)
            throw new ServiceException("Could not cancel the tickets ");

        return rez;

    }
}