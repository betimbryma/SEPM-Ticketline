package at.ac.tuwien.inso.ticketline.client.service;

import at.ac.tuwien.inso.ticketline.client.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.dto.EmployeeDto;

public interface EmployeeService {

    /**
     * Gets an employee with a certain username.
     *
     * @param employee the customer that is to be retrieved
     * @return the employee
     * @throws ServiceException if the employee has no username or if it is null
     */
    public EmployeeDto getEmployee(EmployeeDto employee) throws ServiceException;


    /**
     * Updates the last login date of a certain employee.
     *
     * @param employee the customer whos last login date should be updated
     * @throws ServiceException if the employee has no username or if it is null
     */
    public void updateLastLogin(EmployeeDto employee) throws ServiceException;
}
