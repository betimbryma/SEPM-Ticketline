package at.ac.tuwien.inso.ticketline.client.gui.controller;

import at.ac.tuwien.inso.ticketline.client.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.client.exception.ValidationException;
import at.ac.tuwien.inso.ticketline.client.gui.controller.tabs.NewsController;
import at.ac.tuwien.inso.ticketline.client.service.AuthService;
import at.ac.tuwien.inso.ticketline.client.service.EmployeeService;
import at.ac.tuwien.inso.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.ticketline.client.util.SpringFxmlLoader;
import at.ac.tuwien.inso.ticketline.dto.EmployeeDto;
import at.ac.tuwien.inso.ticketline.dto.UserEvent;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import org.controlsfx.control.NotificationPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.net.URL;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Controller for the login window
 */
@Component
public class LoginController implements Initializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoginController.class);

    @Autowired
    private SpringFxmlLoader springFxmlLoader;

    @Autowired
    private AuthService authService;

    @Autowired
    private EmployeeService employeeService;

    @FXML
    private TextField txtUsername;

    @FXML
    private PasswordField txtPassword;

    @FXML
    private Button btnLogin, btnExit;

    @FXML
    private ComboBox<Locale> cbLanguage;

    @FXML
    private Pane root;

    private NotificationPane notificationPane;

    private final ImageView errorImage = new ImageView(new Image(LoginController.class.getResourceAsStream("/image/icon/warning.png")));

    public static Locale currentLocale;

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        notificationPane = new NotificationPane(root.getChildren().get(0));
        root.getChildren().clear();
        root.getChildren().add(notificationPane);
        cbLanguage.getItems().clear();
        cbLanguage.getItems().addAll(BundleManager.getSupportedLocales());
        cbLanguage.valueProperty().addListener((observable, oldValue, newValue) -> reinitLocale(newValue));
        cbLanguage.getSelectionModel().select(resourceBundle.getLocale());
    }

    /**
     * Reinitializes the UI with the given locale.
     *
     * @param newValue the new value
     */
    private void reinitLocale(Locale newValue) {
        currentLocale = newValue;
        LocaleContextHolder.setLocale(newValue);
        BundleManager.changeLocale(newValue);
        btnExit.setText(BundleManager.getBundle().getString("generic.exit"));
        btnLogin.setText(BundleManager.getBundle().getString("login.login"));
        txtPassword.setPromptText(BundleManager.getBundle().getString("login.password"));
        txtUsername.setPromptText(BundleManager.getBundle().getString("login.username"));
    }

    /**
     * Logs the user in
     *
     * @param event the event
     */
    @FXML
    private void handleLogin(ActionEvent event) {

        // log employee in
        UserEvent login;
        try {
            login = this.authService.login(txtUsername.getText(), txtPassword.getText());
        } catch (ValidationException valid) {
            LOGGER.error(valid.getMessage());
            notificationPane.show(BundleManager.getExceptionBundle().getString("error.loginSuspension"), errorImage);
            return;
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage());
            notificationPane.show(BundleManager.getExceptionBundle().getString("error.connection"), errorImage);
            return;
        }

        if (login != UserEvent.AUTH_SUCCESS) {

            if(login == UserEvent.AUTH_FAILURE) {
                notificationPane.show(BundleManager.getExceptionBundle().getString("error.loginData"), errorImage);
            }
            else if(login == UserEvent.USER_INVALID) {
                notificationPane.show(BundleManager.getExceptionBundle().getString("error.invalidUser"), errorImage);
            }
            else if(login == UserEvent.THREE_ATTEMPTS_LEFT) {
                notificationPane.show(BundleManager.getExceptionBundle().getString("error.threeAttempts"), errorImage);
            }
            else if(login == UserEvent.TWO_ATTEMPTS_LEFT) {
                notificationPane.show(BundleManager.getExceptionBundle().getString("error.twoAttempts"), errorImage);
            }
            else if(login == UserEvent.ONE_ATTEMPT_LEFT) {
                notificationPane.show(BundleManager.getExceptionBundle().getString("error.oneAttempt"), errorImage);
            }
            else if(login == UserEvent.AUTH_NOW_SUSPENDED) {
                notificationPane.show(BundleManager.getExceptionBundle().getString("error.AccountGotSuspended"), errorImage);
            }
            else if(login == UserEvent.AUTH_SUSPENDED) {
                notificationPane.show(BundleManager.getExceptionBundle().getString("error.AccountSuspended"), errorImage);
            }

            return;
        }


        EmployeeDto employee = new EmployeeDto();
        employee.setUsername(txtUsername.getText());

        //retrieve Data of Employee, who has just logged in
        try {
            employee = employeeService.getEmployee(employee);
            LOGGER.info("Last login of Employee " + employee.getFirstName() + " " + employee.getLastName()
                    + ": " + employee.getLastlogin());
        } catch (ServiceException e) {
            e.printStackTrace();
            LOGGER.error("Employee with Username " + employee.getUsername() + " could not be retrieved");
            return;
        }

        ((Node) event.getSource()).setCursor(Cursor.WAIT);
        Stage mainStage = new Stage();
        mainStage.setResizable(true);
        mainStage.setMaximized(true);
        mainStage.setMinWidth(820);
        mainStage.setMinHeight(620);

        SpringFxmlLoader.LoadWrapper loadWrapper = springFxmlLoader.loadAndWrap("/gui/fxml/main_prototype.fxml");
        mainStage.setScene(new Scene((Parent) loadWrapper.getLoadedObject(), 800, 620));


        mainStage.setTitle(BundleManager.getBundle().getString("app.name"));
        mainStage.getIcons().add(new Image(LoginController.class.getResourceAsStream("/image/ticketlineLogo.png")));

        MainController controller = (MainController) loadWrapper.getController();

        NewsController newscontroller = controller.getNewsController();
        newscontroller.initializeNews(employee.getLastlogin());

        //update last login date
        try {
            employee.setLastlogin(new Date());
            employeeService.updateLastLogin(employee);
            LOGGER.info("Updated last login of Employee " + employee.getFirstName() + " " + employee.getLastName()
                    + ": " + employee.getLastlogin());
        } catch (NullPointerException | ServiceException e) {
            e.printStackTrace();
            LOGGER.error("Last Login Date could not be updated");
        }

        //set the employee, that has just logged in, as Employee in the main controller
        controller.setCurrentEmployee(employee);

        controller.initCloseRequest(mainStage);
        ((Node) event.getSource()).setCursor(Cursor.DEFAULT);
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        stage.close();

        mainStage.show();
        controller.startLoadingThread();
    }

    /**
     * Exits the application
     *
     * @param event the event
     */
    @FXML
    private void handleExit(ActionEvent event) {
        ((Stage) ((Node) event.getSource()).getScene().getWindow()).close();
    }

}
