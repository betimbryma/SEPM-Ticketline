package at.ac.tuwien.inso.ticketline.client.util;

import javafx.util.StringConverter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {

    private static StringConverter<LocalDate> europeanDateStringConverter = new StringConverter<LocalDate>() {
        @Override
        public String toString(LocalDate d) {
            return getEuropeanDateString(d);
        }

        @Override
        public LocalDate fromString(String s) {
            return getDateFromString(s);
        }
    };

    public static StringConverter<LocalDate> getEuropeanDateStringConverter() {
        return europeanDateStringConverter;
    }

    public static String getEuropeanDateString(LocalDateTime d) {
        if (d == null) {
            return "";
        }

        int month = d.getMonthValue();
        int day = d.getDayOfMonth();
        int hour = d.getHour();
        int minute = d.getMinute();
        int second = d.getSecond();

        return (day < 10 ? "0" : "") + day + "." + (month < 10 ? "0" : "") + month + "." + d.getYear()
                + " " + (hour < 10 ? "0" : "") + hour + ":" + (minute < 10 ? "0" : "") + minute + ":" + (second < 10 ? "0" : "") + second;
    }

    public static String getEuropeanDateString(LocalDate d) {
        if (d == null) {
            return "";
        }

        int month = d.getMonthValue();
        int day = d.getDayOfMonth();

        return (day < 10 ? "0" : "") + day + "." + (month < 10 ? "0" : "") + month + "." + d.getYear();
    }

    public static String getEuropeanDateTimeString(Date d) {
        Calendar c = Calendar.getInstance();
        c.setTime(d);

        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);

        return getEuropeanDateString(d) + " " + (hour < 10 ? "0" : "") + hour + ":" + (minute < 10 ? "0" : "") + minute;
    }

    public static String getEuropeanDateString(Date d) {
        if (d == null) {
            return "";
        }

        Calendar c = Calendar.getInstance();
        c.setTime(d);

        int month = c.get(Calendar.MONTH) + 1;
        int day = c.get(Calendar.DAY_OF_MONTH);

        return (day < 10 ? "0" : "") + day + "." + (month < 10 ? "0" : "") + month + "." + c.get(Calendar.YEAR);
    }

    public static LocalDate getDateFromString(String s) {
        if (s == null) {
            return null;
        }
        String[] tokens = s.contains(".") ? s.split("\\.") : s.split("-");
        try {
            int day = Integer.parseInt(tokens[0]);
            int month = Integer.parseInt(tokens[1]);
            int year = Integer.parseInt(tokens[2]);


            return LocalDate.of(year, month, day);
        } catch (NumberFormatException | ArrayIndexOutOfBoundsException e) {
            return null;
        }
    }

}
