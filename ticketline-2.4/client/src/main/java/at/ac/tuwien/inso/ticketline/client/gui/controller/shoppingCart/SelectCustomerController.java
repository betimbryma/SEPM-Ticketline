package at.ac.tuwien.inso.ticketline.client.gui.controller.shoppingCart;

import at.ac.tuwien.inso.ticketline.client.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.client.gui.controller.LoginController;
import at.ac.tuwien.inso.ticketline.client.gui.controller.MainController;
import at.ac.tuwien.inso.ticketline.client.gui.controller.tabs.PremiumsController;
import at.ac.tuwien.inso.ticketline.client.gui.controller.tabs.customer.CustomersController;
import at.ac.tuwien.inso.ticketline.client.gui.controller.tabs.search.views.ReservationSelection;
import at.ac.tuwien.inso.ticketline.client.service.CustomerService;
import at.ac.tuwien.inso.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.ticketline.client.util.SpringFxmlLoader;
import at.ac.tuwien.inso.ticketline.dto.CustomerDto;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.function.BiPredicate;
import java.util.function.Function;


@Component
public class SelectCustomerController implements Initializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(SelectCustomerController.class);
    @Autowired
    private SpringFxmlLoader springFxmlLoader;
    @Autowired
    private MainController mainController;
    @Autowired
    private CustomerService customerService;
    @Autowired
    private CustomersController customersController;
    @Autowired
    private SellController sellController;
    @Autowired
    private PremiumsController premiumsController;
    private CustomerDto customerDto;


    @FXML
    private TitledPane selectCustomer;
    @FXML
    private TextField firstNameTF, lastNameTF, postalCodeTF;

    @FXML
    private RadioButton maleRB, femaleRB;

    @FXML
    private TableView<CustomerDto> customerTable;

    @FXML
    private TableColumn<CustomerDto, String> firstname, lastname, city, country, postalcode, street, email, gender, phonenumber, birthday;

    @FXML
    private Label fnameL, lnameL, postalCodeL, genderL;

    @FXML
    private Button backBtn;

    private List<CustomerDto> allCustomers;
    private ObservableList<CustomerDto> filteredCustomers = FXCollections.observableArrayList();
    private ReservationSelection reservationController;


    /**
     * Initializes the controller.
     *
     * @param location location of the fxml file.
     * @param resources resources
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        firstname.setCellValueFactory(new PropertyValueFactory<>("firstname"));
        lastname.setCellValueFactory(new PropertyValueFactory<>("lastname"));
        city.setCellValueFactory(new PropertyValueFactory<>("city"));
        country.setCellValueFactory(new PropertyValueFactory<>("country"));
        postalcode.setCellValueFactory(new PropertyValueFactory<>("postalcode"));
        street.setCellValueFactory(new PropertyValueFactory<>("street"));
        email.setCellValueFactory(new PropertyValueFactory<>("email"));
        //gender.setCellValueFactory(new PropertyValueFactory<>("gender"));
        gender.setCellValueFactory(param -> {
            String genderString = param.getValue().getGender();
            return new SimpleStringProperty(genderString == null ? "-" : (genderString.toLowerCase().equals("male") ? "M" : "F"));
        });
        phonenumber.setCellValueFactory(new PropertyValueFactory<>("phoneNumber"));
        //birthday.setCellValueFactory(new PropertyValueFactory<>("dateOfBirth"));
        birthday.setCellValueFactory(param -> {
            SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.YYYY");
            Date birthday = param.getValue().getDateOfBirth();
            return new SimpleStringProperty(birthday == null ? "" : formatter.format(birthday));
        });

        customerTable.setRowFactory(ev -> {
            final TableRow<CustomerDto> row = new TableRow<CustomerDto>();
            final ContextMenu contextMenu = new ContextMenu();
            row.setOnMouseClicked(mouse -> {
                if (mouse.getClickCount() == 2 && (!row.isEmpty()))
                    onSelect();
            });
            return row;
        });
        reinitLocale(MainController.currentLocale);

        // EventHandler for Gender RadioButtons
        EventHandler<ActionEvent> genderRadioButtonEventHandler = event -> {
            if (event.getSource() == maleRB || event.getSource() == femaleRB) {
                RadioButton sourceButton = (RadioButton) event.getSource();
                RadioButton otherButton = sourceButton == maleRB ? femaleRB : maleRB;

                boolean liveSearchPossible = sourceButton.isSelected() && !otherButton.isSelected();
                if (sourceButton.isSelected()) {
                    otherButton.setSelected(false);
                }
                if (liveSearchPossible) {
                    liveCustomerSearch("", maleRB.isSelected() ? "Male" : "Female", e -> e.getGender(), (e, f) -> e.equals(f));
                } else {
                    filterCustomers();
                }
            }
        };

        maleRB.setOnAction(genderRadioButtonEventHandler);
        femaleRB.setOnAction(genderRadioButtonEventHandler);

        firstNameTF.textProperty().addListener((observable, oldValue, newValue) -> {
            liveCustomerSearch(oldValue, newValue, e -> e.getFirstname(), (e, f) -> e.contains(f));
        });

        lastNameTF.textProperty().addListener((observable, oldValue, newValue) -> {
            liveCustomerSearch(oldValue, newValue, e -> e.getLastname(), (e, f) -> e.contains(f));
        });


        postalCodeTF.textProperty().addListener((observable, oldValue, newValue) -> {
            liveCustomerSearch(oldValue, newValue, e -> e.getPostalcode(), (e, f) -> e.contains(f));
        });

        refreshAllCustomersList();
        filteredCustomers.clear();
        filteredCustomers.addAll(allCustomers);
        customerTable.setItems(filteredCustomers);


    }

    public void reinitLocale(Locale newValue) {
        LocaleContextHolder.setLocale(newValue);
        BundleManager.changeLocale(newValue);
        selectCustomer.setText(BundleManager.getBundle().getString("shoppingcartSelect.title"));

        backBtn.setText(BundleManager.getBundle().getString("shoppingcartSelect.backB"));

        fnameL.setText(BundleManager.getBundle().getString("shoppingcartSelect.fnL"));
        lnameL.setText(BundleManager.getBundle().getString("shoppingcartSelect.lnL"));
        postalCodeL.setText(BundleManager.getBundle().getString("shoppingcartSelect.postalCodeL"));
        genderL.setText(BundleManager.getBundle().getString("shoppingcartSelect.genderL"));

        maleRB.setText(BundleManager.getBundle().getString("shoppingcartSelect.maleL"));
        femaleRB.setText(BundleManager.getBundle().getString("shoppingcartSelect.femaleL"));

        firstname.setText(BundleManager.getBundle().getString("shoppingcartSelect.fn"));
        lastname.setText(BundleManager.getBundle().getString("shoppingcartSelect.ln"));
        city.setText(BundleManager.getBundle().getString("shoppingcartSelect.city"));
        country.setText(BundleManager.getBundle().getString("shoppingcartSelect.country"));
        postalcode.setText(BundleManager.getBundle().getString("shoppingcartSelect.pc"));
        street.setText(BundleManager.getBundle().getString("shoppingcartSelect.street"));
        email.setText(BundleManager.getBundle().getString("shoppingcartSelect.email"));
        gender.setText(BundleManager.getBundle().getString("shoppingcartSelect.gender"));
        phonenumber.setText(BundleManager.getBundle().getString("shoppingcartSelect.phone"));
        birthday.setText(BundleManager.getBundle().getString("shoppingcartSelect.birth"));


    }
    /**
     * Goes back to the SellController and closes this window.
     *
     * @param event back button event
     */
    @FXML
    public void handleBack(ActionEvent event){
        ((Node) event.getSource()).setCursor(Cursor.WAIT);
        if(previousStage!=null) {
            Stage mainStage = new Stage();
            mainStage.setScene(new Scene((Parent) springFxmlLoader.load("/gui/fxml/shoppingCart/sell_prototype.fxml")));
            mainStage.setResizable(false);
            mainStage.setTitle(BundleManager.getBundle().getString("app.name"));
            mainStage.getIcons().add(new Image(LoginController.class.getResourceAsStream("/image/ticketlineLogo.png")));
            mainStage.show();
        }
        ((Node) event.getSource()).setCursor(Cursor.DEFAULT);
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        stage.close();
    }


//    @FXML
//    private void onFindCustomerButtonPressed(ActionEvent event) {
//        refreshAllCustomersList();
//        filterCustomers();
//    }

    private void refreshAllCustomersList() {
        try {
            allCustomers = customerService.getCustomers();
        } catch (ServiceException e) {
            LOGGER.error("Could not retrieve all customers!");
        }
    }

    /**
     * Filters the filteredCustomer List to match the user input
     */
    private void filterCustomers() {
        String forename = firstNameTF.getText().trim();
        String surname = lastNameTF.getText().trim();
        String postalCode = postalCodeTF.getText().trim();
        String gender = maleRB.isSelected() ? "male" : (femaleRB.isSelected() ? "female" : "");

        filteredCustomers.clear();

        for (CustomerDto c : allCustomers) {
            if (isMatch(forename, c.getFirstname()) && isMatch(surname, c.getLastname())
                    && isMatch(postalCode, c.getPostalcode()) && (gender.equals("") || gender.equals(c.getGender().toLowerCase()))) {
                filteredCustomers.add(c);
            }
        }
    }

    /**
     * Looks if a customer value contains a string value typed in by a user
     *
     * @param userInputString the input from the user
     * @param customerValue   the customer value to look if it matches the user input
     * @return true if the customer value contains the user input, false otherwise
     */
    public boolean isMatch(String userInputString, String customerValue) {
        if (userInputString == null || userInputString.length() == 0) { // no restrictions made
            return true;
        }

        if (customerValue == null) {
            return false;
        }
        return customerValue.toLowerCase().contains(userInputString.toLowerCase());
    }

    /**
     * Filter the filteredCustomer List to match the user input of a changed textfield or radiobutton
     *
     * @param oldUserInput             the old value of the changed textfield
     * @param newUserInput             the new value of the changed textfield
     * @param getCustomerValueFunction the function to get the value to filter from the CustomerDTO value (e.g. getFirstname())
     * @param compareFunction          the function to test if a CustomerDTO value is matching the newuUerInput-Value
     */
    private void liveCustomerSearch(String oldUserInput, String newUserInput, Function<CustomerDto, String> getCustomerValueFunction, BiPredicate<String, String> compareFunction) {
        if (newUserInput.toLowerCase().contains(oldUserInput.toLowerCase())) {
            for (int i = 0; i < filteredCustomers.size(); i++) {
                CustomerDto c = filteredCustomers.get(i);
                if (!compareFunction.test(getCustomerValueFunction.apply(c).toLowerCase(), newUserInput.toLowerCase())) {
                    filteredCustomers.remove(i);
                    i--;
                }
            }
        } else { // some valid customers may not be in the filtered list anymore ==> refilter complete list
            filterCustomers();
        }
    }


    @FXML
    private void onSelect(){

        customerDto = customerTable.getSelectionModel().getSelectedItem();

        if(sellController!=null) {
            sellController.setCustomer(customerDto);
            if(previousStage!=null) {
                previousStage.show();
                thisStage.close();
            }
        }
        if(premiumsController!=null){
            premiumsController.setCustomer(customerDto);
            if(previousStage!=null) {
                previousStage.show();
                thisStage.close();
            }
        }
        else if(reservationController != null) {
            reservationController.setCustomer(customerDto);
            thisStage.close();
        }

    }



    public void setSellController(SellController sellController) {

        this.sellController = sellController;
        reservationController = null;

        this.premiumsController = null;
    }

    public void setPremiumController(PremiumsController premiumController){
        this.premiumsController = premiumController;
        this.sellController = null;
        reservationController = null;
    }

    Stage previousStage;
    Stage thisStage;
    public void setPreviousStage(Stage previousStage) {
        this.previousStage = previousStage;
    }
    public void setThisStage(Stage thisStage) {
        this.thisStage = thisStage;
    }

    public CustomerDto getCustomerDto() {
        return this.customerDto;
    }

    public void setReservationController(ReservationSelection reservationController) {
        this.reservationController = reservationController;
        sellController = null;
        this.premiumsController = null;
    }
}
