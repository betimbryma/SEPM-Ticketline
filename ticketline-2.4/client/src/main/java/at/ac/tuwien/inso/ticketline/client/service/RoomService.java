package at.ac.tuwien.inso.ticketline.client.service;

import at.ac.tuwien.inso.ticketline.client.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.dto.RoomDto;

import java.util.List;

public interface RoomService {

    /**
     * Gets all saved rooms
     *
     * @return A list with all saved rooms
     * @throws ServiceException if an error occurs
     */
    List<RoomDto> getAll() throws ServiceException;

}