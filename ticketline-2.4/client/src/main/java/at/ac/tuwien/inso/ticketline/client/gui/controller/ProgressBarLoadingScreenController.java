package at.ac.tuwien.inso.ticketline.client.gui.controller;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Jakob on 06.06.2016.
 */
@Component
public class ProgressBarLoadingScreenController implements Initializable {

    @FXML
    public ProgressBar progressBar;

    @FXML
    private Label loadingText;

    private SimpleDoubleProperty progress;

    private static final Logger LOGGER = LoggerFactory.getLogger(ProgressBarLoadingScreenController.class);

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        progress = new SimpleDoubleProperty();
       // progressBar.progressProperty().bind(progress);
    }

    public void setProgress(double newProgress) {

        if(newProgress >= 0.0 && newProgress <= 1.0) {
            progressBar.setProgress(newProgress);
            LOGGER.debug("New Progress: " + progressBar.getProgress());
        }
        else {
            LOGGER.error(progress + " is not a valid progress value");
        }
    }

    public void setLoadingText(String text) {

        LOGGER.debug("setting loading Text: " + text);

        loadingText.setText(text);
    }
}
