package at.ac.tuwien.inso.ticketline.client.gui.controller.tabs.search.views;

import at.ac.tuwien.inso.ticketline.client.gui.JavaFXUtils;
import at.ac.tuwien.inso.ticketline.client.gui.controller.MainController;
import at.ac.tuwien.inso.ticketline.client.gui.controller.tabs.search.SearchController;
import at.ac.tuwien.inso.ticketline.client.service.TicketService;
import at.ac.tuwien.inso.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.ticketline.dto.ParticipationDto;
import at.ac.tuwien.inso.ticketline.dto.PerformanceDto;
import at.ac.tuwien.inso.ticketline.dto.ShowDto;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.text.Text;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;
import java.util.ResourceBundle;

@Component
public class EventViewController implements Initializable {

    @Autowired
    private MainController mainController;
    @Autowired
    private SearchController searchController;
    @Autowired
    private ShowViewController showViewController;
    @Autowired
    private TicketService ticketService;
    @FXML
    private Button backBut;
    @FXML
    private Label mainLB, titleLB, aboutLB, upcomingLB, nameLB, eventNameText, typeLB, eventTypeText, artistLB, eventArtistText, lengthLB, eventLengthText;
    @FXML
    private Text eventDescriptionText;
    @FXML
    private TableView<ShowDto> eventShowTable;
    @FXML
    private TableColumn<ShowDto, String> eventShowDateColumn, eventShowTimeColumn, eventShowVenueColumn, eventShowRoomColumn, eventShowPriceMinColumn, eventShowPriceMaxColumn;
    private ObservableList<ShowDto> filteredShows;
    private PerformanceDto performance;

    /**
     * This method initializes the Controller.
     *
     * @param location  location of the fxml file
     * @param resources resources
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        filteredShows = FXCollections.observableList(new ArrayList<>());
        initTable();
        reinitLocale(MainController.currentLocale);
    }

    /**
     * Reinitializes the localization.
     * @param newValue the new localization value
     */
    public void reinitLocale(Locale newValue) {
        initTable();
        LocaleContextHolder.setLocale(newValue);
        BundleManager.changeLocale(newValue);
        backBut.setText(BundleManager.getBundle().getString("eventView.backBut"));
        mainLB.setText(BundleManager.getBundle().getString("eventView.main"));
        titleLB.setText(BundleManager.getBundle().getString("eventView.title"));
        nameLB.setText(BundleManager.getBundle().getString("eventView.nameLB"));
        typeLB.setText(BundleManager.getBundle().getString("eventView.typeLB"));
        artistLB.setText(BundleManager.getBundle().getString("eventView.artistLB"));
        lengthLB.setText(BundleManager.getBundle().getString("eventView.lengthLB"));
        aboutLB.setText(BundleManager.getBundle().getString("eventView.aboutLB"));
        upcomingLB.setText(BundleManager.getBundle().getString("eventView.upcomingLB"));
        eventShowDateColumn.setText(BundleManager.getBundle().getString("eventView.dateCol"));
        eventShowTimeColumn.setText(BundleManager.getBundle().getString("eventView.timeCol"));
        eventShowVenueColumn.setText(BundleManager.getBundle().getString("eventView.venueCol"));
        eventShowRoomColumn.setText(BundleManager.getBundle().getString("eventView.roomCol"));
        eventShowPriceMinColumn.setText(BundleManager.getBundle().getString("eventView.priceMinCol"));
        eventShowPriceMaxColumn.setText(BundleManager.getBundle().getString("eventView.priceMaxCol"));
        if(performance != null){
            eventTypeText.setText(searchController.convertPerformanceType(performance));
        }
    }

    /**
     * Initializes the table.
     */
    public void initTable() {
        eventShowDateColumn.setCellValueFactory(param -> {
            SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
            ObservableValue<String> date = new ReadOnlyObjectWrapper<>(format.format(param.getValue().getDateOfPerformance()));
            return date;
        });
        eventShowTimeColumn.setCellValueFactory(param -> {
            SimpleDateFormat format = new SimpleDateFormat("HH:mm");
            ObservableValue<String> time = new ReadOnlyObjectWrapper<>(format.format(param.getValue().getDateOfPerformance()));
            return time;
        });
        eventShowVenueColumn.setCellValueFactory(param -> {
            ObservableValue<String> venue = new ReadOnlyObjectWrapper<>(param.getValue().getRoom().getLocation().getName());
            return venue;
        });
        eventShowRoomColumn.setCellValueFactory(param -> {
            ObservableValue<String> room = new ReadOnlyObjectWrapper<>(param.getValue().getRoom().getName());
            return room;
        });
        eventShowPriceMinColumn.setCellValueFactory(param -> {
            int newPrice = searchController.getAllTicketsMinPrice().get(param.getValue());
            ObservableValue<String> price = new ReadOnlyObjectWrapper<>(Integer.toString(newPrice));
            return price;
        });
        eventShowPriceMaxColumn.setCellValueFactory(param -> {
            int newPrice = searchController.getAllTicketsMaxPrice().get(param.getValue());
            ObservableValue<String> price = new ReadOnlyObjectWrapper<>(Integer.toString(newPrice));
            return price;
        });
        eventShowTable.setRowFactory(tv -> {
            TableRow<ShowDto> row = JavaFXUtils.getTableRowWithTooltip(e -> e.getRoom().getLocation().getDescription());
            row.setOnMouseClicked(event -> {
                if ((!row.isEmpty())) {
                    //showViewController.reset();
                    showViewController.setShow(row.getItem());
                    searchController.getStackPane().getChildren().clear();
                    searchController.getStackPane().getChildren().add(searchController.getShowViewPane());
                }
            });
            return row;
        });
        eventShowTable.setItems(filteredShows);
    }


    /**
     * This method sets the selected performance and creates the list of shows for the performance.
     * This method has to be called when this pane is set in front.
     *
     * @param performance selected performance
     */
    public void setPerformance(PerformanceDto performance) {
        this.performance = performance;
        filteredShows.clear();
        for (ShowDto s : searchController.getAllShows()) {
            if (s.getPerformance().getId().equals(performance.getId())) {
                filteredShows.add(s);
            }
        }
        eventNameText.setText(performance.getName());
        eventTypeText.setText(searchController.convertPerformanceType(performance));
        String artists = "";
        for (ParticipationDto p : searchController.getAllParticipations()) {
            if (performance.getId().equals(p.getPerformance().getId())) {
                artists += p.getArtist().getFirstName() + " " + p.getArtist().getLastName() + " (" + p.getArtistRole() + ")\n";
            }
        }
        eventArtistText.setText(artists);
        eventLengthText.setText(Integer.toString(performance.getDuration()) + "min");
        eventDescriptionText.setText(performance.getDescription());
    }

    /**
     * This method puts the accordion in the foreground.
     *
     * @param event the back button event
     */
    @FXML
    public void handleBack(ActionEvent event) {
        searchController.getStackPane().getChildren().clear();
        searchController.getStackPane().getChildren().add(searchController.getAccordion());
    }

    /**
     * Resets the show list.
     */
    public void reset() {
        filteredShows.clear();
    }
}
