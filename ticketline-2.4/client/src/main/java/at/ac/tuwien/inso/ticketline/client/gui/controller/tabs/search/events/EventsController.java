package at.ac.tuwien.inso.ticketline.client.gui.controller.tabs.search.events;

import at.ac.tuwien.inso.ticketline.client.gui.JavaFXUtils;
import at.ac.tuwien.inso.ticketline.client.gui.controller.MainController;
import at.ac.tuwien.inso.ticketline.client.gui.controller.tabs.search.SearchController;
import at.ac.tuwien.inso.ticketline.client.gui.controller.tabs.search.views.EventViewController;
import at.ac.tuwien.inso.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.ticketline.dto.PerformanceDto;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

@Component
public class EventsController implements Initializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(EventsController.class);
    @Autowired
    private MainController mainController;
    @Autowired
    private SearchController searchController;
    @Autowired
    private EventViewController eventViewController;
    @FXML
    private TextField nameTextField, lengthTextField, contentTextField;
    @FXML
    private ComboBox typeComboBox;
    @FXML
    private TableView<PerformanceDto> performanceTable;
    @FXML
    private TableColumn<PerformanceDto, String> nameColumn, typeColumn, lengthColumn, contentColumn;
    private int length = -1;
    @FXML
    private Button reset;
    @FXML
    private Label nameLable, typeLable, lengthLable, contentLable, durationInfo;

    /**
     * Initializes the controller.
     *
     * @param location  location of the fxml file
     * @param resources resources
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //initTable();
        //initSearchFields();
        reinitLocale(MainController.currentLocale);
    }

    /**
     * Initializes the controller.
     */
    public void init(){
        durationInfo.setText("\u2139");
        initSearchFields();
        initTable();
    }

    /**
     * Reinitializes the localization.
     * @param newValue the new localization value
     */
    public void reinitLocale(Locale newValue) {
        initTable();
        LocaleContextHolder.setLocale(newValue);
        BundleManager.changeLocale(newValue);
        nameColumn.setText(BundleManager.getBundle().getString("eventsearch.name"));
        typeColumn.setText(BundleManager.getBundle().getString("eventsearch.type"));
        lengthColumn.setText(BundleManager.getBundle().getString("eventsearch.length"));
        contentColumn.setText(BundleManager.getBundle().getString("eventsearch.content"));
        nameLable.setText(BundleManager.getBundle().getString("eventsearch.nameL"));
        typeLable.setText(BundleManager.getBundle().getString("eventsearch.typeL"));
        lengthLable.setText(BundleManager.getBundle().getString("eventsearch.lengthL"));
        contentLable.setText(BundleManager.getBundle().getString("eventsearch.contentL"));
        reset.setText(BundleManager.getBundle().getString("eventsearch.resetB"));
        ObservableList names = FXCollections.observableArrayList();
        names.add(BundleManager.getBundle().getString("eventsearch.typeAll"));
        names.add(BundleManager.getBundle().getString("eventsearch.typeMovie"));
        names.add(BundleManager.getBundle().getString("eventsearch.typeFestival"));
        names.add(BundleManager.getBundle().getString("eventsearch.typeConcert"));
        names.add(BundleManager.getBundle().getString("eventsearch.typeMusical"));
        names.add(BundleManager.getBundle().getString("eventsearch.typeOper"));
        names.add(BundleManager.getBundle().getString("eventsearch.typeTheater"));
        typeComboBox.setValue(BundleManager.getBundle().getString("eventsearch.typeAll"));
        typeComboBox.setItems(names);
        eventViewController.reinitLocale(newValue);
    }

    /**
     * Initializes the table.
     */
    public void initTable() {
        durationInfo.setTooltip(new Tooltip("+/- 30min"));
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        typeColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<PerformanceDto, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<PerformanceDto, String> param) {
                ObservableValue<String> type = new ReadOnlyObjectWrapper<String>(searchController.convertPerformanceType(param.getValue()));
                return type;
            }
        });
        lengthColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<PerformanceDto, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<PerformanceDto, String> param) {
                ObservableValue<String> duration = new ReadOnlyObjectWrapper<String>(param.getValue().getDuration() + "min");
                return duration;
            }
        });
        contentColumn.setCellValueFactory(new PropertyValueFactory<>("description"));
        performanceTable.setRowFactory(tv -> {
            TableRow<PerformanceDto> row = JavaFXUtils.getTableRowWithTooltip(e -> e.getDescription());
            row.setOnMouseClicked(event -> {
                if ((!row.isEmpty())) {
                    eventViewController.reset();
                    eventViewController.setPerformance(row.getItem());
                    searchController.getStackPane().getChildren().clear();
                    searchController.getStackPane().getChildren().add(searchController.getEventViewPane());
                }
            });
            return row;
        });
        performanceTable.setItems(searchController.getAllPerformances());
    }

    /**
     * Initializes the search fields.
     */
    public void initSearchFields() {
        ChangeListener<String> listener = new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                search();
            }
        };
        nameTextField.textProperty().addListener(listener);
        lengthTextField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                try {
                    if (lengthTextField.getText().equals("")) {
                        length = -1;
                    } else {
                        length = Integer.parseInt(lengthTextField.getText());
                    }
                } catch (NumberFormatException e) {
                    lengthTextField.setText("");
                    length = -1;
                    search();
                    return;
                }
                search();
            }
        });
        contentTextField.textProperty().addListener(listener);
        ObservableList names = FXCollections.observableArrayList();
        names.add(BundleManager.getBundle().getString("eventsearch.typeAll"));
        names.add(BundleManager.getBundle().getString("eventsearch.typeMovie"));
        names.add(BundleManager.getBundle().getString("eventsearch.typeFestival"));
        names.add(BundleManager.getBundle().getString("eventsearch.typeConcert"));
        names.add(BundleManager.getBundle().getString("eventsearch.typeMusical"));
        names.add(BundleManager.getBundle().getString("eventsearch.typeOper"));
        names.add(BundleManager.getBundle().getString("eventsearch.typeTheater"));
        typeComboBox.setValue(BundleManager.getBundle().getString("eventsearch.typeAll"));
        typeComboBox.setItems(names);
        typeComboBox.valueProperty().addListener(listener);
    }

    /**
     * Performs a search with the input.
     */
    public void search() {
        List<PerformanceDto> performances = new ArrayList<>();
        for (PerformanceDto p : searchController.getAllPerformances()) {
            if (checkName(p) && checkDescription(p) && checkLength(p) && checkType(p)) {
                performances.add(p);
            }
        }
        ObservableList<PerformanceDto> observableList = FXCollections.observableList(performances);
        performanceTable.setItems(observableList);
    }

    /**
     * Checks if name fits.
     * @param p Performance
     * @return boolean
     */
    public boolean checkName(PerformanceDto p) {
        if (p.getName().toLowerCase().contains(nameTextField.getText().toLowerCase())) {
            return true;
        }
        return false;
    }

    /**
     * Checks if description fits.
     * @param p Performance
     * @return boolean
     */
    public boolean checkDescription(PerformanceDto p) {
        if (p.getDescription().toLowerCase().contains(contentTextField.getText().toLowerCase())) {
            return true;
        }
        return false;
    }

    /**
     * Checks if type fits.
     * @param p Performance
     * @return boolean
     */
    public boolean checkType(PerformanceDto p) {
        if(typeComboBox.getValue() != null) {
            if (typeComboBox.getValue().equals("All") || typeComboBox.getValue().equals("Alle")) {
                return true;
            } else if (typeComboBox.getValue().toString().equals(searchController.convertPerformanceType(p))) {
                return true;
            }
            return false;
        }else{
            return true;
        }
    }

    /**
     * Checks if length fits (+/- 30).
     * @param p Performance
     * @return boolean
     */
    public boolean checkLength(PerformanceDto p) {
        if (length == -1) {
            return true;
        }
        if (length >= p.getDuration() - 30 && length <= p.getDuration() + 30) {
            return true;
        }
        return false;
    }

    /**
     * Handle the reset button.
     * @param event button event
     */
    @FXML
    public void handleReset(ActionEvent event) {
        reset();
        search();
    }

    /**
     * Resets the controller.
     */
    public void reset() {
        nameTextField.setText("");
        lengthTextField.setText("");
        contentTextField.setText("");
        if(MainController.currentLocale.equals(Locale.ENGLISH)) {
            typeComboBox.setValue("All");
        } else{
            typeComboBox.setValue("Alle");
        }
        length = -1;
    }
}
