package at.ac.tuwien.inso.ticketline.client.pdf;

import org.apache.pdfbox.pdmodel.PDPageContentStream;

import java.awt.*;
import java.io.IOException;

/**
 * Created by Florian on 18.06.2016.
 */
public class PDFLine implements PDFContent {

    private int posX1;
    private int posY1;
    private int posX2;
    private int posY2;
    private Color color;
    private int lineWidth;

    public PDFLine(int posX1, int posY1, int posX2, int posY2, Color color, int lineWidth) {
        this.posX1 = posX1;
        this.posY1 = posY1;
        this.posX2 = posX2;
        this.posY2 = posY2;
        this.color = color;
        this.lineWidth = lineWidth;
    }

    @Override
    public void print(PDPageContentStream cos) throws IOException {
            cos.setLineWidth(lineWidth);
            cos.setStrokingColor(color);
            cos.moveTo(posX1, posY1);
            cos.lineTo(posX2, posY2);
            cos.closeAndStroke();
    }
}
