package at.ac.tuwien.inso.ticketline.client.service.rest;

import at.ac.tuwien.inso.ticketline.client.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.client.exception.ValidationException;
import at.ac.tuwien.inso.ticketline.client.service.MerchandiseService;
import at.ac.tuwien.inso.ticketline.dto.MerchandiseDto;
import at.ac.tuwien.inso.ticketline.dto.MessageDto;
import at.ac.tuwien.inso.ticketline.dto.PerformanceDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.List;

@Component
public class MerchandiseRestService implements MerchandiseService {

    public static final String GET_Articles_OF_PERFORMANCE = "/service/merchandise/getArticlesOfThisPerformance/";
    public static final String CHECK_MERCHANDISE = "/service/conflict/checkArticles/";
    public static final String RELEASE_ARTICLE = "/service/conflict/releaseArticles/";
    private static final Logger LOGGER = LoggerFactory.getLogger(MerchandiseRestService.class);
    @Autowired
    private RestClient restClient;

    @Override
    public PerformanceDto getArticlesOfPerformance(PerformanceDto reservationDto) throws ServiceException {

        String url = this.restClient.createServiceUrl(GET_Articles_OF_PERFORMANCE);
        LOGGER.info("Saving reservation at {}", url);

        RestTemplate restTemplate = this.restClient.getRestTemplate();
        HttpHeaders httpHeaders = restClient.getHttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        httpHeaders.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        HttpEntity<PerformanceDto> entity = new HttpEntity<>(reservationDto, httpHeaders);

        PerformanceDto reservationDto1;

        try {
            reservationDto1 = restTemplate.postForObject(url, entity, PerformanceDto.class);
        } catch (HttpStatusCodeException e) {
            MessageDto errorMsg = this.restClient.mapExceptionToMessage(e);
            if (errorMsg.hasFieldErrors()) {
                throw new ValidationException(errorMsg.getFieldErrors());
            } else {
                throw new ServiceException(errorMsg.getText());
            }
        } catch (RestClientException e) {
            throw new ServiceException("Could not save reservation: " + e.getMessage(), e);
        }

        return reservationDto1;

    }

    @Override
    public MerchandiseDto checkMerchandise(MerchandiseDto merchandiseDto) throws ServiceException {
        RestTemplate restTemplate = this.restClient.getRestTemplate();
        String url = this.restClient.createServiceUrl(CHECK_MERCHANDISE);
        LOGGER.info("Reserve article {}", url);
        HttpHeaders headers = this.restClient.getHttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        HttpEntity<MerchandiseDto> entity = new HttpEntity<>(merchandiseDto, headers);
        MerchandiseDto merchandiseDto2;
        LOGGER.debug(" " + merchandiseDto);
        LOGGER.debug(" " + merchandiseDto.getName() + " " + merchandiseDto.getId());
        try {
            ParameterizedTypeReference<MerchandiseDto> parameterizedTypeReference = new ParameterizedTypeReference<MerchandiseDto>() {};
            ResponseEntity<MerchandiseDto> responseEntity = restTemplate.exchange(url, HttpMethod.POST, entity, parameterizedTypeReference);
            merchandiseDto2 = responseEntity.getBody();
        } catch (HttpStatusCodeException e) {
            MessageDto errorMsg = this.restClient.mapExceptionToMessage(e);
            if (errorMsg.hasFieldErrors()) {
                throw new ValidationException(errorMsg.getFieldErrors());
            } else {
                throw new ServiceException(errorMsg.getText());
            }
        } catch (RestClientException e) {
            throw new ServiceException("Could not reserve article: " + e.getMessage(), e);
        }

        return merchandiseDto2;
    }





    @Override
    public List<MerchandiseDto> releaseArticle(List<MerchandiseDto> merchandiseDtos) throws ServiceException {
        RestTemplate restTemplate = this.restClient.getRestTemplate();
        String url = this.restClient.createServiceUrl(RELEASE_ARTICLE);
        LOGGER.info("Reserve tickets {}", url);
        HttpHeaders headers = this.restClient.getHttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        HttpEntity<List<MerchandiseDto>> entity = new HttpEntity<>(merchandiseDtos, headers);
        List<MerchandiseDto> merchandiseDtos1;
        try {
            ParameterizedTypeReference<List<MerchandiseDto>> parameterizedTypeReference = new ParameterizedTypeReference<List<MerchandiseDto>>() {};
            ResponseEntity<List<MerchandiseDto>> responseEntity = restTemplate.exchange(url, HttpMethod.POST, entity, parameterizedTypeReference);
            merchandiseDtos1 = responseEntity.getBody();
        } catch (HttpStatusCodeException e) {
            MessageDto errorMsg = this.restClient.mapExceptionToMessage(e);
            if (errorMsg.hasFieldErrors()) {
                throw new ValidationException(errorMsg.getFieldErrors());
            } else {
                throw new ServiceException(errorMsg.getText());
            }
        } catch (RestClientException e) {
            throw new ServiceException("Could not reserve tickets: " + e.getMessage(), e);
        }

        return merchandiseDtos1;
    }
}