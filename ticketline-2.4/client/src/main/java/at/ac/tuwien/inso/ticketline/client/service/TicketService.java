package at.ac.tuwien.inso.ticketline.client.service;

import at.ac.tuwien.inso.ticketline.client.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.dto.*;

import java.util.List;

public interface TicketService {

    List<MapEntryDto<TicketDto,TicketIdentifierDto>> getTicketsForShow(Integer showDto) throws ServiceException;

    void setUserStatusDto(UserStatusDto userStatusDto);

   // List<MapEntryDto<TicketDto,TicketIdentifierDto>> getTicketIdentifiersForTickets(Integer showDto) throws ServiceException;

    List<TicketIdentifierDto> getTicketIdentifiers() throws ServiceException;

    List<TicketDto> getAllTickets(Integer showDto) throws ServiceException ;

    UserStatusDto getEmployee();

    List<MapEntryDto<TicketDto,TicketIdentifierDto>> getTicketIdentifiersAndTickets(Integer showDto) throws ServiceException;

    List<TicketIdentifierDto> reserveTickets(List<TicketIdentifierDto> ticketDtoList) throws ServiceException;

    Integer getSoldByShow(Integer show) throws ServiceException;

    List<MapEntryDto<ShowDto, Integer>> getMinPrices() throws ServiceException;

    List<MapEntryDto<ShowDto, Integer>> getMaxPrices() throws ServiceException;

    void changeReservation(TicketIdentifierDto ticketIdentifierDto) throws ServiceException;

    List<TicketIdentifierDto> getTicketsForCustomer(Integer show, Integer customer) throws ServiceException;

    /**
     * Check ticket for differentcases
     * @param ticketDto
     * @param i  1 = if ticket is supposed to be free and than sold
     *           2 = if ticket is supposed to be reserved and than sold
     *           3 = if ticket is supposed to be free and then reserved
     * @return
     * @throws ServiceException
     */
    Integer checkTicket(TicketDto ticketDto,Integer i)throws ServiceException;//     /checkTicket
    void releaseTicket(List<TicketDto> tickets) throws  ServiceException;//    /releaseTicket


}
