package at.ac.tuwien.inso.ticketline.client.service;

import at.ac.tuwien.inso.ticketline.client.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.dto.ReservationDto;
import at.ac.tuwien.inso.ticketline.dto.TicketIdentifierDto;

import java.util.List;

/**
 * Created by Betim on 5/16/2016.
 */
public interface ReservationService {
    ReservationDto saveReservation(ReservationDto reservationDto) throws ServiceException;
    List<ReservationDto> getReservations() throws ServiceException;
    Integer cancelReservation(List<TicketIdentifierDto> ticketIdentifierDtos) throws ServiceException;
}
