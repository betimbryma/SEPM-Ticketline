package at.ac.tuwien.inso.ticketline.client.service;

import at.ac.tuwien.inso.ticketline.client.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.dto.ArtistDto;

import java.util.List;

public interface ArtistService {

    /**
     * Gets all saved artists
     *
     * @return A list with all saved artists
     * @throws ServiceException if an error occurs
     */
    List<ArtistDto> getAll() throws ServiceException;
}
