package at.ac.tuwien.inso.ticketline.client.gui.controller.tabs;

import at.ac.tuwien.inso.ticketline.client.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.client.gui.controller.MainController;
import at.ac.tuwien.inso.ticketline.client.service.ReservationService;
import at.ac.tuwien.inso.ticketline.client.service.TicketService;
import at.ac.tuwien.inso.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.ticketline.client.util.DateUtil;
import at.ac.tuwien.inso.ticketline.client.util.SpringFxmlLoader;
import at.ac.tuwien.inso.ticketline.dto.ReservationDto;
import at.ac.tuwien.inso.ticketline.dto.TicketIdentifierDto;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.util.Callback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.net.URL;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

@Component
public class ReservationsController implements Initializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReservationsController.class);
    @Autowired
    private MainController mainController;

    @Autowired
    private ReservationService reservationService;

    @Autowired
    private TicketService ticketService;

    @Autowired
    private SpringFxmlLoader springFxmlLoader;

    @FXML
    private TableView<TicketIdentifierDto> reservationTable;

    /* @FXML
     private TableColumn<ReservationDto, String> ;*/
    @FXML
    private TableColumn<TicketIdentifierDto, String> rnumb, customer, show, date;

    @FXML
    private Button find;

    @FXML
    private TextField reservationN;

    @FXML
    private Label reservationnumber;

    private List<ReservationDto> allReservations;
    private List<TicketIdentifierDto> allTicketIdentifiers;
    private ObservableList<ReservationDto> filteredReservations = FXCollections.observableArrayList();
    private ObservableList<TicketIdentifierDto> filteredTicketIdentifiers = FXCollections.observableArrayList();

    private CancelController cancelcontroller;

    @FXML
    private AnchorPane mainPane;
    @FXML
    private AnchorPane cancelPane;

    @FXML
    private StackPane stackPane;

    /**
     * Initializes the controller.
     *
     * @param location  location of the fxml file
     * @param resources resources
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        rnumb.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<TicketIdentifierDto, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<TicketIdentifierDto, String> param) {
                ObservableValue<String> event = new ReadOnlyObjectWrapper<String>((param.getValue().getReservation() == null) ? "null" : param.getValue().getReservation().getReservationNumber() + "");
                return event;
            }
        });
        customer.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<TicketIdentifierDto, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<TicketIdentifierDto, String> param) {
                String name = "";
                if (param.getValue().getReservation() != null) {
                    String fname = param.getValue().getReservation().getCustomer().getFirstname();
                    String lname = param.getValue().getReservation().getCustomer().getLastname();
                    name = (fname.equals(lname) ? fname : fname + " " + lname);
                } else {
                    name = "Null";
                }
                ObservableValue<String> event = new ReadOnlyObjectWrapper<String>(name);
                return event;
            }
        });
        show.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<TicketIdentifierDto, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<TicketIdentifierDto, String> param) {
                ObservableValue<String> event = new ReadOnlyObjectWrapper<String>(param.getValue().getTicket().getShow().getPerformance().getName() + "");
                return event;
            }
        });

        date.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<TicketIdentifierDto, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<TicketIdentifierDto, String> param) {
                ObservableValue<String> event = new ReadOnlyObjectWrapper<String>(DateUtil.getEuropeanDateString(param.getValue().getTicket().getShow().getDateOfPerformance()));
                return event;
            }
        });
        /*reservationN.textProperty().addListener((observable, oldValue, newValue) -> {
            liveReservationSearch(oldValue, newValue, e -> e.getReservation().getReservationNumber() + "", (e, f) -> e.contains(f));
        });*/
        reservationTable.setRowFactory(ev -> {
            final TableRow<TicketIdentifierDto> row = new TableRow<TicketIdentifierDto>();
            final ContextMenu contextMenu = new ContextMenu();
            final MenuItem menuItem1 = new MenuItem(BundleManager.getBundle().getString("reservation.cancelbuy"));
            menuItem1.setOnAction(context -> onCancel());
            contextMenu.getItems().add(menuItem1);
            row.contextMenuProperty().bind(Bindings.when(row.emptyProperty()).then((ContextMenu) null).otherwise(contextMenu));

            row.setOnMouseClicked(mouse -> {
                if (mouse.getClickCount() == 2 && (!row.isEmpty()))
                    onCancel();
            });

            return row;
        });
        refreshAllReservationsList();
        //filteredReservations.addAll(allReservations);
        filteredTicketIdentifiers.addAll(allTicketIdentifiers);

        for (int i = 0; i < filteredTicketIdentifiers.size(); i++) {
            int res = filteredTicketIdentifiers.get(i).getReservation().getReservationNumber();
            for (int j = i + 1; j < filteredTicketIdentifiers.size(); j++) {
                if (filteredTicketIdentifiers.get(j).getReservation().getReservationNumber() == res) {
                    System.out.println("--------------+ " + filteredTicketIdentifiers.get(j).getReservation().getCustomer().getFirstname());

                    filteredTicketIdentifiers.remove(j);
                    j--;
                }
            }

        }
        reservationTable.setItems(filteredTicketIdentifiers);
        reinitLocale(MainController.currentLocale);
    }

    /**
     * changes Language
     *
     * @param newValue
     */
    public void reinitLocale(Locale newValue) {
        LocaleContextHolder.setLocale(newValue);
        BundleManager.changeLocale(newValue);

        rnumb.setText(BundleManager.getBundle().getString("reservation.rnumber"));
        customer.setText(BundleManager.getBundle().getString("reservation.customer"));
        show.setText(BundleManager.getBundle().getString("reservation.show"));
        date.setText(BundleManager.getBundle().getString("reservation.date"));

        find.setText(BundleManager.getBundle().getString("reservation.find"));

        reservationnumber.setText(BundleManager.getBundle().getString("reservation.numberLable"));
        if (cancelcontroller != null) {
            cancelcontroller.reinitLocale(MainController.currentLocale);
        }

    }

    /**
     * finds and displays reservations
     */
    @FXML
    public void onFindReservationButtonPressed() {
        if (reservationN.getText().equals("")) {
            refreshAllReservationsList();
            for (int i = 0; i < filteredTicketIdentifiers.size(); i++) {
                filteredTicketIdentifiers.remove(i);
            }
            filteredTicketIdentifiers.addAll(allTicketIdentifiers);

            for (int i = 0; i < filteredTicketIdentifiers.size(); i++) {
                int res = filteredTicketIdentifiers.get(i).getReservation().getReservationNumber();
                for (int j = i + 1; j < filteredTicketIdentifiers.size(); j++) {
                    if (filteredTicketIdentifiers.get(j).getReservation().getReservationNumber() == res) {
                        filteredTicketIdentifiers.remove(j);
                        j--;
                    }
                }

            }
            reservationTable.setItems(filteredTicketIdentifiers);
        } else {
            refreshAllReservationsList();
            filterReservations();
        }

    }

    /**
     * refreshs reservations list
     */
    public void refreshAllReservationsList() {
        try {
            //allReservations = reservationService.getReservations();
            allTicketIdentifiers = ticketService.getTicketIdentifiers();

            for (int i = 0; i < allTicketIdentifiers.size(); i++) {
                if (!allTicketIdentifiers.get(i).getValid() || allTicketIdentifiers.get(i).getReservation() == null) {
                    allTicketIdentifiers.remove(i);
                    i--;
                }
            }


        } catch (ServiceException e) {
            LOGGER.error("Could not retrieve all reservations!");
        }
    }

    /**
     * filters the reservation list
     */
    private void filterReservations() {
        String Reservationsnumber = reservationN.getText().trim();


        filteredTicketIdentifiers.clear();

        for (TicketIdentifierDto t : allTicketIdentifiers) {
            if (Reservationsnumber.equals("" + t.getReservation().getReservationNumber())) {

                filteredTicketIdentifiers.add(t);
                break;
            }
        }
    }


    /**
     * determines if the found reservations match the input of the user
     * @param userInputString
     * @param reservationValue
     * @return
     */
    public boolean isMatch(String userInputString, String reservationValue) {
        if (userInputString == null || userInputString.length() == 0) { // no restrictions made
            return true;
        }

        if (reservationValue == null) {
            return false;
        }

        return reservationValue.toLowerCase().contains(userInputString.toLowerCase());
    }

    /*private void liveReservationSearch(String oldUserInput, String newUserInput, Function<TicketIdentifierDto, String> getReservationValueFunction, BiPredicate<String, String> compareFunction) {
        if (newUserInput.toLowerCase().contains(oldUserInput.toLowerCase())) {
            for (int i = 0; i < filteredTicketIdentifiers.size(); i++) {
                TicketIdentifierDto c = filteredTicketIdentifiers.get(i);
                if (!compareFunction.test(getReservationValueFunction.apply(c).toLowerCase(), newUserInput.toLowerCase())) {
                    filteredTicketIdentifiers.remove(i);
                    i--;
                }
            }


            } else { // some valid customers may not be in the filtered list anymore ==> refilter complete list
            filterReservations();
        }
    }*/

    /**
     * opens a new window, where the user can choose what to do with his reserevtion
     */
    private void onCancel() {


        LOGGER.info("Cancel context menu item clicked");
        TicketIdentifierDto ticketIdentifierDto = reservationTable.getSelectionModel().getSelectedItem();
        if (ticketIdentifierDto != null) {
            SpringFxmlLoader.LoadWrapper loadWrapper = springFxmlLoader.loadAndWrap("/gui/fxml/tabs/reservations/cancelreservation.fxml");
            cancelcontroller = (CancelController) loadWrapper.getController();
            cancelcontroller.setTicketIdentifier(ticketIdentifierDto, allTicketIdentifiers);
            cancelcontroller.setStackPane(stackPane);
            cancelcontroller.setReservationAnchorPane(mainPane);
            cancelcontroller.setReservationController(this);
            stackPane.getChildren().clear();
            stackPane.getChildren().add((Node) loadWrapper.getLoadedObject());
            reservationTable.getColumns().get(0).setVisible(false);
            reservationTable.getColumns().get(0).setVisible(true);
        }

    }

    private void onBuy() {

    }


}
