package at.ac.tuwien.inso.ticketline.client.service;

import at.ac.tuwien.inso.ticketline.client.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.dto.NewsDto;

import java.util.Date;
import java.util.List;

public interface NewsService {

    /**
     * Gets the news.
     *
     * @return the news
     * @throws ServiceException the service exception
     */
    public List<NewsDto> getNews() throws ServiceException;

    /**
     * Publish news.
     *
     * @param news the news
     * @return the integer
     * @throws ServiceException the service exception
     */
    public Integer publishNews(NewsDto news) throws ServiceException;

    /**
     * Get the news that have occurred before the date in the parameter
     * @param date the date from which our search is 'controlled'
     * @return a list of news
     * @throws ServiceException if something happens in the server
     */
    List<NewsDto> getBefore(Date date) throws ServiceException;

}
