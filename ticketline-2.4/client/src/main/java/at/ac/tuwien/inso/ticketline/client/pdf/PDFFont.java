package at.ac.tuwien.inso.ticketline.client.pdf;

import org.apache.pdfbox.pdmodel.font.PDFont;

public class PDFFont {

    private PDFont font;
    private int size;
    private int lineDistance;

    public PDFFont(PDFont font, int size, int lineDistance) {
        this.font = font;
        this.size = size;
        this.lineDistance = lineDistance;
    }

    public PDFont getFont() {
        return font;
    }

    public int getSize() {
        return size;
    }

    public int getLineDistance() {
        return lineDistance;
    }
}
