package at.ac.tuwien.inso.ticketline.client.gui.controller.tabs.search.views;

import at.ac.tuwien.inso.ticketline.client.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.client.gui.controller.LoginController;
import at.ac.tuwien.inso.ticketline.client.gui.controller.shoppingCart.CreateCustomerShoppingCartController;
import at.ac.tuwien.inso.ticketline.client.gui.controller.shoppingCart.SelectCustomerController;
import at.ac.tuwien.inso.ticketline.client.service.CustomerService;
import at.ac.tuwien.inso.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.ticketline.client.util.DialogUtil;
import at.ac.tuwien.inso.ticketline.client.util.SpringFxmlLoader;
import at.ac.tuwien.inso.ticketline.dto.CustomerDto;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * Created by Betim on 5/16/2016.
 */
@Component
public class ReservationSelection {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReservationSelection.class);
    private CustomerDto customerDto;
    @Autowired
    CustomerService customerService;
    @Autowired
    SpringFxmlLoader springFxmlLoader;

    @FXML
    Label label_forename;
    @FXML
    Label label_lastname;
    @FXML
    Label label_street;
    @FXML
    Label label_postalcode;
    @FXML
    Label label_city;
    @FXML
    Label label_country;
    @FXML
    Label label_gender;
    @FXML
    Label label_birthdate;
    @FXML
    Label label_email;
    @FXML
    Label label_phone;

    @FXML
    CheckBox check_anonym;

    @FXML
    Button btn_select;
    @FXML
    Button btn_create;
    @FXML
    Button btn_reserve;

    private ShowViewController showViewController;

    @FXML
    private void onNewCustomerClicked(ActionEvent event){

        ((Node) event.getSource()).setCursor(Cursor.WAIT);

        Stage nextStage = new Stage();
        SpringFxmlLoader.LoadWrapper loadWrapper = springFxmlLoader.loadAndWrap("/gui/fxml/shoppingCart/create_customer_shopping_cart_prototype.fxml");
        nextStage.setScene(new Scene((Parent) loadWrapper.getLoadedObject()));

        CreateCustomerShoppingCartController controller = (CreateCustomerShoppingCartController) loadWrapper.getController();
        controller.setReservationController(this);

        ((Node) event.getSource()).setCursor(Cursor.DEFAULT);
        Node source = (Node) event.getSource();
        Stage thisStage = (Stage) source.getScene().getWindow();

        controller.setPreviousStage(thisStage);

        nextStage.setResizable(false);
        nextStage.setTitle(BundleManager.getBundle().getString("app.name"));
        nextStage.getIcons().add(new Image(LoginController.class.getResourceAsStream("/image/ticketlineLogo.png")));

        nextStage.initModality(Modality.WINDOW_MODAL);
        nextStage.initOwner(thisStage);
        nextStage.show();

        //thisStage.close();

    }

    @FXML
    private void onAnonymousCustomerClicked(ActionEvent event){
        if(check_anonym.isSelected()) {
            btn_create.setDisable(true);
            btn_select.setDisable(true);
            btn_reserve.setDisable(false);
        }
        else {
            if(customerDto == null || customerDto.getId() == 1) {
                btn_create.setDisable(false);
                btn_select.setDisable(false);
                btn_reserve.setDisable(true);
            }
            else {
                btn_create.setDisable(false);
                btn_select.setDisable(false);
                btn_reserve.setDisable(false);
            }
        }
    }

    @FXML
    private void onSelectCustomerClicked(ActionEvent event){
        ((Node) event.getSource()).setCursor(Cursor.WAIT);

        Node source = (Node) event.getSource();
        Stage thisStage = (Stage) source.getScene().getWindow();

        Stage nextStage = new Stage();
        SpringFxmlLoader.LoadWrapper loadWrapper = springFxmlLoader.loadAndWrap("/gui/fxml/shoppingCart/select_customer_prototype.fxml");
        nextStage.setScene(new Scene((Parent) loadWrapper.getLoadedObject()));

        SelectCustomerController controller = (SelectCustomerController) loadWrapper.getController();
        controller.setPreviousStage(thisStage);
        controller.setThisStage(nextStage);
        controller.setReservationController(this);

        nextStage.setResizable(false);
        nextStage.setTitle(BundleManager.getBundle().getString("app.name"));
        nextStage.getIcons().add(new Image(LoginController.class.getResourceAsStream("/image/ticketlineLogo.png")));

        ((Node) event.getSource()).setCursor(Cursor.DEFAULT);

        nextStage.initModality(Modality.WINDOW_MODAL);
        nextStage.initOwner(thisStage);
        nextStage.show();
        //thisStage.close();
    }

    public CustomerDto getCustomerDto(){
        return this.customerDto;
    }

    public void setCustomer(CustomerDto customer) {
        try {
            this.customerDto = customer;
            label_forename.setText(customerDto.getFirstname());
        }catch (NullPointerException e){
            //not initialized!
            return;
        }
        label_lastname.setText(customerDto.getLastname());
        label_street.setText(customerDto.getStreet());
        label_postalcode.setText(customerDto.getPostalcode());
        label_city.setText(customerDto.getCity());
        label_country.setText(customerDto.getCountry());
        label_gender.setText(customerDto.getGender());

        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.YYYY");
        Date birthday = customerDto.getDateOfBirth();
        label_birthdate.setText(birthday == null ? "" : formatter.format(birthday));

        String email = customerDto.getEmail();
        label_email.setText(email == null || email.trim().isEmpty() ? "-" : email);

        String phone = customerDto.getPhoneNumber();
        label_phone.setText(phone == null || phone.trim().isEmpty() ? "-" : phone);

        check_anonym.setSelected(false);
        btn_create.setDisable(false);
        btn_select.setDisable(false);
        btn_reserve.setDisable(false);


        LOGGER.info("set customer for receipt - ID: " + customer.getId()
                + " Name: " + customer.getFirstname() + " " + customer.getLastname());
    }

    @FXML
    private void onReservationClicked(ActionEvent event) {

        LOGGER.debug("Reservation Button pressed");

        //sell for anonymous customer
        if(check_anonym.isSelected()) {

            try{
                customerDto = customerService.getCustomerByID(1);
            } catch (ServiceException ex) {
                DialogUtil.showExceptionDialog("Anonymous customer","Couldn't get anonymous customer",ex);
            }
        }

        //tell the ShowViewController to reserve the Tickets for a certain customer instead of waiting and then get

        showViewController.reserveTickets(customerDto);

        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.close();

    }

    public void setShowViewController(ShowViewController showViewController) {
        this.showViewController = showViewController;
    }

}
