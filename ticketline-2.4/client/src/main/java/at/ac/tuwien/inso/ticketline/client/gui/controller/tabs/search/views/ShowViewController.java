package at.ac.tuwien.inso.ticketline.client.gui.controller.tabs.search.views;

import at.ac.tuwien.inso.ticketline.client.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.client.gui.controller.LoginController;
import at.ac.tuwien.inso.ticketline.client.gui.controller.MainController;
import at.ac.tuwien.inso.ticketline.client.gui.controller.tabs.search.SearchController;
import at.ac.tuwien.inso.ticketline.client.service.ReceiptService;
import at.ac.tuwien.inso.ticketline.client.service.ReservationService;
import at.ac.tuwien.inso.ticketline.client.service.TicketService;
import at.ac.tuwien.inso.ticketline.client.service.rest.EmployeeRestClient;
import at.ac.tuwien.inso.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.ticketline.client.util.DialogUtil;
import at.ac.tuwien.inso.ticketline.client.util.SpringFxmlLoader;
import at.ac.tuwien.inso.ticketline.dto.*;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.VPos;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.controlsfx.control.NotificationPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;

@Component
public class ShowViewController implements Initializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(ShowViewController.class);
    @Autowired
    private MainController mainController;
    @Autowired
    private SearchController searchController;
    @Autowired
    private SpringFxmlLoader springFxmlLoader;
    @Autowired
    private ReservationService reservationService;
    @Autowired
    private EmployeeRestClient employeeRestClient;

    @FXML
    private Label mainLB, titleLB, nameLB, typeLB, venueLB, roomLB, dateLB, timeLB, durationLB, artistLB, totalSeatsLB, freeSeatsLB, soldSeatsLB, reservedSeatsLB, selectedSeatsLB;
    @FXML
    private Label nameText, typeText, artistText, venueText, roomText, dateText, timeText, lengthText, seatsSelected, reservedSeats, soldSeats, freeSeats, totalSeats;
    @FXML
    private Button addToCartButton, reserveButton, backButton;

    @FXML
    private BorderPane borderPane;

    @FXML
    private AnchorPane anchorPane;

    @FXML
    private ScrollPane scrollPane;

    @FXML
    private GridPane gridPane;

    @FXML
    private Label info;

    @Autowired
    private TicketService ticketService;
    @Autowired
    private ReceiptService receiptService;

    private List<MapEntryDto<TicketDto, TicketIdentifierDto>> mapEntryDtoList;
    //private ObservableList<Node> circles;
    private List<TicketCircle> circles;
    private List<TicketCircle> selected;
    private List<TicketCircle> ticketsinCart;
    private HashMap<TicketCircle, TicketDto> ticketSeats;

    private ShowDto show;

    private NotificationPane notificationPane;

    /**
     * Initializes the controller.
     *
     * @param location  location of the fxml file
     * @param resources resources
     */
    @FXML
    public void initialize(URL location, ResourceBundle resources) {
        /*circles = anchorPane.getChildren();
        for(int i=1;i<circles.size();i++){
            Circle label = (Circle) circles.get(i);
            label.setOnMouseClicked(e -> mouseClickedOnSeat(e));label.setFill(Color.TRANSPARENT);

        }*/
        reinitLocale(MainController.currentLocale);
    }

    /**
     * changes the Language
     *
     * @param newValue
     */
    public void reinitLocale(Locale newValue) {
        LocaleContextHolder.setLocale(newValue);
        BundleManager.changeLocale(newValue);

        mainLB.setText(BundleManager.getBundle().getString("showView.main"));
        titleLB.setText(BundleManager.getBundle().getString("showView.title"));
        nameLB.setText(BundleManager.getBundle().getString("showView.nameLB"));
        typeLB.setText(BundleManager.getBundle().getString("showView.typeLB"));
        venueLB.setText(BundleManager.getBundle().getString("showView.venueLB"));
        roomLB.setText(BundleManager.getBundle().getString("showView.roomLB"));
        dateLB.setText(BundleManager.getBundle().getString("showView.dateLB"));
        timeLB.setText(BundleManager.getBundle().getString("showView.timeLB"));
        durationLB.setText(BundleManager.getBundle().getString("showView.durationLB"));
        artistLB.setText(BundleManager.getBundle().getString("showView.artistLB"));
        totalSeatsLB.setText(BundleManager.getBundle().getString("showView.totalSeatsLB"));
        freeSeatsLB.setText(BundleManager.getBundle().getString("showView.freeSeatsLB"));
        soldSeatsLB.setText(BundleManager.getBundle().getString("showView.soldSeatsLB"));
        reservedSeatsLB.setText(BundleManager.getBundle().getString("showView.reservedSeatsLB"));
        selectedSeatsLB.setText(BundleManager.getBundle().getString("showView.selectedSeatsLB"));

        backButton.setText(BundleManager.getBundle().getString("showView.backBut"));
        addToCartButton.setText(BundleManager.getBundle().getString("showView.addToCartBut"));
        reserveButton.setText(BundleManager.getBundle().getString("showView.reserveBut"));

    }

    /**
     * Adds Column Constraints to girdpane
     *
     * @param p
     * @return c
     */
    private ColumnConstraints columnWithPercentage(final double p) {
        final ColumnConstraints c = new ColumnConstraints();
        c.setHgrow(Priority.ALWAYS);
        c.setFillWidth(true);
        c.setPercentWidth(p);
        return c;
    }

    /**
     * Adds Row Constraints to girdpane
     *
     * @param p
     * @return c
     */
    private RowConstraints rowWithPercentage(final double p) {
        final RowConstraints c = new RowConstraints();
        c.setVgrow(Priority.ALWAYS);
        c.setFillHeight(true);
        c.setPercentHeight(p);
        return c;
    }

    /**
     * splits seating plan horizontly
     *
     * @param description
     * @return splitHorizontal
     */
    private int[] horizontalSplit(String description) {
        String splitH = "";

        int splitB = -1;
        int splitE = -1;
        int comma = -1;

        for (int i = 0; i < description.length(); i++) {
            if (description.charAt(i) == '(') {
                splitB = i;
            }
            if (description.charAt(i) == ')') {
                splitE = i;
            }
            if (description.charAt(i) == ',') {
                comma = i;
            }
        }
        splitH = description.substring(splitB + 1, comma);

        int horizontalCount = 0;

        for (int i = 0; i < splitH.length(); i++) {
            if (splitH.charAt(i) == '|') {
                horizontalCount++;
            }
        }


        int[] splitHorizontal = new int[horizontalCount];

        int beginindex = -1;
        int endindex = -1;

        int counter = 0;

        for (int i = 0; i < splitH.length(); i++) {
            if (splitH.charAt(i) == ' ') {
                beginindex = i;
            }
            if (splitH.charAt(i) == '|') {
                endindex = i;
            }
            if (beginindex != -1 && endindex != -1 && beginindex != endindex) {
                splitHorizontal[counter] = Integer.parseInt(splitH.substring(beginindex + 1, endindex));
                beginindex = endindex;
                counter++;
            }
        }


        return splitHorizontal;
    }

    /**
     * splits seating plan verticly
     *
     * @param description
     * @return splitVertical
     */
    private int[] verticalSplit(String description) {
        String splitV = "";

        int splitB = -1;
        int splitE = -1;
        int comma = -1;

        for (int i = 0; i < description.length(); i++) {
            if (description.charAt(i) == '(') {
                splitB = i;
            }
            if (description.charAt(i) == ')') {
                splitE = i;
            }
            if (description.charAt(i) == ',') {
                comma = i;
            }
        }
        splitV = description.substring(comma + 1, splitE);

        int verticalCount = 0;


        for (int i = 0; i < splitV.length(); i++) {
            if (splitV.charAt(i) == '|') {
                verticalCount++;
            }
        }

        int[] splitVertical = new int[verticalCount];

        int beginindex = -1;
        int endindex = -1;

        int counter = 0;


        for (int i = 0; i < splitV.length(); i++) {
            if (splitV.charAt(i) == ' ') {
                beginindex = i;
            }
            if (splitV.charAt(i) == '|') {
                endindex = i;
            }
            if (beginindex != -1 && endindex != -1 && beginindex != endindex) {
                splitVertical[counter] = Integer.parseInt(splitV.substring(beginindex + 1, endindex));
                beginindex = endindex;
                counter++;
            }
        }

        return splitVertical;
    }

    /**
     * Initializes seating plan
     *
     * @param show
     */
    public void setShow(ShowDto show) {
        circles = new LinkedList<>();
        selected = new ArrayList<>();
        ticketsinCart = new ArrayList<>();
        ticketSeats = new HashMap<>();
        mapEntryDtoList = new ArrayList<>();

        String description = show.getRoom().getDescription();
        int[] splitHorizontal = horizontalSplit(description);
        int[] splitVertical = verticalSplit(description);


        int x = -1;
        int y = -1;
        for (int i = 1; i < description.length(); i++) {
            if (description.charAt(i) == 'X') {
                x = i;
            }
            if (description.charAt(i) == '}') {
                y = i;
            }
        }

        int rows = Integer.parseInt(description.substring(1, x));
        int cols = Integer.parseInt(description.substring(x + 1, y));

        TicketCircle[] c = new TicketCircle[rows * cols];
        gridPane = new GridPane();
        gridPane.setPadding(new Insets(10, 10, 10, 10));
        gridPane.setHgap(4);
        gridPane.setVgap(10);
        int row = rows;
        int col = cols;
        int counter = 0;


        ImageView image = new ImageView();
        Image i2;
        if (description.contains("Screen")) {
            i2 = new Image("image/Screen.PNG");
        } else {
            i2 = new Image("image/Stage.PNG");
        }
        image.setImage(i2);

        gridPane.setConstraints(image, 0, 0);
        for (int k = 0; k < col + splitHorizontal.length; k++) {
            gridPane.getColumnConstraints().add(
                    columnWithPercentage(25)
            );
        }


        gridPane.setHalignment(image, HPos.CENTER);
        gridPane.setValignment(image, VPos.CENTER);
        gridPane.add(image, 0, 0, col + splitHorizontal.length, 1);

        int seatcnt = 1;
        int rowcnt = 1;

        int splithorizontalCounter = 0;
        int splitverticalCounter = 0;

        int ctV = 1;
        int ctH = 1;
        for (int i = 3; i < (row + 3 + splitVertical.length) * 2 /*- 3 - splitVertical.length*/; i = i + 2) {
            if (counter == row * col) {
                break;
            }
            ctH = 1;
            if (((splitverticalCounter >= splitVertical.length) || ((ctV != splitVertical[splitverticalCounter] + 1)))) {
                for (int j = 0; j < col + splitHorizontal.length; j++) {
                    if (((splithorizontalCounter >= splitHorizontal.length) || ((ctH != splitHorizontal[splithorizontalCounter] + 1)))) {

                        c[counter] = new TicketCircle();
                        c[counter].setStroke(Color.BLACK);
                        c[counter].setRadius(10.0f);
                        c[counter].setFill(Color.TRANSPARENT);


                        gridPane.setConstraints(c[counter], j, i);
                        gridPane.setHalignment(c[counter], HPos.CENTER);
                        gridPane.setValignment(c[counter], VPos.CENTER);
                        gridPane.getChildren().add(c[counter]);

                        c[counter].setRow(rowcnt);
                        c[counter].setSeat(seatcnt);
                        c[counter].setColumn(seatcnt);

                        c[counter].setOnMouseClicked(e -> mouseClickedOnSeat(e));
                        c[counter].setOnMouseEntered(e -> mouseEnteredOnSeat(e));


                        circles.add(c[counter]);

                        counter++;
                        seatcnt++;
                    } else {

                        splithorizontalCounter++;
                        ctH = 0;
                    }
                    ctH++;

                }

                seatcnt = 1;
                rowcnt++;
                splithorizontalCounter = 0;
            } else {

                splitverticalCounter++;
                ctV = 0;
            }
            ctV++;
        }

        scrollPane.setContent(gridPane);
        notificationPane = new NotificationPane(gridPane.getChildren().get(0));

        this.show = show;
        initShow();
        nameText.setText(show.getPerformance().getName());
        typeText.setText(show.getPerformance().getPerformancetype().substring(0, 1).toUpperCase() + show.getPerformance().getPerformancetype().substring(1).toLowerCase());
        String artists = "";
        for (ParticipationDto p : show.getPerformance().getParticipations()) {
            artists += p.getArtist().getFirstName() + " " + p.getArtist().getLastName() + " (" + p.getArtistRole() + ")\n";
        }

        artistText.setText(artists);
        venueText.setText(show.getRoom().getLocation().getName());
        roomText.setText(show.getRoom().getName());
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        dateText.setText(format.format(show.getDateOfPerformance()));
        format = new SimpleDateFormat("HH:mm");
        timeText.setText(format.format(show.getDateOfPerformance()));
        lengthText.setText(Integer.toString(show.getPerformance().getDuration()) + " min");
        /*for(int i = 1 ; i < circles.size(); i++){
            Circle circle = (Circle) circles.get(i);
            circle.setFill(Color.TRANSPARENT);
        }*/

        List<TicketIdentifierDto> allTickets = new ArrayList<>();
        /*try {
            mapEntryDtoList = ticketService.getTicketsForShow(show.getId());
        } catch (ServiceException se) {
            DialogUtil.showErrorDialog(BundleManager.getExceptionBundle().getString("showView.ticketLoadExHeader"),
                    BundleManager.getExceptionBundle().getString("showView.ticketLoadExBody"),
                    se.getMessage());
        }
        */
        try {
            List<TicketIdentifierDto> tID = ticketService.getTicketIdentifiers();
            List<TicketDto> t = ticketService.getAllTickets(show.getId());

            for (int i = 0; i < t.size(); i++) {
                mapEntryDtoList.add(new MapEntryDto<TicketDto, TicketIdentifierDto>(t.get(i), null));
                for (int j = 0; j < tID.size(); j++) {
                    if (tID.get(j).getTicket().equals(t.get(i)) && tID.get(j).getValid()) {
                        mapEntryDtoList.get(i).setValue(tID.get(j));
                        break;
                    }

                }
            }
        } catch (ServiceException e) {
            e.printStackTrace();
        }

        totalSeats.setText("" + col * row);
        reservedSeats.setText(0 + "");
        freeSeats.setText((mapEntryDtoList.size()) + "");
        soldSeats.setText(0 + "");
        seatsSelected.setText(0 + "");
        int cnt = 0;
        /*List for checking tickets in Cart*/
        List<TicketDto> tick = new ArrayList<>();
        for (TicketDto td : receiptService.getTicketCart()) {

            if (td.getShow().getId() == show.getId()) {
                tick.add(td);
            }
        }

        /*List for checking tickets in Cart*/
        for (int i = 0; i < mapEntryDtoList.size(); i++) {
            TicketDto ticketDto = mapEntryDtoList.get(i).getKey();

            c[i].setPrice(ticketDto.getPrice());
            c[i].setCategory(ticketDto.getSeat().getCategory());

            TicketIdentifierDto ticketIdentifierDto = mapEntryDtoList.get(i).getValue();
//            SeatDto seatDto = ticketDto.getSeat();
            c[i].setStatus("Free");
            c[i].setTicketDto(ticketDto);

            if (ticketIdentifierDto != null && ticketIdentifierDto.getValid()) {

                TicketCircle circle = circles.get(i);
                if (ticketIdentifierDto.getReservation() != null) {
                    circle.setFill(Color.LIGHTSEAGREEN);
                    reservedSeats.setText((Integer.parseInt(reservedSeats.getText()) + 1) + "");
                    c[i].setStatus("Reservated");
                } else {
                    circle.setFill(Color.GOLD);
                    soldSeats.setText((Integer.parseInt(soldSeats.getText()) + 1) + "");
                    c[i].setStatus("Sold");
                }
                freeSeats.setText((Integer.parseInt(freeSeats.getText()) - 1) + "");
            }
            /*List for checking tickets in Cart*/
            for (TicketDto td : tick) {
                if (td.getId().equals(ticketDto.getId())) {
                    TicketCircle circle = circles.get(i);
                    if (ticketIdentifierDto != null && ticketIdentifierDto.getValid()) {
                        //   receiptService.getTicketCart().remove(td);
                    } else {
                        circle.setFill(Color.DARKORANGE);
                    }
                }
            }
            /*List for checking tickets in Cart*/
            ticketSeats.put(circles.get(i), ticketDto);


            cnt++;

        }

    }

    /**
     * Adds information to the selected show
     */
    private void initShow() {

        List<ParticipationDto> participations = new ArrayList<>();
        for (ParticipationDto p : searchController.getAllParticipations()) {
            if (show.getPerformance().getId().equals(p.getPerformance().getId())) {
                participations.add(p);
            }
        }
        show.getPerformance().setParticipations(participations);
        List<ShowDto> shows = new ArrayList<>();
        for (ShowDto s : searchController.getAllShows()) {
            if (show.getPerformance().getId().equals(s.getPerformance().getId())) {
                shows.add(s);
            }
        }
        show.getPerformance().setShows(shows);
    }

    /**
     * This method puts the accordion in the foreground.
     *
     * @param event the back button event
     */
    @FXML
    public void handleBack(ActionEvent event) {
        //searchController.reset();
        searchController.getStackPane().getChildren().clear();
        searchController.getStackPane().getChildren().add(searchController.getAccordion());
    }


    /**
     * resets the seating plan
     */
    public void reset() {
        if (show != null) {
            setShow(show);
        }
    }

    /**
     * If in the cart the empty button is clicked, then this methon sets all the seat, that were added to the cart, to it's normal stat.
     */
    public void resetEmptyCart() {
        try {
            for (int i = 0; i < ticketsinCart.size(); i++) {
                ticketsinCart.get(i).setFill(Color.TRANSPARENT);
            }
            ticketsinCart.clear();
        } catch (Exception e) {
            //don't throrw exception
        }
    }

    /**
     * Resets seating plan after tickets have been sold.
     */
    public void resetWhenSold() {

        try {
            for (int i = 0; i < ticketsinCart.size(); i++) {
                ticketsinCart.get(i).setFill(Color.GOLD);
                soldSeats.setText((Integer.parseInt(soldSeats.getText()) + 1) + "");
                freeSeats.setText((Integer.parseInt(freeSeats.getText()) - 1) + "");

            }
            ticketsinCart.clear();
        } catch (Exception e) {
            //don't throrw exception
        }
    }


    /**
     * Resets seating plan after tickets have been removed from cart.
     *
     * @param a
     */
    public void resetRemoveTicket(TicketDto a) {
        try {
            for (int i = 0; i < circles.size(); i++) {
                if (circles.get(i).getTicketDto().getId() == a.getId()) {
                    circles.get(i).setFill(Color.TRANSPARENT);
                }
            }

            for (int j = 0; j < ticketsinCart.size(); j++) {
                if (ticketsinCart.get(j).getTicketDto().getId() == a.getId()) {
                    ticketsinCart.remove(j);
                }
            }
        } catch (Exception e) {

        }
    }

    /**
     * changes color of selected seat
     *
     * @param mouse
     */
    public void mouseClickedOnSeat(MouseEvent mouse) {
        TicketCircle circle = (TicketCircle) mouse.getSource();

        // seat is not reserved, bought or already in shopping cart
        if (circle.getFill() != Color.LIGHTSEAGREEN && circle.getFill() != Color.GOLD && circle.getFill() != Color.DARKORANGE) {
            if (circle.getFill() == Color.BLUEVIOLET) { // seat was already selected
                circle.setFill(Color.TRANSPARENT);
                Integer seats = Integer.parseInt(seatsSelected.getText());
                selected.remove(circle);
                seats--;
                seatsSelected.setText(seats.toString());
            } else {
                circle.setFill(Color.BLUEVIOLET);
                Integer seats = Integer.parseInt(seatsSelected.getText());
                selected.add(circle);
                seats++;
                seatsSelected.setText(seats.toString());
            }
        }
    }

    /**
     * shows information, if mouse hovers over a seat
     *
     * @param mouse
     */
    private void mouseEnteredOnSeat(MouseEvent mouse) {
        TicketCircle circle = (TicketCircle) mouse.getSource();

        info.setText(circle.toString());
        circle.setOnMouseExited((e -> mouseExitOnSeat()));
    }


    /**
     * deletes information, if mouse exists seat
     */
    private void mouseExitOnSeat() {
        info.setText("");
    }

    /**
     * add reservation and calls the Reservation Controller
     *
     * @param event
     */
    @FXML
    public void onReserveClicked(ActionEvent event) {
        if (!selected.isEmpty()) {
            Stage nextStage = new Stage();
            SpringFxmlLoader.LoadWrapper loadWrapper = springFxmlLoader.loadAndWrap("/gui/fxml/tabs/search/views/reservation_way.fxml");
            nextStage.setScene(new Scene((Parent) loadWrapper.getLoadedObject()));

            ReservationSelection controller = (ReservationSelection) loadWrapper.getController();
            controller.setShowViewController(this);

            ((Node) event.getSource()).setCursor(Cursor.DEFAULT);

            nextStage.setResizable(false);
            nextStage.setTitle(BundleManager.getBundle().getString("app.name"));
            nextStage.getIcons().add(new Image(LoginController.class.getResourceAsStream("/image/ticketlineLogo.png")));

            // make the current window not clickable
            Node source = (Node) event.getSource();
            Stage thisStage = (Stage) source.getScene().getWindow();
            nextStage.initModality(Modality.WINDOW_MODAL);
            nextStage.initOwner(thisStage);

            nextStage.show();

        } else {
            DialogUtil.showInformationDialog(BundleManager.getExceptionBundle().getString("showView.reservationExTitle"),
                    BundleManager.getExceptionBundle().getString("showView.noSeatExHeader"),
                    BundleManager.getExceptionBundle().getString("showView.noSeatExBody"));
        }

    }


    @FXML
    public void onAddToCartClicked() {
        List<TicketDto> inCart = new ArrayList<>();
        boolean add = true;
        if (!selected.isEmpty()) {
            for (int i = 0; i < selected.size(); i++) {
                TicketDto ticketDto = ticketSeats.get(selected.get(i));
                try {
                    boolean added = receiptService.addFreeTicketToCart(ticketDto);
                    if (!added) {
                        //#TODO please throw a proper exception
                        selected.get(i).setFill(Color.TRANSPARENT);
                        selected.remove(i);
                        i--;
                        add = false;
                    } else {
                        inCart.add(ticketDto);
                        selected.get(i).setFill(Color.DARKORANGE);
                    }
                } catch (ServiceException e) {
                    LOGGER.debug("Error in database");
                    DialogUtil.showErrorDialog("Error Dialog", "Error in Server", e.getMessage());
                }
            }
            if (add == false) {

                if (DialogUtil.showConfirmationDialog("Ticket selling", "" + " tickets are being used in another system. Do you want to add the rest of them?") != ButtonType.OK) {
                    for (int e = 0; e < inCart.size(); e++) {
                        receiptService.getTicketCart().remove(inCart.get(e));
                    }
                    try {
                        ticketService.releaseTicket(inCart);

                    } catch (ServiceException e) {
                        e.printStackTrace();
                    }


                    for (TicketCircle circle2 : selected) {
                        circle2.setFill(Color.TRANSPARENT);
                    }
                    selected.clear();
                    seatsSelected.setText("0");


                }
            }
            ticketsinCart.addAll(selected);
            selected.clear();
            seatsSelected.setText("0");
            receiptService.isCartEmpty();
        } else {
            DialogUtil.showInformationDialog(BundleManager.getExceptionBundle().getString("showView.reservationExTitle"),
                    BundleManager.getExceptionBundle().getString("showView.noSeatExHeader"),
                    BundleManager.getExceptionBundle().getString("showView.noSeatExBody"));
        }

    }

    /**
     * Reserves the selected Tickets for a certain customer
     *
     * @param customerDto the customer who wants to reserve Tickets
     */
    public void reserveTickets(CustomerDto customerDto) {

        if (customerDto != null) {
            try {
                ReservationDto reservationDto = new ReservationDto();
                EmployeeDto employeeDto = new EmployeeDto();
                employeeDto.setUsername(ticketService.getEmployee().getUsername());
                reservationDto.setEmployee(employeeRestClient.getEmployee(employeeDto));
                reservationDto.setCustomer(customerDto);
                Random random = new Random();
                int n = 100000 + random.nextInt(900000);
                reservationDto.setReservationNumber(n);
                reservationDto = reservationService.saveReservation(reservationDto);
                List<TicketIdentifierDto> ticketIdentifierDtos = new ArrayList<>();
                for (TicketCircle circle : selected) {
                    TicketDto ticketDto = ticketSeats.get(circle);
                    if (receiptService.canTheTicketBeReserved(ticketDto)) {
                        TicketIdentifierDto ticketIdentifierDto = new TicketIdentifierDto();
                        ticketIdentifierDto.setValid(true);
                        ticketIdentifierDto.setTicket(ticketDto);
                        ticketIdentifierDto.setReservation(reservationDto);
                        ticketIdentifierDtos.add(ticketIdentifierDto);
                        circle.setFill(Color.LIGHTSEAGREEN);
                        reservedSeats.setText((Integer.parseInt(reservedSeats.getText()) + 1) + "");
                        freeSeats.setText((Integer.parseInt(freeSeats.getText()) - 1) + "");

                    } else {
                        if (DialogUtil.showConfirmationDialog("Ticket selling", "b" + " tickets are being used in another system. Do you want to add the rest of them?") == ButtonType.OK) {
                            circle.setFill(Color.TRANSPARENT);
                            continue;
                        } else {
                            for (TicketCircle circle2 : selected) {
                                circle2.setFill(Color.TRANSPARENT);
                            }
                            selected.clear();
                            seatsSelected.setText("0");
                            if (ticketIdentifierDtos.size() > 0) {
                                ticketService.reserveTickets(ticketIdentifierDtos);
                            }
                            return;
                        }
                    }
                }
                ticketService.reserveTickets(ticketIdentifierDtos);

                int numberOfSelectedSeats = selected.size();
                seatsSelected.setText("0");
                selected.clear();

                DialogUtil.showSuccessDialog(BundleManager.getBundle().getString("reservation.success"),
                        numberOfSelectedSeats + " " + BundleManager.getBundle().getString("reservation.info")
                                + " " + customerDto.getFirstname() + " " + customerDto.getLastname(),
                        BundleManager.getBundle().getString("reservation.number") + " " + n
                );

            } catch (ServiceException se) {
                DialogUtil.showExceptionDialog(BundleManager.getExceptionBundle().getString("showView.reservationExTitle"),
                        BundleManager.getExceptionBundle().getString("showView.reservationExHeader"), se);
            }
        }
    }


    @FXML
    private void popUp(ActionEvent event) {
        if (this.show != null) {

            ShowViewPopUp showViewPopUp;
            SpringFxmlLoader.LoadWrapper loadWrapper = springFxmlLoader.loadAndWrap("/gui/fxml/tabs/search/views/showViewPopUp.fxml");
            showViewPopUp = (ShowViewPopUp) loadWrapper.getController();
            Stage newStage = new Stage();
            newStage.setScene(new Scene((Parent) loadWrapper.getLoadedObject()));
            newStage.setTitle(BundleManager.getBundle().getString("app.name"));
            newStage.getIcons().add(new Image(LoginController.class.getResourceAsStream("/image/ticketlineLogo.png")));
            borderPane.getChildren().clear();
            this.borderPane.setCenter(new ScrollPane());
            showViewPopUp.setSold(soldSeatsLB.getText());
            showViewPopUp.setReserved(reservedSeatsLB.getText());
            if (MainController.currentLocale.equals(Locale.ENGLISH)) {
                showViewPopUp.setInCart("In cart");
            } else {
                showViewPopUp.setInCart("In den Einkaufswagen");
            }
            showViewPopUp.setButton(this.reserveButton);
            showViewPopUp.setStage(newStage);
            showViewPopUp.setShowViewController(this);
            showViewPopUp.setScrollPane(this.scrollPane);


            Node source = (Node) event.getSource();
            Stage thisStage = (Stage) source.getScene().getWindow();

            newStage.initOwner(thisStage);
            newStage.initModality(Modality.WINDOW_MODAL);
            newStage.setMaximized(true);
            newStage.show();

        }

    }

    public void setScrollPane(ScrollPane scrollPane) {
        this.scrollPane = scrollPane;
        this.borderPane.setCenter(scrollPane);
    }
}
