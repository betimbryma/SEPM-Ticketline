package at.ac.tuwien.inso.ticketline.client.service.rest;

import at.ac.tuwien.inso.ticketline.client.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.client.service.ArtistService;
import at.ac.tuwien.inso.ticketline.dto.ArtistDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.List;

@Component
public class ArtistRestClient implements ArtistService {

    public static final String GET_ALL_ARTISTS = "/service/artist/";
    private static final Logger LOGGER = LoggerFactory.getLogger(ArtistRestClient.class);

    @Autowired
    private RestClient restClient;

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ArtistDto> getAll() throws ServiceException {
        RestTemplate restTemplate = this.restClient.getRestTemplate();
        String url = this.restClient.createServiceUrl(GET_ALL_ARTISTS);
        HttpEntity<String> entity = new HttpEntity<>(this.restClient.getHttpHeaders());
        LOGGER.info("Retrieving artists from {}", url);
        List<ArtistDto> artists;

        try {
            ParameterizedTypeReference<List<ArtistDto>> ref = new ParameterizedTypeReference<List<ArtistDto>>() {
            };
            ResponseEntity<List<ArtistDto>> response = restTemplate.exchange(URI.create(url), HttpMethod.GET, entity, ref);
            artists = response.getBody();
        } catch (RestClientException e) {
            throw new ServiceException("Could not retrieve artists: " + e.getMessage(), e);
        }

        return artists;
    }
}
