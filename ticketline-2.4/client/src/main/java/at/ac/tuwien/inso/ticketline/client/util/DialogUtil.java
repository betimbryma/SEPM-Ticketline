package at.ac.tuwien.inso.ticketline.client.util;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Optional;

/**
 * This Class provides several methods for showing Dialog Windows.
 */
public class DialogUtil {

    /**
     * Shows an error dialog.
     *
     * @param title   The Title of the Dialog Window
     * @param header  The Header of the Dialog Window
     * @param content The Content of the Dialog Window
     */
    public static void showErrorDialog(String title, String header, String content) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(content);

        alert.showAndWait();
    }

    /**
     * Shows a Dialog Window informing the User about the success of an Operation
     *
     * @param title   The Title of the Dialog Window
     * @param header  The Header of the Dialog Window
     * @param content The Content of the Dialog Window
     */
    public static void showSuccessDialog(String title, String header, String content) {
        Alert alert = new Alert(AlertType.ERROR);
        alert.setGraphic(new ImageView(new Image(DialogUtil.class.getResourceAsStream("/image/icon/success_icon.png"), 50, 50, true, true)));

        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(content);

        alert.showAndWait();
    }

    /**
     * Shows a Dialog Window asking the User for confirmation
     *
     * @param title   The Title of the Confirmation Window
     * @param content The Content of the Dialog Window
     * @return Returns ButtonType.OK if the user confirms
     */
    public static ButtonType showConfirmationDialog(String title, String content) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(content);

        Optional<ButtonType> result = alert.showAndWait();
        return result.get();
    }

    /**
     * Shows a Dialog Window informing the User about the failure of an operation. Additional information is provided
     * by printing the StackTrace of the Exception to a TextArea in the expandable part of the Window.
     *
     * @param title  The Title of the Dialog Window
     * @param header The Header of the Dialog Window
     * @param e      The Exception, whichs StackTrace should be shown
     */
    public static void showExceptionDialog(String title, String header, Exception e) {
        Alert alert = new Alert(AlertType.ERROR);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(e.getMessage());

        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw);
        String exceptionText = sw.toString();

        Label label = new Label("The exception stacktrace was:");
        TextArea textArea = new TextArea(exceptionText);
        textArea.setEditable(false);
        textArea.setWrapText(true);

        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);

        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(label, 0, 0);
        expContent.add(textArea, 0, 1);

        // Set expandable Exception into the dialog pane.
        alert.getDialogPane().setExpandableContent(expContent);

        alert.showAndWait();
    }

    /**
     * Creates an alert pop-up that displays an information-dialog
     *
     * @param dialog  the title of the pop-up
     * @param header  the header that contains the info
     * @param content the details
     */
    public static void showInformationDialog(String dialog, String header, String content) {
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle(dialog);
        alert.setHeaderText(header);
        alert.setContentText(content);

        alert.showAndWait();
    }

    /**
     * Creates a warning alert pop-up that displays an information-dialog
     *
     * @param dialog  the title of the pop-up
     * @param header  the header that contains the info
     * @param content the details
     */
    public static void showWarningDialog(String dialog, String header, String content) {
        Alert alert = new Alert(AlertType.WARNING);
        alert.setTitle(dialog);
        alert.setHeaderText(header);
        alert.setContentText(content);

        alert.showAndWait();
    }
}
