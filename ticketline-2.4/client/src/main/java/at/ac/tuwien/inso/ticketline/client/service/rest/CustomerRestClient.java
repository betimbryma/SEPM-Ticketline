package at.ac.tuwien.inso.ticketline.client.service.rest;

import at.ac.tuwien.inso.ticketline.client.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.client.exception.ValidationException;
import at.ac.tuwien.inso.ticketline.client.service.CustomerService;
import at.ac.tuwien.inso.ticketline.dto.CustomerDto;
import at.ac.tuwien.inso.ticketline.dto.MessageDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.Collections;
import java.util.List;

@Component
public class CustomerRestClient implements CustomerService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomerRestClient.class);

    public static final String SAVE_CUSTOMER_URL = "/service/customer/";
    public static final String GET_ALL_CUSTOMERS = "/service/customer/";
    public static final String EDIT_CUSTOMER_URL = "/service/customer/";
    public static final String GET_CUSTOMER_ID = "/service/customer/";

    @Autowired
    private RestClient restClient;

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer saveCustomer(CustomerDto customer) throws ServiceException {

        RestTemplate restTemplate = this.restClient.getRestTemplate();

        String url = this.restClient.createServiceUrl(SAVE_CUSTOMER_URL);

        LOGGER.info("Saving customer at {}", url);

        HttpHeaders headers = this.restClient.getHttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        HttpEntity<CustomerDto> entity = new HttpEntity<>(customer, headers);
        MessageDto msg;

        try {
            msg = restTemplate.postForObject(url, entity, MessageDto.class);
        } catch (HttpStatusCodeException e) {
            MessageDto errorMsg = this.restClient.mapExceptionToMessage(e);
            if (errorMsg.hasFieldErrors()) {
                throw new ValidationException(errorMsg.getFieldErrors());
            } else {
                throw new ServiceException(errorMsg.getText());
            }
        } catch (RestClientException e) {
            throw new ServiceException("Could not save customer: " + e.getMessage(), e);
        }

        Integer id;

        try {
            id = Integer.valueOf(msg.getText());
        } catch (NumberFormatException e) {
            throw new ServiceException("Invalid ID: " + msg.getText());
        }
        return id;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<CustomerDto> getCustomers() throws ServiceException {
        RestTemplate restTemplate = this.restClient.getRestTemplate();
        String url = this.restClient.createServiceUrl(GET_ALL_CUSTOMERS);
        HttpEntity<String> entity = new HttpEntity<>(this.restClient.getHttpHeaders());
        LOGGER.info("Retrieving customers from {}", url);
        List<CustomerDto> customers;
        try {
            ParameterizedTypeReference<List<CustomerDto>> ref = new ParameterizedTypeReference<List<CustomerDto>>() {
            };
            ResponseEntity<List<CustomerDto>> response = restTemplate.exchange(URI.create(url), HttpMethod.GET, entity, ref);
            customers = response.getBody();

        } catch (RestClientException e) {
            throw new ServiceException("Could not retrieve customers: " + e.getMessage(), e);
        }
        return customers;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer editCustomer(CustomerDto customerDto) throws ServiceException {

        String url = this.restClient.createServiceUrl(EDIT_CUSTOMER_URL + customerDto.getId());
        LOGGER.info("Editing customer at {}", url);

        RestTemplate restTemplate = this.restClient.getRestTemplate();
        HttpHeaders httpHeaders = restClient.getHttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        httpHeaders.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        HttpEntity<CustomerDto> entity = new HttpEntity<>(customerDto, httpHeaders);

        MessageDto messageDto;

        try {
            messageDto = restTemplate.postForObject(url, entity, MessageDto.class);
        } catch (HttpStatusCodeException e) {
            MessageDto errorMsg = this.restClient.mapExceptionToMessage(e);
            if (errorMsg.hasFieldErrors()) {
                throw new ValidationException(errorMsg.getFieldErrors());
            } else {
                throw new ServiceException(errorMsg.getText());
            }
        } catch (RestClientException e) {
            throw new ServiceException("Could not edit customer: " + e.getMessage(), e);
        }

        Integer ID;
        try {
            ID = Integer.valueOf(messageDto.getText());
        } catch (NumberFormatException num) {
            throw new ServiceException("Invalid ID:" + messageDto.getText());
        }

        return ID;

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CustomerDto getCustomerByID(Integer ID) throws ServiceException {

        RestTemplate restTemplate = this.restClient.getRestTemplate();
        String url = this.restClient.createServiceUrl(GET_CUSTOMER_ID + ID);
        //HttpHeaders headers = this.restClient.getHttpHeaders();
        //headers.setContentType(MediaType.APPLICATION_JSON);
        //headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<>(this.restClient.getHttpHeaders());
        LOGGER.info("Getting customer by ID from {}", url);
        CustomerDto customerDto;

        try {
            ParameterizedTypeReference<CustomerDto> parameterizedTypeReference = new ParameterizedTypeReference<CustomerDto>() {
            };
            ResponseEntity<CustomerDto> responseEntity = restTemplate.exchange(URI.create(url), HttpMethod.GET, entity, parameterizedTypeReference);

            customerDto = responseEntity.getBody();
        } catch (RestClientException se) {
            throw new ServiceException("Could not get the customer " + se.getMessage());
        }

        return customerDto;

    }


}
