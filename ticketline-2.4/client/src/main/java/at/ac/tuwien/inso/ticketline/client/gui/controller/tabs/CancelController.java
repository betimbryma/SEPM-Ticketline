package at.ac.tuwien.inso.ticketline.client.gui.controller.tabs;

/**
 * Created by Ahmed on 27.05.2016.
 */

import at.ac.tuwien.inso.ticketline.client.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.client.gui.controller.LoginController;
import at.ac.tuwien.inso.ticketline.client.gui.controller.MainController;
import at.ac.tuwien.inso.ticketline.client.gui.controller.shoppingCart.SellController;
import at.ac.tuwien.inso.ticketline.client.gui.controller.tabs.search.SearchController;
import at.ac.tuwien.inso.ticketline.client.gui.controller.tabs.search.views.ShowViewPopUp;
import at.ac.tuwien.inso.ticketline.client.gui.controller.tabs.search.views.TicketCircle;
import at.ac.tuwien.inso.ticketline.client.service.ReceiptService;
import at.ac.tuwien.inso.ticketline.client.service.ReservationService;
import at.ac.tuwien.inso.ticketline.client.service.TicketService;
import at.ac.tuwien.inso.ticketline.client.service.rest.EmployeeRestClient;
import at.ac.tuwien.inso.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.ticketline.client.util.DialogUtil;
import at.ac.tuwien.inso.ticketline.client.util.SpringFxmlLoader;
import at.ac.tuwien.inso.ticketline.dto.MapEntryDto;
import at.ac.tuwien.inso.ticketline.dto.ShowDto;
import at.ac.tuwien.inso.ticketline.dto.TicketDto;
import at.ac.tuwien.inso.ticketline.dto.TicketIdentifierDto;
import javafx.fxml.FXML;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.VPos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import org.controlsfx.control.NotificationPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.awt.event.ActionEvent;
import java.net.URL;
import java.util.*;

@Component
public class CancelController {
    private static final Logger LOGGER = LoggerFactory.getLogger(CancelController.class);

    @Autowired
    private MainController mainController;
    @Autowired
    private SearchController searchController;
    @Autowired
    private SpringFxmlLoader springFxmlLoader;
    @Autowired
    private ReservationService reservationService;
    @Autowired
    private EmployeeRestClient employeeRestClient;

    @FXML
    private BorderPane borderPane;
    @FXML
    private Label reservationTitileL, rnumL, customerL, showL, timeL, seatsL;

    @FXML
    private Label rnum, customer, Show, time, seats;

    @FXML
    private Button buyselect, cancelselect, cancelall, buyall, backB;

    @FXML
    private TitledPane titledPane;

    @FXML
    private ScrollPane scrollPane;

    @FXML
    private GridPane gridPane;

    @FXML
    private Label info;

    @Autowired
    private TicketService ticketService;
    @Autowired
    private ReceiptService receiptService;

    private List<MapEntryDto<TicketDto, TicketIdentifierDto>> mapEntryDtoList;
    //private ObservableList<Node> circles;
    private List<TicketCircle> circles;
    private List<TicketCircle> selected;
    private HashMap<TicketCircle, TicketDto> ticketSeats;
    private List<TicketCircle> ticketsinCart;


    private ShowDto show;

    private NotificationPane notificationPane;
    private StackPane stackPane;

    private AnchorPane reservationAnchorPane;
    private ReservationsController reservationsController;

    List<TicketIdentifierDto> reservated;
    List<TicketIdentifierDto> sold;

    /**
     * Initializes the controller.
     *
     * @param location  location of the fxml file
     * @param resources resources
     */
    @FXML
    public void initialize(URL location, ResourceBundle resources) {
        reinitLocale(MainController.currentLocale);
    }

    /**
     * changes the Language
     *
     * @param newValue
     */
    public void reinitLocale(Locale newValue) {
        LocaleContextHolder.setLocale(newValue);
        BundleManager.changeLocale(newValue);
        reservationTitileL.setText(BundleManager.getBundle().getString("cancel.titleL"));
        rnumL.setText(BundleManager.getBundle().getString("cancel.rnumberL"));
        customerL.setText(BundleManager.getBundle().getString("cancel.customerL"));
        showL.setText(BundleManager.getBundle().getString("cancel.showL"));
        timeL.setText(BundleManager.getBundle().getString("cancel.timeL"));
        seatsL.setText(BundleManager.getBundle().getString("cancel.seatsL"));
        buyselect.setText(BundleManager.getBundle().getString("cancel.buyselectedB"));
        cancelselect.setText(BundleManager.getBundle().getString("cancel.cancelselectedB"));
        cancelall.setText(BundleManager.getBundle().getString("cancel.cancelallB"));
        buyall.setText(BundleManager.getBundle().getString("cancel.buyallB"));
        backB.setText(BundleManager.getBundle().getString("cancel.backB"));


    }

    /**
     * sets Stackpane
     * @param stackPane
     */
    public void setStackPane(StackPane stackPane) {
        this.stackPane = stackPane;
    }

    /**
     * sets Customer Controller
     * @param reservationsController
     */
    public void setCustomersController(ReservationsController reservationsController) {
        this.reservationsController = reservationsController;
    }

    /**
     * Adds Column Constraints to girdpane
     *
     * @param value
     * @return c
     */
    private ColumnConstraints columnWithPercentage(final double value) {
        final ColumnConstraints c = new ColumnConstraints();
        c.setHgrow(Priority.ALWAYS);
        c.setFillWidth(true);
        c.setPercentWidth(value);
        return c;
    }

    /**
     * Adds Row Constraints to girdpane
     *
     * @param value
     * @return c
     */
    private RowConstraints rowWithPercentage(final double value) {
        final RowConstraints c = new RowConstraints();
        c.setVgrow(Priority.ALWAYS);
        c.setFillHeight(true);
        c.setPercentHeight(value);
        return c;
    }

    /**
     * splits seating plan horizontly
     *
     * @param description
     * @return splitHorizontal
     */
    private int[] horizontalSplit(String description) {
        String splitH = "";

        int splitB = -1;
        int splitE = -1;
        int comma = -1;

        for (int i = 0; i < description.length(); i++) {
            if (description.charAt(i) == '(') {
                splitB = i;
            }
            if (description.charAt(i) == ')') {
                splitE = i;
            }
            if (description.charAt(i) == ',') {
                comma = i;
            }
        }
        splitH = description.substring(splitB + 1, comma);

        int horizontalCount = 0;

        for (int i = 0; i < splitH.length(); i++) {
            if (splitH.charAt(i) == '|') {
                horizontalCount++;
            }
        }


        int[] splitHorizontal = new int[horizontalCount];

        int beginindex = -1;
        int endindex = -1;

        int counter = 0;

        for (int i = 0; i < splitH.length(); i++) {
            if (splitH.charAt(i) == ' ') {
                beginindex = i;
            }
            if (splitH.charAt(i) == '|') {
                endindex = i;
            }
            if (beginindex != -1 && endindex != -1 && beginindex != endindex) {
                splitHorizontal[counter] = Integer.parseInt(splitH.substring(beginindex + 1, endindex));
                beginindex = endindex;
                counter++;
            }
        }


        return splitHorizontal;
    }

    /**
     * splits seating plan verticly
     *
     * @param description
     * @return splitVertical
     */
    private int[] verticalSplit(String description) {
        String splitV = "";

        int splitB = -1;
        int splitE = -1;
        int comma = -1;

        for (int i = 0; i < description.length(); i++) {
            if (description.charAt(i) == '(') {
                splitB = i;
            }
            if (description.charAt(i) == ')') {
                splitE = i;
            }
            if (description.charAt(i) == ',') {
                comma = i;
            }
        }
        splitV = description.substring(comma + 1, splitE);

        int verticalCount = 0;


        for (int i = 0; i < splitV.length(); i++) {
            if (splitV.charAt(i) == '|') {
                verticalCount++;
            }
        }

        int[] splitVertical = new int[verticalCount];

        int beginindex = -1;
        int endindex = -1;

        int counter = 0;


        for (int i = 0; i < splitV.length(); i++) {
            if (splitV.charAt(i) == ' ') {
                beginindex = i;
            }
            if (splitV.charAt(i) == '|') {
                endindex = i;
            }
            if (beginindex != -1 && endindex != -1 && beginindex != endindex) {
                splitVertical[counter] = Integer.parseInt(splitV.substring(beginindex + 1, endindex));
                beginindex = endindex;
                counter++;
            }
        }

        return splitVertical;
    }

    /**
     * Initializes seating plan
     *
     * @param allTicketIdentifiers
     */
    public void setTicketIdentifier(TicketIdentifierDto tID, List<TicketIdentifierDto> allTicketIdentifiers) {


        this.show = tID.getTicket().getShow();
        System.out.println(tID.getReservation().getReservationNumber() + "");
        rnum.setText(tID.getReservation().getReservationNumber() + "");
        String fname = tID.getReservation().getCustomer().getFirstname();
        String lname = tID.getReservation().getCustomer().getLastname();
        String name = (fname.equals(lname) ? fname : fname + " " + lname);
        customer.setText(name);
        Show.setText(tID.getTicket().getShow().getPerformance().getName());
        time.setText(tID.getTicket().getShow().getDateOfPerformance() + "");

        reservated = new LinkedList<>();
        sold = new LinkedList<>();
        ticketsinCart = new ArrayList<>();

        int id = tID.getReservation().getReservationNumber();
        for (int i = 0; i < allTicketIdentifiers.size(); i++) {

            if (allTicketIdentifiers.get(i).getReservation() != null && id == allTicketIdentifiers.get(i).getReservation().getReservationNumber()) {

                if (allTicketIdentifiers.get(i).getValid()) {
                    reservated.add(allTicketIdentifiers.get(i));
                }
            }

        }


        selected = new LinkedList<>();
        circles = new LinkedList<>();
        ticketSeats = new HashMap<>();

        String description = tID.getTicket().getShow().getRoom().getDescription();
        int[] splitHorizontal = horizontalSplit(description);
        int[] splitVertical = verticalSplit(description);


        int x = -1;
        int y = -1;
        for (int i = 1; i < description.length(); i++) {
            if (description.charAt(i) == 'X') {
                x = i;
            }
            if (description.charAt(i) == '}') {
                y = i;
            }
        }

        int rows = Integer.parseInt(description.substring(1, x));
        int cols = Integer.parseInt(description.substring(x + 1, y));

        TicketCircle[] c = new TicketCircle[rows * cols];
        gridPane = new GridPane();
        gridPane.setPadding(new Insets(10, 10, 10, 10));
        gridPane.setHgap(4);
        gridPane.setVgap(10);
        int row = rows;
        int col = cols;
        int counter = 0;


        ImageView image = new ImageView();
        Image i2;
        if (description.contains("Screen")) {
            i2 = new Image("image/Screen.PNG");
        } else {
            i2 = new Image("image/Stage.PNG");
        }
        image.setImage(i2);

        gridPane.setConstraints(image, 0, 0);
        for (int k = 0; k < col + splitHorizontal.length; k++) {
            gridPane.getColumnConstraints().add(
                    columnWithPercentage(25)
            );
        }


        gridPane.setHalignment(image, HPos.CENTER);
        gridPane.setValignment(image, VPos.CENTER);
        gridPane.add(image, 0, 0, col + splitHorizontal.length, 1);

        int seatcnt = 1;
        int rowcnt = 1;

        int splithorizontalCounter = 0;
        int splitverticalCounter = 0;

        int ctV = 1;
        int ctH = 1;
        for (int i = 3; i < (row + 3 + splitVertical.length) * 2; i = i + 2) {
            if (counter == row * col) {
                break;
            }
            ctH = 1;
            if (((splitverticalCounter >= splitVertical.length) || ((ctV != splitVertical[splitverticalCounter] + 1)))) {
                for (int j = 0; j < col + splitHorizontal.length; j++) {
                    if (((splithorizontalCounter >= splitHorizontal.length) || ((ctH != splitHorizontal[splithorizontalCounter] + 1)))) {

                        c[counter] = new TicketCircle();
                        c[counter].setStroke(Color.BLACK);
                        c[counter].setRadius(10.0f);
                        c[counter].setFill(Color.TRANSPARENT);


                        gridPane.setConstraints(c[counter], j, i);
                        gridPane.setHalignment(c[counter], HPos.CENTER);
                        gridPane.setValignment(c[counter], VPos.CENTER);
                        gridPane.getChildren().add(c[counter]);

                        c[counter].setRow(rowcnt);
                        c[counter].setSeat(seatcnt);
                        c[counter].setColumn(seatcnt);


                        circles.add(c[counter]);

                        counter++;
                        seatcnt++;
                    } else {

                        splithorizontalCounter++;
                        ctH = 0;
                    }
                    ctH++;

                }

                seatcnt = 1;
                rowcnt++;
                splithorizontalCounter = 0;
            } else {

                splitverticalCounter++;
                ctV = 0;
            }
            ctV++;
        }

        scrollPane.setContent(gridPane);
        notificationPane = new NotificationPane(gridPane.getChildren().get(0));


        seats.setText(reservated.size() + "");

        for (int i = 0; i < circles.size(); i++) {
            for (int j = 0; j < reservated.size(); j++) {
                if (circles.get(i).getRow() == reservated.get(j).getTicket().getSeat().getRow().getOrder()) {
                    if (circles.get(i).getSeat() == reservated.get(j).getTicket().getSeat().getGallery().getOrder()) {

                        if (reservated.get(j).getValid() && reservated.get(j).getReservation() != null) {
                            circles.get(i).setFill(Color.LIGHTSEAGREEN);
                            circles.get(i).setStatus("Reservated");

                        } else if (reservated.get(j).getValid() && reservated.get(j).getReservation() == null) {
                            circles.get(i).setFill(Color.GOLD);
                            circles.get(i).setStatus("Sold");

                        }
                        circles.get(i).setTicketDto(reservated.get(j).getTicket());
                        circles.get(i).setPrice(reservated.get(j).getTicket().getPrice());
                        circles.get(i).setCategory(reservated.get(j).getTicket().getSeat().getCategory());
                        circles.get(i).setOnMouseEntered(e -> mouseEnteredOnSeat(e));
                        circles.get(i).setOnMouseClicked(e -> mouseClickedOnSeat(e));

                    }
                }
            }
        }

    }


    /**
     * This method puts the accordion in the foreground.
     *
     * @param event the back button event
     */
    @FXML
    public void handleBack(ActionEvent event) {
        searchController.reset();
        searchController.getStackPane().getChildren().clear();
        searchController.getStackPane().getChildren().add(searchController.getAccordion());
    }

    /*public void reset() {

        if (show != null) {
            setShow(show);
        }
    }
*/
    /**
     * changes color of selected seat
     * @param mouse
     */
    private void mouseClickedOnSeat(MouseEvent mouse) {

        TicketCircle circle = (TicketCircle) mouse.getSource();
        if (circle.getFill() == Color.LIGHTSEAGREEN) {

            circle.setFill(Color.BLUEVIOLET);
            selected.add(circle);


        } else if (circle.getFill() == Color.BLUEVIOLET) {

            circle.setFill(Color.LIGHTSEAGREEN);
            selected.remove(circle);

        }
    }

    /**
     * shows information, if mouse hovers over a seat
     * @param mouse
     */
    private void mouseEnteredOnSeat(MouseEvent mouse) {
        TicketCircle circle = (TicketCircle) mouse.getSource();

        info.setText(circle.toString());
        info.setTextFill(Color.RED);
        circle.setOnMouseExited((e -> mouseExitOnSeat()));
    }

    /**
     * deletes information, if mouse exists seat
     */
    private void mouseExitOnSeat() {
        info.setText("");
    }

    /**
     * calcles selected reservations
     */
    @FXML
    public void CancelSelectedButton() {


        for (int i = 0; i < selected.size(); i++) {
            for (int j = 0; j < reservated.size(); j++) {
                if (selected.get(i).getRow() == reservated.get(j).getTicket().getSeat().getRow().getOrder()) {
                    if (selected.get(i).getSeat() == reservated.get(j).getTicket().getSeat().getGallery().getOrder()) {
                        seats.setText(Integer.parseInt(seats.getText()) - 1 + "");
                        selected.get(i).setFill(Color.TRANSPARENT);
                        selected.get(i).setStatus("Free");
                        reservated.get(j).setValid(false);
                        try {
                            ticketService.changeReservation(reservated.get(j));
                        } catch (ServiceException e) {
                            e.printStackTrace();
                        }

                    }
                }
            }
        }

        for (int i = 0; i < selected.size(); i++) {
            selected.remove(i);
        }
        selected.clear();
        reservationsController.refreshAllReservationsList();

    }

    /**
     * cancles all resrevations
     */
    @FXML
    public void CancelAllButton() {

        for (int i = 0; i < circles.size(); i++) {
            for (int j = 0; j < reservated.size(); j++) {
                if (circles.get(i).getRow() == reservated.get(j).getTicket().getSeat().getRow().getOrder()) {
                    if (circles.get(i).getSeat() == reservated.get(j).getTicket().getSeat().getGallery().getOrder()) {
                        if(circles.get(i).getFill()!= Color.DARKORANGE) {
                            circles.get(i).setFill(Color.TRANSPARENT);
                            circles.get(i).setStatus("Free");
                            reservated.get(j).setValid(false);
                            try {
                                ticketService.changeReservation(reservated.get(j));
                            } catch (ServiceException e) {
                                e.printStackTrace();
                            }
                        }

                    }
                }
            }
        }
        selected.clear();
        seats.setText("0");
        reservated.clear();
        reservationsController.refreshAllReservationsList();
    }

    /**
     * adds selescted seats to cart
     */
    @FXML
    public void AddToCartSelectedButton() {
        //ticketsinCart.clear();

        for (int i = 0; i < selected.size(); i++) {
            for (int j = 0; j < reservated.size(); j++) {
                if (selected.get(i).getRow() == reservated.get(j).getTicket().getSeat().getRow().getOrder()) {
                    if (selected.get(i).getSeat() == reservated.get(j).getTicket().getSeat().getGallery().getOrder()) {


                        /*ADD TICKETIDENTIFIER TO cART- BEGIN*/
                        try {
                            boolean added = receiptService.addReservedTicketToCart(reservated.get(j).getTicket());
                            if (!added) {
                                if(DialogUtil.showConfirmationDialog("Ticket selling","b"+" tickets are being used in another system. Do you want to add the rest of them?")== ButtonType.OK){
                                    selected.get(i).setFill(Color.LIGHTSEAGREEN);
                                }else{

                                    selected.clear();
                                    return;
                                }
                            } else {
                                seats.setText(Integer.parseInt(seats.getText()) - 1 + "");

                                selected.get(i).setFill(Color.DARKORANGE);
                                selected.get(i).setStatus("Sold");

                                ticketsinCart.add(selected.get(i));
                                try {
                                    sellController.setCustomer(reservated.get(j).getReservation().getCustomer());
                                } catch (NullPointerException e) {
                                    //nothing serious
                                }
                                TicketIdentifierDto oldTE = new TicketIdentifierDto();
                                oldTE.setId(reservated.get(j).getId());

                                oldTE.setReservation(reservated.get(j).getReservation());
                                oldTE.setVoidationTime(reservated.get(j).getVoidationTime());
                                oldTE.setVoidedBy(reservated.get(j).getVoidedBy());
                                oldTE.setReservation(reservated.get(j).getReservation());
                                receiptService.isCartEmpty();
                                reservated.get(j).getTicket().setOldTE(oldTE);
                            }
                        } catch (ServiceException e) {
                            LOGGER.debug("Error in database");
                            DialogUtil.showErrorDialog("Error Dialog", "Error in Server", e.getMessage());
                        }


                        /*END*/

                    }
                }
            }
        }

        for (int i = 0; i < selected.size(); i++) {
            selected.remove(i);
        }
        selected.clear();
        reservationsController.refreshAllReservationsList();

    }

    @Autowired
    private SellController sellController;

    /**
     * add all seats to cart
     */
    @FXML
    public void AddToCartAllButton() {
        //ticketsinCart.clear();

        for (int i = 0; i < circles.size(); i++) {
            for (int j = 0; j < reservated.size(); j++) {
                if (circles.get(i).getRow() == reservated.get(j).getTicket().getSeat().getRow().getOrder()) {
                    if (circles.get(i).getSeat() == reservated.get(j).getTicket().getSeat().getGallery().getOrder()&&circles.get(i).getFill()!= Color.GOLD&&circles.get(i).getFill()!= Color.DARKORANGE) {

                          /*ADD TICKETIDENTIFIER TO cART- BEGIN*/
                        try {
                            boolean added = receiptService.addReservedTicketToCart(reservated.get(j).getTicket());
                            if (!added) {
                                //#TODO please throw in this case a proper exception, the ticket is being used by somebody else
                                if(DialogUtil.showConfirmationDialog("Ticket selling","b"+" tickets are being used in another system. Do you want to add the rest of them?")== ButtonType.OK){
                                    circles.get(i).setFill(Color.LIGHTSEAGREEN);
                                }else{
                                    return;
                                }
                            } else {
                                try {
                                    sellController.setCustomer(reservated.get(j).getReservation().getCustomer());
                                } catch (NullPointerException e) {
                                    //nothing serious
                                }
                                circles.get(i).setFill(Color.DARKORANGE);
                                circles.get(i).setStatus("Sold");

                                ticketsinCart.add(circles.get(i));

                                TicketIdentifierDto oldTE = new TicketIdentifierDto();
                                oldTE.setId(reservated.get(j).getId());

                                oldTE.setReservation(reservated.get(j).getReservation());
                                oldTE.setVoidationTime(reservated.get(j).getVoidationTime());
                                oldTE.setVoidedBy(reservated.get(j).getVoidedBy());
                                oldTE.setReservation(reservated.get(j).getReservation());
                                receiptService.isCartEmpty();
                                reservated.get(j).getTicket().setOldTE(oldTE);
                            }
                        } catch (ServiceException e) {
                            LOGGER.debug("Error in database");
                            DialogUtil.showErrorDialog("Error Dialog", "Error in Server", e.getMessage());
                        }
                        /*END*/
                    }
                }
            }
        }

        //reservated.clear();
        seats.setText("0");
        selected.clear();
        reservationsController.refreshAllReservationsList();

    }

    /**
     * sets ReservationAnchorPane
     * @param reservationAnchorPane
     */
    public void setReservationAnchorPane(AnchorPane reservationAnchorPane) {
        this.reservationAnchorPane = reservationAnchorPane;
    }

    /**
     * sets ReservationController
     * @param reservationController
     */
    public void setReservationController(ReservationsController reservationController) {
        this.reservationsController = reservationController;
    }

    /**
     * return to the previous window
     */
    @FXML
    private void onBackButtonPressed() {
        LOGGER.info("User clikced the back button, going back to the main-customers page");
        //reservationsController.refreshAllReservationsList();
        reservationsController.onFindReservationButtonPressed();
        stackPane.getChildren().clear();
        stackPane.getChildren().add(reservationAnchorPane);
    }

    /**
     * If in the cart the empty button is clicked, then this methon sets all the seat, that were added to the cart, to it's normal stat.
     */
    public void resetEmptyCart() {
        try {
            for (int i = 0; i < ticketsinCart.size(); i++) {
                ticketsinCart.get(i).setFill(Color.LIGHTSEAGREEN);
            }
            ticketsinCart.clear();
        }catch(NullPointerException e){

        }
    }

    /**
     * Resets seating plan after tickets have been sold.
     */
    public void resetWhenSold() {
        try {
            for (int i = 0; i < ticketsinCart.size(); i++) {
                ticketsinCart.get(i).setFill(Color.GOLD);
            }
            ticketsinCart.clear();
        } catch (NullPointerException e) {

        }
    }

    /**
     * Resets seating plan after tickets have been removed from cart.
     *
     * @param a
     */
    public void resetRemoveTicket(TicketDto a) {

        for (int i = 0; i < circles.size(); i++) {
                if (circles.get(i).getTicketDto()!=null && circles.get(i).getTicketDto().getId() == a.getId()) {
                    circles.get(i).setFill(Color.LIGHTSEAGREEN);
                }

        }

        for (int j = 0; j < ticketsinCart.size(); j++) {
                if (ticketsinCart.get(j).getTicketDto().getId() == a.getId()) {
                    ticketsinCart.remove(j);
                }

        }
    }

    @FXML
    public void popUp(){
        CancelPopUpController cancelPopUpController;
        SpringFxmlLoader.LoadWrapper loadWrapper = springFxmlLoader.loadAndWrap("/gui/fxml/tabs/reservations/cancel_popup.fxml");
        cancelPopUpController = (CancelPopUpController) loadWrapper.getController();
        Stage newStage = new Stage();
        newStage.setScene(new Scene((Parent) loadWrapper.getLoadedObject()));
        newStage.setTitle(BundleManager.getBundle().getString("app.name"));
        newStage.getIcons().add(new Image(LoginController.class.getResourceAsStream("/image/ticketlineLogo.png")));
        borderPane.getChildren().clear();
        this.borderPane.setCenter(new ScrollPane());


        if (MainController.currentLocale.equals(Locale.ENGLISH)){
            cancelPopUpController.setReserved("Reserved seats");
            cancelPopUpController.setInCart("In cart");
            cancelPopUpController.setSold("Sold seats");
        }
        else{
            cancelPopUpController.setReserved("Reservierte Plätze");
            cancelPopUpController.setInCart("In den Einkaufswagen");
            cancelPopUpController.setSold("Verkaufte Plätze");
        }


        cancelPopUpController.setStage(newStage);
        cancelPopUpController.setCancelController(this);
        cancelPopUpController.setScrollPane(this.scrollPane);
        newStage.show();
    }

    public void setScrollPane(ScrollPane scrollPane) {
        this.scrollPane=scrollPane;
        this.borderPane.setCenter(scrollPane);
    }

}
