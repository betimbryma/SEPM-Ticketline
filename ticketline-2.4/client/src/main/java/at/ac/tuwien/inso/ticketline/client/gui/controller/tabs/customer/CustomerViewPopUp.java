package at.ac.tuwien.inso.ticketline.client.gui.controller.tabs.customer;

import at.ac.tuwien.inso.ticketline.client.gui.controller.tabs.search.views.TicketCircle;
import at.ac.tuwien.inso.ticketline.client.service.CustomerService;
import at.ac.tuwien.inso.ticketline.client.service.ReservationService;
import at.ac.tuwien.inso.ticketline.dto.CustomerDto;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

/**
 * Created by Betim on 6/19/2016.
 */
@Component
public class CustomerViewPopUp {

    @FXML
    private BorderPane borderPane;
    @FXML
    private Label sold;
    private Sequence sequence;
    private ArrayList<String> sekuencat;
    private ArrayList<String> numberSequence;
    @FXML
    private Label inCart;
    private CustomerDto customerDto;
    private ArrayList<TicketCircle> selected;
    private Stage stage;
    private ScrollPane scrollPane;

    @FXML
    private Label reserved;


    @Autowired
    ReservationService reservationService;
    @Autowired
    CustomerService customerService;
    //@Autowired
    CustomerViewController customerViewController;


    public void setScrollPane(ScrollPane scrollPane) {
        this.borderPane.setCenter(scrollPane);
        this.scrollPane = scrollPane;
    }

    public Sequence getSequence() {
        return sequence;
    }

    public void setSequence(Sequence sequence) {
        this.sequence = sequence;
    }

    public ArrayList<String> getSekuencat() {
        return sekuencat;
    }

    public void setSekuencat(ArrayList<String> sekuencat) {
        this.sekuencat = sekuencat;
    }

    public ArrayList<String> getNumberSequence() {
        return numberSequence;
    }

    public void setNumberSequence(ArrayList<String> numberSequence) {
        this.numberSequence = numberSequence;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
        stage.setOnCloseRequest(e -> customerViewController.setScrollPane(scrollPane));
    }

    public void setCustomerViewController(CustomerViewController customerViewController) {
        this.customerViewController = customerViewController;
    }

    @FXML
    public void onCancel(ActionEvent event) {
        if (customerViewController != null) {
            customerViewController.onCancelPressed(event);
        }
    }

    @FXML
    private void onSell() {
        if (customerViewController != null) {
            customerViewController.onSellPressed();
        }
    }

    public void setSold(String sold) {
        this.sold.setText(sold);
    }

    public void setInCart(String inCart) {
        this.inCart.setText(inCart);
    }

    public void setReserved(String reserved) {
        this.reserved.setText(reserved);
    }
}
