package at.ac.tuwien.inso.ticketline.client.service.rest;

import at.ac.tuwien.inso.ticketline.client.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.client.service.LocationService;
import at.ac.tuwien.inso.ticketline.dto.LocationDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.List;

@Component
public class LocationRestClient implements LocationService {

    public static final String GET_ALL_LOCATIONS = "/service/location/";
    private static final Logger LOGGER = LoggerFactory.getLogger(LocationRestClient.class);

    @Autowired
    private RestClient restClient;

    /**
     * {@inheritDoc}
     */
    @Override
    public List<LocationDto> getAll() throws ServiceException {
        RestTemplate restTemplate = this.restClient.getRestTemplate();
        String url = this.restClient.createServiceUrl(GET_ALL_LOCATIONS);
        HttpEntity<String> entity = new HttpEntity<>(this.restClient.getHttpHeaders());
        LOGGER.info("Retrieving locations from {}", url);
        List<LocationDto> locations;

        try {
            ParameterizedTypeReference<List<LocationDto>> ref = new ParameterizedTypeReference<List<LocationDto>>() {
            };
            ResponseEntity<List<LocationDto>> response = restTemplate.exchange(URI.create(url), HttpMethod.GET, entity, ref);
            locations = response.getBody();

        } catch (RestClientException e) {
            throw new ServiceException("Could not retrieve locations: " + e.getMessage(), e);
        }

        return locations;
    }
}
