package at.ac.tuwien.inso.ticketline.client.service;

import at.ac.tuwien.inso.ticketline.client.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.dto.*;
import javafx.collections.ObservableList;


public interface ReceiptService {


    void emptyCart()  throws ServiceException ;
    void emptyCartWhenLogOut()  throws ServiceException ;
    void emptyCartWhenSuccesfulSold();
    void isCartEmpty();
    ObservableList<MerchandiseDto> getTheArticleCart() ;
    ObservableList<TicketDto> getTicketCart();
    void addToArticleCart(MerchandiseDto merchandiseDto) ;
    void removeArticleFromCart(MerchandiseDto merchandiseDto) throws ServiceException;
    void setNewCart();
    void receiptSucces();
    void setUserStatusDto(UserStatusDto userStatusDto);
    ReceiptDto createCashReceipt(CashDto cashDto,boolean premium) throws ServiceException;
    ReceiptDto createBankReceipt(BankAccountDto bankAccountDto) throws ServiceException;
    ReceiptDto createCreditReceipt(CreditCardDto creditCardDto) throws ServiceException;
    boolean addFreeTicketToCart(TicketDto ticketDto) throws ServiceException;
    boolean addReservedTicketToCart(TicketDto ticketDto) throws ServiceException;
    boolean canTheTicketBeReserved(TicketDto ticketDto) throws ServiceException;
}
