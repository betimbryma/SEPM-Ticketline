package at.ac.tuwien.inso.ticketline.client.gui.controller.tabs.search.artists;

import at.ac.tuwien.inso.ticketline.client.gui.controller.MainController;
import at.ac.tuwien.inso.ticketline.client.gui.controller.tabs.search.SearchController;
import at.ac.tuwien.inso.ticketline.client.gui.controller.tabs.search.views.ArtistViewController;
import at.ac.tuwien.inso.ticketline.client.service.ArtistService;
import at.ac.tuwien.inso.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.ticketline.dto.ArtistDto;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

@Component
public class ArtistsController implements Initializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(ArtistsController.class);

    @Autowired
    private MainController mainController;
    @Autowired
    private ArtistViewController artistViewController;
    @Autowired
    private SearchController searchController;
    @Autowired
    private ArtistService artistService;
    @FXML
    private TableView<ArtistDto> artistTable;
    @FXML
    private TableColumn<ArtistDto, String> firstNameColumn, lastNameColumn, descriptionColumn;
    @FXML
    private TextField firstNameText, lastNameText;
    @FXML
    private Label fnameLable, lnameLable;
    @FXML
    private Button reset;

    /**
     * This method initializes the ArtistsController. It creates ChangeListeners for both TextFields and fills the table.
     *
     * @param location  location of the fxml file
     * @param resources resources
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //initTable();
        //initSearchFields();
        reinitLocale(MainController.currentLocale);
    }

    /**
     * Initializes the controller.
     */
    public void init(){
        initTable();
        initSearchFields();
    }

    /**
     * Reinitializes the localization.
     * @param newValue the new localization value
     */
    public void reinitLocale(Locale newValue) {
        LocaleContextHolder.setLocale(newValue);
        BundleManager.changeLocale(newValue);
        firstNameColumn.setText(BundleManager.getBundle().getString("searchartists.fn"));
        lastNameColumn.setText(BundleManager.getBundle().getString("searchartists.ln"));
        descriptionColumn.setText(BundleManager.getBundle().getString("searchartists.description"));
        reset.setText(BundleManager.getBundle().getString("searchartists.resetB"));
        fnameLable.setText(BundleManager.getBundle().getString("searchartists.fnL"));
        lnameLable.setText(BundleManager.getBundle().getString("searchartists.lnL"));
        artistViewController.reinitLocale(newValue);
    }

    /**
     * Initializes the table.
     */
    public void initTable(){
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        descriptionColumn.setCellValueFactory(new PropertyValueFactory<>("description"));
        artistTable.setRowFactory(tv -> {
            TableRow<ArtistDto> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if ((!row.isEmpty())) {
                    artistViewController.reset();
                    artistViewController.setArtist(row.getItem());
                    searchController.getStackPane().getChildren().clear();
                    searchController.getStackPane().getChildren().add(searchController.getArtistViewPane());
                }
            });
            return row;
        });
        artistTable.setItems(searchController.getAllArtists());
    }

    /**
     * Initializes the search fields.
     */
    public void initSearchFields(){
        ChangeListener<String> listener = new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                search();
            }
        };
        firstNameText.textProperty().addListener(listener);
        lastNameText.textProperty().addListener(listener);
    }

    /**
     * Performs a search with the input.
     */
    public void search() {
        List<ArtistDto> artists = new ArrayList<>();
        for (ArtistDto a : searchController.getAllArtists()) {


            if (checkFirstName(a) && checkLastName(a)) {
                artists.add(a);
            }
        }
        ObservableList<ArtistDto> observableList = FXCollections.observableList(artists);
        artistTable.setItems(observableList);
    }

    /**
     * Checks if first name fits.
     * @param a Artist
     * @return boolean
     */
    public boolean checkFirstName(ArtistDto a){
        if(a.getFirstName().toLowerCase().contains(firstNameText.getText().toLowerCase())){
            return true;
        }
        return false;
    }

    /**
     * Checks if last name fits.
     * @param a Artist
     * @return boolean
     */
    public boolean checkLastName(ArtistDto a){
        if(a.getLastName().toLowerCase().contains(lastNameText.getText().toLowerCase())){
            return true;
        }
        return false;
    }

    /**
     * Handle the reset button.
     * @param event button event
     */
    @FXML
    public void handleReset(ActionEvent event){
        reset();
        search();
    }

    /**
     * Resets the controller.
     */
    public void reset() {
        firstNameText.setText("");
        lastNameText.setText("");
    }
}
