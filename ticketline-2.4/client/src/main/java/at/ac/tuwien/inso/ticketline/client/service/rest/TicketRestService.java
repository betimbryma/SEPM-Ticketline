package at.ac.tuwien.inso.ticketline.client.service.rest;

import at.ac.tuwien.inso.ticketline.client.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.client.exception.ValidationException;
import at.ac.tuwien.inso.ticketline.client.service.TicketService;
import at.ac.tuwien.inso.ticketline.dto.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.Collections;
import java.util.List;

@Component
public class TicketRestService implements TicketService {

    @Autowired
    RestClient restClient;

    private UserStatusDto userStatusDto;

    private final static String GET_TICKETS_BY_SHOW = "/service/tickets/getByShow";
    private final static String GET_TICKETIDENTIFIERS_FOR_TICKET = "/service/tickets/TicketIdentifiers";
    private final static String GET_ALL_TICKETIDENTIFIERS = "/service/tickets/getAll";
    private final static String GET_ALL_TICKETS = "/service/tickets/getAllTickets";
    private final static String RESERVE_TCIKETS = "/service/tickets/reserve";
    private final static String GET_SOLD_BY_SHOW = "/service/tickets/getSoldByShow";
    private final static String GET_MIN_PRICES = "/service/tickets/getMinPrices";
    private final static String GET_MAX_PRICES = "/service/tickets/getMaxPrices";
    private final static String CHANGE_RESERVATION = "/service/tickets/changereserve";
    private final static String GET_TICKETS_FOR_CUSTOMER = "/service/tickets/customerTickets";
    private final static String checkFreeTicketForSell = "/service/conflict/checkFreeTicketForSell";
    private final static String checkFreeTicketForReserve = "/service/conflict/checkFreeTicketForReserve";
    private final static String checkReserveTicketForSell = "/service/conflict/checkReserveTicketForSell";
    private final static String RELEASE_TICKET = "/service/conflict/releaseTicket";
    private static final Logger LOGGER = LoggerFactory.getLogger(TicketRestService.class);

    @Override
    public List<MapEntryDto<TicketDto, TicketIdentifierDto>> getTicketsForShow(Integer showDto) throws ServiceException {
        RestTemplate restTemplate = this.restClient.getRestTemplate();
        String url = this.restClient.createServiceUrl(GET_TICKETS_BY_SHOW);
        HttpHeaders headers = this.restClient.getHttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        HttpEntity<Integer> entity = new HttpEntity<>(showDto, headers);
        LOGGER.info("Getting tickets from {}", url);
        List<MapEntryDto<TicketDto, TicketIdentifierDto>> ticketDtoList;

        try {
            ParameterizedTypeReference<List<MapEntryDto<TicketDto, TicketIdentifierDto>>> parameterizedTypeReference = new ParameterizedTypeReference<List<MapEntryDto<TicketDto, TicketIdentifierDto>>>() {
            };
            ResponseEntity<List<MapEntryDto<TicketDto, TicketIdentifierDto>>> responseEntity = restTemplate.exchange(url, HttpMethod.POST, entity, parameterizedTypeReference);

            ticketDtoList = responseEntity.getBody();
        } catch (RestClientException se) {
            throw new ServiceException("Could not get the tickets " + se.getMessage());
        }

        return ticketDtoList;
    }

    @Override
    public void setUserStatusDto(UserStatusDto userStatusDto) {
        this.userStatusDto = userStatusDto;
    }

    @Override
    public List<TicketIdentifierDto> getTicketIdentifiers() throws ServiceException {
        RestTemplate restTemplate = this.restClient.getRestTemplate();
        String url = this.restClient.createServiceUrl(GET_ALL_TICKETIDENTIFIERS);
        HttpEntity<String> entity = new HttpEntity<>(this.restClient.getHttpHeaders());
        LOGGER.info("Retrieving ticketIdentifiers from {}", url);
        List<TicketIdentifierDto> ticketIdentifierDtos;
        try {
            ParameterizedTypeReference<List<TicketIdentifierDto>> ref = new ParameterizedTypeReference<List<TicketIdentifierDto>>() {
            };
            ResponseEntity<List<TicketIdentifierDto>> response = restTemplate.exchange(URI.create(url), HttpMethod.GET, entity, ref);
            ticketIdentifierDtos = response.getBody();

        } catch (RestClientException e) {
            throw new ServiceException("Could not retrieve ticketIdentifiers: " + e.getMessage(), e);
        }
        return ticketIdentifierDtos;
    }

    @Override
    public List<TicketDto> getAllTickets(Integer showDto) throws ServiceException {
        RestTemplate restTemplate = this.restClient.getRestTemplate();
        String url = this.restClient.createServiceUrl(GET_ALL_TICKETS);
        LOGGER.info("get All Tickets {}", url);
        HttpHeaders headers = this.restClient.getHttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        HttpEntity<Integer> entity = new HttpEntity<>(showDto, headers);
        List<TicketDto> ticketDtos;
        try {
            ParameterizedTypeReference<List<TicketDto>> parameterizedTypeReference = new ParameterizedTypeReference<List<TicketDto>>() {
            };
            ResponseEntity<List<TicketDto>> responseEntity = restTemplate.exchange(url, HttpMethod.POST, entity, parameterizedTypeReference);
            ticketDtos = responseEntity.getBody();
        } catch (HttpStatusCodeException e) {
            MessageDto errorMsg = this.restClient.mapExceptionToMessage(e);
            if (errorMsg.hasFieldErrors()) {
                throw new ValidationException(errorMsg.getFieldErrors());
            } else {
                throw new ServiceException(errorMsg.getText());
            }
        } catch (RestClientException e) {
            throw new ServiceException("Could not reserve tickets: " + e.getMessage(), e);
        }

        return ticketDtos;
    }

    @Override
    public List<MapEntryDto<TicketDto, TicketIdentifierDto>> getTicketIdentifiersAndTickets(Integer showDto) throws ServiceException {
        RestTemplate restTemplate = this.restClient.getRestTemplate();
        String url = this.restClient.createServiceUrl(GET_TICKETIDENTIFIERS_FOR_TICKET);
        HttpHeaders headers = this.restClient.getHttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        HttpEntity<Integer> entity = new HttpEntity<>(showDto, headers);
        LOGGER.info("Getting tickets AND ticketidentifiers from {}", url);
        List<MapEntryDto<TicketDto, TicketIdentifierDto>> list;

        try {
            ParameterizedTypeReference<List<MapEntryDto<TicketDto, TicketIdentifierDto>>> parameterizedTypeReference = new ParameterizedTypeReference<List<MapEntryDto<TicketDto, TicketIdentifierDto>>>() {
            };
            ResponseEntity<List<MapEntryDto<TicketDto, TicketIdentifierDto>>> responseEntity = restTemplate.exchange(url, HttpMethod.POST, entity, parameterizedTypeReference);

            list = responseEntity.getBody();
        } catch (RestClientException se) {
            throw new ServiceException("Could not get the tickets " + se.getMessage());
        }

        return list;
    }

    @Override
    public List<TicketIdentifierDto> reserveTickets(List<TicketIdentifierDto> ticketDtoList) throws ServiceException {
        RestTemplate restTemplate = this.restClient.getRestTemplate();
        String url = this.restClient.createServiceUrl(RESERVE_TCIKETS);
        LOGGER.info("Reserve tickets {}", url);
        HttpHeaders headers = this.restClient.getHttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        HttpEntity<List<TicketIdentifierDto>> entity = new HttpEntity<>(ticketDtoList, headers);
        List<TicketIdentifierDto> ticketIdentifierDtos;
        try {
            ParameterizedTypeReference<List<TicketIdentifierDto>> parameterizedTypeReference = new ParameterizedTypeReference<List<TicketIdentifierDto>>() {
            };
            ResponseEntity<List<TicketIdentifierDto>> responseEntity = restTemplate.exchange(url, HttpMethod.POST, entity, parameterizedTypeReference);
            ticketIdentifierDtos = responseEntity.getBody();
        } catch (HttpStatusCodeException e) {
            MessageDto errorMsg = this.restClient.mapExceptionToMessage(e);
            if (errorMsg.hasFieldErrors()) {
                throw new ValidationException(errorMsg.getFieldErrors());
            } else {
                throw new ServiceException(errorMsg.getText());
            }
        } catch (RestClientException e) {
            throw new ServiceException("Could not reserve tickets: " + e.getMessage(), e);
        }

        return ticketIdentifierDtos;
    }

    @Override
    public Integer getSoldByShow(Integer show) throws ServiceException {
        RestTemplate restTemplate = this.restClient.getRestTemplate();
        String url = this.restClient.createServiceUrl(GET_SOLD_BY_SHOW);
        HttpHeaders headers = this.restClient.getHttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        HttpEntity<Integer> entity = new HttpEntity<>(show, headers);
        LOGGER.info("Getting number of sold tickets from {}", url);
        Integer sold;

        try {
            ParameterizedTypeReference<Integer> parameterizedTypeReference = new ParameterizedTypeReference<Integer>() {
            };
            ResponseEntity<Integer> responseEntity = restTemplate.exchange(url, HttpMethod.POST, entity, parameterizedTypeReference);

            sold = responseEntity.getBody();
        } catch (RestClientException se) {
            throw new ServiceException("Could not get number of sold tickets " + se.getMessage());
        }

        return sold;
    }

    @Override
    public List<MapEntryDto<ShowDto, Integer>> getMinPrices() throws ServiceException {
        RestTemplate restTemplate = this.restClient.getRestTemplate();
        String url = this.restClient.createServiceUrl(GET_MIN_PRICES);
        HttpHeaders headers = this.restClient.getHttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        HttpEntity<Integer> entity = new HttpEntity<>(headers);
        LOGGER.info("Getting minimum prices from {}", url);
        List<MapEntryDto<ShowDto, Integer>> prices;

        try {
            ParameterizedTypeReference<List<MapEntryDto<ShowDto, Integer>>> parameterizedTypeReference = new ParameterizedTypeReference<List<MapEntryDto<ShowDto, Integer>>>() {
            };
            ResponseEntity<List<MapEntryDto<ShowDto, Integer>>> responseEntity = restTemplate.exchange(url, HttpMethod.POST, entity, parameterizedTypeReference);

            prices = responseEntity.getBody();
        } catch (RestClientException se) {
            throw new ServiceException("Could not get max prices " + se.getMessage());
        }

        return prices;
    }

    @Override
    public List<MapEntryDto<ShowDto, Integer>> getMaxPrices() throws ServiceException {
        RestTemplate restTemplate = this.restClient.getRestTemplate();
        String url = this.restClient.createServiceUrl(GET_MAX_PRICES);
        HttpHeaders headers = this.restClient.getHttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        HttpEntity<Integer> entity = new HttpEntity<>(headers);
        LOGGER.info("Getting maximum prices from {}", url);
        List<MapEntryDto<ShowDto, Integer>> prices;

        try {
            ParameterizedTypeReference<List<MapEntryDto<ShowDto, Integer>>> parameterizedTypeReference = new ParameterizedTypeReference<List<MapEntryDto<ShowDto, Integer>>>() {
            };
            ResponseEntity<List<MapEntryDto<ShowDto, Integer>>> responseEntity = restTemplate.exchange(url, HttpMethod.POST, entity, parameterizedTypeReference);

            prices = responseEntity.getBody();
        } catch (RestClientException se) {
            throw new ServiceException("Could not get min prices " + se.getMessage());
        }

        return prices;
    }

    @Override
    public void changeReservation(TicketIdentifierDto ticketIdentifierDto) throws ServiceException {
        String url = this.restClient.createServiceUrl(CHANGE_RESERVATION);
        LOGGER.info("Changing Reservation at {}", url);

        RestTemplate restTemplate = this.restClient.getRestTemplate();
        HttpHeaders httpHeaders = restClient.getHttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        httpHeaders.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        HttpEntity<TicketIdentifierDto> entity = new HttpEntity<>(ticketIdentifierDto, httpHeaders);

        MessageDto messageDto;

        try {
            messageDto = restTemplate.postForObject(url, entity, MessageDto.class);
        } catch (HttpStatusCodeException e) {
            MessageDto errorMsg = this.restClient.mapExceptionToMessage(e);
            if (errorMsg.hasFieldErrors()) {
                throw new ValidationException(errorMsg.getFieldErrors());
            } else {
                throw new ServiceException(errorMsg.getText());
            }
        } catch (RestClientException e) {
            throw new ServiceException("Could not change the reservation: " + e.getMessage(), e);
        }

        Integer ID;
        try {
            ID = Integer.valueOf(messageDto.getText());
        } catch (NumberFormatException num) {
            throw new ServiceException("Invalid ID:" + messageDto.getText());
        }

    }

    @Override
    public List<TicketIdentifierDto> getTicketsForCustomer(Integer show, Integer customer) throws ServiceException {
        MapEntryDto<Integer, Integer> mapEntryDto = new MapEntryDto<>(show, customer);
        RestTemplate restTemplate = this.restClient.getRestTemplate();
        String url = this.restClient.createServiceUrl(GET_TICKETS_FOR_CUSTOMER);
        HttpHeaders headers = this.restClient.getHttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        HttpEntity<MapEntryDto> entity = new HttpEntity<>(mapEntryDto, headers);
        LOGGER.info("Getting tickets from {}", url);
        List<TicketIdentifierDto> ticketDtoList;

        try {
            ParameterizedTypeReference<List<TicketIdentifierDto>> parameterizedTypeReference = new ParameterizedTypeReference<List<TicketIdentifierDto>>() {
            };
            ResponseEntity<List<TicketIdentifierDto>> responseEntity = restTemplate.exchange(url, HttpMethod.POST, entity, parameterizedTypeReference);

            ticketDtoList = responseEntity.getBody();
        } catch (RestClientException se) {
            throw new ServiceException("Could not get the tickets " + se.getMessage());
        }

        return ticketDtoList;
    }


    @Override
    public Integer checkTicket(TicketDto ticketDto, Integer i) throws ServiceException {
        String url2 = "";
        switch (i) {
            case 1:
                url2 = checkFreeTicketForSell;
                break;
            case 2:
                url2 = checkReserveTicketForSell;
                break;
            case 3:
                url2 = checkFreeTicketForReserve;
                break;
        }

        RestTemplate restTemplate = this.restClient.getRestTemplate();
        String url = this.restClient.createServiceUrl(url2);
        LOGGER.info("Check Ticket at {}", url);
        HttpHeaders headers = this.restClient.getHttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        HttpEntity<TicketDto> entity = new HttpEntity<>(ticketDto, headers);
        MessageDto msg;
        try {
            msg = restTemplate.postForObject(url, entity, MessageDto.class);
        } catch (HttpStatusCodeException e) {
            MessageDto errorMsg = this.restClient.mapExceptionToMessage(e);
            if (errorMsg.hasFieldErrors()) {
                throw new ValidationException(errorMsg.getFieldErrors());
            } else {
                throw new ServiceException(errorMsg.getText());
            }
        } catch (RestClientException e) {
            throw new ServiceException("Could not publish news: " + e.getMessage(), e);
        }
        Integer id;
        try {
            id = Integer.valueOf(msg.getText());
        } catch (NumberFormatException e) {
            throw new ServiceException("Invalid ID: " + msg.getText());
        }
        return id;
    }


    @Override
    public void releaseTicket(List<TicketDto> ticketDtoList) throws ServiceException {
        RestTemplate restTemplate = this.restClient.getRestTemplate();
        String url = this.restClient.createServiceUrl(RELEASE_TICKET);
        LOGGER.info("Release tickets {}", url);
        HttpHeaders headers = this.restClient.getHttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        HttpEntity<List<TicketDto>> entity = new HttpEntity<>(ticketDtoList, headers);
        List<TicketDto> ticketIdentifierDtos;
        try {
            ParameterizedTypeReference<List<TicketDto>> parameterizedTypeReference = new ParameterizedTypeReference<List<TicketDto>>() {
            };
            ResponseEntity<List<TicketDto>> responseEntity = restTemplate.exchange(url, HttpMethod.POST, entity, parameterizedTypeReference);
            ticketIdentifierDtos = responseEntity.getBody();
        } catch (HttpStatusCodeException e) {
            MessageDto errorMsg = this.restClient.mapExceptionToMessage(e);
            if (errorMsg.hasFieldErrors()) {
                throw new ValidationException(errorMsg.getFieldErrors());
            } else {
                throw new ServiceException(errorMsg.getText());
            }
        } catch (RestClientException e) {
            throw new ServiceException("Could not release tickets: " + e.getMessage(), e);
        }
    }

    @Override
    public UserStatusDto getEmployee() {
        return this.userStatusDto;
    }

}
