package at.ac.tuwien.inso.ticketline.client.gui.controller.tabs.customer;

import at.ac.tuwien.inso.ticketline.client.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.client.exception.ValidationException;
import at.ac.tuwien.inso.ticketline.client.gui.JavaFXUtils;
import at.ac.tuwien.inso.ticketline.client.gui.controller.MainController;
import at.ac.tuwien.inso.ticketline.client.service.CustomerService;
import at.ac.tuwien.inso.ticketline.client.util.*;
import at.ac.tuwien.inso.ticketline.dto.CustomerDto;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import org.controlsfx.validation.ValidationResult;
import org.controlsfx.validation.ValidationSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Locale;

@Component
public class EditCustomerController {

    private static final Logger LOGGER = LoggerFactory.getLogger(EditCustomerController.class);

    @FXML
    private Label fname, lname, birth, mail, phone, str, cty, pc, ctry, gender;
    @FXML
    private TextField firstname, lastname, email, phonenumber, street, city, zipcode, country;

    @FXML
    private DatePicker birthday;
    @FXML
    private Button back, save;
    @FXML
    private Label mainLable, pageLable, editLable;
    @FXML
    private RadioButton male, female;
    private TableView<CustomerDto> customers;

    private CustomerDto customerDto;
    private StackPane stackPane;
    private AnchorPane customersAnchorPane;
    @Autowired
    private CustomersController customersController;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private SpringFxmlLoader springFxmlLoader;

    public void setCustomerDto(CustomerDto customerDto, TableView<CustomerDto> tableView) {
        this.customers = tableView;
        this.customerDto = customerDto;
        if (customerDto.getFirstname() != null && !customerDto.getFirstname().trim().isEmpty())
            this.firstname.setText(customerDto.getFirstname());

        if (customerDto.getLastname() != null && !customerDto.getLastname().trim().isEmpty())
            this.lastname.setText(customerDto.getLastname());

        if (customerDto.getEmail() != null && !customerDto.getEmail().trim().isEmpty())
            this.email.setText(customerDto.getEmail());

        if (customerDto.getPhoneNumber() != null && !customerDto.getPhoneNumber().trim().isEmpty())
            this.phonenumber.setText(customerDto.getPhoneNumber());

        if (customerDto.getStreet() != null && !customerDto.getStreet().trim().isEmpty())
            this.street.setText(customerDto.getStreet());

        if (customerDto.getCity() != null && !customerDto.getCity().trim().isEmpty())
            this.city.setText(customerDto.getCity());

        if (customerDto.getPostalcode() != null && !customerDto.getPostalcode().trim().isEmpty())
            this.zipcode.setText(customerDto.getPostalcode());

        if (customerDto.getCountry() != null && !customerDto.getCountry().trim().isEmpty())
            this.country.setText(customerDto.getCountry());

        if (customerDto.getDateOfBirth() != null)
            this.birthday.setValue(customerDto.getDateOfBirth().toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
        if (customerDto.getGender() != null) {
            if (customerDto.getGender().equals("male"))
                male.setSelected(true);
            else
                female.setSelected(true);
        }

        new ValidationSupport().registerValidator(firstname, true, (Control c, String s) ->
                ValidationResult.fromErrorIf(c, BundleManager.getBundle().getString("edcustomer.fnValError"), !UserInputValidator.validatePersonName(s, false)));

        new ValidationSupport().registerValidator(lastname, true, (Control c, String s) ->
                ValidationResult.fromErrorIf(c, BundleManager.getBundle().getString("edcustomer.lnValError"), !UserInputValidator.validatePersonName(s, false)));

        new ValidationSupport().registerValidator(email, false, (Control c, String s) ->
                ValidationResult.fromErrorIf(c, BundleManager.getBundle().getString("edcustomer.emailValError"), !UserInputValidator.validateEmail(s, true)));

        new ValidationSupport().registerValidator(phonenumber, false, (Control c, String s) ->
                ValidationResult.fromErrorIf(c, BundleManager.getBundle().getString("edcustomer.phoneValError"), !UserInputValidator.validatePhoneNumber(s, true)));

        new ValidationSupport().registerValidator(street, true, (Control c, String s) ->
                ValidationResult.fromErrorIf(c, BundleManager.getBundle().getString("edcustomer.streetValError"), !UserInputValidator.validateNonEmptyString(s)));

        new ValidationSupport().registerValidator(city, true, (Control c, String s) ->
                ValidationResult.fromErrorIf(c, BundleManager.getBundle().getString("edcustomer.cityValError"), !UserInputValidator.validateNoNumbersString(s, false)));

        new ValidationSupport().registerValidator(zipcode, true, (Control c, String s) ->
                ValidationResult.fromErrorIf(c, BundleManager.getBundle().getString("edcustomer.postalCodeValError"), !UserInputValidator.validateNonEmptyString(s)));

        new ValidationSupport().registerValidator(country, true, (Control c, String s) ->
                ValidationResult.fromErrorIf(c, BundleManager.getBundle().getString("edcustomer.countryValError"), !UserInputValidator.validateNoNumbersString(s, false)));

        new ValidationSupport().registerValidator(birthday, false, (Control c, LocalDate d) ->
                ValidationResult.fromErrorIf(c, BundleManager.getBundle().getString("edcustomer.bdValError"), !UserInputValidator.validateBirthday(d, true)));

        JavaFXUtils.setChangeListenerToDatePicker(birthday);

        birthday.setConverter(DateUtil.getEuropeanDateStringConverter());

    }

    @FXML
    private void onSaveButtonClicked(ActionEvent actionEvent) {
        LOGGER.info("User clicked on the save button");
        try {
            CustomerDto editedCustomer = CustomerDataValidator.validateAndCreate(firstname.getText().trim(), lastname.getText().trim(),
                    street.getText().trim(), zipcode.getText().trim(), city.getText().trim(), country.getText().trim(), customerDto.getGender(),
                    birthday.getValue(), email.getText().trim(), phonenumber.getText().trim());
            if (male.isSelected())
                editedCustomer.setGender("male");
            else if (female.isSelected())
                editedCustomer.setGender("female");

            editedCustomer.setId(customerDto.getId());
            if (!customerDto.equals(editedCustomer)) {
                customerService.editCustomer(editedCustomer);

                if (MainController.currentLocale.equals(Locale.ENGLISH)) {
                    DialogUtil.showSuccessDialog("Edit customer", "Customer was successfully edited", "The customer: " + customerDto.getFirstname() + " has been updated");
                } else {
                    DialogUtil.showSuccessDialog("Kunde bearbeiten", "Die Kundendaten wurden erfolgreich aktualisiert",
                            "Die Kundendaten des Kunden: " + customerDto.getFirstname() + " wurden erfolgreich aktualisert");
                }


                if (customersController != null) {
                    customersController.refreshCustomerList();
                }
                onBackButtonPressed();

            } else {
                if (MainController.currentLocale.equals(Locale.ENGLISH))
                    DialogUtil.showInformationDialog("Edit customer", "No changes were made", "The customer's data was not edited");
                else
                    DialogUtil.showSuccessDialog("Kunde bearbeiten", "Es wurden keine Änderungen vorgenommen",
                            "Die Kundendaten des Kunden: " + customerDto.getFirstname() + " wurden nicht geändert");
                onBackButtonPressed();
            }

        } catch (ValidationException e) {
            LOGGER.error("Customer data is not valid");
            return;
        } catch (ServiceException e) {
            LOGGER.error("Customer could not be edited!");
            DialogUtil.showErrorDialog("Customer edit", "There were problems editing this customer", e.getMessage());
            return;
        }
    }

    @FXML
    private void onBackButtonPressed() {
        LOGGER.info("User clikced the back button, going back to the main-customers page");
        stackPane.getChildren().clear();
        stackPane.getChildren().add(customersAnchorPane);
        customersController.returned();

    }

    public void setStackPane(StackPane stackPane) {
        this.stackPane = stackPane;
    }

    public void setCustomersAnchorPane(AnchorPane customersAnchorPane) {
        this.customersAnchorPane = customersAnchorPane;
    }

    public void setCustomersController(CustomersController customersController) {
        this.customersController = customersController;
    }

    public void reinitLocale(Locale newValue) {
        LocaleContextHolder.setLocale(newValue);
        BundleManager.changeLocale(newValue);

        back.setText(BundleManager.getBundle().getString("edcustomer.backB"));
        save.setText(BundleManager.getBundle().getString("edcustomer.saveB"));

        mainLable.setText(BundleManager.getBundle().getString("edcustomer.mainLable"));
        pageLable.setText(BundleManager.getBundle().getString("edcustomer.pageLable"));
        editLable.setText(BundleManager.getBundle().getString("edcustomer.editCustomerLable"));

        fname.setText(BundleManager.getBundle().getString("edcustomer.fnField"));
        lname.setText(BundleManager.getBundle().getString("edcustomer.lnField"));
        birth.setText(BundleManager.getBundle().getString("edcustomer.birthField"));
        mail.setText(BundleManager.getBundle().getString("edcustomer.emailField"));
        phone.setText(BundleManager.getBundle().getString("edcustomer.phoneField"));
        str.setText(BundleManager.getBundle().getString("edcustomer.streetField"));
        cty.setText(BundleManager.getBundle().getString("edcustomer.cityField"));
        pc.setText(BundleManager.getBundle().getString("edcustomer.zipField"));
        ctry.setText(BundleManager.getBundle().getString("edcustomer.countryField"));
        gender.setText(BundleManager.getBundle().getString("edcustomer.gender"));
        male.setText(BundleManager.getBundle().getString("edcustomer.male"));
        female.setText(BundleManager.getBundle().getString("edcustomer.female"));
    }
}