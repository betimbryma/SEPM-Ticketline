package at.ac.tuwien.inso.ticketline.client.gui.controller.tabs;

import at.ac.tuwien.inso.ticketline.client.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.client.gui.JavaFXUtils;
import at.ac.tuwien.inso.ticketline.client.gui.controller.MainController;
import at.ac.tuwien.inso.ticketline.client.gui.controller.shoppingCart.ShoppingCartController;
import at.ac.tuwien.inso.ticketline.client.service.MerchandiseService;
import at.ac.tuwien.inso.ticketline.client.service.PerformanceService;
import at.ac.tuwien.inso.ticketline.client.service.ReceiptService;
import at.ac.tuwien.inso.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.ticketline.client.util.DialogUtil;
import at.ac.tuwien.inso.ticketline.dto.MerchandiseDto;
import at.ac.tuwien.inso.ticketline.dto.PerformanceDto;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.net.URL;
import java.util.*;

/**
 * @author Michael Oppitz
 * @date 03.05.2016
 */
@Component
public class MerchandiseController implements Initializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(MerchandiseController.class);
    @Autowired
    public MerchandiseService merchandiseService;
    @Autowired
    public ReceiptService receiptService;
    int theNumber = 0;
    @Autowired
    private MainController mainController;
    @Autowired
    private ShoppingCartController shoppingCartController;
    @Autowired
    private PerformanceService performanceService;
    private ObservableList<PerformanceDto> performanceDtos;
    private HashMap<Integer, PerformanceDto> performancesWithArticles;
    private PerformanceDto currentPerformance;
    @FXML
    private Label title, SperformanceL, SarticleL, ScartL, removeL;
    @FXML
    private TableView<PerformanceDto> tablePerformanceDto;
    @FXML
    private TableColumn<PerformanceDto, Integer> tablePerformanceDtoId;
    @FXML
    private TableColumn<PerformanceDto, String> tablePerformanceDtoName;
    @FXML
    private TableColumn<PerformanceDto, Integer> tablePerformanceDtoDuration;
    @FXML
    private TextField searchPerformanceByIDText;

    @FXML
    private TableView<MerchandiseDto> tableMerchandiseDto;
    @FXML
    private TableColumn<MerchandiseDto, Integer> tableMerchandiseDtoId;
    @FXML
    private TableColumn<MerchandiseDto, String> tableMerchandiseDtoName;
    @FXML
    private TableColumn<MerchandiseDto, Integer> tableMerchandiseDtoPrice;
    @FXML
    private TableColumn<MerchandiseDto, Integer> tableMerchandiseDtoAvaiable;

    @FXML
    private TextField searchArticleByIDText;
    @FXML
    private TableView<MerchandiseDto> tableCart;
    @FXML
    private TableColumn<MerchandiseDto, Integer> tableCartId;
    @FXML
    private TableColumn<MerchandiseDto, String> tableCartName;
    @FXML
    private TableColumn<MerchandiseDto, Integer> tableCartPrice;
    @FXML
    private TextField searchArticleInCartByIDText;

    @FXML
    public void initialize(URL location, ResourceBundle resources) {
        initialize();
    }

    public void initialize() {
        try {
            performanceDtos = FXCollections.observableArrayList(performanceService.getAll());
            performancesWithArticles = null;
            receiptService.setNewCart();
            theNumber = 0;
            LOGGER.debug("" + receiptService.getTheArticleCart());
            fillTableCart(receiptService.getTheArticleCart());
            if (currentPerformance != null) {
                performancesWithArticles = new HashMap<>();

                for (int i = 0; i < performanceDtos.size(); i++) {
                    if (performanceDtos.get(i).getId() == currentPerformance.getId()) {
                        currentPerformance = performanceDtos.get(i);
                        break;
                    }
                }
                currentPerformance = merchandiseService.getArticlesOfPerformance(currentPerformance);
                performancesWithArticles.put(currentPerformance.getId(), currentPerformance);
                fillTableMerchandiseDto(FXCollections.observableArrayList(currentPerformance.getArticles()));
            }
            fillTablePerformanceDto(performanceDtos);
            reinitLocale(MainController.currentLocale);

            tablePerformanceDto.setRowFactory(param -> JavaFXUtils.getTableRowWithTooltip(e -> e.getDescription()));
            tableMerchandiseDto.setRowFactory(param -> JavaFXUtils.getTableRowWithTooltip(e -> e.getDescription()));
            tableCart.setRowFactory(param -> JavaFXUtils.getTableRowWithTooltip(e -> e.getDescription()));

        } catch (ServiceException e) {

            DialogUtil.showExceptionDialog("Can not send initialize", "", e);
        }
    }

    public void reinitLocale(Locale newValue) {
        LocaleContextHolder.setLocale(newValue);
        BundleManager.changeLocale(newValue);

        title.setText(BundleManager.getBundle().getString("merchandise.titleL"));
        SperformanceL.setText(BundleManager.getBundle().getString("merchandise.performanceL"));
        SarticleL.setText(BundleManager.getBundle().getString("merchandise.articleL"));
        ScartL.setText(BundleManager.getBundle().getString("merchandise.cartL"));
        removeL.setText(BundleManager.getBundle().getString("merchandise.removeL"));


        tablePerformanceDtoId.setText(BundleManager.getBundle().getString("merchandise.performanceID"));
        tablePerformanceDtoName.setText(BundleManager.getBundle().getString("merchandise.performanceName"));
        tablePerformanceDtoDuration.setText(BundleManager.getBundle().getString("merchandise.performanceDuration"));

        tableMerchandiseDtoId.setText(BundleManager.getBundle().getString("merchandise.articleID"));
        tableMerchandiseDtoName.setText(BundleManager.getBundle().getString("merchandise.articleName"));
        tableMerchandiseDtoPrice.setText(BundleManager.getBundle().getString("merchandise.articlePrice"));
        tableMerchandiseDtoAvaiable.setText(BundleManager.getBundle().getString("merchandise.articleAvailable"));

        tableCartId.setText(BundleManager.getBundle().getString("merchandise.cartID"));
        tableCartName.setText(BundleManager.getBundle().getString("merchandise.cartName"));
        tableCartPrice.setText(BundleManager.getBundle().getString("merchandise.cartPrice"));

        searchArticleInCartByIDText.setPromptText(BundleManager.getBundle().getString("merchandise.cartTF"));
        searchPerformanceByIDText.setPromptText(BundleManager.getBundle().getString("merchandise.performanceTF"));
        searchArticleByIDText.setPromptText(BundleManager.getBundle().getString("merchandise.articleTF"));

    }

    private void fillTablePerformanceDto(ObservableList<PerformanceDto> obs) {
        tablePerformanceDtoName.setCellValueFactory(new PropertyValueFactory<>("name"));
        tablePerformanceDtoId.setCellValueFactory(new PropertyValueFactory<>("id"));
        tablePerformanceDtoDuration.setCellValueFactory(new PropertyValueFactory<>("duration"));
        tablePerformanceDtoId.setSortType(TableColumn.SortType.ASCENDING);
        tablePerformanceDto.setItems(obs);
    }


    @FXML
    private void searchPerformanceByID() {
        ObservableList<PerformanceDto> perf = FXCollections.observableArrayList();
        if (searchPerformanceByIDText.getText().equals("")) {
            fillTablePerformanceDto(performanceDtos);
            return;
        } else {
            int i = -666;
            try {
                i = Integer.valueOf(searchPerformanceByIDText.getText());

            } catch (NullPointerException | NumberFormatException e) {
            }
            for (PerformanceDto performanceDto : performanceDtos) {

                try {
                    if (performanceDto.getId().equals(i)) {
                        perf.add(performanceDto);
                    }

                } catch (NullPointerException e) {
                }
                if (performanceDto.getName().toLowerCase().contains(searchPerformanceByIDText.getText().toLowerCase())) {
                    perf.add(performanceDto);
                }
            }
        }
        fillTablePerformanceDto(perf);
    }

    @FXML
    private void setArticlesOfThisDto() {
        /*------*/
        if (theNumber % 2 == 1) {
            theNumber = 0;
            try {
                currentPerformance.getArticles().remove(currentPerformance.getArticles().size() - 1);
            } catch (NullPointerException | IndexOutOfBoundsException e) {
                //nichts machen
            }
        }
        /*+++++*/
        PerformanceDto a = tablePerformanceDto.getSelectionModel().getSelectedItem();
        tablePerformanceDto.getSelectionModel().clearSelection();
        if (a == null) {
            return;
        }

        if (performancesWithArticles == null) {
            performancesWithArticles = new HashMap<>();
        }

        if (!performancesWithArticles.containsKey(a.getId())) {
            ObservableList<PerformanceDto> m = performanceDtos;
            for (PerformanceDto p : m) {
                if (p.getId() == a.getId()) {
                    try {
                        currentPerformance = merchandiseService.getArticlesOfPerformance(p);
                        performancesWithArticles.put(currentPerformance.getId(), currentPerformance);
                    } catch (ServiceException e) {
                        DialogUtil.showExceptionDialog("Can not send initialize", "", e);
                    }
                }
            }
        } else {
            currentPerformance = performancesWithArticles.get(a.getId());
        }

        fillTableMerchandiseDto(FXCollections.observableArrayList(currentPerformance.getArticles()));


    }

    private void fillTableMerchandiseDto(ObservableList<MerchandiseDto> obs) {
        tableMerchandiseDtoId.setCellValueFactory(new PropertyValueFactory<>("id"));
        tableMerchandiseDtoName.setCellValueFactory(new PropertyValueFactory<>("name"));
        tableMerchandiseDtoPrice.setCellValueFactory(new PropertyValueFactory<>("price"));
        tableMerchandiseDtoAvaiable.setCellValueFactory(new PropertyValueFactory<>("available"));
        tableMerchandiseDtoAvaiable.setSortType(TableColumn.SortType.ASCENDING);
        tableMerchandiseDto.setItems(obs);
    }


    @FXML
    private void searchArticleByID() {
        ObservableList<MerchandiseDto> merch = FXCollections.observableArrayList();
        if (currentPerformance == null) {

            DialogUtil.showErrorDialog("Information Dialog", "Please choose a performance!", "Firtly you choose the performance. Than you can search the Articles.");
            return;
        }

        if (searchArticleByIDText.getText().equals("")) {
            setRealAvaiableValues();

            return;
        } else {


            if (theNumber % 2 == 1) {
                theNumber = 0;
                currentPerformance.getArticles().remove(currentPerformance.getArticles().size() - 1);
            }


            int i = -666;
            try {
                i = Integer.valueOf(searchArticleByIDText.getText());

            } catch (NumberFormatException | NullPointerException e) {
                LOGGER.debug("not a number");
            }

            if (performancesWithArticles != null) {
                Iterator it = performancesWithArticles.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry pair = (Map.Entry) it.next();
                    PerformanceDto p = (PerformanceDto) pair.getValue();
                    for (MerchandiseDto m : p.getArticles()) {

                        try {
                            if (m.getId().equals(i)) {
                                merch.add(m);
                            }
                        } catch (NullPointerException e) {
                            LOGGER.debug("not a number");
                        }
                        System.out.println("WTF" + m.getDescription());
                        if (m.getName().toLowerCase().contains(searchArticleByIDText.getText().toLowerCase())) {
                            merch.add(m);
                        }

                    }
                    // it.remove(); // avoids a ConcurrentModificationException
                }
            } else {
                DialogUtil.showErrorDialog("There is no Performance open", "Please open the Performance to search its Articles!", "");
            }

            fillTableMerchandiseDto(merch);

            //  DialogUtil.showErrorDialog("Information Dialog","Please write an existing ID!","Please write an existing ID. If the article your searching is still not Avaible, you may have chosen the wrong Performance or click Refresh.");
        }
    }


    @FXML
    private void setAretikel() {
        MerchandiseDto a = tableMerchandiseDto.getSelectionModel().getSelectedItem();
        tableMerchandiseDto.getSelectionModel().clearSelection();
        if (a == null)
            return;
        if (a.getDescription().equals("Not a product")) {
            return;
        }

        try {
            MerchandiseDto checked = merchandiseService.checkMerchandise(a);
            if (checked.getAvailable() != -1) {
                a.setAvailable(checked.getAvailable());
                receiptService.addToArticleCart(a);
                fillTableCart(receiptService.getTheArticleCart());
                try {
                    shoppingCartController.setTotal();
                } catch (NullPointerException e) {
                }
            } else {
                a.setAvailable(0);
                DialogUtil.showErrorDialog("Error Dialog", "Sorry the Products are not Avaiale!", "It is already sold!");
            }
        } catch (ServiceException e) {
            LOGGER.debug("Error in database");
            DialogUtil.showErrorDialog("Error Dialog", "Error in Server", e.getMessage());
        }

        setRealAvaiableValues();
        receiptService.isCartEmpty();


    }


    public void fillTableCart(ObservableList<MerchandiseDto> obs) {
        tableCartId.setCellValueFactory(new PropertyValueFactory<>("id"));
        tableCartName.setCellValueFactory(new PropertyValueFactory<>("name"));
        tableCartPrice.setCellValueFactory(new PropertyValueFactory<>("price"));
        tableCartId.setSortType(TableColumn.SortType.ASCENDING);
        tableCart.setItems(obs);
    }


    @FXML
    private void searchArticleInCartByID() {
        if (searchArticleInCartByIDText.getText().equals("")) {
            fillTableCart(receiptService.getTheArticleCart());
            return;
        } else {
            int i = -666;
            try {
                i = Integer.valueOf(searchArticleInCartByIDText.getText());

            } catch (NullPointerException | NumberFormatException e) {
                LOGGER.debug("not a number");
            }
            ObservableList<MerchandiseDto> merchandiseDtos = receiptService.getTheArticleCart();
            ObservableList<MerchandiseDto> m = FXCollections.observableArrayList();
            Boolean prod = true;
            for (MerchandiseDto merchandiseDto : merchandiseDtos) {
                if (merchandiseDto.getId() == i || merchandiseDto.getName().toLowerCase().contains(searchArticleInCartByIDText.getText().toLowerCase())) {
                    m.add(merchandiseDto);
                    prod = false;
                }
            }
            if (prod) {
                DialogUtil.showErrorDialog("Information Dialog", "Please write an existing ID!", "The Article with this ID doesn't exist. Wrong Marvin!");
            } else {
                fillTableCart(m);
            }
        }
    }

    @FXML
    private void removeArticleFromCart() {
        MerchandiseDto a = tableCart.getSelectionModel().getSelectedItem();
        tableCart.getSelectionModel().clearSelection();
        if (a == null) {
            return;

        }
        removeArticle(a);
    }

    public void removeArticle(MerchandiseDto a) {
        if (a == null) {
            return;

        }
        for (MerchandiseDto m : receiptService.getTheArticleCart()) {
            if (m.getId() == a.getId()) {
                m.setAvailable(m.getAvailable() + 1);
                try {
                    receiptService.removeArticleFromCart(m);
                } catch (ServiceException e) {
                    LOGGER.debug("Error in database");
                    DialogUtil.showErrorDialog("Error Dialog", "Error in Server", e.getMessage());
                }
                fillTableCart(receiptService.getTheArticleCart());
                setRealAvaiableValues();

                try {
                    shoppingCartController.setTotal();
                } catch (NullPointerException e) {

                }
                receiptService.isCartEmpty();
                return;
            }
        }
    }

    public void refreshArticleCart() {
        if (receiptService.getTheArticleCart() != null) {
            fillTableCart(receiptService.getTheArticleCart());
            setRealAvaiableValues();
        }
    }


    private void setRealAvaiableValues() {
        theNumber++;
        if (currentPerformance == null) {
            LOGGER.debug("Current Performance == null");
            return;
        }

        if (theNumber % 2 == 1) {
            MerchandiseDto kot = new MerchandiseDto();
            kot.setDescription("Not a product");
            currentPerformance.getArticles().add(kot);
            fillTableMerchandiseDto(FXCollections.observableArrayList(currentPerformance.getArticles()));
        } else {
            currentPerformance.getArticles().remove(currentPerformance.getArticles().size() - 1);
            fillTableMerchandiseDto(FXCollections.observableArrayList(currentPerformance.getArticles()));
        }
    }
}
