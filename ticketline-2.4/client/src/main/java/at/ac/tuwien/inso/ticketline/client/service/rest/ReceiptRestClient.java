package at.ac.tuwien.inso.ticketline.client.service.rest;

import at.ac.tuwien.inso.ticketline.client.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.client.exception.ValidationException;
import at.ac.tuwien.inso.ticketline.client.gui.controller.shoppingCart.SellController;
import at.ac.tuwien.inso.ticketline.client.gui.controller.shoppingCart.ShoppingCartController;
import at.ac.tuwien.inso.ticketline.client.gui.controller.tabs.CancelController;
import at.ac.tuwien.inso.ticketline.client.gui.controller.tabs.MerchandiseController;
import at.ac.tuwien.inso.ticketline.client.gui.controller.tabs.customer.CustomerViewController;
import at.ac.tuwien.inso.ticketline.client.gui.controller.tabs.search.views.ShowViewController;
import at.ac.tuwien.inso.ticketline.client.service.MerchandiseService;
import at.ac.tuwien.inso.ticketline.client.service.ReceiptService;
import at.ac.tuwien.inso.ticketline.client.service.TicketService;
import at.ac.tuwien.inso.ticketline.dto.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.*;

@Component
public class ReceiptRestClient implements ReceiptService {


    @Autowired
    private ShoppingCartController shoppingCartController;
    @Autowired
    private ShowViewController showViewController;
    @Autowired
    private CustomerViewController customerViewController;
    @Autowired
    private SellController sellController;
    @Autowired
    private MerchandiseService merchandiseService;
    @Autowired
    private TicketService ticketService;
    @Autowired
    private MerchandiseController merchandiseController;
    @Autowired
    private CancelController cancelController;

    private static final Logger LOGGER = LoggerFactory.getLogger(ReceiptService.class);
    private ObservableList<MerchandiseDto> theArticleCart = FXCollections.observableArrayList();
    private ObservableList<TicketDto> ticketCart = FXCollections.observableArrayList();
    private ReceiptDto receiptDto;
    private List<ReceiptentryDto> receiptentryDtos;
    private UserStatusDto userStatusDto;
    //private static ObservableList<> theTicketCart = FXCollections.observableArrayList();

    private ObservableList<MerchandiseDto> m2;
    private int theNumber = 0;

    @Override
    public void emptyCart() throws ServiceException {


        if(theArticleCart!=null)
            merchandiseService.releaseArticle(theArticleCart);
        if(ticketService!=null)
            ticketService.releaseTicket(ticketCart);

        theArticleCart = FXCollections.observableArrayList();
        ticketCart = FXCollections.observableArrayList();


        if(customerViewController!=null) {
            if(sellController!=null) {
                CustomerDto customerDto = sellController.getCustomerDto();
                customerViewController.resetCurrentShow(customerDto);
            }
        }

        shoppingCartController.initialize();
        showViewController.resetEmptyCart();
        cancelController.resetEmptyCart();
        merchandiseController.initialize();
    }

    @Override
    public void emptyCartWhenLogOut() throws ServiceException  {
        if(theArticleCart!=null)
            merchandiseService.releaseArticle(theArticleCart);
        if(ticketCart!=null)
            ticketService.releaseTicket(ticketCart);
        theArticleCart = FXCollections.observableArrayList();
        ticketCart = FXCollections.observableArrayList();
    }
    @Override
    public void emptyCartWhenSuccesfulSold(){
        theArticleCart = FXCollections.observableArrayList();
        ticketCart = FXCollections.observableArrayList();

        if(customerViewController!=null) {
            if(sellController!=null) {
                CustomerDto customerDto = sellController.getCustomerDto();
                customerViewController.resetCurrentShow(customerDto);
            }
        }
        shoppingCartController.initialize();
        showViewController.resetWhenSold();
        cancelController.resetWhenSold();

    }
    @Override
    public void isCartEmpty() {
        int i = 0;

        if (ticketCart != null) {
            i += ticketCart.size();
        }
        if (theArticleCart != null) {
            i += theArticleCart.size();
        }
        shoppingCartController.isCartEmpty(i > 0);
        shoppingCartController.setTotal();
    }

    @Override
    public void receiptSucces() {
        theArticleCart = FXCollections.observableArrayList();
    }

    @Override
    public ObservableList<MerchandiseDto> getTheArticleCart() {
        return theArticleCart;
    }

    @Override
    public ObservableList<TicketDto> getTicketCart() {
        return ticketCart;
    }

    @Override
    public void setNewCart() {
        theArticleCart = FXCollections.observableArrayList();
    }

    @Override
    public void addToArticleCart(MerchandiseDto merchandiseDto) {
        theArticleCart.add(merchandiseDto);
    }


    /*Methods for creating Receipt*/
    @Override
    public void removeArticleFromCart(MerchandiseDto merchandiseDto) throws ServiceException {
        List<MerchandiseDto> merchandiseDtos = new ArrayList<>();
        merchandiseDtos.add(merchandiseDto);
        merchandiseService.releaseArticle(merchandiseDtos);
        theArticleCart.remove(merchandiseDto);
    }

    @Override
    public void setUserStatusDto(UserStatusDto userStatusDto) {
        this.userStatusDto = userStatusDto;
    }


    @Override
    public ReceiptDto createCashReceipt(CashDto cashDto,boolean premium) throws ServiceException {
        ReceiptDto receiptDto = createReceipt(cashDto.getCustomer());
        receiptDto.setPremium(premium);
        receiptDto.setCashDto(cashDto);
        sell(receiptDto);
        return receiptDto;
    }

    @Override
    public ReceiptDto createBankReceipt(BankAccountDto bankAccountDto) throws ServiceException {
        ReceiptDto receiptDto = createReceipt(bankAccountDto.getCustomer());
        receiptDto.setBankAccountDto(bankAccountDto);
        sell(receiptDto);
        return receiptDto;
    }

@Override
    public ReceiptDto createCreditReceipt(CreditCardDto creditCardDto) throws ServiceException {
        ReceiptDto receiptDto = createReceipt(creditCardDto.getCustomer());
        receiptDto.setCreditCardDto(creditCardDto);
        sell(receiptDto);
    return receiptDto;
    }

    private ReceiptDto createReceipt(CustomerDto customerDto) throws ServiceException {
        HashMap<MerchandiseDto, Integer> mHashmap = new HashMap<>();
        receiptentryDtos = new ArrayList<>();
        int Total = 0;
        for (MerchandiseDto m : theArticleCart) {
            if (mHashmap.containsKey(m)) {
                int i = mHashmap.get(m) + 1;
                mHashmap.remove(m);
                mHashmap.put(m, i);
            } else {
                mHashmap.put(m, 1);
            }
            Total += m.getPrice();
        }
        Iterator it = mHashmap.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();

            MerchandiseDto m = (MerchandiseDto) pair.getKey();
            int amount = (int) pair.getValue();
            LOGGER.debug(amount + "receiptentry amount");
            ReceiptentryDto receiptentryDto = new ReceiptentryDto();
            receiptentryDto.setAmount(amount);
            receiptentryDto.setArticle(m);
            receiptentryDto.setPosition(1); //                                                          set position ?????
            receiptentryDto.setUnitPrice(m.getPrice() * amount);

            receiptentryDtos.add(receiptentryDto);
            it.remove(); // avoids a ConcurrentModificationException
        }

        for (TicketDto ticketDto : ticketCart) {
            TicketIdentifierDto ticketIdentifierDto = new TicketIdentifierDto();
            ticketIdentifierDto.setValid(true);
            ticketIdentifierDto.setTicket(ticketDto);


            ReceiptentryDto receiptentryDto = new ReceiptentryDto();
            receiptentryDto.setAmount(1);
            receiptentryDto.setTicketIdentifierDto(ticketIdentifierDto);
            receiptentryDto.setPosition(1); //                                                          set position ?????
            receiptentryDto.setUnitPrice(ticketDto.getPrice());
            receiptentryDtos.add(receiptentryDto);
        }
        ReceiptDto receiptDto = new ReceiptDto();
        receiptDto.setCustomer(customerDto);
        receiptDto.setEmployee(userStatusDto);
        receiptDto.setReceiptentryDto(receiptentryDtos);
        receiptDto.setTransactionDate(new Date());
        receiptDto.setTransactionState("PAID");
        return receiptDto;
    }

    @Autowired
    private RestClient restClient;
    public static final String SELL_URL = "/service/receipt/sellAll/";

    /**
     * {@inheritDoc}
     */
    public Integer sell(ReceiptDto receiptDto) throws ServiceException {

        RestTemplate restTemplate = this.restClient.getRestTemplate();

        String url = this.restClient.createServiceUrl(SELL_URL);

        LOGGER.info("Saving customer at {}", url);

        HttpHeaders headers = this.restClient.getHttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        HttpEntity<ReceiptDto> entity = new HttpEntity<>(receiptDto, headers);
        MessageDto msg;

        try {
            msg = restTemplate.postForObject(url, entity, MessageDto.class);
        } catch (HttpStatusCodeException e) {
            MessageDto errorMsg = this.restClient.mapExceptionToMessage(e);
            if (errorMsg.hasFieldErrors()) {
                throw new ValidationException(errorMsg.getFieldErrors());
            } else {
                throw new ServiceException(errorMsg.getText());
            }
        } catch (RestClientException e) {
            throw new ServiceException("Could not save customer: " + e.getMessage(), e);
        }

        Integer id;

        try {
            id = Integer.valueOf(msg.getText());
        } catch (NumberFormatException e) {
            throw new ServiceException("Invalid ID: " + msg.getText());
        }
        return id;
    }
    public boolean addFreeTicketToCart(TicketDto ticketDto) throws ServiceException{
        boolean use=false;
        int i = ticketService.checkTicket(ticketDto,1);
        System.out.println(i);
        if(i==0){
            use=true;
            ticketCart.add(ticketDto);
        }else{
            use=false;
        }
        return use;
    }
    public boolean addReservedTicketToCart(TicketDto ticketDto) throws ServiceException{
        boolean use=false;
        int i = ticketService.checkTicket(ticketDto,2);
        System.out.println(i);
        if(i==2){
            use=true;
            ticketCart.add(ticketDto);
        }else{
            use=false;
        }
        return use;
    }

    public boolean canTheTicketBeReserved(TicketDto ticketDto) throws ServiceException{
        boolean use=false;
        int i = ticketService.checkTicket(ticketDto,3);
        System.out.println(i);
        if(i==0){
            use=true;
        }else{
            use=false;
        }
        return use;
    }

}
