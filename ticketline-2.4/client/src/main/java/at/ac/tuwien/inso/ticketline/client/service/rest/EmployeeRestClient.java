package at.ac.tuwien.inso.ticketline.client.service.rest;

import at.ac.tuwien.inso.ticketline.client.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.client.exception.ValidationException;
import at.ac.tuwien.inso.ticketline.client.service.EmployeeService;
import at.ac.tuwien.inso.ticketline.dto.EmployeeDto;
import at.ac.tuwien.inso.ticketline.dto.MessageDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.Collections;

@Component
public class EmployeeRestClient implements EmployeeService {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmployeeRestClient.class);

    public static final String GET_EMPLOYEE_URL = "/service/employee/";
    public static final String UPDATE_LAST_LOGIN_URL = "/service/employee/updateLastLogin";

    @Autowired
    private RestClient restClient;

    @Override
    public EmployeeDto getEmployee(EmployeeDto employeeDto) throws ServiceException {

        RestTemplate restTemplate = this.restClient.getRestTemplate();
        String url = this.restClient.createServiceUrl(GET_EMPLOYEE_URL + employeeDto.getUsername());
        HttpEntity<String> entity = new HttpEntity<>(this.restClient.getHttpHeaders());

        LOGGER.info("Retrieving employee from {}", url);
        EmployeeDto employee;
        try {
            ParameterizedTypeReference<EmployeeDto> ref = new ParameterizedTypeReference<EmployeeDto>() {
            };
            ResponseEntity<EmployeeDto> response = restTemplate.exchange(URI.create(url), HttpMethod.GET, entity, ref);
            employee = response.getBody();

        } catch (RestClientException e) {
            throw new ServiceException("Could not retrieve news: " + e.getMessage(), e);
        }
        return employee;

    }

    @Override
    public void updateLastLogin(EmployeeDto employee) throws ServiceException {
        RestTemplate restTemplate = this.restClient.getRestTemplate();
        String url = this.restClient.createServiceUrl(UPDATE_LAST_LOGIN_URL);

        LOGGER.info("Update last login of user " + employee.getUsername() + "  at {}", url);

        HttpHeaders headers = this.restClient.getHttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        HttpEntity<EmployeeDto> entity = new HttpEntity<>(employee, headers);

        restTemplate.put(url, entity);
    }

}
