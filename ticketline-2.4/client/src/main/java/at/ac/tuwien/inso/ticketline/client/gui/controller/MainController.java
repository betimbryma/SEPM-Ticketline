package at.ac.tuwien.inso.ticketline.client.gui.controller;

import at.ac.tuwien.inso.ticketline.client.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.client.gui.JavaFXUtils;
import at.ac.tuwien.inso.ticketline.client.gui.controller.tabs.MerchandiseController;
import at.ac.tuwien.inso.ticketline.client.gui.controller.tabs.NewsController;
import at.ac.tuwien.inso.ticketline.client.gui.controller.tabs.PremiumsController;
import at.ac.tuwien.inso.ticketline.client.gui.controller.tabs.ReservationsController;
import at.ac.tuwien.inso.ticketline.client.gui.controller.tabs.customer.CustomersController;
import at.ac.tuwien.inso.ticketline.client.gui.controller.tabs.search.SearchController;
import at.ac.tuwien.inso.ticketline.client.service.AuthService;
import at.ac.tuwien.inso.ticketline.client.service.ReceiptService;
import at.ac.tuwien.inso.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.ticketline.client.util.DialogUtil;
import at.ac.tuwien.inso.ticketline.client.util.SpringFxmlLoader;
import at.ac.tuwien.inso.ticketline.dto.EmployeeDto;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;

@Component
public class MainController implements Initializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(MainController.class);

    private SearchController searchController;
    private CustomersController customersController;
    private MerchandiseController merchandiseController;
    private NewsController newsController;
    private PremiumsController premiumsController;
    private ReservationsController reservationsController;

    @Autowired
    private SpringFxmlLoader springFxmlLoader;
    @Autowired
    private AuthService authService;

    @FXML
    private TabPane tabPane;
    @FXML
    private Tab newsTab;
    @FXML
    private Tab searchTab;
    @FXML
    private Tab customersTab;
    @FXML
    private Tab reservationsTab;
    @FXML
    private Tab merchandiseTab;
    @FXML
    private Tab premiumsTab;
    @FXML
    private Circle loadingSymbol;
    @FXML
    private Button logout, refresh;
    @FXML
    private Button btn_shopcart;
    @FXML
    private ImageView btn_langImg;
    @FXML
    private Label salesperson;
    @FXML
    private Label employeename;
    @FXML
    private ImageView shppingCartImage;

    // the employee, that is currently logged in
    private EmployeeDto employee;


    public static Locale currentLocale = Locale.GERMAN;

    /**
     * This method initializes the MainController.
     *
     * @param location  location of the fxml file
     * @param resources resources
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initMainController();
        shppingCartImage.setImage(new Image("./image/ShoppingCart.png"));
        tabPane.getSelectionModel().selectedItemProperty().addListener(
                new ChangeListener<Tab>() {
                    @Override
                    public void changed(ObservableValue<? extends Tab> observable, Tab oldValue, Tab newValue) {
                        if (newValue == newsTab) {
                            //this tab is now active
                        }
                        if (newValue == searchTab) {
                            if(searchController.isStillLoading()){
                                DialogUtil.showInformationDialog(BundleManager.getBundle().getString("main.loadingTitle"),
                                        BundleManager.getBundle().getString("main.loadingHeader"),
                                        BundleManager.getBundle().getString("main.loadingBody"));
                                tabPane.getSelectionModel().select(oldValue);
                            }else{
                                if(!searchController.isInitialized()) {
                                    searchController.init();
                                    searchController.setInitialized(true);
                                }else{
                                    searchController.reset();
                                }
                            }
                        }
                        if (newValue == customersTab) {
                            //this tab is now active
                        }
                        if (newValue == reservationsTab) {
                            //this tab is now active
                        }
                        if (newValue == merchandiseTab) {
                            //this tab is now active
                        }
                        if (newValue == premiumsTab) {
                            //this tab is now active
                        }
                    }
                }
        );
        if (LoginController.currentLocale.equals(Locale.ENGLISH)) {
            currentLocale = Locale.ENGLISH;
            btn_langImg.setImage(new Image("./image/English.png"));
            reinitLocale(currentLocale);
        } else {
            currentLocale = Locale.GERMAN;
            btn_langImg.setImage(new Image("./image/German.png"));
            reinitLocale(currentLocale);
        }
    }

    private void reinitLocale(Locale newValue) {
        LocaleContextHolder.setLocale(newValue);
        BundleManager.changeLocale(newValue);
        btn_shopcart.setText(BundleManager.getBundle().getString("shoppingcart.title"));
        newsTab.setText(BundleManager.getBundle().getString("main.news"));
        searchTab.setText(BundleManager.getBundle().getString("main.search"));
        customersTab.setText(BundleManager.getBundle().getString("main.customer"));
        reservationsTab.setText(BundleManager.getBundle().getString("main.reservation"));
        merchandiseTab.setText(BundleManager.getBundle().getString("main.merchandise"));
        premiumsTab.setText(BundleManager.getBundle().getString("main.premiums"));

        logout.setText(BundleManager.getBundle().getString("main.logout"));
        refresh.setText(BundleManager.getBundle().getString("search.refresh"));
        salesperson.setText(BundleManager.getBundle().getString("main.salesperson"));

        customersController.reinitLocale(currentLocale);
        premiumsController.reinitLocale(currentLocale);
        reservationsController.reinitLocale(currentLocale);
        newsController.reinitLocale(currentLocale);
        searchController.reinitLocale(currentLocale);
        merchandiseController.reinitLocale(currentLocale);


    }

    /**
     * Starts the search loading threat.
     */
    public void startLoadingThread(){
        searchController.startLoadingThread();
    }

    @FXML
    private void onChangeLanguageButtonPressed(ActionEvent event) {
        if (currentLocale.equals(Locale.ENGLISH)) {
            currentLocale = Locale.GERMAN;
            btn_langImg.setImage(new Image("./image/German.png"));
        } else {
            currentLocale = Locale.ENGLISH;
            btn_langImg.setImage(new Image("./image/English.png"));
        }

        reinitLocale(currentLocale);
    }


    /**
     * This method initializes all tabs and their controllers.
     */
    private void initMainController() {
        SpringFxmlLoader.LoadWrapper loadWrapper = springFxmlLoader.loadAndWrap("/gui/fxml/tabs/news/news.fxml");
        newsController = ((NewsController) loadWrapper.getController());
        newsTab.setContent((Node) loadWrapper.getLoadedObject());
        loadWrapper = springFxmlLoader.loadAndWrap("/gui/fxml/tabs/search/search.fxml");
        searchController = ((SearchController) loadWrapper.getController());
        searchTab.setContent((Node) loadWrapper.getLoadedObject());
        loadWrapper = springFxmlLoader.loadAndWrap("/gui/fxml/tabs/customers/customers.fxml");
        customersController = ((CustomersController) loadWrapper.getController());
        customersController.setMainController(this);
        customersTab.setContent((Node) loadWrapper.getLoadedObject());
        loadWrapper = springFxmlLoader.loadAndWrap("/gui/fxml/tabs/reservations/reservations.fxml");
        reservationsController = ((ReservationsController) loadWrapper.getController());
        reservationsTab.setContent((Node) loadWrapper.getLoadedObject());
        loadWrapper = springFxmlLoader.loadAndWrap("/gui/fxml/tabs/merchandise/merchandise.fxml");
        merchandiseController = ((MerchandiseController) loadWrapper.getController());
        merchandiseTab.setContent((Node) loadWrapper.getLoadedObject());
        loadWrapper = springFxmlLoader.loadAndWrap("/gui/fxml/tabs/premiums/premiums.fxml");
        premiumsController = ((PremiumsController) loadWrapper.getController());
        premiumsTab.setContent((Node) loadWrapper.getLoadedObject());
    }

    /**
     * This method creates popup when the shopping cart button is pressed.
     *
     * @param event event that shopping cart button is pressed
     */
    @FXML
    public void handleShoppingCart(ActionEvent event) {
        ((Node) event.getSource()).setCursor(Cursor.WAIT);
        Stage mainStage = new Stage();
        mainStage.setMinWidth(800);
        mainStage.setMinHeight(600);
        mainStage.setResizable(true);

        mainStage.setScene(new Scene((Parent) springFxmlLoader.load("/gui/fxml/shoppingCart/shopping_cart_prototype.fxml")));
        mainStage.setTitle(BundleManager.getBundle().getString("app.name"));
        mainStage.getIcons().add(new Image(LoginController.class.getResourceAsStream("/image/ticketlineLogo.png")));
        mainStage.initModality(Modality.WINDOW_MODAL);
        mainStage.initOwner(((Node) event.getSource()).getScene().getWindow());


        mainStage.setWidth(800);
        mainStage.setHeight(600);
        mainStage.showAndWait();


        ((Node) event.getSource()).setCursor(Cursor.DEFAULT);

    }

    /**
     * This method logs the user (after a confirmation) out of the system and creates the starting window.
     *
     * @param event event that log out button is pressed
     */
    @Autowired
    ReceiptService receiptService;

    /**
     * Refreshes all the pre-loaded data.
     * @param event the refresh button event
     */
    @FXML
    public void handleRefresh(ActionEvent event){
        getLoadingSymbol().setFill(Color.RED);
        searchController.setInitialized(false);
        searchController.setStillLoading(true);
        tabPane.getSelectionModel().select(newsTab);
        customersController.refreshCustomerList();
        startLoadingThread();
    }

    @FXML
    public void handleLogOut(ActionEvent event) {
        if (DialogUtil.showConfirmationDialog("Log Out Ticketline",
                BundleManager.getBundle().getString("logout")) == ButtonType.OK) {
            try {
                receiptService.emptyCartWhenLogOut();
            } catch (ServiceException e) {
                LOGGER.debug("Error in database");
                DialogUtil.showErrorDialog("Error Dialog", "Error in Server", e.getMessage());
            }
            logOut();
            ((Node) event.getSource()).setCursor(Cursor.WAIT);
            Stage mainStage = new Stage();
            mainStage.setScene(new Scene((Parent) springFxmlLoader.load("/gui/fxml/login/login.fxml")));
            mainStage.setResizable(false);
            mainStage.setTitle(BundleManager.getBundle().getString("app.name"));
            mainStage.getIcons().add(new Image(LoginController.class.getResourceAsStream("/image/ticketlineLogo.png")));
            mainStage.show();
            ((Node) event.getSource()).setCursor(Cursor.DEFAULT);
            Node source = (Node) event.getSource();
            Stage stage = (Stage) source.getScene().getWindow();
            searchController.setInitialized(false);
            stage.close();
        }
    }

    /**
     * This method initializes a close request on the window, so the user is logged out when closing the window.
     *
     * @param stage the current stage
     */
    public void initCloseRequest(Stage stage) {
        stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                logOut();
                LOGGER.info("Exit system.");
                System.exit(0);
            }
        });
    }

    /**
     * This method logs the user out.
     */
    public void logOut() {
        try {
            this.authService.logout();
        } catch (ServiceException e) {
            LOGGER.error("Logout failed: " + e.getMessage(), e);
            Alert alert = JavaFXUtils.createAlert(e);
            alert.showAndWait();
            return;
        }
    }

    public SearchController getSearchController() {
        return searchController;
    }

    public void setSearchController(SearchController searchController) {
        this.searchController = searchController;
    }

    public CustomersController getCustomersController() {
        return customersController;
    }

    public void setCustomersController(CustomersController customersController) {
        this.customersController = customersController;
    }

    public MerchandiseController getMerchandiseController() {
        return merchandiseController;
    }

    public void setMerchandiseController(MerchandiseController merchandiseController) {
        this.merchandiseController = merchandiseController;
    }

    public NewsController getNewsController() {
        return newsController;
    }

    public void setNewsController(NewsController newsController) {
        this.newsController = newsController;
    }

    public PremiumsController getPremiumsController() {
        return premiumsController;
    }

    public void setPremiumsController(PremiumsController premiumsController) {
        this.premiumsController = premiumsController;
    }

    public ReservationsController getReservationsController() {
        return reservationsController;
    }

    public void setReservationsController(ReservationsController reservationsController) {
        this.reservationsController = reservationsController;
    }

    //sets the employee, that is currently logged in and updates the name in the GUI
    public void setCurrentEmployee(EmployeeDto employee) {
        this.employee = employee;

        employeename.setText(this.employee.getFirstName() + " " + this.employee.getLastName());
    }

    // gets the employee, that is currently logged in
    public EmployeeDto getCurrentEmployee() {
        return this.employee;
    }

    public Circle getLoadingSymbol() {
        return loadingSymbol;
    }

    public void setLoadingSymbol(Circle loadingSymbol) {
        this.loadingSymbol = loadingSymbol;
    }
}
