package at.ac.tuwien.inso.ticketline.client.gui.controller.tabs.search.shows;

import at.ac.tuwien.inso.ticketline.client.gui.JavaFXUtils;
import at.ac.tuwien.inso.ticketline.client.gui.controller.MainController;
import at.ac.tuwien.inso.ticketline.client.gui.controller.tabs.search.SearchController;
import at.ac.tuwien.inso.ticketline.client.gui.controller.tabs.search.views.ShowViewController;
import at.ac.tuwien.inso.ticketline.client.service.TicketService;
import at.ac.tuwien.inso.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.ticketline.client.util.DateUtil;
import at.ac.tuwien.inso.ticketline.dto.MapEntryDto;
import at.ac.tuwien.inso.ticketline.dto.ShowDto;
import at.ac.tuwien.inso.ticketline.dto.TicketDto;
import at.ac.tuwien.inso.ticketline.dto.TicketIdentifierDto;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.util.Callback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

@Component
public class ShowsController implements Initializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(ShowsController.class);
    @Autowired
    private MainController mainController;
    @Autowired
    private SearchController searchController;
    @Autowired
    private ShowViewController showViewController;
    @Autowired
    private TicketService ticketService;
    @FXML
    private TextField eventTextField, venueTextField, roomTextField, priceTextField;
    @FXML
    private DatePicker datePicker;
    @FXML
    private ComboBox hourComboBox, minuteComboBox;
    @FXML
    private TableView<ShowDto> showTable;
    @FXML
    private TableColumn<ShowDto, String> eventTableColumn, dateTableColumn, timeTableColumn, venueTableColumn, roomTableColumn, priceMinTableColumn, priceMaxTableColumn;
    @FXML
    private Label eventL, venueL, roomL, dateL, timeL, priceL, priceInfo;
    @FXML
    private Button reset, clear;
    private ObservableList<MapEntryDto<TicketDto,TicketIdentifierDto>> ticketsForShow;
    private int price = -1;

    /**
     * Initializes the controller.
     *
     * @param location  location of the fxml file
     * @param resources resources
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //initTable();
        //initSearchFields();
        reinitLocale(MainController.currentLocale);
        priceInfo.setText("\u2139");
    }

    /**
     * Initializes the controller.
     */
    public void init(){
        initTable();
        initSearchFields();
    }

    /**
     * Reinitializes the localization.
     * @param newValue the new localization value
     */
    public void reinitLocale(Locale newValue) {
        LocaleContextHolder.setLocale(newValue);
        BundleManager.changeLocale(newValue);
        eventL.setText(BundleManager.getBundle().getString("showsearch.event"));
        venueL.setText(BundleManager.getBundle().getString("showsearch.venue"));
        roomL.setText(BundleManager.getBundle().getString("showsearch.room"));
        dateL.setText(BundleManager.getBundle().getString("showsearch.date"));
        timeL.setText(BundleManager.getBundle().getString("showsearch.time"));
        priceL.setText(BundleManager.getBundle().getString("showsearch.price"));
        reset.setText(BundleManager.getBundle().getString("showsearch.reset"));
        clear.setText(BundleManager.getBundle().getString("showsearch.clear"));
        eventTableColumn.setText(BundleManager.getBundle().getString("showsearch.ename"));
        dateTableColumn.setText(BundleManager.getBundle().getString("showsearch.edate"));
        timeTableColumn.setText(BundleManager.getBundle().getString("showsearch.etime"));
        priceMinTableColumn.setText(BundleManager.getBundle().getString("showsearch.epriceMin"));
        priceMaxTableColumn.setText(BundleManager.getBundle().getString("showsearch.epriceMax"));
        venueTableColumn.setText(BundleManager.getBundle().getString("showsearch.evenues"));
        roomTableColumn.setText(BundleManager.getBundle().getString("showsearch.eroom"));

        showViewController.reinitLocale(newValue);
    }

    /**
     * Initializes the table.
     */
    public void initTable() {
        priceInfo.setTooltip(new Tooltip("+/- 10€"));
        eventTableColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ShowDto, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<ShowDto, String> param) {
                ObservableValue<String> event = new ReadOnlyObjectWrapper<String>(param.getValue().getPerformance().getName());
                return event;
            }
        });
        dateTableColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ShowDto, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<ShowDto, String> param) {
                SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                ObservableValue<String> date = new ReadOnlyObjectWrapper<>(format.format(param.getValue().getDateOfPerformance()));
                return date;
            }
        });
        timeTableColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ShowDto, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<ShowDto, String> param) {
                SimpleDateFormat format = new SimpleDateFormat("HH:mm");
                ObservableValue<String> time = new ReadOnlyObjectWrapper<>(format.format(param.getValue().getDateOfPerformance()));
                return time;
            }
        });
        venueTableColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ShowDto, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<ShowDto, String> param) {
                ObservableValue<String> venue = new ReadOnlyObjectWrapper<String>(param.getValue().getRoom().getLocation().getName());
                return venue;
            }
        });
        roomTableColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ShowDto, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<ShowDto, String> param) {
                ObservableValue<String> room = new ReadOnlyObjectWrapper<String>(param.getValue().getRoom().getName());
                return room;
            }
        });
        priceMinTableColumn.setCellValueFactory(param -> {
            int newPrice = searchController.getAllTicketsMinPrice().get(param.getValue());
            ObservableValue<String> price = new ReadOnlyObjectWrapper<>(Integer.toString(newPrice));
            return price;
        });
        priceMaxTableColumn.setCellValueFactory(param -> {
            int newPrice = searchController.getAllTicketsMaxPrice().get(param.getValue());
            ObservableValue<String> price = new ReadOnlyObjectWrapper<>(Integer.toString(newPrice));
            return price;
        });
        showTable.setRowFactory(tv -> {
            TableRow<ShowDto> row = JavaFXUtils.getTableRowWithTooltip(e -> e.getPerformance().getDescription());
            row.setOnMouseClicked(event -> {
                if ((!row.isEmpty())) {
                    //showViewController.reset();
                    showViewController.setShow(row.getItem());
                    searchController.getStackPane().getChildren().clear();
                    searchController.getStackPane().getChildren().add(searchController.getShowViewPane());
                }
            });
            return row;
        });
        showTable.setItems(searchController.getAllShows());

        datePicker.setConverter(DateUtil.getEuropeanDateStringConverter());

    }

    /**
     * Initializes the search fields.
     */
    public void initSearchFields() {
        ChangeListener listener = new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                search();
            }
        };
        eventTextField.textProperty().addListener(listener);
        venueTextField.textProperty().addListener(listener);
        roomTextField.textProperty().addListener(listener);
        priceTextField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                try {
                    if (priceTextField.getText().equals("")) {
                        price = -1;
                    } else {
                        price = Integer.parseInt(priceTextField.getText());
                    }
                } catch (NumberFormatException e) {
                    priceTextField.setText("");
                    price = -1;
                    search();
                    return;
                }
                search();
            }
        });
        datePicker.valueProperty().addListener(new ChangeListener<LocalDate>() {
            @Override
            public void changed(ObservableValue<? extends LocalDate> observable, LocalDate oldValue, LocalDate newValue) {
                search();
            }
        });
        ObservableList hour = FXCollections.observableArrayList();
        String hours;
        hour.add("");
        for (int i = 0; i < 24; i++) {
            hours = Integer.toString(i);
            if (hours.length() < 2) {
                hours = "0" + hours;
            }
            hour.add(hours);
        }
        hourComboBox.setValue("");
        hourComboBox.setItems(hour);
        hourComboBox.valueProperty().addListener(listener);
        ObservableList minute = FXCollections.observableArrayList();
        minute.add("");
        String minutes;
        for (int i = 0; i < 60; i += 5) {
            minutes = Integer.toString(i);
            if (minutes.length() < 2) {
                minutes = "0" + minutes;
            }
            minute.add(minutes);
        }
        minuteComboBox.setValue("");
        minuteComboBox.setItems(minute);
        minuteComboBox.valueProperty().addListener(listener);
    }

    /**
     * Performs a search with the input.
     */
    public void search() {
        List<ShowDto> shows = new ArrayList<>();
        for (ShowDto s : searchController.getAllShows()) {
            if (checkEvent(s) && checkVenue(s) && checkRoom(s) && checkPrice(s) && checkDate(s) && checkTime(s)) {
                shows.add(s);
            }
        }
        ObservableList<ShowDto> observableList = FXCollections.observableList(shows);
        showTable.setItems(observableList);
    }

    /**
     * Checks if event fits.
     * @param s Show
     * @return boolean
     */
    public boolean checkEvent(ShowDto s) {
        if (s.getPerformance().getName().toLowerCase().contains(eventTextField.getText().toLowerCase())) {
            return true;
        }
        return false;
    }

    /**
     * Checks if venue fits.
     * @param s Show
     * @return boolean
     */
    public boolean checkVenue(ShowDto s) {
        if (s.getRoom().getLocation().getName().toLowerCase().contains(venueTextField.getText().toLowerCase())) {
            return true;
        }
        return false;
    }

    /**
     * Checks if room fits.
     * @param s Show
     * @return boolean
     */
    public boolean checkRoom(ShowDto s) {
        if (s.getRoom().getName().toLowerCase().contains(roomTextField.getText().toLowerCase())) {
            return true;
        }
        return false;
    }

    /**
     * Checks if price fits (+/- 10).
     * @param s Show
     * @return boolean
     */
    public boolean checkPrice(ShowDto s) {
        if(price == -1){
            return true;
        }
        if(price >= searchController.getAllTicketsMinPrice().get(s) - 10 && price <= searchController.getAllTicketsMaxPrice().get(s) + 10){
            return true;
        }
        return false;
    }

    /**
     * Checks if date fits.
     * @param s Show
     * @return boolean
     */
    public boolean checkDate(ShowDto s) {
        if (datePicker.getValue() == null) {
            return true;
        }
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String d1 = datePicker.getValue().toString();
        String d2 = format.format(s.getDateOfPerformance());
        if (d1.equals(d2)) {
            return true;
        }
        return false;
    }

    /**
     * Checks if time fits.
     * @param s Show
     * @return boolean
     */
    public boolean checkTime(ShowDto s) {
        if(hourComboBox.getValue() != null && minuteComboBox.getValue() != null) {
            if (hourComboBox.getValue().equals("")) {
                return true;
            }
            SimpleDateFormat hours = new SimpleDateFormat("HH");
            SimpleDateFormat minutes = new SimpleDateFormat("mm");

            String hour = hours.format(s.getDateOfPerformance());
            String minute = minutes.format(s.getDateOfPerformance());
            if (minuteComboBox.getValue().equals("")) {
                if (hourComboBox.getValue().equals(hour)) {
                    return true;
                }
            } else {
                if (hourComboBox.getValue().equals(hour) && minuteComboBox.getValue().equals(minute)) {
                    return true;
                }
            }
            return false;
        }else{
            return true;
        }
    }

    /**
     * Handle the reset button.
     * @param event button event
     */
    @FXML
    public void handleReset(ActionEvent event) {
        reset();
        search();
    }

    /**
     * Handle the clear button.
     * @param event button event
     */
    @FXML
    public void handleClear(ActionEvent event) {
        datePicker.setValue(null);
    }

    /**
     * Resets the controller.
     */
    public void reset() {
        eventTextField.setText("");
        venueTextField.setText("");
        roomTextField.setText("");
        priceTextField.setText("");
        datePicker.setValue(null);
        hourComboBox.setValue("");
        minuteComboBox.setValue("");
    }
}
