package at.ac.tuwien.inso.ticketline.client.gui.controller.tabs.search.topEvents;

import at.ac.tuwien.inso.ticketline.client.gui.controller.MainController;
import at.ac.tuwien.inso.ticketline.client.gui.controller.tabs.search.SearchController;
import at.ac.tuwien.inso.ticketline.client.gui.controller.tabs.search.views.EventViewController;
import at.ac.tuwien.inso.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.ticketline.dto.PerformanceDto;
import at.ac.tuwien.inso.ticketline.dto.ShowDto;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Bounds;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.util.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;

@Component
public class TopEventsController implements Initializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(TopEventsController.class);
    @Autowired
    private SearchController searchController;
    @Autowired
    private EventViewController eventViewController;
    @FXML
    private HBox barChartPane;
    @FXML
    private ComboBox typeComboBox, monthComboBox, yearComboBox;
    @FXML
    private Button reset;
    private List<Pair<PerformanceDto, Integer>> soldForPerformance;

    /**
     * Initializes the controller.
     *
     * @param location  location of the fxml file
     * @param resources resources
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initComboBoxes();
        reinitLocale(MainController.currentLocale);
    }

    /**
     * Initializes the controller.
     */
    public void init() {
        initComboBoxes();
        initChart();
        reset();
    }

    /**
     * Reinitializes the localization.
     *
     * @param newValue the new localization value
     */
    public void reinitLocale(Locale newValue) {
        //initChart();

        reset.setText(BundleManager.getBundle().getString("topeventssearch.resetB"));

        if (searchController.isInitialized()) {
            LocaleContextHolder.setLocale(newValue);
            BundleManager.changeLocale(newValue);
            ObservableList names = FXCollections.observableArrayList();
            names.add(BundleManager.getBundle().getString("topeventssearch.typeAll"));
            names.add(BundleManager.getBundle().getString("topeventssearch.typeMovie"));
            names.add(BundleManager.getBundle().getString("topeventssearch.typeFestival"));
            names.add(BundleManager.getBundle().getString("topeventssearch.typeConcert"));
            names.add(BundleManager.getBundle().getString("topeventssearch.typeMusical"));
            names.add(BundleManager.getBundle().getString("topeventssearch.typeOper"));
            names.add(BundleManager.getBundle().getString("topeventssearch.typeTheater"));
            typeComboBox.setValue(BundleManager.getBundle().getString("topeventssearch.typeAll"));
            typeComboBox.setItems(names);
            eventViewController.reinitLocale(newValue);
        }
    }

    /**
     * Initializes the chart.
     */
    public void initChart() {
        barChartPane.getChildren().clear();
        CategoryAxis xAxis = new CategoryAxis();
        NumberAxis yAxis = new NumberAxis();
        BarChart<String, Number> barChart = new BarChart<>(xAxis, yAxis);
        barChart.setLegendVisible(false);
        xAxis.setTickLabelsVisible(false);
        soldForPerformance = countSoldTickets();

        XYChart.Series series = new XYChart.Series();
        series.setName("Top 10");
        for (int i = 0; i < soldForPerformance.size() && i < 10; i++) {
            if (soldForPerformance.get(i).getValue() != 0) {
                XYChart.Data<String, Integer> data = new XYChart.Data(soldForPerformance.get(i).getKey().getName(), soldForPerformance.get(i).getValue(), soldForPerformance.get(i).getKey());
                data.nodeProperty().addListener(new ChangeListener<Node>() {
                    @Override
                    public void changed(ObservableValue<? extends Node> observable, Node oldValue, Node newValue) {
                        if (newValue != null) {
                            initializeBar(data);
                        }
                    }
                });
                series.getData().add(data);
            }
        }
        if (series.getData().size() < 10) {
            int difference = 10 - series.getData().size();
            for (int i = 0; i < difference; i++) {
                series.getData().add(new XYChart.Data("" + i, 0));
            }
        }
        barChart.getData().addAll(series);
        barChart.setPrefSize(barChartPane.getScene().getWidth(), barChartPane.getPrefHeight());
        barChartPane.getChildren().add(barChart);
    }

    /**
     * Initializes the bar.
     *
     * @param data the data for this bar
     */
    public void initializeBar(XYChart.Data<String, Integer> data) {
        Node node = data.getNode();
        Text dataText = new Text(data.getXValue() + "");
        dataText.setRotate(-90);
        node.parentProperty().addListener(new ChangeListener<Parent>() {
            @Override
            public void changed(ObservableValue<? extends Parent> ov, Parent oldParent, Parent parent) {
                Group parents = (Group) parent;
                parents.getChildren().add(dataText);
            }
        });
        node.boundsInParentProperty().addListener(new ChangeListener<Bounds>() {
            @Override
            public void changed(ObservableValue<? extends Bounds> ov, Bounds oldBounds, Bounds bounds) {
                dataText.setLayoutX(Math.round(bounds.getMinX() + bounds.getWidth() / 2 - dataText.prefWidth(-1) / 2));
                dataText.setLayoutY(bounds.getMaxY() - dataText.getLayoutBounds().getWidth() / 2);
            }
        });
        EventHandler mouseEntered = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                node.setStyle("-fx-bar-fill: gray;");
            }
        };
        EventHandler mouseExited = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                node.setStyle("-fx-bar-fill: lightgray;");
            }
        };
        EventHandler mouseClicked = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                eventViewController.reset();
                eventViewController.setPerformance((PerformanceDto) data.getExtraValue());
                searchController.getStackPane().getChildren().clear();
                searchController.getStackPane().getChildren().add(searchController.getEventViewPane());
            }
        };
        node.setOnMouseEntered(mouseEntered);
        node.setOnMouseExited(mouseExited);
        node.setOnMouseClicked(mouseClicked);
        dataText.setOnMouseEntered(mouseEntered);
        dataText.setOnMouseExited(mouseExited);
        dataText.setOnMouseClicked(mouseClicked);
        node.setStyle("-fx-bar-fill: lightgray;");
    }

    /**
     * Initializes the combo boxes.
     */
    public void initComboBoxes() {
        ChangeListener listener = new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (monthComboBox.getValue() != null && yearComboBox.getValue() != null) {
                    searchController.reloadTickets(getMonth(), getYear());
                }
                if (searchController.isInitialized()) {
                    initChart();
                }
            }
        };
        ObservableList month = FXCollections.observableArrayList();
        String months;
        for (int i = 1; i <= 12; i++) {
            months = Integer.toString(i);
            month.add(months);
        }
        monthComboBox.setValue(Integer.toString(Calendar.getInstance().get(Calendar.MONTH) + 1));
        monthComboBox.setItems(month);
        monthComboBox.valueProperty().addListener(listener);
        ObservableList year = FXCollections.observableArrayList();
        String years;
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        for (int i = currentYear - 2; i <= currentYear + 2; i++) {
            years = Integer.toString(i);
            year.add(years);
        }
        yearComboBox.setValue(Integer.toString(currentYear));
        yearComboBox.setItems(year);
        yearComboBox.valueProperty().addListener(listener);
        ObservableList names = FXCollections.observableArrayList();
        names.add(BundleManager.getBundle().getString("topeventssearch.typeAll"));
        names.add(BundleManager.getBundle().getString("topeventssearch.typeMovie"));
        names.add(BundleManager.getBundle().getString("topeventssearch.typeFestival"));
        names.add(BundleManager.getBundle().getString("topeventssearch.typeConcert"));
        names.add(BundleManager.getBundle().getString("topeventssearch.typeMusical"));
        names.add(BundleManager.getBundle().getString("topeventssearch.typeOper"));
        names.add(BundleManager.getBundle().getString("topeventssearch.typeTheater"));
        typeComboBox.setValue(BundleManager.getBundle().getString("topeventssearch.typeAll"));
        typeComboBox.setItems(names);
        typeComboBox.valueProperty().addListener(listener);
    }

    /**
     * Counts all sold tickets for performances.
     *
     * @return returns list of performances and the amount of tickets sold
     */
    public List<Pair<PerformanceDto, Integer>> countSoldTickets() {
        List<Pair<PerformanceDto, Integer>> soldForPerformance = new ArrayList<>();
        for (PerformanceDto p : searchController.getAllPerformances()) {
            int sold = 0;
            if (checkType(p)) {
                for (ShowDto s : searchController.getAllShows()) {
                    if (s.getPerformance().getId().equals(p.getId()) && checkDate(s)) {
                        if (searchController.getAllSoldTickets().get(s) != null) {
                            sold += searchController.getAllSoldTickets().get(s);
                        }
                    }
                }
            }
            soldForPerformance.add(new Pair<>(p, sold));
        }
        Collections.sort(soldForPerformance, new Comparator<Pair<PerformanceDto, Integer>>() {
            @Override
            public int compare(final Pair<PerformanceDto, Integer> o1, final Pair<PerformanceDto, Integer> o2) {
                return -o1.getValue().compareTo(o2.getValue());
            }
        });
        return soldForPerformance;
    }

    /**
     * Checks if type fits.
     *
     * @param p Performance
     * @return boolean
     */
    public boolean checkType(PerformanceDto p) {
        if (typeComboBox.getValue() != null) {
            if (typeComboBox.getValue().equals("All") || typeComboBox.getValue().equals("Alle")) {
                return true;
            } else if (typeComboBox.getValue().toString().equals(searchController.convertPerformanceType(p))) {
                return true;
            }
            return false;
        } else {
            return true;
        }
    }

    /**
     * Checks if date fits.
     *
     * @param s Show
     * @return boolean
     */
    public boolean checkDate(ShowDto s) {
        if (monthComboBox.getValue() != null && yearComboBox.getValue() != null) {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM");
            String d1 = "";
            if (monthComboBox.getValue().toString().length() == 1) {
                d1 = yearComboBox.getValue().toString() + "-0" + monthComboBox.getValue().toString();
            } else {
                d1 = yearComboBox.getValue().toString() + "-" + monthComboBox.getValue().toString();
            }
            String d2 = format.format(s.getDateOfPerformance());
            if (d1.equals(d2)) {
                return true;
            }
            return false;
        } else {
            return true;
        }
    }

    /**
     * Handle the reset button.
     *
     * @param event button event
     */
    @FXML
    public void handleReset(ActionEvent event) {
        reset();

    }

    /**
     * Resets the controller.
     */
    public void reset() {
        monthComboBox.setValue(Integer.toString(Calendar.getInstance().get(Calendar.MONTH) + 1));
        yearComboBox.setValue(Integer.toString(Calendar.getInstance().get(Calendar.YEAR)));
        if (MainController.currentLocale.equals(Locale.ENGLISH)) {
            typeComboBox.setValue("All");
        } else {
            typeComboBox.setValue("Alle");
        }
        initChart();
    }

    /**
     * Returns month from combo box.
     *
     * @return month as int
     */
    public int getMonth() {
        return Integer.parseInt(monthComboBox.getValue().toString());
    }

    /**
     * Returns year from combo box.
     *
     * @return year as int
     */
    public int getYear() {
        return Integer.parseInt(yearComboBox.getValue().toString());
    }
}
