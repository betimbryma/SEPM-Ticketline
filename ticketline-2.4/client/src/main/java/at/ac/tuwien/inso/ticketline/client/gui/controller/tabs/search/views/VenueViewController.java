package at.ac.tuwien.inso.ticketline.client.gui.controller.tabs.search.views;

import at.ac.tuwien.inso.ticketline.client.gui.JavaFXUtils;
import at.ac.tuwien.inso.ticketline.client.gui.controller.MainController;
import at.ac.tuwien.inso.ticketline.client.gui.controller.tabs.search.SearchController;
import at.ac.tuwien.inso.ticketline.client.service.TicketService;
import at.ac.tuwien.inso.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.ticketline.dto.LocationDto;
import at.ac.tuwien.inso.ticketline.dto.RoomDto;
import at.ac.tuwien.inso.ticketline.dto.ShowDto;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;
import java.util.ResourceBundle;

@Component
public class VenueViewController implements Initializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(ShowViewController.class);
    @Autowired
    private MainController mainController;
    @Autowired
    private SearchController searchController;
    @Autowired
    private ShowViewController showViewController;
    @Autowired
    private TicketService ticketService;
    @FXML
    private Label mainLabel, titleLabel, nameLB, nameLabel, roomsLB, roomsLabel, streetLB, streetLabel,
            cityLB, cityLabel, postalCodeLB, postalCodeLabel, countryLB, countryLabel, showsLB;
    @FXML
    private Button backButton;
    @FXML
    private TableView<ShowDto> showTable;
    @FXML
    private TableColumn<ShowDto, String> eventNameColumn, eventTypeColumn, dateColumn, timeColumn, roomColumn, priceMinColumn, priceMaxColumn;
    private ObservableList<ShowDto> filteredShows;
    private LocationDto location;

    /**
     * Initializes the controller.
     *
     * @param location  location of the fxml file
     * @param resources resources
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        filteredShows = FXCollections.observableList(new ArrayList<>());
        initTable();
        reinitLocale(MainController.currentLocale);
    }

    /**
     * Reinitializes the localization.
     * @param newValue the new localization value
     */
    public void reinitLocale(Locale newValue) {
        initTable();
        LocaleContextHolder.setLocale(newValue);
        BundleManager.changeLocale(newValue);
        backButton.setText(BundleManager.getBundle().getString("venView.backBut"));
        mainLabel.setText(BundleManager.getBundle().getString("venView.main"));
        titleLabel.setText(BundleManager.getBundle().getString("venView.title"));
        nameLB.setText(BundleManager.getBundle().getString("venView.nameLB"));
        roomsLB.setText(BundleManager.getBundle().getString("venView.roomsLB"));
        streetLB.setText(BundleManager.getBundle().getString("venView.streetLB"));
        cityLB.setText(BundleManager.getBundle().getString("venView.cityLB"));
        postalCodeLB.setText(BundleManager.getBundle().getString("venView.postalCodeLB"));
        countryLB.setText(BundleManager.getBundle().getString("venView.countryLB"));
        showsLB.setText(BundleManager.getBundle().getString("venView.showsLB"));
        eventNameColumn.setText(BundleManager.getBundle().getString("venView.nameCol"));
        eventTypeColumn.setText(BundleManager.getBundle().getString("venView.typeCol"));
        dateColumn.setText(BundleManager.getBundle().getString("venView.dateCol"));
        timeColumn.setText(BundleManager.getBundle().getString("venView.timeCol"));
        roomColumn.setText(BundleManager.getBundle().getString("venView.roomCol"));
        priceMinColumn.setText(BundleManager.getBundle().getString("venView.priceMinCol"));
        priceMaxColumn.setText(BundleManager.getBundle().getString("venView.priceMaxCol"));
    }

    /**
     * Initializes all tables.
     */
    public void initTable() {
        eventNameColumn.setCellValueFactory(param -> {
            ObservableValue<String> name = new ReadOnlyObjectWrapper<>(param.getValue().getPerformance().getName());
            return name;
        });
        eventTypeColumn.setCellValueFactory(param -> {
            ObservableValue<String> type = new ReadOnlyObjectWrapper<>(searchController.convertPerformanceType(param.getValue().getPerformance()));
            return type;
        });
        dateColumn.setCellValueFactory(param -> {
            SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
            ObservableValue<String> date = new ReadOnlyObjectWrapper<>(format.format(param.getValue().getDateOfPerformance()));
            return date;
        });
        timeColumn.setCellValueFactory(param -> {
            SimpleDateFormat format = new SimpleDateFormat("HH:mm");
            ObservableValue<String> time = new ReadOnlyObjectWrapper<>(format.format(param.getValue().getDateOfPerformance()));
            return time;
        });
        roomColumn.setCellValueFactory(param -> {
            ObservableValue<String> room = new ReadOnlyObjectWrapper<>(param.getValue().getRoom().getName());
            return room;
        });
        priceMinColumn.setCellValueFactory(param -> {
            int newPrice = searchController.getAllTicketsMinPrice().get(param.getValue());
            ObservableValue<String> price = new ReadOnlyObjectWrapper<>(Integer.toString(newPrice));
            return price;
        });
        priceMaxColumn.setCellValueFactory(param -> {
            int newPrice = searchController.getAllTicketsMaxPrice().get(param.getValue());
            ObservableValue<String> price = new ReadOnlyObjectWrapper<>(Integer.toString(newPrice));
            return price;
        });
        showTable.setRowFactory(tv -> {
            TableRow<ShowDto> row = JavaFXUtils.getTableRowWithTooltip(e -> e.getPerformance().getDescription());
            row.setOnMouseClicked(event -> {
                if (!row.isEmpty()) {
                    //showViewController.reset();
                    showViewController.setShow(row.getItem());
                    searchController.getStackPane().getChildren().clear();
                    searchController.getStackPane().getChildren().add(searchController.getShowViewPane());
                }
            });
            return row;
        });
        showTable.setItems(filteredShows);
    }

    /**
     * Sets the chosen location.
     * @param location the chosen location
     */
    public void setLocation(LocationDto location) {
        this.location = location;
        filteredShows.clear();
        for (ShowDto s : searchController.getAllShows()) {
            if (s.getRoom().getLocation().getId().equals(location.getId())) {
                filteredShows.add(s);
            }
        }
        nameLabel.setText(location.getName());
        String rooms = "";
        for (RoomDto r : searchController.getAllRooms()) {
            if (r.getLocation().getId().equals(location.getId())) {
                rooms += r.getName() + "\n";
            }
        }
        roomsLabel.setText(rooms);
        streetLabel.setText(location.getStreet());
        cityLabel.setText(location.getCity());
        postalCodeLabel.setText(location.getPostalCode());
        countryLabel.setText(location.getCountry());
    }

    /**
     * This method puts the accordion in the foreground.
     *
     * @param event the back button event
     */
    @FXML
    public void handleBack(ActionEvent event) {
        searchController.getStackPane().getChildren().clear();
        searchController.getStackPane().getChildren().add(searchController.getAccordion());
    }

    /**
     * Resets the show list.
     */
    public void reset() {
        filteredShows.clear();
    }
}
