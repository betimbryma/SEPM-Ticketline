package at.ac.tuwien.inso.ticketline.client.service;

import at.ac.tuwien.inso.ticketline.client.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.dto.PremiumDto;

import java.util.List;

/**
 * Created by eni on 04.06.2016.
 */
public interface PremiumService {

    List<PremiumDto> getPremiums() throws ServiceException;
    PremiumDto searchPremium(PremiumDto premiumDto) throws ServiceException;
    PremiumDto returnPremium(PremiumDto premiumDto) throws ServiceException;
}
