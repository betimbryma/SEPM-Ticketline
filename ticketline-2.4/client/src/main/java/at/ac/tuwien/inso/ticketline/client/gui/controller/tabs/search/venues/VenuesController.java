package at.ac.tuwien.inso.ticketline.client.gui.controller.tabs.search.venues;

import at.ac.tuwien.inso.ticketline.client.gui.JavaFXUtils;
import at.ac.tuwien.inso.ticketline.client.gui.controller.MainController;
import at.ac.tuwien.inso.ticketline.client.gui.controller.tabs.search.SearchController;
import at.ac.tuwien.inso.ticketline.client.gui.controller.tabs.search.views.VenueViewController;
import at.ac.tuwien.inso.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.ticketline.dto.LocationDto;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

@Component
public class VenuesController implements Initializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(VenuesController.class);
    @Autowired
    private MainController mainController;
    @Autowired
    private SearchController searchController;
    @Autowired
    private VenueViewController venueViewController;
    @FXML
    private TableView<LocationDto> locationTable;
    @FXML
    private TableColumn<LocationDto, String> nameColumn, streetColumn, postalCodeColumn, cityColumn, countryColumn;
    @FXML
    private TextField nameTextField, streetTextField, postalCodeTextField, cityTextField, countryTextField;
    @FXML
    private Button reset;
    @FXML
    private Label nameLable, streetLable, cityLable, zipLable, countryLable;

    /**
     * Initializes the controller.
     *
     * @param location  location of the fxml file
     * @param resources resources
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //initTable();
        //initSearchFields();
        reinitLocale(MainController.currentLocale);
    }

    /**
     * Initializes the controller.
     */
    public void init(){
        initTable();
        initSearchFields();
    }

    /**
     * Reinitializes the localization.
     * @param newValue the new localization value
     */
    public void reinitLocale(Locale newValue) {
        LocaleContextHolder.setLocale(newValue);
        BundleManager.changeLocale(newValue);
        nameColumn.setText(BundleManager.getBundle().getString("searchvenues.name"));
        streetColumn.setText(BundleManager.getBundle().getString("searchvenues.street"));
        postalCodeColumn.setText(BundleManager.getBundle().getString("searchvenues.pc"));
        cityColumn.setText(BundleManager.getBundle().getString("searchvenues.city"));
        countryColumn.setText(BundleManager.getBundle().getString("searchvenues.country"));
        reset.setText(BundleManager.getBundle().getString("searchvenues.resetB"));
        nameLable.setText(BundleManager.getBundle().getString("searchvenues.nameL"));
        streetLable.setText(BundleManager.getBundle().getString("searchvenues.streetL"));
        cityLable.setText(BundleManager.getBundle().getString("searchvenues.cityL"));
        zipLable.setText(BundleManager.getBundle().getString("searchvenues.pcL"));
        countryLable.setText(BundleManager.getBundle().getString("searchvenues.countryL"));
        venueViewController.reinitLocale(newValue);
    }

    /**
     * Initializes the table.
     */
    public void initTable(){
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        streetColumn.setCellValueFactory(new PropertyValueFactory<>("street"));
        postalCodeColumn.setCellValueFactory(new PropertyValueFactory<>("postalCode"));
        cityColumn.setCellValueFactory(new PropertyValueFactory<>("city"));
        countryColumn.setCellValueFactory(new PropertyValueFactory<>("country"));
        locationTable.setRowFactory(tv -> {
            TableRow<LocationDto> row = JavaFXUtils.getTableRowWithTooltip(e -> e.getDescription());


            row.setOnMouseClicked(event -> {
                if ((!row.isEmpty())) {
                    venueViewController.reset();
                    venueViewController.setLocation(row.getItem());
                    searchController.getStackPane().getChildren().clear();
                    searchController.getStackPane().getChildren().add(searchController.getVenueViewPane());
                }
            });
            return row;
        });
        locationTable.setItems(searchController.getAllLocations());
    }

    /**
     * Initializes the search fields.
     */
    public void initSearchFields(){
        ChangeListener<String> listener = new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                search();
            }
        };
        nameTextField.textProperty().addListener(listener);
        streetTextField.textProperty().addListener(listener);
        postalCodeTextField.textProperty().addListener(listener);
        cityTextField.textProperty().addListener(listener);
        countryTextField.textProperty().addListener(listener);
    }

    /**
     * Performs a search with the input.
     */
    public void search(){
        List<LocationDto> locations = new ArrayList<>();
        for(LocationDto l: searchController.getAllLocations()){
            if(checkName(l) && checkStreet(l) && checkPostalCode(l) && checkCity(l) && checkCountry(l)){
                locations.add(l);
            }
        }
        ObservableList<LocationDto> observableList = FXCollections.observableList(locations);
        locationTable.setItems(observableList);
    }

    /**
     * Checks if name fits.
     * @param l Location
     * @return boolean
     */
    public boolean checkName(LocationDto l){
        if(l.getName().toLowerCase().contains(nameTextField.getText().toLowerCase())){
            return true;
        }
        return false;
    }

    /**
     * Checks if street fits.
     * @param l Location
     * @return boolean
     */
    public boolean checkStreet(LocationDto l){
        if(l.getStreet().toLowerCase().contains(streetTextField.getText().toLowerCase())){
            return true;
        }
        return false;    }

    /**
     * Checks if postal code fits.
     * @param l Location
     * @return boolean
     */
    public boolean checkPostalCode(LocationDto l){
        if(l.getPostalCode().toLowerCase().contains(postalCodeTextField.getText().toLowerCase())){
            return true;
        }
        return false;    }

    /**
     * Checks if city fits.
     * @param l Location
     * @return boolean
     */
    public boolean checkCity(LocationDto l){
        if(l.getCity().toLowerCase().contains(cityTextField.getText().toLowerCase())){
            return true;
        }
        return false;    }

    /**
     * Checks if country fits.
     * @param l Location
     * @return boolean
     */
    public boolean checkCountry(LocationDto l){
        if(l.getCountry().toLowerCase().contains(countryTextField.getText().toLowerCase())){
            return true;
        }
        return false;
    }

    /**
     * Handle the reset button.
     * @param event button event
     */
    @FXML
    public void handleReset(ActionEvent event){
        reset();
        search();
    }

    /**
     * Resets the controller.
     */
    public void reset(){
        nameTextField.setText("");
        streetTextField.setText("");
        postalCodeTextField.setText("");
        cityTextField.setText("");
        countryTextField.setText("");
    }
}
