package at.ac.tuwien.inso.ticketline.client.service;

import at.ac.tuwien.inso.ticketline.client.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.dto.CustomerDto;

import java.util.List;

public interface CustomerService {

    /**
     * Saves a customer.
     *
     * @param customer the customer that is to be saved
     * @return the id
     * @throws ServiceException the service exception
     */
    public Integer saveCustomer(CustomerDto customer) throws ServiceException;

    /**
     * Gets all saved customers
     *
     * @return A list with all saved customers
     * @throws ServiceException if an error occurs
     */
    List<CustomerDto> getCustomers() throws ServiceException;

    /**
     * Edit a customer
     *
     * @param customerDto the customer to edit
     * @return it's ID
     * @throws ServiceException if there are any exceptions in the server side
     */
    Integer editCustomer(CustomerDto customerDto) throws ServiceException;

    /**
     * Get customer by ID
     * @param ID of the customer
     * @return that customer
     * @throws ServiceException if there are any exceptions in the server side
     */
    CustomerDto getCustomerByID(Integer ID) throws ServiceException;
}
