package at.ac.tuwien.inso.ticketline.client.service;

import at.ac.tuwien.inso.ticketline.client.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.dto.PerformanceDto;

import java.util.List;

public interface PerformanceService {

    /**
     * Gets all saved performances
     *
     * @return A list with all saved performances
     * @throws ServiceException if an error occurs
     */
    List<PerformanceDto> getAll() throws ServiceException;

}
