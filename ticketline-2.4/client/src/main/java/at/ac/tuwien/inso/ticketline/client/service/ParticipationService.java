package at.ac.tuwien.inso.ticketline.client.service;

import at.ac.tuwien.inso.ticketline.client.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.dto.ParticipationDto;

import java.util.List;

public interface ParticipationService {

    /**
     * Gets all saved participations
     *
     * @return A list with all saved participations
     * @throws ServiceException if an error occurs
     */
    List<ParticipationDto> getAll() throws ServiceException;

}
