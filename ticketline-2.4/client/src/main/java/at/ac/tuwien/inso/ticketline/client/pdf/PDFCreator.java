package at.ac.tuwien.inso.ticketline.client.pdf;

import at.ac.tuwien.inso.ticketline.client.util.DateUtil;
import at.ac.tuwien.inso.ticketline.dto.*;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.*;
import java.util.List;

public class PDFCreator {

    private static final Logger LOGGER = LoggerFactory.getLogger(PDFCreator.class);

    private static final String EURO_SIGN = "\u20AC";
    private static final String HEADER_IMAGE_PATH = "src/main/resources/image/ticketlineLogoHeader.png";

    private static final PDFFont NORMAL_FONT = new PDFFont(PDType1Font.HELVETICA, 11, 14);
    private static final PDFFont BOLD_FONT = new PDFFont(PDType1Font.HELVETICA_BOLD, 11, 14);

    private static final int LEFT_OFFSET = 40;
    private static final int RIGHT_OFFSET = (int) PDRectangle.A4.getWidth() - LEFT_OFFSET;
    private static final int TOP_OFFSET = 666;
    private static final int BOTTOM_OFFSET = 50;
    private static final int TOP_OFFSET_TICKET_LIST = 520;
    private static final int BOTTOM_OFFSET_TICKET_LIST = 140;
    private static final int RIGHT_OFFSET_TICKET_LIST_AMOUNT = RIGHT_OFFSET - 40;

    private Stage parentStage;
    private ReceiptDto receipt;

    private int newPremiumPoints;

    private PDDocument document;
    private List<PDPage> pages;
    private Map<Integer, List<PDFContent>> contentMap;
    private boolean storno;

    public PDFCreator(Stage parentStage) {
        this(parentStage, false);
    }

    public PDFCreator(Stage parentStage, boolean storno) {
        this.parentStage = parentStage;
        this.storno = storno;
        contentMap = new HashMap<>();
        pages = new LinkedList<>();
        document = new PDDocument();
    }


    public void createInvoicePDF(ReceiptDto receipt) throws IOException {
        this.receipt = receipt;
        if (receipt == null) {
            return;
        }

        FileChooser fc = new FileChooser();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd__HH-mm-ss");
        fc.setInitialFileName("invoice_" + df.format(new Date(System.currentTimeMillis())) + "-" + ".pdf");
        fc.setInitialDirectory(new File("."));
        fc.setTitle("Rechnung speichern");

        fc.getExtensionFilters().add(new FileChooser.ExtensionFilter("PDF Dateien", "*.pdf"));
        File outputFile = fc.showSaveDialog(parentStage);
        if (outputFile == null) {
            return;
        }


        List<ReceiptentryDto> articles = new LinkedList<>();
        List<ReceiptentryDto> tickets = new LinkedList<>();

        for (ReceiptentryDto entry : receipt.getReceiptentryDto()) {
            if (entry.getArticle() != null) {
                articles.add(entry);
            } else if (entry.getTicketIdentifierDto() != null) {
                tickets.add(entry);
            }
        }

        // Sort Tickets by Performance, Date and Price
        tickets.sort((o1, o2) -> (o1.getTicketIdentifierDto().getTicket().getShow().getPerformance().getName() + " "
                + o1.getTicketIdentifierDto().getTicket().getShow().getDateOfPerformance() + " "
                + o1.getTicketIdentifierDto().getTicket().getPrice()).compareToIgnoreCase(
                o2.getTicketIdentifierDto().getTicket().getShow().getPerformance().getName() + " "
                        + o2.getTicketIdentifierDto().getTicket().getShow().getDateOfPerformance() + " "
                        + o2.getTicketIdentifierDto().getTicket().getPrice()));

        // Sort Articles by Performance and Name
        articles.sort((o1, o2) -> (o1.getArticle().getPerformanceDto().getName() + " " + o1.getArticle().getName()).compareToIgnoreCase(
                o2.getArticle().getPerformanceDto().getName() + " " + o2.getArticle().getName()));


        int actualPage = 0;
        int actualLineHeight = TOP_OFFSET_TICKET_LIST;

        int[] newPosition = addTickets(tickets, actualPage, TOP_OFFSET_TICKET_LIST, actualLineHeight);
        actualPage = newPosition[0];
        actualLineHeight = newPosition[1];

        newPosition = addArticles(articles, actualPage, TOP_OFFSET_TICKET_LIST, actualLineHeight);
        newPosition = setSum(newPosition[0], newPosition[1], tickets, articles);
        newPosition = setPayment(newPosition[0], newPosition[1]);


        setPremiumPoints(receipt.getCustomer(), newPosition[0], newPosition[1]);

        printAllPages();
        document.save(outputFile);
        document.close();

        Desktop.getDesktop().open(outputFile);
    }


    private int[] setPremiumPoints(CustomerDto customerDto, int page, int actualHeight) {
        if (customerDto != null && customerDto.getId() > 1) {
            int customerPremiumPoints;
            try {
                customerPremiumPoints = customerDto.getPremiumPoints();
            } catch (NullPointerException e) {
                customerPremiumPoints = 0;
            }
            if (storno) {
                addContentToPage(page, new PDFText(NORMAL_FONT, LEFT_OFFSET, actualHeight, "Premiumpunktestand: " + customerPremiumPoints + " (-" + newPremiumPoints + ")"));

            } else {
                addContentToPage(page, new PDFText(NORMAL_FONT, LEFT_OFFSET, actualHeight, "Premiumpunktestand: " + (customerPremiumPoints + newPremiumPoints) + " (+" + newPremiumPoints + ")"));
            }
            actualHeight -= NORMAL_FONT.getLineDistance();
        }

        return new int[]{page, actualHeight};
    }

    private int[] setPayment(int page, int actualHeight) {
        String paymentString;
        if (storno) {
            paymentString = "Betrag bar an Kunden ausbezahlt.";
        } else {

            if (receipt.getCreditCardDto() != null) {
                paymentString = "Betrag mit Kreditkarte bezahlt (Besitzer: " + receipt.getCreditCardDto().getOwner() + ", Kartennummer: " + receipt.getCreditCardDto().getCreditcardNumber() + ").";
            } else if (receipt.getBankAccountDto() != null) {
                paymentString = "Betrag mit Bankomat bezahlt (Bank: " + receipt.getBankAccountDto().getBank() + ", Kontonummer: " + receipt.getBankAccountDto().getAccountNumber() + ").";
            } else if (receipt.getCashDto() != null) {
                paymentString = "Betrag bar bezahlt.";
            } else {
                paymentString = "Betrag nicht bezahlt!";
            }
        }

        addContentToPage(page, new PDFText(NORMAL_FONT, LEFT_OFFSET, actualHeight, paymentString));
        actualHeight -= NORMAL_FONT.getLineDistance();
        return new int[]{page, actualHeight};
    }

    private int[] setSum(int page, int actualHeight, List<ReceiptentryDto> tickets, List<ReceiptentryDto> articles) throws IOException {
        int sumPriceTickets = 0;
        int sumPriceArticles = 0;
        int articlesAmount = 0;

        for (ReceiptentryDto entry : tickets) {
            sumPriceTickets += entry.getTicketIdentifierDto().getTicket().getPrice();
        }

        for (ReceiptentryDto entry : articles) {
            sumPriceArticles += entry.getUnitPrice();
            articlesAmount += entry.getAmount();
        }

        actualHeight -= NORMAL_FONT.getLineDistance();

        int sumPriceTicketsNetto = (int) (sumPriceTickets / 1.2);
        int sumPriceArticlesNetto = (int) (sumPriceArticles / 1.2);

        addContentToPage(page, new PDFLine(LEFT_OFFSET, actualHeight + 20, RIGHT_OFFSET, actualHeight + 20, Color.BLACK, 2));
        actualHeight += 5;

        if (tickets.size() > 0) {
            addContentToPage(page, getRightAllignedText(NORMAL_FONT, RIGHT_OFFSET_TICKET_LIST_AMOUNT, actualHeight, tickets.size() + (tickets.size() > 1 ? " Tickets:" : " Ticket:")));
            addContentToPage(page, getRightAllignedText(NORMAL_FONT, RIGHT_OFFSET, actualHeight, (storno ? "-" : "") + sumPriceTicketsNetto + EURO_SIGN));
            actualHeight -= NORMAL_FONT.getLineDistance();
        }

        if (articlesAmount > 0) {
            addContentToPage(page, getRightAllignedText(NORMAL_FONT, RIGHT_OFFSET_TICKET_LIST_AMOUNT, actualHeight, articlesAmount + " Artikel:"));
            addContentToPage(page, getRightAllignedText(NORMAL_FONT, RIGHT_OFFSET, actualHeight, (storno ? "-" : "") + sumPriceArticlesNetto + EURO_SIGN));
            actualHeight -= NORMAL_FONT.getLineDistance();
        }

        addContentToPage(page, getRightAllignedText(NORMAL_FONT, RIGHT_OFFSET_TICKET_LIST_AMOUNT, actualHeight, "+20% Mwst:"));
        addContentToPage(page, getRightAllignedText(NORMAL_FONT, RIGHT_OFFSET, actualHeight, (storno ? "-" : "") + (sumPriceTickets + sumPriceArticles - sumPriceTicketsNetto - sumPriceArticlesNetto) + EURO_SIGN));
        actualHeight -= NORMAL_FONT.getLineDistance();

        actualHeight -= 5;
        addContentToPage(page, new PDFLine(LEFT_OFFSET, actualHeight + 15, RIGHT_OFFSET, actualHeight + 15, Color.BLACK, 2));
        addContentToPage(page, new PDFText(BOLD_FONT, LEFT_OFFSET, actualHeight, "Summe:"));
        addContentToPage(page, getRightAllignedText(BOLD_FONT, RIGHT_OFFSET, actualHeight, (storno ? "-" : "") + (sumPriceArticles + sumPriceTickets) + EURO_SIGN));

        addContentToPage(page, new PDFLine(LEFT_OFFSET, actualHeight - 5, RIGHT_OFFSET, actualHeight - 5, Color.BLACK, 2));
        actualHeight -= NORMAL_FONT.getLineDistance() + 10;

        newPremiumPoints = (sumPriceArticles + sumPriceTickets) / 10;
        return new int[]{page, actualHeight};
    }

    private int[] addTickets(List<ReceiptentryDto> tickets, int page, int minHeight, int actualHeight) throws IOException {

        List<String> ticketStringsList = new LinkedList<>();
        List<String> ticketPriceList = new LinkedList<>();
        List<String> ticketPriceSumList = new LinkedList<>();

        for (int i = 0; i < tickets.size(); i++) {
            ReceiptentryDto entry = tickets.get(i);
            TicketDto ticket = entry.getTicketIdentifierDto().getTicket();
            ShowDto show = ticket.getShow();

            Set<Integer> rows = new HashSet<>();
            int amount = 0;

            for (; i < tickets.size(); i++) {
                TicketDto nextTicket = tickets.get(i).getTicketIdentifierDto().getTicket();

                if (!ticket.getShow().getPerformance().getId().equals(nextTicket.getShow().getPerformance().getId()) || !ticket.getPrice().equals(nextTicket.getPrice())) {
                    i--;
                    break;
                }

                rows.add(nextTicket.getSeat().getRow().getOrder());
                amount++;
            }

            ticketStringsList.add(amount + "x    " + (amount < 10 ? "  " : "") + "Ticket: " + show.getPerformance().getName()
                    + " - " + DateUtil.getEuropeanDateTimeString(show.getDateOfPerformance()));

            ticketStringsList.add("           Ort: " + show.getRoom().getLocation().getName()
                    + " - " + show.getRoom().getName()
                    + " - In " + rows.size() + (rows.size() > 1 ? " Reihen" : " Reihe"));


            ticketPriceList.add("");
            ticketPriceList.add(amount + " x " + (storno ? "-" : "") + ticket.getPrice() + EURO_SIGN + ":");
            ticketPriceSumList.add("");
            ticketPriceSumList.add((storno ? "-" : "") + (amount * ticket.getPrice()) + EURO_SIGN);
        }


        String[] showTicketStrings = new String[ticketStringsList.size()];
        String[] showTicketPriceStrings = new String[ticketPriceList.size()];
        String[] showTicketPriceSumStrings = new String[ticketPriceSumList.size()];

        ticketStringsList.toArray(showTicketStrings);
        ticketPriceList.toArray(showTicketPriceStrings);
        ticketPriceSumList.toArray(showTicketPriceSumStrings);

        setLinesLeftAligned(page, NORMAL_FONT, LEFT_OFFSET, actualHeight, minHeight, BOTTOM_OFFSET_TICKET_LIST, showTicketStrings);
        setLinesRightAligned(page, NORMAL_FONT, RIGHT_OFFSET_TICKET_LIST_AMOUNT, actualHeight, minHeight, BOTTOM_OFFSET_TICKET_LIST, showTicketPriceStrings);
        return setLinesRightAligned(page, NORMAL_FONT, RIGHT_OFFSET, actualHeight, minHeight, BOTTOM_OFFSET_TICKET_LIST, showTicketPriceSumStrings);
    }

    private int[] addArticles(List<ReceiptentryDto> articles, int page, int minHeight, int actualHeight) throws IOException {
        String[] articlesString = new String[articles.size()];
        String[] articlesPriceStrings = new String[articles.size()];
        String[] articlesAmounttrings = new String[articles.size()];

        for (int i = 0; i < articles.size(); i++) {
            ReceiptentryDto entry = articles.get(i);
            MerchandiseDto article = entry.getArticle();

            articlesString[i] = entry.getAmount() + "x     " + (entry.getAmount() < 10 ? "  " : "") + "Artikel: " + article.getPerformanceDto().getName()
                    + " - " + article.getName();

            articlesAmounttrings[i] = entry.getAmount() + " x " + article.getPrice() + EURO_SIGN + ":";
            articlesPriceStrings[i] = entry.getUnitPrice() + EURO_SIGN;
        }

        setLinesLeftAligned(page, NORMAL_FONT, LEFT_OFFSET, actualHeight, minHeight, BOTTOM_OFFSET_TICKET_LIST, articlesString);
        setLinesRightAligned(page, NORMAL_FONT, RIGHT_OFFSET_TICKET_LIST_AMOUNT, actualHeight, minHeight, BOTTOM_OFFSET_TICKET_LIST, articlesAmounttrings);
        return setLinesRightAligned(page, NORMAL_FONT, RIGHT_OFFSET, actualHeight, minHeight, BOTTOM_OFFSET_TICKET_LIST, articlesPriceStrings);

    }

    private int[] setLinesLeftAligned(int pageNumber, PDFFont font, int posX, int posY, String... lines) {
        return setLinesLeftAligned(pageNumber, font, posX, posY, posY, BOTTOM_OFFSET + 20, lines);
    }

    private int[] setLinesLeftAligned(int pageNumber, PDFFont font, int posX, int posY, int minPosY, int maxPosY, String... lines) {
        int newPosY = posY;
        for (String s : lines) {
            if (s == null) {
                continue;
            }
            addContentToPage(pageNumber, new PDFText(font, posX, newPosY, s));

            newPosY -= font.getLineDistance();
            if (newPosY <= maxPosY) {
                pageNumber++;
                newPosY = minPosY;
            }
        }
        return new int[]{pageNumber, newPosY};
    }

    private int[] setLinesRightAligned(int pageNumber, PDFFont font, int posX, int posY, String... lines) throws IOException {
        return setLinesRightAligned(pageNumber, font, posX, posY, posY, BOTTOM_OFFSET + 20, lines);
    }

    private int[] setLinesRightAligned(int pageNumber, PDFFont font, int posX, int posY, int minPosY, int maxPosY, String... lines) throws IOException {
        int newPosY = posY;
        for (String s : lines) {
            addContentToPage(pageNumber, getRightAllignedText(font, posX, newPosY, s));

            newPosY -= font.getLineDistance();
            if (newPosY <= maxPosY) {
                pageNumber++;
                newPosY = minPosY;
            }

        }
        return new int[]{pageNumber, newPosY};
    }

    private PDFText getRightAllignedText(PDFFont font, int posX, int posY, String s) throws IOException {
        return new PDFText(font, posX - getTextWidth(font, s), posY, s);
    }

    private void addContentToPage(int pageNumber, PDFContent content) {
        contentMap.putIfAbsent(pageNumber, new LinkedList<>());
        contentMap.get(pageNumber).add(content);
    }

    private int getTextWidth(PDFFont font, String s) throws IOException {
        return (int) ((font.getFont().getStringWidth(s) / 1000.0f) * font.getSize());
    }


    private void printAllPages() throws IOException {
        String dateString = "Datum: " + DateUtil.getEuropeanDateString(LocalDateTime.now());
        String customerNR;
        try {
            int id = receipt.getCustomer().getId();
            if (id <= 1) {
                throw new NullPointerException();
            }
            customerNR = receipt.getCustomer().getId() + "";
        } catch (NullPointerException e) {
            customerNR = "-";
        }

        UserStatusDto employee = receipt.getEmployee();
        CustomerDto customer = receipt.getCustomer();

        String employeeName;
        try {
            employeeName = employee.getFirstName() + " " + employee.getLastName();
        } catch (NullPointerException e) {
            employeeName = " ";
        }

        String[] ticketLineAddress = {employeeName, "Ticketline GmbH", "Karlsplatz 1", "1040 Wien", "Österreich"};
        String[] customerAddress = null;

        if (customer != null && customer.getId() > 1) {
            customerAddress = new String[]{customer.getGender().toLowerCase().equals("male") ? "Herr" : "Frau",
                    customer.getFirstname() + " " + customer.getLastname(),
                    customer.getStreet(),
                    customer.getPostalcode() + " " + customer.getCity(),
                    customer.getCountry()
            };
        }


        for (int i = 0; i < contentMap.size(); i++) {
            PDPageContentStream cos = addPage();
            addHeaderImage(i);
            addInvoiceDetails(i, dateString, customerNR, customerAddress, ticketLineAddress);
            addPageNumber(i);
            printPage(cos, i);
            cos.close();
        }
    }

    private void printPage(PDPageContentStream cos, int pageIndex) throws IOException {
        if (cos == null) {
            // TODO
            throw new NullPointerException();
        }

        for (PDFContent t : contentMap.get(pageIndex)) {
            t.print(cos);
        }
    }

    private void addHeaderImage(int pageNumber) {
        try {
            PDImageXObject image = PDImageXObject.createFromFile(HEADER_IMAGE_PATH, document);

            float imgWidth = PDRectangle.A4.getWidth() - 20;
            float imgHeight = image.getHeight() * imgWidth / image.getWidth();

            int posX = 10;
            int posY = (int) PDRectangle.A4.getHeight() - image.getHeight() - 10;

            addContentToPage(pageNumber, new PDFImage(image, posX, posY, imgWidth, imgHeight));

        } catch (IOException e) {
            LOGGER.error("Header Image for PDF-Invoice not found!");
        }
    }

    private void addInvoiceDetails(int pageNumber, String dateString, String customerNR, String[] customerAddress, String[] ticketLineAddress) throws IOException {
        addContentToPage(pageNumber, getRightAllignedText(BOLD_FONT, RIGHT_OFFSET, TOP_OFFSET + 20, dateString));
        addContentToPage(pageNumber, new PDFText(BOLD_FONT, LEFT_OFFSET, TOP_OFFSET + 20, "Kundennummer: " + customerNR));

        addContentToPage(pageNumber, new PDFText(new PDFFont(PDType1Font.HELVETICA_BOLD, 20, 25), LEFT_OFFSET, TOP_OFFSET - 90, (storno ? "Storno-Rechnung" : "Rechnung")));
        addContentToPage(pageNumber, new PDFLine(LEFT_OFFSET, TOP_OFFSET - 100, RIGHT_OFFSET, TOP_OFFSET - 100, Color.BLACK, 3));

        setLinesRightAligned(pageNumber, NORMAL_FONT, RIGHT_OFFSET, TOP_OFFSET, ticketLineAddress);
        if (customerAddress != null) {
            setLinesLeftAligned(pageNumber, NORMAL_FONT, LEFT_OFFSET, TOP_OFFSET, customerAddress);
        }
    }

    private void addPageNumber(int pageNumber) {
        addContentToPage(pageNumber, new PDFText(NORMAL_FONT, RIGHT_OFFSET, BOTTOM_OFFSET, String.valueOf(pageNumber + 1)));
    }


    private PDPageContentStream addPage() {
        PDPage newPage = new PDPage(PDRectangle.A4);
        pages.add(newPage);
        document.addPage(newPage);

        PDPageContentStream cos = null;
        try {
            cos = new PDPageContentStream(document, newPage);

        } catch (IOException ioex) {
        }
        return cos;
    }


}