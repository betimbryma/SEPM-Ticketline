package at.ac.tuwien.inso.ticketline.client.util;

//import at.ac.tuwien.inso.ticketline.client.exception.ServiceException;
//import at.ac.tuwien.inso.ticketline.client.exception.ValidationException;

import at.ac.tuwien.inso.ticketline.client.exception.ValidationException;
import at.ac.tuwien.inso.ticketline.client.gui.controller.MainController;
import at.ac.tuwien.inso.ticketline.dto.CustomerDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.Locale;

/**
 * This class validates the User input for creating or editing a customer
 */
public class CustomerDataValidator {

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomerDataValidator.class);


    public static CustomerDto validateAndCreate(String firstname, String lastname, String street,
                                                String postalcode, String city, String country,
                                                String gender, LocalDate localbirthdate,
                                                String email, String phonenumber) throws ValidationException {

        if (!UserInputValidator.validatePersonName(firstname, false)) {
            LOGGER.error("Customers Firstname is invalid");
            if (MainController.currentLocale.equals(Locale.ENGLISH)) {
                DialogUtil.showErrorDialog("Error creating customer",
                        "The customers firstname is empty or invalid",
                        "Please write the customers firstname into the according textfield.");
            } else {
                DialogUtil.showErrorDialog("Fehler beim Erstellen eines neuen Kunden!",
                        "Der Vorname des Kunden ist leer oder ung\u00fcltig.",
                        "Bitte tragen Sie den Vornamen des Kunden in das entsprechende Feld ein.");
            }
            throw new ValidationException("No firstname specified");
        }

        if (!UserInputValidator.validatePersonName(lastname, false)) {
            LOGGER.error("Customers Lastname is invalid");
            if (MainController.currentLocale.equals(Locale.ENGLISH)) {
                DialogUtil.showErrorDialog("Error creating customer",
                        "The customers lastname is empty or invalid",
                        "Please write the customers lastname into the according textfield.");
            } else {
                DialogUtil.showErrorDialog("Fehler beim Erstellen eines neuen Kunden!",
                        "Der Nachname des Kunden ist leer oder ung\u00fcltig.",
                        "Bitte tragen Sie den Nachnamen des Kunden in das entsprechende Feld ein.");
            }
            throw new ValidationException("No lastname specified");
        }

        if (!UserInputValidator.validateNonEmptyString(street)) {
            LOGGER.error("Customers Street has not been specified");
            if (MainController.currentLocale.equals(Locale.ENGLISH)) {
                DialogUtil.showErrorDialog("Error creating customer",
                        "The street where the customer lives must be specified",
                        "Please write the street where the customer lives into the according textfield");
            } else {
                DialogUtil.showErrorDialog("Fehler beim Erstellen eines neuen Kunden!",
                        "Die Straße, wo der Kunde wohnt, muss eingegeben werden.",
                        "Bitte tragen Sie die Straße, wo der Kunde wohnt, in das entsprechende Feld ein.");
            }
            throw new ValidationException("No street specified");
        }

        if (!UserInputValidator.validateNoNumbersString(city, false)) {
            LOGGER.error("Customers City has not been specified");
            if (MainController.currentLocale.equals(Locale.ENGLISH)) {
                DialogUtil.showErrorDialog("Error creating customer",
                        "The city where the customer lives must be specified",
                        "Please write the city where the customer lives into the according textfield");
            } else {
                DialogUtil.showErrorDialog("Fehler beim Erstellen eines neuen Kunden!",
                        "Die Stadt, wo der Kunden wohnt, muss eingegeben werden.",
                        "Bitte tragen Sie die Stadt, wo der Kunden lebt, in das entsprechende Feld ein.");
            }
            throw new ValidationException("No firstname specified");
        }

        if (!UserInputValidator.validateNonEmptyString(postalcode)) {
            LOGGER.error("Customers Postal Code has not been specified");
            if (MainController.currentLocale.equals(Locale.ENGLISH)) {
                DialogUtil.showErrorDialog("Error creating customer",
                        "The customers postal code must be specified",
                        "Please write the customers postal code into the according textfield");
            } else {
                DialogUtil.showErrorDialog("Fehler beim Erstellen eines neuen Kunden!",
                        "Die Postleitzahl des Kunden muss eingegeben werden.",
                        "Bitte tragen Sie die Postleitzahl des Kunden in das entsprechende Feld ein.");
            }
            throw new ValidationException("No postal code specified");
        }

        if (!UserInputValidator.validateNoNumbersString(country, false)) {
            LOGGER.error("Customers Country has not been specified");
            if (MainController.currentLocale.equals(Locale.ENGLISH)) {
                DialogUtil.showErrorDialog("Error creating customer",
                        "The country where the customer lives must be specified",
                        "Please write the country where the customer lives into the according textfield");
            } else {
                DialogUtil.showErrorDialog("Fehler beim Erstellen eines neuen Kunden!",
                        "Das Land, wo der Kunden wohnt, muss eingegeben werden.",
                        "Bitte tragen Sie das Land, wo der Kunden lebt, in das entsprechende Feld ein.");
            }
            throw new ValidationException("No country specified");
        }

        //convert from localDate to Date
        Date birthdate = null;

        if (localbirthdate != null) {
            if (localbirthdate.isAfter(LocalDate.now())) {
                LOGGER.error("Birthday is set to a future day!");
                if (MainController.currentLocale.equals(Locale.ENGLISH)) {
                    DialogUtil.showErrorDialog("Error creating customer",
                            "The Birthdate cannot be in the future!", "");
                } else {
                    DialogUtil.showErrorDialog("Fehler beim Erstellen eines neuen Kunden!",
                            "Das Geburtsdatum kann nicht in der Zukunft liegen!", "");
                }

                throw new ValidationException("Invalid birthdate specified");
            }
            String year = "" + localbirthdate.getYear();
            String month = "" + localbirthdate.getMonthValue();
            String day = "" + localbirthdate.getDayOfMonth();

            if (localbirthdate.getMonthValue() < 10) {
                month = "0" + month;
            }
            if (localbirthdate.getDayOfMonth() < 10) {
                day = "0" + day;
            }

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
            try {
                birthdate = formatter.parse(year + "/" + month + "/" + day);
            } catch (ParseException e) {
                LOGGER.error("Could not parse Birthdate");
                if (MainController.currentLocale.equals(Locale.ENGLISH)) {
                    DialogUtil.showErrorDialog("Error creating customer",
                            "The birthdate is invalid",
                            "Please choose a valid date using the date picker");
                } else {
                    DialogUtil.showErrorDialog("Fehler beim Erstellen eines neuen Kunden!",
                            "Das Geburtsdatum ist ung\u00fcltig.",
                            "Bitte wählen Sie ein g\u00fcltiges Datum mit der Datumsauswahl.");
                }
                throw new ValidationException("Invalid birthdate specified");
            }
        }

        if (!UserInputValidator.validateEmail(email, true)) {
            LOGGER.error("Customers Emailadress is invalid");
            if (MainController.currentLocale.equals(Locale.ENGLISH)) {
                DialogUtil.showErrorDialog("Error creating customer",
                        "The customers email adress is invalid", "");
            } else {
                DialogUtil.showErrorDialog("Fehler beim Erstellen eines neuen Kunden!",
                        "Die E-Mail des Kunden ist ungültig.", "");
            }
            throw new ValidationException("Invalid Email address");
        }

        CustomerDto customer = new CustomerDto();
        customer.setFirstname(firstname);
        customer.setLastname(lastname);
        customer.setStreet(street);
        customer.setPostalcode(postalcode);
        customer.setCity(city);
        customer.setCountry(country);
        customer.setGender(gender);
        customer.setDateOfBirth(birthdate);
        customer.setEmail(email);
        customer.setPhoneNumber(phonenumber);

        LOGGER.info("Customer is valid");

        return customer;
    }


}