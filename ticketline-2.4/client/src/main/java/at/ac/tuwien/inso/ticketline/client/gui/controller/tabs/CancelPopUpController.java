package at.ac.tuwien.inso.ticketline.client.gui.controller.tabs;

import at.ac.tuwien.inso.ticketline.client.gui.controller.tabs.search.views.TicketCircle;
import at.ac.tuwien.inso.ticketline.client.service.ReservationService;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

/**
 * Created by Betim on 6/22/2016.
 */
@Component
public class CancelPopUpController {
    @FXML
    private BorderPane borderPane;
    @FXML
    private Label sold;

    private ArrayList<String> sekuencat;
    private ArrayList<String> numberSequence;
    @FXML
    private Label inCart;

    private ArrayList<TicketCircle> selected;
    private Stage stage;
    private ScrollPane scrollPane;

    @FXML
    private Label reserved;


    @Autowired
    ReservationService reservationService;

    //@Autowired
    CancelController cancelController;

    public void setCancelController(CancelController cancelController) {
        this.cancelController = cancelController;
    }

    public void setScrollPane(ScrollPane scrollPane) {
        this.borderPane.setCenter(scrollPane);
        this.scrollPane = scrollPane;
    }




    public ArrayList<String> getSekuencat() {
        return sekuencat;
    }

    public void setSekuencat(ArrayList<String> sekuencat) {
        this.sekuencat = sekuencat;
    }

    public ArrayList<String> getNumberSequence() {
        return numberSequence;
    }

    public void setNumberSequence(ArrayList<String> numberSequence) {
        this.numberSequence = numberSequence;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
        stage.setOnCloseRequest(e -> cancelController.setScrollPane(scrollPane));
    }


    @FXML
    public void onCancel() {
        if (cancelController != null) {
            cancelController.CancelSelectedButton();
        }
    }

    @FXML
    private void onSell() {
        if (cancelController != null) {
            cancelController.AddToCartSelectedButton();
        }
    }

    public void setSold(String sold) {
        this.sold.setText(sold);
    }

    public void setInCart(String inCart) {
        this.inCart.setText(inCart);
    }

    public void setReserved(String reserved) {
        this.reserved.setText(reserved);
    }
}
