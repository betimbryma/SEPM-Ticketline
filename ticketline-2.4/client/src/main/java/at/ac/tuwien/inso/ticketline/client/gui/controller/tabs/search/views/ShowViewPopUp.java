package at.ac.tuwien.inso.ticketline.client.gui.controller.tabs.search.views;

import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import org.springframework.stereotype.Component;

/**
 * Created by Betim on 6/20/2016.
 */
@Component
public class ShowViewPopUp {

    @FXML
    private BorderPane borderPane;
    @FXML
    private Label inCart;
    @FXML
    private Label sold;

    @FXML
    private Button cancel;
    @FXML
    private Button sell;

    @FXML
    private Label reserved;

    private Stage stage;

    private ScrollPane scrollPane;

    private ShowViewController showViewController;

    private Button button;

    @FXML
    private void onCancel(){
        if(showViewController != null){
            button.fire();
        }
    }

    @FXML
    private void onSell(){
        if(showViewController != null){
            showViewController.onAddToCartClicked();
        }
    }

    public void setButton(Button button){
        this.button = button;
    }

    public void setSold(String sold) {
        this.sold.setText(sold);
    }

    public void setInCart(String inCart) {
        this.inCart.setText(inCart);
    }

    public void setReserved(String reserved) {
        this.reserved.setText(reserved);
    }

    public void setScrollPane(ScrollPane scrollPane){

        this.scrollPane = scrollPane;
        this.borderPane.setCenter(scrollPane);
        //Event.fireEvent(borderPane, new MouseEvent(MouseEvent.MOUSE_CLICKED, 0, 0, 0, 0, MouseButton.PRIMARY, 1, true, true, true, true, true, true, true, true, true, true, null));
        //borderPane.requestLayout();
        //.requestFocus();
    }

    public void setShowViewController(ShowViewController showViewController) {
        this.showViewController = showViewController;
    }

    public void setStage(Stage stage){
        this.stage=stage;
        stage.setOnCloseRequest(e -> showViewController.setScrollPane(scrollPane));
    }


}
