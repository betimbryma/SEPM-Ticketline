package at.ac.tuwien.inso.ticketline.client.service.rest;

import at.ac.tuwien.inso.ticketline.client.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.client.exception.ValidationException;
import at.ac.tuwien.inso.ticketline.client.service.PremiumService;
import at.ac.tuwien.inso.ticketline.dto.MessageDto;
import at.ac.tuwien.inso.ticketline.dto.PremiumDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.Collections;
import java.util.List;

/**
 * Created by eni on 04.06.2016.
 */
@Component
public class PremimuRestClient implements PremiumService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PremimuRestClient.class);
    public static final String CEHCK_PREMIUMS = "/service/premium/checkPremium/";
    public static final String Return_PREMIUMS = "/service/premium/ReturnPremium/";
    public static final String Premiums = "/service/premium/premiums/";

    @Autowired
    private RestClient restClient;

    @Override
    public PremiumDto searchPremium(PremiumDto merchandiseDto) throws ServiceException {
        RestTemplate restTemplate = this.restClient.getRestTemplate();
        String url = this.restClient.createServiceUrl(CEHCK_PREMIUMS);
        LOGGER.info("Reserve tickets {}", url);
        HttpHeaders headers = this.restClient.getHttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        HttpEntity<PremiumDto> entity = new HttpEntity<>(merchandiseDto, headers);
        PremiumDto merchandiseDto2;
        try {
            ParameterizedTypeReference<PremiumDto> parameterizedTypeReference = new ParameterizedTypeReference<PremiumDto>() {};
            ResponseEntity<PremiumDto> responseEntity = restTemplate.exchange(url, HttpMethod.POST, entity, parameterizedTypeReference);
            merchandiseDto2 = responseEntity.getBody();
        } catch (HttpStatusCodeException e) {
            MessageDto errorMsg = this.restClient.mapExceptionToMessage(e);
            if (errorMsg.hasFieldErrors()) {
                throw new ValidationException(errorMsg.getFieldErrors());
            } else {
                throw new ServiceException(errorMsg.getText());
            }
        } catch (RestClientException e) {
            throw new ServiceException("Could not reserve tickets: " + e.getMessage(), e);
        }

        return merchandiseDto2;
    }
    @Override
    public PremiumDto returnPremium(PremiumDto merchandiseDto) throws ServiceException {
        RestTemplate restTemplate = this.restClient.getRestTemplate();
        String url = this.restClient.createServiceUrl(Return_PREMIUMS);
        LOGGER.info("Reserve tickets {}", url);
        HttpHeaders headers = this.restClient.getHttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        HttpEntity<PremiumDto> entity = new HttpEntity<>(merchandiseDto, headers);
        PremiumDto merchandiseDto2;
        try {
            ParameterizedTypeReference<PremiumDto> parameterizedTypeReference = new ParameterizedTypeReference<PremiumDto>() {};
            ResponseEntity<PremiumDto> responseEntity = restTemplate.exchange(url, HttpMethod.POST, entity, parameterizedTypeReference);
            merchandiseDto2 = responseEntity.getBody();
        } catch (HttpStatusCodeException e) {
            MessageDto errorMsg = this.restClient.mapExceptionToMessage(e);
            if (errorMsg.hasFieldErrors()) {
                throw new ValidationException(errorMsg.getFieldErrors());
            } else {
                throw new ServiceException(errorMsg.getText());
            }
        } catch (RestClientException e) {
            throw new ServiceException("Could not reserve tickets: " + e.getMessage(), e);
        }

        return merchandiseDto2;
    }

    @Override
    public List<PremiumDto> getPremiums() throws ServiceException {
        RestTemplate restTemplate = this.restClient.getRestTemplate();
        String url = this.restClient.createServiceUrl(Premiums);
        HttpEntity<String> entity = new HttpEntity<>(this.restClient.getHttpHeaders());
        LOGGER.info("Retrieving news from {}", url);
        List<PremiumDto> premium;
        try {
            ParameterizedTypeReference<List<PremiumDto>> ref = new ParameterizedTypeReference<List<PremiumDto>>() {};
            ResponseEntity<List<PremiumDto>> response = restTemplate.exchange(URI.create(url), HttpMethod.GET, entity, ref);
            premium = response.getBody();

        } catch (RestClientException e) {
            throw new ServiceException("Could not retrieve news: " + e.getMessage(), e);
        }
        return premium;
    }
}
