package at.ac.tuwien.inso.ticketline.client.util;

import java.time.LocalDate;

public class UserInputValidator {

    private static final String UPPER_CASE_SPECIAL_ALPHABETIC_CHARS = "\u00c0\u00c1\u00c2\u00c3\u00c4\u00c5" +
            "\u00c6\u00c7\u00c8\u00c9\u00ca\u00cb\u00cc\u00cd\u00ce\u00cf\u00d1\u00d2\u00d3\u00d4\u00d5\u00d6" +
            "\u00d9\u00da\u00db\u00dc\u00dd";
    private static final String LOWER_CASE_SPECIAL_ALPHABETIC_CHARS = "\u00df\u00e0\u00e1\u00e2\u00e3\u00e4\u00e5\u00e6" +
            "\u00e7\u00e8\u00e9\u00ea\u00eb\u00ec\u00ed\u00ee\u00ef\u00f1\u00f2\u00f3\u00f4\u00f5\u00f6\u00f8\u00f9" +
            "\u00fa\u00fb\u00fc\u00fd\u00ff";
    private static final String ALL_SPECIAL_ALPHABETIC_CHARS = UPPER_CASE_SPECIAL_ALPHABETIC_CHARS + LOWER_CASE_SPECIAL_ALPHABETIC_CHARS;


    public static boolean validateEmail(String email, boolean nullAllowed) {
        if (email == null || email.trim().length() == 0) {
            return nullAllowed;
        }
        return email.trim().matches("[A-Za-z0-9](\\.?([A-Za-z0-9]\\w)*[A-Za-z0-9])*@[A-Za-z0-9](\\.?([A-Za-z0-9]\\w)*[A-Za-z0-9])*\\.[A-Za-z0-9]{2,}");
    }

    public static boolean validatePersonName(String name, boolean nullAllowed) {
        if (name == null || name.trim().length() == 0) {
            return nullAllowed;
        }
        return name.trim().matches("[A-Za-z" + ALL_SPECIAL_ALPHABETIC_CHARS + "]+([\\s|\\-][A-Za-z" + ALL_SPECIAL_ALPHABETIC_CHARS + "]+)*");
    }

    public static boolean validateNonEmptyString(String s) {
        return s != null && s.trim().length() > 0;
    }

    public static boolean validateNoNumbersString(String s, boolean nullAllowed) {
        if (s == null || s.trim().length() == 0) {
            return nullAllowed;
        }
        return s.trim().matches("[A-Za-z \\-" + ALL_SPECIAL_ALPHABETIC_CHARS + "]+");
    }

    public static boolean validatePhoneNumber(String pn, boolean nullAllowed) {
        if (pn == null || pn.trim().length() == 0) {
            return nullAllowed;
        }
        return pn.trim().matches("(\\+ ?)?([0-9]+((\\-| )[0-9])?|( |\\-)?\\([0-9]+(\\-[0-9]+)?\\)( |\\-[0-9])?)+");
    }

    public static boolean validateBirthday(LocalDate date, boolean nullAllowed) {
        if (date == null) {
            return nullAllowed;
        }

        return date.isBefore(LocalDate.now());
    }

    public static boolean validateNumbersWithSpacesOnly(String numberString, boolean nullAllowed) {
        if (numberString == null || numberString.trim().length() == 0) {
            return nullAllowed;
        }
        return numberString.trim().matches("([0-9]([\\-|\\ ][0-9])?)+");
    }

    public static boolean validateIBAN(String iban, boolean nullAllowed) {
        if (iban == null || iban.trim().length() == 0) {
            return nullAllowed;
        }

        return iban.trim().matches("[A-Za-z]{2}([0-9]\\ ?){2,30}");
    }

    public static boolean validateBIC(String bic, boolean nullAllowed) {
        if (bic == null || bic.trim().length() == 0) {
            return nullAllowed;
        }

        return bic.trim().matches("[A-Za-z]{6}[A-Za-z1-9][A-NP-Za-np-z0-9](XXX|[A-WY-Za-wy-z0-9][A-Za-z0-9]{2})?");
    }

    public static boolean validateAlphanumericValueWithSpaces(String s, boolean nullAllowed) {
        if (s == null || s.trim().length() == 0) {
            return nullAllowed;
        }
        return s.trim().matches("[A-Za-z" + ALL_SPECIAL_ALPHABETIC_CHARS + "0-9\\ ]+");
    }
}
