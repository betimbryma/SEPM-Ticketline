package at.ac.tuwien.inso.ticketline.client.service;

import at.ac.tuwien.inso.ticketline.client.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.dto.*;

import java.util.List;

public interface ShowService {

    /**
     * Gets all saved shows
     *
     * @return A list with all saved shows
     * @throws ServiceException if an error occurs
     */
    List<ShowDto> getAll() throws ServiceException;

    List<MapEntryDto<ShowDto,List<TicketIdentifierDto>>> getShowsForCustomer(CustomerDto customerDto) throws ServiceException;

    List<TicketIdentifierDto> getTicketsForShowAndCustomer(List<TicketDto> ticketDtoList) throws ServiceException;

}
