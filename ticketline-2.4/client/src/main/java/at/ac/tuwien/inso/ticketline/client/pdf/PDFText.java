package at.ac.tuwien.inso.ticketline.client.pdf;

import org.apache.pdfbox.pdmodel.PDPageContentStream;

import java.io.IOException;

public class PDFText implements PDFContent {

    private PDFFont font;
    private int posX;
    private int posY;
    private String text;

    public PDFText(PDFFont font, int posX, int posY, String text) {
        this.font = font;
        this.posX = posX;
        this.posY = posY;
        this.text = text;
    }

    public void print(PDPageContentStream cos) throws IOException {
        cos.beginText();
        cos.setFont(font.getFont(), font.getSize());
        cos.newLineAtOffset(posX, posY);
        cos.showText(text);
        cos.endText();

    }
}