package at.ac.tuwien.inso.ticketline.client.gui.controller.shoppingCart;

import at.ac.tuwien.inso.ticketline.client.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.client.gui.controller.LoginController;
import at.ac.tuwien.inso.ticketline.client.gui.controller.MainController;
import at.ac.tuwien.inso.ticketline.client.gui.controller.tabs.MerchandiseController;
import at.ac.tuwien.inso.ticketline.client.gui.controller.tabs.search.SearchController;
import at.ac.tuwien.inso.ticketline.client.pdf.PDFCreator;
import at.ac.tuwien.inso.ticketline.client.service.CustomerService;
import at.ac.tuwien.inso.ticketline.client.service.ReceiptService;
import at.ac.tuwien.inso.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.ticketline.client.util.DialogUtil;
import at.ac.tuwien.inso.ticketline.client.util.SpringFxmlLoader;
import at.ac.tuwien.inso.ticketline.client.util.UserInputValidator;
import at.ac.tuwien.inso.ticketline.dto.*;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.controlsfx.validation.ValidationResult;
import org.controlsfx.validation.ValidationSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;

@Component
public class SellController implements Initializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(SellController.class);

    @Autowired
    private SpringFxmlLoader springFxmlLoader;
    @Autowired
    private MainController mainController;
    @Autowired
    private SearchController searchController;

    @FXML
    private Button selectCustomerBtn, newCustomerBtn;

    @FXML
    Label label_forename;
    @FXML
    Label label_lastname;
    @FXML
    Label label_street;
    @FXML
    Label label_postalcode;
    @FXML
    Label label_city;
    @FXML
    Label label_country;
    @FXML
    Label label_gender;
    @FXML
    Label label_birthdate;
    @FXML
    Label label_email;
    @FXML
    Label label_phone;
    @FXML
    private CheckBox checkAnonym;
    private CustomerDto customerDto;

    @FXML
    private TitledPane sellArticles;

    @FXML
    private Label titlePayment, bicL, ibanL, accountnL, bankownerL, creditownerL, ccNL, ccTL, bankL, bankCodeL;

    @FXML
    private Label fnameL, lnameL, streetL, pcL, cityL, countryL, genderL, birthL, emailL, phoneL;

    @FXML
    private Button sellBtn;

    /*Method of Payment*/
    @FXML
    private RadioButton bankAccount;
    @FXML
    private RadioButton creditCard;
    @FXML
    private RadioButton cash;


    /*Bank Account  */
    @FXML
    private TextField bIC;
    @FXML
    private TextField iBAN;
    @FXML
    private TextField account_Number;
    @FXML
    private TextField bank;
    @FXML
    private TextField bank_Code;
    @FXML
    private TextField owner_Bank_Account;

    /*Credit Card   */
    @FXML
    private TextField credit_Card_Number;
    @FXML
    private TextField owner_Credit_card;
    @FXML
    private RadioButton visa;
    @FXML
    private RadioButton mastercard;
    @FXML
    private RadioButton aexpress;


    @FXML
    private Button backBtn;

    @Autowired
    private MerchandiseController merchandiseController;
    @Autowired
    private ReceiptService receiptService;
    @Autowired
    private CustomerService customerService;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        selectMethod();
        reinitLocale(MainController.currentLocale);
        for (TicketDto t : receiptService.getTicketCart()) {
            if (t.getOldTE() != null) {
                if (t.getOldTE().getReservation() != null) {
                    if (t.getOldTE().getReservation().getCustomer() != null) {
                        setCustomer(t.getOldTE().getReservation().getCustomer());
                        this.customerDto = t.getOldTE().getReservation().getCustomer();
                    }
                }
            }
        }

        // debit card
        new ValidationSupport().registerValidator(bIC, false, (Control c, String s) ->
                ValidationResult.fromErrorIf(c, BundleManager.getBundle().getString("sell.invalidBIC"), !UserInputValidator.validateBIC(s, true)));

        new ValidationSupport().registerValidator(iBAN, false, (Control c, String s) ->
                ValidationResult.fromErrorIf(c, BundleManager.getBundle().getString("sell.invalidIBAN"), !UserInputValidator.validateIBAN(s, true)));

        new ValidationSupport().registerValidator(account_Number, true, (Control c, String s) ->
                ValidationResult.fromErrorIf(c, BundleManager.getBundle().getString("sell.numbersOnlyErrorMsg"), !UserInputValidator.validateNumbersWithSpacesOnly(s, true)));

        new ValidationSupport().registerValidator(bank, true, (Control c, String s) ->
                ValidationResult.fromErrorIf(c, BundleManager.getBundle().getString("sell.invalidBankName"), !UserInputValidator.validateAlphanumericValueWithSpaces(s, false)));

        new ValidationSupport().registerValidator(bank_Code, true, (Control c, String s) ->
                ValidationResult.fromErrorIf(c, BundleManager.getBundle().getString("sell.invalidBankNumber"), !UserInputValidator.validateNonEmptyString(s)));

        new ValidationSupport().registerValidator(owner_Bank_Account, false, (Control c, String s) ->
                ValidationResult.fromErrorIf(c, BundleManager.getBundle().getString("sell.invalidBankOwner"), !UserInputValidator.validatePersonName(s, true)));

        // credit card
        new ValidationSupport().registerValidator(credit_Card_Number, true, (Control c, String s) ->
                ValidationResult.fromErrorIf(c, BundleManager.getBundle().getString("sell.numbersOnlyErrorMsg"), !UserInputValidator.validateNumbersWithSpacesOnly(s, false)));

        new ValidationSupport().registerValidator(owner_Credit_card, true, (Control c, String s) ->
                ValidationResult.fromErrorIf(c, BundleManager.getBundle().getString("sell.numbersOnlyErrorMsg"), !UserInputValidator.validatePersonName(s, false)));


    }

    public void reinitLocale(Locale newValue) {
        LocaleContextHolder.setLocale(newValue);
        BundleManager.changeLocale(newValue);
        LocaleContextHolder.setLocale(newValue);
        BundleManager.changeLocale(newValue);
        sellArticles.setText(BundleManager.getBundle().getString("shoppingcartSell.title"));

        backBtn.setText(BundleManager.getBundle().getString("shoppingcartSell.backB"));
        sellBtn.setText(BundleManager.getBundle().getString("shoppingcartSell.sellB"));
        selectCustomerBtn.setText(BundleManager.getBundle().getString("shoppingcartSell.selectB"));
        newCustomerBtn.setText(BundleManager.getBundle().getString("shoppingcartSell.createB"));

        titlePayment.setText(BundleManager.getBundle().getString("shoppingcartSell.titlePayment"));
        bicL.setText(BundleManager.getBundle().getString("shoppingcartSell.bic"));
        ibanL.setText(BundleManager.getBundle().getString("shoppingcartSell.iban"));
        accountnL.setText(BundleManager.getBundle().getString("shoppingcartSell.accnumber"));
        bankownerL.setText(BundleManager.getBundle().getString("shoppingcartSell.BankOwner"));
        creditownerL.setText(BundleManager.getBundle().getString("shoppingcartSell.CreditOwner"));
        ccNL.setText(BundleManager.getBundle().getString("shoppingcartSell.ccN"));
        ccTL.setText(BundleManager.getBundle().getString("shoppingcartSell.ccT"));
        bankL.setText(BundleManager.getBundle().getString("shoppingcartSell.bank"));
        bankCodeL.setText(BundleManager.getBundle().getString("shoppingcartSell.bankcode"));

        fnameL.setText(BundleManager.getBundle().getString("shoppingcartSell.fn"));
        lnameL.setText(BundleManager.getBundle().getString("shoppingcartSell.ln"));
        streetL.setText(BundleManager.getBundle().getString("shoppingcartSell.street"));
        pcL.setText(BundleManager.getBundle().getString("shoppingcartSell.pc"));
        cityL.setText(BundleManager.getBundle().getString("shoppingcartSell.city"));
        countryL.setText(BundleManager.getBundle().getString("shoppingcartSell.country"));
        genderL.setText(BundleManager.getBundle().getString("shoppingcartSell.gender"));
        birthL.setText(BundleManager.getBundle().getString("shoppingcartSell.birth"));
        emailL.setText(BundleManager.getBundle().getString("shoppingcartSell.email"));
        phoneL.setText(BundleManager.getBundle().getString("shoppingcartSell.phone"));

        checkAnonym.setText(BundleManager.getBundle().getString("shoppingcartSell.sellanonym"));

        bankAccount.setText(BundleManager.getBundle().getString("shoppingcartSell.bankaccountRB"));
        creditCard.setText(BundleManager.getBundle().getString("shoppingcartSell.ccRB"));
        cash.setText(BundleManager.getBundle().getString("shoppingcartSell.cashRB"));
        visa.setText(BundleManager.getBundle().getString("shoppingcartSell.visaRB"));
        mastercard.setText(BundleManager.getBundle().getString("shoppingcartSell.masterRB"));
        aexpress.setText(BundleManager.getBundle().getString("shoppingcartSell.expressRB"));

        bIC.setPromptText(BundleManager.getBundle().getString("shoppingcartSell.bicTF"));
        iBAN.setPromptText(BundleManager.getBundle().getString("shoppingcartSell.ibanTF"));
        account_Number.setPromptText(BundleManager.getBundle().getString("shoppingcartSell.accnumberTF"));
        bank.setPromptText(BundleManager.getBundle().getString("shoppingcartSell.bankTF"));
        bank_Code.setPromptText(BundleManager.getBundle().getString("shoppingcartSell.bankcodeTF"));
        owner_Bank_Account.setPromptText(BundleManager.getBundle().getString("shoppingcartSell.BankOwnerTF"));
        credit_Card_Number.setPromptText(BundleManager.getBundle().getString("shoppingcartSell.ccNTF"));
        owner_Credit_card.setPromptText(BundleManager.getBundle().getString("shoppingcartSell.CreditOwnerTF"));


    }

    @FXML
    public void handleCheckBox(ActionEvent event) {
        if (!checkAnonym.isSelected()) {
            selectCustomerBtn.setDisable(false);
            newCustomerBtn.setDisable(false);
        } else {
            selectCustomerBtn.setDisable(true);
            newCustomerBtn.setDisable(true);
        }
    }

    @FXML
    public void handleSelectCustomer(ActionEvent event) {

        ((Node) event.getSource()).setCursor(Cursor.WAIT);

        Stage nextStage = new Stage();
        SpringFxmlLoader.LoadWrapper loadWrapper = springFxmlLoader.loadAndWrap("/gui/fxml/shoppingCart/select_customer_prototype.fxml");
        nextStage.setScene(new Scene((Parent) loadWrapper.getLoadedObject()));

        SelectCustomerController controller = (SelectCustomerController) loadWrapper.getController();
        controller.setSellController(this);

        ((Node) event.getSource()).setCursor(Cursor.DEFAULT);
        Node source = (Node) event.getSource();
        Stage thisStage = (Stage) source.getScene().getWindow();
        controller.setPreviousStage(thisStage);

        nextStage.setResizable(false);
        nextStage.setTitle(BundleManager.getBundle().getString("app.name"));
        nextStage.getIcons().add(new Image(LoginController.class.getResourceAsStream("/image/ticketlineLogo.png")));

        nextStage.initModality(Modality.WINDOW_MODAL);
        nextStage.initOwner(thisStage);

        controller.setThisStage(nextStage);
        nextStage.showAndWait();
//        thisStage.close();
    }

    /**
     * opens Create Customer Scene
     *
     * @param event
     */
    @FXML
    public void handleNewCustomer(ActionEvent event) {

        ((Node) event.getSource()).setCursor(Cursor.WAIT);

        Stage nextStage = new Stage();
        SpringFxmlLoader.LoadWrapper loadWrapper = springFxmlLoader.loadAndWrap("/gui/fxml/shoppingCart/create_customer_shopping_cart_prototype.fxml");
        nextStage.setScene(new Scene((Parent) loadWrapper.getLoadedObject()));

        CreateCustomerShoppingCartController controller = (CreateCustomerShoppingCartController) loadWrapper.getController();
        controller.setSellController(this);

        ((Node) event.getSource()).setCursor(Cursor.DEFAULT);
        Node source = (Node) event.getSource();
        Stage thisStage = (Stage) source.getScene().getWindow();

        controller.setPreviousStage(thisStage);

        nextStage.setResizable(false);
        nextStage.setTitle(BundleManager.getBundle().getString("app.name"));
        nextStage.getIcons().add(new Image(LoginController.class.getResourceAsStream("/image/ticketlineLogo.png")));

        nextStage.initModality(Modality.WINDOW_MODAL);
        nextStage.initOwner(thisStage);

        nextStage.showAndWait();
//        thisStage.close();
    }


    @FXML
    public void handleBack(ActionEvent event) {
        Stage stage = (Stage) backBtn.getScene().getWindow();
        stage.close();
    }

    /**
     * Sets the customer, who should be on the receipt, and displays his/her data on the labels
     *
     * @param customer The customer, who should be on the receipt
     */
    public void setCustomer(CustomerDto customer) {

        try {
            this.customerDto = customer;
            label_forename.setText(customerDto.getFirstname());
        } catch (NullPointerException e) {
            //not initialized!
            return;
        }
        label_lastname.setText(customerDto.getLastname());
        label_street.setText(customerDto.getStreet());
        label_postalcode.setText(customerDto.getPostalcode());
        label_city.setText(customerDto.getCity());
        label_country.setText(customerDto.getCountry());
        label_gender.setText(customerDto.getGender());

        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.YYYY");
        Date birthday = customerDto.getDateOfBirth();
        label_birthdate.setText(birthday == null ? "" : formatter.format(birthday));

        String email = customerDto.getEmail();
        label_email.setText(email == null || email.trim().isEmpty() ? "-" : email);

        String phone = customerDto.getPhoneNumber();
        label_phone.setText(phone == null || phone.trim().isEmpty() ? "-" : phone);

        selectCustomerBtn.setDisable(true);
        newCustomerBtn.setDisable(true);
        checkAnonym.setSelected(false);
        LOGGER.info("set customer for receipt - ID: " + customer.getId()
                + " Name: " + customer.getFirstname() + " " + customer.getLastname());
    }

    @FXML
    private void selectMethod() {
        if (bankAccount.isSelected()) {
            bIC.setDisable(false);
            iBAN.setDisable(false);
            account_Number.setDisable(false);
            bank.setDisable(false);
            bank_Code.setDisable(false);
            owner_Bank_Account.setDisable(false);

            credit_Card_Number.setDisable(true);
            owner_Credit_card.setDisable(true);
            //  axpress.setDisable(true);
            //  mastercard.setDisable(true);
            //  visa.setDisable(true);

        } else if (creditCard.isSelected()) {
            bIC.setDisable(true);
            iBAN.setDisable(true);
            account_Number.setDisable(true);
            bank.setDisable(true);
            bank_Code.setDisable(true);
            owner_Bank_Account.setDisable(true);

            credit_Card_Number.setDisable(false);
            owner_Credit_card.setDisable(false);
            //   axpress.setDisable(false);
            //   mastercard.setDisable(false);
            //    visa.setDisable(false);

        } else if (cash.isSelected()) {
            bIC.setDisable(true);
            iBAN.setDisable(true);
            account_Number.setDisable(true);
            bank.setDisable(true);
            bank_Code.setDisable(true);
            owner_Bank_Account.setDisable(true);

            credit_Card_Number.setDisable(true);
            owner_Credit_card.setDisable(true);
            //  axpress.setDisable(true);
            //  mastercard.setDisable(true);
            //  visa.setDisable(true);
        }

    }


    @FXML
    public void handleSell(ActionEvent event) {
        createReceipt(event);
    }


    private void createReceipt(ActionEvent event) {
        if (checkAnonym.isSelected()) {
            LOGGER.debug("The Anonym Customer");
            try {
                customerDto = getAnonymClient();
            } catch (ServiceException e) {
                DialogUtil.showExceptionDialog("Can not search the Anonym Client", "", e);
            }
        }


        if (!cash.isSelected() && (customerDto == null || customerDto.getId() <= 0)) {
            DialogUtil.showErrorDialog(BundleManager.getExceptionBundle().getString("error"),
                    BundleManager.getExceptionBundle().getString("sell.customerEmptyExHeader"),
                    BundleManager.getExceptionBundle().getString("sell.customerEmptyExBody"));
            return;
        }


        Node source = (Node) event.getSource();
        Stage thisStage = (Stage) source.getScene().getWindow();
        PDFCreator pdfCreator = new PDFCreator(thisStage);

        String methP;
        if (bankAccount.isSelected()) {
            BankAccountDto bankAccountDto = new BankAccountDto();
            bankAccountDto.setBIC(bIC.getText());
            bankAccountDto.setIBAN(iBAN.getText());
            bankAccountDto.setOwner(owner_Bank_Account.getText());
            if (!UserInputValidator.validateBIC(bIC.getText(), true) || !UserInputValidator.validateIBAN(iBAN.getText(), true)
                    || !UserInputValidator.validateNumbersWithSpacesOnly(account_Number.getText(), false)
                    || !UserInputValidator.validateAlphanumericValueWithSpaces(bank.getText(), false)
                    || !UserInputValidator.validateNonEmptyString(bank_Code.getText())) {
                DialogUtil.showErrorDialog(BundleManager.getExceptionBundle().getString("error"),
                        BundleManager.getExceptionBundle().getString("sell.accountFailureExHeader"),
                        BundleManager.getExceptionBundle().getString("sell.accountFailureExBody"));
                return;
            }
            bankAccountDto.setAccountNumber(account_Number.getText());
            bankAccountDto.setBank(bank.getText());
            bankAccountDto.setBankCode(bank_Code.getText());
            bankAccountDto.setCustomer(customerDto);
            bankAccountDto.setDeleted(false);

            methP = BundleManager.getBundle().getString("sell.bankAccount");

            try {
                if (ButtonType.OK == DialogUtil.showConfirmationDialog(BundleManager.getBundle().getString("confirmation"),
                        BundleManager.getBundle().getString("customer") + ": " + customerDto.getFirstname() + " " + customerDto.getLastname() + "\n"
                                + BundleManager.getBundle().getString("sell.methodOfPayment") + methP + "\n"
                                + BundleManager.getBundle().getString("sell.OKconfirmation"))) {
                    ReceiptDto r = receiptService.createBankReceipt(bankAccountDto);

                    try {
                        pdfCreator.createInvoicePDF(r);
                    } catch (IOException e) {
                        showPDFFailureDialog();
                    }

                    otherCodeAfterSell();
                    DialogUtil.showSuccessDialog(BundleManager.getBundle().getString("sell.receiptCreatedTitle"),
                            BundleManager.getBundle().getString("sell.receiptCreatedHeader"),
                            BundleManager.getBundle().getString("customer") + ": " + customerDto.getFirstname() + " " + customerDto.getLastname() + "\n"
                                    + BundleManager.getBundle().getString("sell.methodOfPayment") + ": " + methP + "\n"
                                    + BundleManager.getBundle().getString("sell.OKconfirmation"));
                    close(event);
                }
            } catch (ServiceException e) {
                DialogUtil.showExceptionDialog(BundleManager.getExceptionBundle().getString("sell.cannotCreateReceipt"), "One of the tickets maybe is sold, please remove all from cart and check again!", e);
            }
        } else if (creditCard.isSelected()) {
            CreditCardDto creditCardDto = new CreditCardDto();
            if (!UserInputValidator.validateNumbersWithSpacesOnly(credit_Card_Number.getText(), false)
                    || !UserInputValidator.validatePersonName(owner_Credit_card.getText(), false)) {
                DialogUtil.showErrorDialog(BundleManager.getExceptionBundle().getString("error"),
                        BundleManager.getExceptionBundle().getString("sell.credCardFailureExHeader"),
                        BundleManager.getExceptionBundle().getString("sell.credCardFailureExBody"));
                return;
            }
            creditCardDto.setCreditcardNumber(credit_Card_Number.getText());

            creditCardDto.setOwner(owner_Credit_card.getText());
            if (aexpress.isSelected()) {
                creditCardDto.setCreditcardType(CreditcardType.AMERICAN_EXPRESS);
            } else if (mastercard.isSelected()) {
                creditCardDto.setCreditcardType(CreditcardType.MASTERCARD);
            } else if (visa.isSelected()) {
                creditCardDto.setCreditcardType(CreditcardType.VISA);
            } else {
                DialogUtil.showErrorDialog(BundleManager.getExceptionBundle().getString("error"),
                        BundleManager.getExceptionBundle().getString("sell.noCreditCardTypeExHeader"),
                        BundleManager.getExceptionBundle().getString("sell.noCreditCardTypeExBody"));
                return;
            }
            creditCardDto.setValidThru(new Date());

            creditCardDto.setCustomer(customerDto);
            creditCardDto.setDeleted(false);
            methP = BundleManager.getBundle().getString("sell.creditCard");

            try {
                if (ButtonType.OK == DialogUtil.showConfirmationDialog(
                        BundleManager.getBundle().getString("confirmation"),
                        BundleManager.getBundle().getString("customer") + ": " + customerDto.getFirstname() + " " + customerDto.getLastname() + "\n"
                                + BundleManager.getBundle().getString("sell.methodOfPayment") + ": " + methP + "\n"
                                + BundleManager.getBundle().getString("sell.OKconfirmation"))) {

                    ReceiptDto r = receiptService.createCreditReceipt(creditCardDto);

                    try {
                        pdfCreator.createInvoicePDF(r);
                    } catch (IOException e) {
                        showPDFFailureDialog();
                    }

                    otherCodeAfterSell();
                    DialogUtil.showSuccessDialog(BundleManager.getBundle().getString("sell.receiptCreatedTitle"),
                            BundleManager.getBundle().getString("sell.receiptCreatedHeader"),
                            BundleManager.getBundle().getString("customer") + ": " + customerDto.getFirstname() + " " + customerDto.getLastname() + "\n"
                                    + BundleManager.getBundle().getString("sell.methodOfPayment") + ": " + methP + "\n"
                                    + BundleManager.getBundle().getString("sell.OKconfirmation"));

                    close(event);
                }
            } catch (ServiceException e) {
                DialogUtil.showExceptionDialog(BundleManager.getExceptionBundle().getString("sell.cannotCreateReceipt"), "One of the tickets maybe is sold, please remove all from cart and check again!", e);
            }
        } else if (cash.isSelected()) {
            CashDto cashDto = new CashDto();
            methP = BundleManager.getBundle().getString("sell.cash");

            if (customerDto == null || customerDto.getId() <= 0) {
                DialogUtil.showErrorDialog(BundleManager.getExceptionBundle().getString("error"),
                        BundleManager.getExceptionBundle().getString("sell.customerEmptyExHeader"),
                        BundleManager.getExceptionBundle().getString("sell.customerEmptyExBody"));
                return;
            } else {
                cashDto.setCustomer(customerDto);
                cashDto.setDeleted(false);
            }


            try {
                if (ButtonType.OK == DialogUtil.showConfirmationDialog(
                        BundleManager.getBundle().getString("confirmation"),
                        BundleManager.getBundle().getString("customer") + ": " + customerDto.getFirstname() + " " + customerDto.getLastname() + "\n"
                                + BundleManager.getBundle().getString("sell.methodOfPayment") + ": " + methP + "\n"
                                + BundleManager.getBundle().getString("sell.OKconfirmation"))) {

                    ReceiptDto r = receiptService.createCashReceipt(cashDto, false);
                    try {
                        pdfCreator.createInvoicePDF(r);
                    } catch (IOException e) {
                        showPDFFailureDialog();
                    }


                    otherCodeAfterSell();
                    DialogUtil.showSuccessDialog(BundleManager.getBundle().getString("sell.receiptCreatedTitle"),
                            BundleManager.getBundle().getString("sell.receiptCreatedHeader"),
                            BundleManager.getBundle().getString("customer") + ": " + customerDto.getFirstname() + " " + customerDto.getLastname() + "\n"
                                    + BundleManager.getBundle().getString("sell.methodOfPayment") + ": " + methP + "\n"
                                    + BundleManager.getBundle().getString("sell.OKconfirmation"));
                    close(event);
                }
            } catch (ServiceException e) {
                DialogUtil.showExceptionDialog(BundleManager.getExceptionBundle().getString("sell.cannotCreateReceipt"),
                        "One of the tickets maybe is sold, please remove all from cart and check again!", e);
            }
        }
    }

    private void showPDFFailureDialog() {
        DialogUtil.showErrorDialog(BundleManager.getExceptionBundle().getString("sell.pdfCreationErrTitle"),
                BundleManager.getExceptionBundle().getString("sell.pdfCreationErrHeader"),
                "");
    }

    private void close(ActionEvent event) {
        Node source = (Node) event.getSource();
        Stage thisStage = (Stage) source.getScene().getWindow();
        thisStage.close();
    }

    private CustomerDto getAnonymClient() throws ServiceException {
        return customerService.getCustomerByID(1);
    }

    public CustomerDto getCustomerDto() {
        return this.customerDto;
    }


    public void otherCodeAfterSell() {
        receiptService.emptyCartWhenSuccesfulSold();
        merchandiseController.initialize();
    }
}
