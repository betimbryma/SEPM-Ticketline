package at.ac.tuwien.inso.ticketline.client.gui.controller.tabs.customer;

import at.ac.tuwien.inso.ticketline.client.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.client.exception.ValidationException;
import at.ac.tuwien.inso.ticketline.client.gui.JavaFXUtils;
import at.ac.tuwien.inso.ticketline.client.gui.controller.MainController;
import at.ac.tuwien.inso.ticketline.client.service.CustomerService;
import at.ac.tuwien.inso.ticketline.client.util.*;
import at.ac.tuwien.inso.ticketline.dto.CustomerDto;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.stage.Stage;
import org.controlsfx.validation.ValidationResult;
import org.controlsfx.validation.ValidationSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.net.URL;
import java.time.LocalDate;
import java.util.Locale;
import java.util.ResourceBundle;

@Component
public class CreateCustomerController implements Initializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(CreateCustomerController.class);

    @FXML
    private TextField tf_customerFirstname;
    @FXML
    private TextField tf_customerLastname;
    @FXML
    private TextField tf_customerPostalcode;
    @FXML
    private TextField tf_customerStreet;
    @FXML
    private TextField tf_customerCity;
    @FXML
    private TextField tf_customerCountry;
    @FXML
    private TextField tf_customerEmail;
    @FXML
    private TextField tf_customerPhonenumber;

    @FXML
    private RadioButton button_male;
    @FXML
    private RadioButton button_female;

    @FXML
    private DatePicker dp_birthdate;

    @FXML
    private Label fname, lname, birth, Email, phone, city, str, pc, country;

    @FXML
    private Button create, back;

    @FXML
    private TitledPane title;

    @Autowired
    private CustomerService customerService;

    private CustomersController customersController;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        new ValidationSupport().registerValidator(tf_customerFirstname, true, (Control c, String s) ->
                ValidationResult.fromErrorIf(c, BundleManager.getBundle().getString("crcustomer.fnValError"), !UserInputValidator.validatePersonName(s, false)));

        new ValidationSupport().registerValidator(tf_customerLastname, true, (Control c, String s) ->
                ValidationResult.fromErrorIf(c, BundleManager.getBundle().getString("crcustomer.lnValError"), !UserInputValidator.validatePersonName(s, false)));

        new ValidationSupport().registerValidator(tf_customerEmail, false, (Control c, String s) ->
                ValidationResult.fromErrorIf(c, BundleManager.getBundle().getString("crcustomer.emailValError"), !UserInputValidator.validateEmail(s, true)));

        new ValidationSupport().registerValidator(tf_customerCity, true, (Control c, String s) ->
                ValidationResult.fromErrorIf(c, BundleManager.getBundle().getString("crcustomer.cityValError"), !UserInputValidator.validateNoNumbersString(s, false)));

        new ValidationSupport().registerValidator(tf_customerCountry, true, (Control c, String s) ->
                ValidationResult.fromErrorIf(c, BundleManager.getBundle().getString("crcustomer.countryValError"), !UserInputValidator.validateNoNumbersString(s, false)));

        new ValidationSupport().registerValidator(tf_customerStreet, true, (Control c, String s) ->
                ValidationResult.fromErrorIf(c, BundleManager.getBundle().getString("crcustomer.streetValError"), !UserInputValidator.validateNonEmptyString(s)));

        new ValidationSupport().registerValidator(tf_customerPostalcode, true, (Control c, String s) ->
                ValidationResult.fromErrorIf(c, BundleManager.getBundle().getString("crcustomer.postalCodeValError"), !UserInputValidator.validateNonEmptyString(s)));

        new ValidationSupport().registerValidator(tf_customerPhonenumber, false, (Control c, String s) ->
                ValidationResult.fromErrorIf(c, BundleManager.getBundle().getString("crcustomer.phoneValError"), !UserInputValidator.validatePhoneNumber(s, true)));

        new ValidationSupport().registerValidator(dp_birthdate, false, (Control c, LocalDate d) ->
                ValidationResult.fromErrorIf(c, BundleManager.getBundle().getString("crcustomer.bdValError"), !UserInputValidator.validateBirthday(d, true)));

        JavaFXUtils.setChangeListenerToDatePicker(dp_birthdate);

        dp_birthdate.setConverter(DateUtil.getEuropeanDateStringConverter());
        reinitLocale(MainController.currentLocale);
    }

    public void reinitLocale(Locale newValue) {
        LocaleContextHolder.setLocale(newValue);
        BundleManager.changeLocale(newValue);
        button_male.setText(BundleManager.getBundle().getString("crcustomer.male"));
        button_female.setText(BundleManager.getBundle().getString("crcustomer.female"));

        fname.setText(BundleManager.getBundle().getString("crcustomer.fn"));
        lname.setText(BundleManager.getBundle().getString("crcustomer.ln"));
        birth.setText(BundleManager.getBundle().getString("crcustomer.birth"));
        Email.setText(BundleManager.getBundle().getString("crcustomer.email"));
        phone.setText(BundleManager.getBundle().getString("crcustomer.phone"));
        city.setText(BundleManager.getBundle().getString("crcustomer.city"));
        str.setText(BundleManager.getBundle().getString("crcustomer.street"));
        pc.setText(BundleManager.getBundle().getString("crcustomer.pc"));
        country.setText(BundleManager.getBundle().getString("crcustomer.country"));

        create.setText(BundleManager.getBundle().getString("crcustomer.createB"));
        back.setText(BundleManager.getBundle().getString("crcustomer.backB"));

        title.setText(BundleManager.getBundle().getString("crcustomer.title"));
    }

    public void setCustomersController(CustomersController customersController) {
        this.customersController = customersController;
    }

    /**
     * Reads Data from Textfields and creates Customer if Data is valid.
     * If Data is not valid, a Dialogue is shown.
     *
     * @param event the event
     */
    @FXML
    private void onCreateButtonPressed(ActionEvent event) {

        LOGGER.debug("create Button pressed");

        CustomerDto customer;

        LOGGER.debug("create Button pressed");

        String gender = "";
        if (button_male.isSelected()) {
            gender = "male";
        } else if (button_female.isSelected()) {
            gender = "female";
        }


        try {
            System.err.println("Text: " + tf_customerFirstname.getText());
            System.err.println("Text: " + tf_customerLastname.getText());
            customer = CustomerDataValidator.validateAndCreate(tf_customerFirstname.getText(),
                    tf_customerLastname.getText(), tf_customerStreet.getText(), tf_customerPostalcode.getText(),
                    tf_customerCity.getText(), tf_customerCountry.getText(), gender, dp_birthdate.getValue(),
                    tf_customerEmail.getText(), tf_customerPhonenumber.getText());

            this.customerService.saveCustomer(customer);

            Node source = (Node) event.getSource();
            Stage stage = (Stage) source.getScene().getWindow();
            if (customersController != null) { // refresh customer List
                customersController.refreshCustomerList();
            }
            stage.close();
        } catch (ValidationException e) {
            LOGGER.error("customer data is not valid");
            return;
        } catch (ServiceException e) {
            if (MainController.currentLocale.equals(Locale.ENGLISH)) {
                DialogUtil.showErrorDialog("Error creating customer",
                        "Customer could not be created",
                        "An error in the application occured");
            } else {
                DialogUtil.showErrorDialog("Fehler beim Erstellen eines neuen Kunden!",
                        "Der Kunde konnte nicht ertellt werden.",
                        "Es ist ein Fehler im Programm aufgetaucht.");
            }
        }
    }

    /**
     * Shows a Dialog and closes the window if user confirms
     *
     * @param event the event
     */
    @FXML
    private void onBackButtonPressed(ActionEvent event) {
        if (MainController.currentLocale.equals(Locale.ENGLISH)) {

            if (DialogUtil.showConfirmationDialog("Closing customer creator",
                    "Do you really wish to quit the customer creation?") == ButtonType.OK) {
                Node source = (Node) event.getSource();
                Stage stage = (Stage) source.getScene().getWindow();
                stage.close();
            }
        } else {
            if (DialogUtil.showConfirmationDialog("Fenster schließen",
                    "Möchten Sie wirklich die Erstellung eines neuen Kunden abbrechen?") == ButtonType.OK) {
                Node source = (Node) event.getSource();
                Stage stage = (Stage) source.getScene().getWindow();
                stage.close();
            }
        }
    }


}
