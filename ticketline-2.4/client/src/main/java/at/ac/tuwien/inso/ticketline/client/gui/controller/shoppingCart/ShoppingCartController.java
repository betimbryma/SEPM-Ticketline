package at.ac.tuwien.inso.ticketline.client.gui.controller.shoppingCart;

import at.ac.tuwien.inso.ticketline.client.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.client.gui.controller.LoginController;
import at.ac.tuwien.inso.ticketline.client.gui.controller.MainController;
import at.ac.tuwien.inso.ticketline.client.gui.controller.tabs.CancelController;
import at.ac.tuwien.inso.ticketline.client.gui.controller.tabs.MerchandiseController;
import at.ac.tuwien.inso.ticketline.client.gui.controller.tabs.customer.CustomerViewController;
import at.ac.tuwien.inso.ticketline.client.gui.controller.tabs.search.views.ShowViewController;
import at.ac.tuwien.inso.ticketline.client.service.ReceiptService;
import at.ac.tuwien.inso.ticketline.client.service.TicketService;
import at.ac.tuwien.inso.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.ticketline.client.util.DialogUtil;
import at.ac.tuwien.inso.ticketline.client.util.SpringFxmlLoader;
import at.ac.tuwien.inso.ticketline.dto.MerchandiseDto;
import at.ac.tuwien.inso.ticketline.dto.TicketDto;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * @author Michael Oppitz
 * @date 04.05.2016
 */
@Component
public class ShoppingCartController implements Initializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(ShoppingCartController.class);

    @Autowired
    private SpringFxmlLoader springFxmlLoader;
    @Autowired
    private MainController mainController;
    @Autowired
    private ReceiptService receiptService;
    @Autowired
    private CustomerViewController customerViewController;
    @Autowired
    private TicketService ticketService;
    @Autowired
    private CancelController cancelController;

    @Autowired
    private MerchandiseController merchandiseController;
    @Autowired
    private ShowViewController showViewController;
    @FXML
    private TitledPane shoppingcart;
    @FXML
    private Button backBtn, emptyBtn, continueBtn;
    @FXML
    private TextField ticketSearchTF, articleSearchTF;

    @FXML
    private Label ticketsL, merchandiseL, totalL, sumL;
    @FXML
    private TableView<TicketDto> tableTickets;
    @FXML
    private TableColumn<TicketDto, Integer> tableTicketDescription;
    @FXML
    private TableColumn<TicketDto, Integer> tableTicketPrice;
    @FXML
    private TableColumn<TicketDto, String> tableTicketDate;
    @FXML
    private TableColumn<TicketDto, Integer> tableTicketRow;
    @FXML
    private TableColumn<TicketDto, Integer> tableTicketColumn;

    private void fillTableTicket(ObservableList<TicketDto> obs) {
        setTotal();
        // obs.get(0).getSeat().getRow().getOrder()
        if (obs == null)
            return;
        LOGGER.debug("Fillin the Table with the articles");
        tableTicketDescription.setCellValueFactory(new PropertyValueFactory<>("description"));
        tableTicketPrice.setCellValueFactory(new PropertyValueFactory<>("price"));
        tableTicketDate.setCellValueFactory(p -> new ReadOnlyObjectWrapper(p.getValue().getShow().getDateOfPerformance().toString()));
        tableTicketRow.setCellValueFactory(p -> new ReadOnlyObjectWrapper(p.getValue().getSeat().getRow().getOrder()));
        tableTicketColumn.setCellValueFactory(p -> new ReadOnlyObjectWrapper(p.getValue().getSeat().getGallery().getOrder()));
        tableTickets.setItems(obs);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initialize();
    }

    public void initialize() {
        LOGGER.debug("initialize Shopping Cart");
        fillArticleTable(receiptService.getTheArticleCart());
        fillTableTicket(receiptService.getTicketCart());
        reinitLocale(MainController.currentLocale);
        receiptService.isCartEmpty();

    }

    public void reinitLocale(Locale newValue) {
        LocaleContextHolder.setLocale(newValue);
        BundleManager.changeLocale(newValue);
        shoppingcart.setText(BundleManager.getBundle().getString("shoppingcart.title"));

        backBtn.setText(BundleManager.getBundle().getString("shoppingcart.backB"));
        emptyBtn.setText(BundleManager.getBundle().getString("shoppingcart.emptyB"));
        continueBtn.setText(BundleManager.getBundle().getString("shoppingcart.continueB"));

        ticketsL.setText(BundleManager.getBundle().getString("shoppingcart.ticketsL"));
        merchandiseL.setText(BundleManager.getBundle().getString("shoppingcart.merchandiseL"));
        totalL.setText(BundleManager.getBundle().getString("shoppingcart.totalL"));

        tableTicketDescription.setText(BundleManager.getBundle().getString("shoppingcart.tEvent"));
        tableTicketPrice.setText(BundleManager.getBundle().getString("shoppingcart.tPrice"));
        tableTicketDate.setText(BundleManager.getBundle().getString("shoppingcart.tDate"));
        tableTicketRow.setText(BundleManager.getBundle().getString("shoppingcart.tRow"));
        tableTicketColumn.setText(BundleManager.getBundle().getString("shoppingcart.tColumn"));

        tableCartId.setText(BundleManager.getBundle().getString("shoppingcart.mID"));
        tableCartName.setText(BundleManager.getBundle().getString("shoppingcart.mName"));
        tableCartPrice.setText(BundleManager.getBundle().getString("shoppingcart.mPrice"));

        ticketSearchTF.setPromptText(BundleManager.getBundle().getString("shoppingcart.ticketSearchTF"));
        articleSearchTF.setPromptText(BundleManager.getBundle().getString("shoppingcart.ticketSearchTF"));
    }

    @FXML
    public void handleBack(ActionEvent event) {
        Stage stage = (Stage) backBtn.getScene().getWindow();
        stage.close();
    }

    @FXML
    public void handleEmpty(ActionEvent event) {
        try {
            receiptService.emptyCart();
        } catch (ServiceException e) {
            LOGGER.debug("Error in database");
            DialogUtil.showErrorDialog("Error Dialog", "Error in Server", e.getMessage());
        }
        merchandiseController.refreshArticleCart();
        fillArticleTable(receiptService.getTheArticleCart());
        fillTableTicket(receiptService.getTicketCart());
    }

    @FXML
    public void handleContinue(ActionEvent event) {
        ((Node) event.getSource()).setCursor(Cursor.WAIT);
        Stage mainStage = new Stage();
        mainStage.setScene(new Scene((Parent) springFxmlLoader.load("/gui/fxml/shoppingCart/sell_prototype.fxml")));
        mainStage.setResizable(false);
        mainStage.setTitle(BundleManager.getBundle().getString("app.name"));
        mainStage.getIcons().add(new Image(LoginController.class.getResourceAsStream("/image/ticketlineLogo.png")));
        mainStage.initModality(Modality.WINDOW_MODAL);
        mainStage.initOwner(((Node) event.getSource()).getScene().getWindow());

        ((Node) event.getSource()).setCursor(Cursor.DEFAULT);
        mainStage.showAndWait();

        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        stage.close();
    }


    @FXML
    private TableView<MerchandiseDto> tableCart;
    @FXML
    private TableColumn<MerchandiseDto, Integer> tableCartId;
    @FXML
    private TableColumn<MerchandiseDto, String> tableCartName;
    @FXML
    private TableColumn<MerchandiseDto, Integer> tableCartPrice;

    public void fillArticleTable(ObservableList<MerchandiseDto> obs) {
        setTotal();
        if (obs == null)
            return;
        LOGGER.debug("Fillin the Table with the articles");
        tableCartId.setCellValueFactory(new PropertyValueFactory<>("id"));
        tableCartName.setCellValueFactory(param -> {
            MerchandiseDto article = param.getValue();
            return new SimpleStringProperty(article.getPerformanceDto().getName() + " - " + article.getName());
        });
        tableCartPrice.setCellValueFactory(new PropertyValueFactory<>("price"));
        tableCartId.setSortType(TableColumn.SortType.ASCENDING);
        tableCart.setItems(obs);
    }

    @FXML
    private TextField searchArticleInCartByIDText;

    @FXML
    private void searchArticleInCartByID() {

        LOGGER.debug("Search new articles");

        if (searchArticleInCartByIDText.getText().equals("")) {
            fillArticleTable(receiptService.getTheArticleCart());
            return;
        } else {
            int i = -666;
            try {
                i = Integer.valueOf(searchArticleInCartByIDText.getText());

            } catch (NumberFormatException e) {
                LOGGER.debug("not a number");
            }
            ObservableList<MerchandiseDto> merchandiseDtos = receiptService.getTheArticleCart();
            ObservableList<MerchandiseDto> m = FXCollections.observableArrayList();
            for (MerchandiseDto merchandiseDto : merchandiseDtos) {
                if (merchandiseDto.getId() == i || merchandiseDto.getName().toLowerCase().contains(searchArticleInCartByIDText.getText().toLowerCase())) {
                    m.add(merchandiseDto);
                }
            }
            fillArticleTable(m);
        }
    }

    @FXML
    private void refreshTable() {
        //fill table ticket


        //fill table Merchandise
        fillArticleTable(receiptService.getTheArticleCart());
        fillTableTicket(receiptService.getTicketCart());
    }

    public void setTotal() {
        int total = 0;
        for (MerchandiseDto d : receiptService.getTheArticleCart()) {
            total += d.getPrice();
        }

        for (TicketDto d : receiptService.getTicketCart()) {
            total += d.getPrice();
        }
        try {
            sumL.setText(String.valueOf(total) + "\u20AC");
        } catch (NullPointerException | NumberFormatException e) {
            // don't handle
        }
    }

    public void isCartEmpty(boolean value) {
        try {
            emptyBtn.setDisable(!value);
            continueBtn.setDisable(!value);
        } catch (NullPointerException e) {

        }
    }

    @FXML
    public void removeTicket() {
        TicketDto a = tableTickets.getSelectionModel().getSelectedItem();
        tableTickets.getSelectionModel().clearSelection();
        if (a == null) {
            return;
        }
        if (DialogUtil.showConfirmationDialog("Remove Ticket", "Confirm your action to remove Ticket \"" + a.getDescription() + "\" from Cart!") == ButtonType.OK) {
            receiptService.getTicketCart().remove(a);
            List<TicketDto> t = new ArrayList<>();
            t.add(a);
            try {
                ticketService.releaseTicket(t);
            } catch (ServiceException e) {
                LOGGER.debug("Error in database");
                DialogUtil.showErrorDialog("Error Dialog", "Error in Server", e.getMessage());
            }
            if (customerViewController != null) {
                customerViewController.ticketRemoved(a);
            }

            if (showViewController != null) {
                showViewController.resetRemoveTicket(a);
            }
            cancelController.resetRemoveTicket(a);
            receiptService.isCartEmpty();
        }
    }

    @FXML
    public void removeArticle() {
        MerchandiseDto a = tableCart.getSelectionModel().getSelectedItem();
        tableCart.getSelectionModel().clearSelection();
        if (a == null) {
            return;
        }

        if (DialogUtil.showConfirmationDialog("Remove Article", "Confirm your action to remove Article " + a.getName()) == ButtonType.OK) {
            merchandiseController.removeArticle(a);
        }
        receiptService.isCartEmpty();
    }


    @FXML
    public void filterTicketTable() {
        ObservableList<TicketDto> list = FXCollections.observableArrayList();
        ObservableList<TicketDto> allTickets = receiptService.getTicketCart();

        String filter = ticketSearchTF.getText();

        if (filter == null || filter.trim().length() == 0) {
            list.addAll(allTickets);
        } else {
            filter = filter.trim().toLowerCase();
            for (TicketDto ticket : allTickets) {
                if (ticket.getShow().getPerformance().getName().trim().toLowerCase().contains(filter)) {
                    list.add(ticket);
                }
            }
        }


        fillTableTicket(list);
    }

    @FXML
    public void filterArticleTable() {
        ObservableList<MerchandiseDto> list = FXCollections.observableArrayList();
        ObservableList<MerchandiseDto> allArticles = receiptService.getTheArticleCart();

        String filter = articleSearchTF.getText();

        if(filter == null || filter.trim().length() == 0) {
            list.addAll(allArticles);
        } else {
            filter = filter.trim().toLowerCase();
            for(MerchandiseDto article : allArticles) {
                String articleName = article.getPerformanceDto().getName() + " - " + article.getName();
                if(articleName.trim().toLowerCase().contains(filter)) {
                    list.addAll(article);
                }
            }
        }

        fillArticleTable(list);

    }

}
