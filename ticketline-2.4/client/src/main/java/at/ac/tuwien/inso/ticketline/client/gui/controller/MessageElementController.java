package at.ac.tuwien.inso.ticketline.client.gui.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import org.springframework.stereotype.Component;

/**
 * Created by Jakob on 29.05.2016.
 */
@Component
public class MessageElementController {

    @FXML
    private Label messageLabel;

    public void initializeMessage(String message) {
        this.messageLabel.setText(message);
    }
}
