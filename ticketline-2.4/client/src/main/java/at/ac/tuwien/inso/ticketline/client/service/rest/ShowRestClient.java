package at.ac.tuwien.inso.ticketline.client.service.rest;

import at.ac.tuwien.inso.ticketline.client.exception.ServiceException;
import at.ac.tuwien.inso.ticketline.client.service.ShowService;
import at.ac.tuwien.inso.ticketline.dto.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.Collections;
import java.util.List;

@Component
public class ShowRestClient implements ShowService {

    public static final String GET_ALL_SHOWS = "/service/show/";
    public static final String GET_SHOWS_FOR_CUSTOMER = "/service/show/customerShows";
    public static final String GET_TICKETS_FOR_CUSTOMER = "/service/show/customerTickets";
    private static final Logger LOGGER = LoggerFactory.getLogger(ShowRestClient.class);

    @Autowired
    private RestClient restClient;

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ShowDto> getAll() throws ServiceException {
        RestTemplate restTemplate = this.restClient.getRestTemplate();
        String url = this.restClient.createServiceUrl(GET_ALL_SHOWS);
        HttpEntity<String> entity = new HttpEntity<>(this.restClient.getHttpHeaders());
        LOGGER.info("Retrieving shows from {}", url);
        List<ShowDto> shows;

        try {
            ParameterizedTypeReference<List<ShowDto>> ref = new ParameterizedTypeReference<List<ShowDto>>() {
            };
            ResponseEntity<List<ShowDto>> response = restTemplate.exchange(URI.create(url), HttpMethod.GET, entity, ref);
            shows = response.getBody();

        } catch (RestClientException e) {
            throw new ServiceException("Could not retrieve shows: " + e.getMessage(), e);
        }

        return shows;
    }

    @Override
    public List<MapEntryDto<ShowDto,List<TicketIdentifierDto>>> getShowsForCustomer(CustomerDto customerDto) throws ServiceException {

        RestTemplate restTemplate = this.restClient.getRestTemplate();
        String url = this.restClient.createServiceUrl(GET_SHOWS_FOR_CUSTOMER);
        HttpHeaders headers = this.restClient.getHttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        HttpEntity<CustomerDto> entity = new HttpEntity<>(customerDto, headers);
        LOGGER.info("Getting shows from {}",url);
        List<MapEntryDto<ShowDto,List<TicketIdentifierDto>>> showDtoList;

        try {
            ParameterizedTypeReference<List<MapEntryDto<ShowDto,List<TicketIdentifierDto>>>> parameterizedTypeReference = new ParameterizedTypeReference<List<MapEntryDto<ShowDto,List<TicketIdentifierDto>>>>() {};
            ResponseEntity<List<MapEntryDto<ShowDto,List<TicketIdentifierDto>>>> responseEntity = restTemplate.exchange(url, HttpMethod.POST, entity, parameterizedTypeReference);

            showDtoList=responseEntity.getBody();
        } catch (RestClientException se) {
            throw new ServiceException("Could not get the shows "+se.getMessage());
        }

        return showDtoList;
    }

    @Override
    public List<TicketIdentifierDto> getTicketsForShowAndCustomer(List<TicketDto> ticketDtoList) throws ServiceException {

        RestTemplate restTemplate = this.restClient.getRestTemplate();
        String url = this.restClient.createServiceUrl(GET_TICKETS_FOR_CUSTOMER);
        HttpHeaders headers = this.restClient.getHttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        HttpEntity<List<TicketDto>> entity = new HttpEntity<>(ticketDtoList, headers);
        LOGGER.info("Getting tickets for a certain show and customer from {}",url);
        List<TicketIdentifierDto> ticketIdentifierDtoList;

        try {
            ParameterizedTypeReference<List<TicketIdentifierDto>> parameterizedTypeReference = new ParameterizedTypeReference<List<TicketIdentifierDto>>() {};
            ResponseEntity<List<TicketIdentifierDto>> responseEntity = restTemplate.exchange(url, HttpMethod.POST, entity, parameterizedTypeReference);

            ticketIdentifierDtoList=responseEntity.getBody();
        } catch (RestClientException se) {
            throw new ServiceException("Could not get the shows and their tickets for this customer and show"+se.getMessage());
        }

        return ticketIdentifierDtoList;
    }
}
