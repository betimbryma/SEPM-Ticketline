package at.ac.tuwien.inso.ticketline.datagenerator.generator;

import at.ac.tuwien.inso.ticketline.dao.CashDao;
import at.ac.tuwien.inso.ticketline.dao.CustomerDao;
import at.ac.tuwien.inso.ticketline.model.Customer;
import at.ac.tuwien.inso.ticketline.model.Gender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * This class generates data for customers
 *
 * @see at.ac.tuwien.inso.ticketline.model.Customer
 */
@Component
public class CustomerGenerator implements DataGenerator {

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomerGenerator.class);

    @Autowired
    private CustomerDao dao;
    @Autowired
    private CashDao cashDao;

    /**
     * {@inheritDoc}
     */
    public void generate() {
        LOGGER.info("+++++ Generate Customer Data +++++");

        List<Customer> customers = new LinkedList<>();

        customers.add(generateCustomer("Anonymus", "Anonymus", Gender.MALE, "holoholo.huni@gmx.at", "06788966354", 23, 11, 1995, "Mrdorstreet", "666", "Schwaz", "Greece"));

        customers.add(generateCustomer("Homer", "Simpson", Gender.MALE, "homer.simpson@gmail.com", "+123456789", 2, 5, 1970, "Evergreenterrace 1137", "9851496", "Springfield", "USA"));
        customers.add(generateCustomer("Marge", "Simpson", Gender.FEMALE, "marge.simpson@gmail.com", "+123456789", 24, 9, 1973, "Evergreenterrace 1137", "9851496", "Springfield", "USA"));
        customers.add(generateCustomer("Bart", "Simpson", Gender.MALE, "bart.simpson@gmail.com", null, 31, 1, 2000, "Evergreenterrace 1137", "9851496", "Springfield", "USA"));
        customers.add(generateCustomer("Lisa", "Simpson", Gender.FEMALE, "lisa.simpson@gmail.com", null, 12, 12, 2002, "Evergreenterrace 1137", "9851496", "Springfield", "USA"));
        customers.add(generateCustomer("Abraham Jebediah", "Simpson", Gender.MALE, null, null, 8, 8, 1922, "Retirement-Home-Avenue 12", "9851496", "Springfield", "USA"));
        customers.add(generateCustomer("Apu", "Nahasapeemapetilon", Gender.MALE, "apu.nahasapeemapetilon37@hotmail.com", "+13154687", 20, 3, 1975, "Street 1", "IN-1348796464", "New Delhi", "India"));
        customers.add(generateCustomer("Charles", "Burns", Gender.MALE, "montgomery.burns@nuclear.us", "+666669", 12, 4, 1901, "Nuclear-Power-Plant-Street 1", "A-9851496", "Vienna", "Austria-Hungary"));
        customers.add(generateCustomer("Waylon", "Smithers", Gender.MALE, "malibu_stacy_fanboy69@nuclear.us", "+23969-69669", 3, 5, 1978, "Little-Appartment-Street 69", "9851496", "Springfield", "USA"));
        customers.add(generateCustomer("Lenford", "Leonard", Gender.MALE, "lenny@me.com", null, 5, 12, 1969, "Little-Appartment-Street 13", "9851496", "Springfield", "USA"));
        customers.add(generateCustomer("Carl", "Carlson", Gender.MALE, "carl@me.com", null, 10, 12, 1972, "Big-Appartment-Street 13", "9851496", "Springfield", "USA"));

        customers.add(generateCustomer("Ned", "Flanders", Gender.MALE, "ned.flanders@godisgreat.jesus", "+123987654", 26, 2, 1950, "Evergreenterrace 1139", "9851496", "Springfield", "USA"));
        customers.add(generateCustomer("Maude", "Flanders", Gender.FEMALE, "maude.flanders@jesuslovesyou.god", "+123987654", 29, 6, 1964, "Evergreenterrace 1139", "9851496", "Springfield", "USA"));
        customers.add(generateCustomer("Rod", "Flanders", Gender.MALE, null, null, 26, 2, 2006, "Evergreenterrace 1139", "9851496", "Springfield", "USA"));
        customers.add(generateCustomer("Todd", "Flanders", Gender.MALE, null, null, 31, 5, 2001, "Evergreenterrace 1139", "9851496", "Springfield", "USA"));
        customers.add(generateCustomer("Milhous", "van Houten", Gender.MALE, null, null, 23, 11, 2000, "HollandStreet 52", "9851496", "Springfield", "USA"));
        customers.add(generateCustomer("Seymour", "Skinner", Gender.MALE, "seymour@springfieldelementaryschool.us", "+12333333", 20, 6, 1958, "SchoolStreet 1", "9851496", "Springfield", "USA"));
        customers.add(generateCustomer("Edna", "Krabappel", Gender.FEMALE, "edna@springfieldelementaryschool.us", "+12333333", 31, 8, 1970, "Little-Appartment-Street 22", "9851496", "Springfield", "USA"));
        customers.add(generateCustomer("Kang", "ET", Gender.MALE, "kang@tentacle.universe", "+8888888", -1, -1, -1, "UnknownPlanet-Avenue", "888", "Another Galaxy", "Outer Space"));
        customers.add(generateCustomer("Kodos", "ET", Gender.FEMALE, "kodos@tentacle.universe", "+8888888", -1, -1, -1, "UnknownPlanet-Avenue", "888", "Another Galaxy", "Outer Space"));
        customers.add(generateCustomer("Joseph", "Quimby", Gender.MALE, "unbribeableMajor@major.gv.at", "+123123321", 5, 6, 1977, "Townhall 1 Appartement 1", "A-1010", "Wien", "Austria"));

        customers.add(generateCustomer("Bob", "Sideshow", Gender.MALE, "mustKillBart@tingeltangel.it", "+39 1321 45656", 10, 12, 1979, "Plaza del Sol 1", "I-3132", "Firenze", "Italy"));
        customers.add(generateCustomer("Bumblebee", "Man", Gender.MALE, "ayayay@unborromuyamoroso.es", "+34 5466 54646", 12, 9, 1967, "Piazze die spagna 52", "E-12354", "Madrid", "Spain"));
        customers.add(generateCustomer("Selma", "Bouvier", Gender.FEMALE, "iAmSingle@pleaseDate.me", "+56897324", 30, 5, 1976, "SingleStreet 12", "9851496", "Springfield", "USA"));
        customers.add(generateCustomer("Patty", "Bouvier", Gender.FEMALE, "singleAndDesperate@pleaseDate.me", "+56897324", 30, 5, 1976, "SingleStreet 12", "9851496", "Springfield", "USA"));
        customers.add(generateCustomer("Duff", "Man", Gender.MALE, "duffbeer4life@beer.ru", "+3216458", 5, 6, 1985, "BeerStreet 12", "R-99999", "Moscow", "Russia"));
        customers.add(generateCustomer("Clancy", "Wiggum", Gender.MALE, "driveSafely@police.us", "911", 1, 1, 1960, "Police-Station-Place 1", "9851496", "Springfield", "USA"));
        customers.add(generateCustomer("Sarah", "Wiggum", Gender.FEMALE, null, null, 2, 6, 1963, "Police-Station-Place 1", "9851496", "Springfield", "USA"));
        customers.add(generateCustomer("Julius", "Hibbert", Gender.MALE, "best_doctor_in_town@doctor.us", "911", 26, 9, 1965, "Hospital-Place 1", "9851496", "Springfield", "USA"));
        customers.add(generateCustomer("Nick", "Riviera", Gender.MALE, "cheapest_doctor_in_town@doctor.us", "0900 123 123", 29, 12, 1976, "Hospital-Place 56", "9851496", "Springfield", "USA"));
        customers.add(generateCustomer("Otto", "Mann", Gender.MALE, "legalizeIt@now.nl", null, 24, 11, 1986, "Schoolbus-Driver-Seat", "9851496", "Springfield", "USA"));

        for (Customer c : customers) {
            dao.save(c);
        }

        /*Cash cash = new Cash();
        cash.setCustomer(ci);
        cash.setDeleted(false);
        cashDao.save(cash);*/
    }

    private Customer generateCustomer(String firstName, String lastName, Gender gender, String email, String phone,
                                      int day, int month, int year, String street, String postalcode, String city, String country) {
        Customer c = new Customer();
        c.setFirstname(firstName);
        c.setLastname(lastName);
        if (gender == null) gender = Gender.MALE;
        c.setGender(gender);
        if (email != null) c.setEmail(email);
        if (phone != null) c.setPhoneNumber(phone);
        c.setAddress(GeneratorUtils.generateAddress(street, postalcode, city, country));

        Date birthDay = GeneratorUtils.generateDate(year, month, day);
        if (birthDay != null) c.setDateOfBirth(birthDay);


        return c;
    }
}
