package at.ac.tuwien.inso.ticketline.datagenerator.generator;

import at.ac.tuwien.inso.ticketline.dao.*;
import at.ac.tuwien.inso.ticketline.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.function.Function;

/**
 * Created by eni on 08.05.2016.
 */
@Component
public class PerformanceGenerator implements DataGenerator {
    private static final Logger LOGGER = LoggerFactory.getLogger(PerformanceGenerator.class);

    @Autowired
    private PerformanceDao performanceDao;
    @Autowired
    private MerchandiseDao merchandiseDao;
    @Autowired
    private ArtistDao artistDao;
    @Autowired
    private ParticipationDao participationDao;
    @Autowired
    private ShowDao showDao;
    @Autowired
    private RoomDao roomDao;
    @Autowired
    private LocationDao locationDao;
    @Autowired
    private TicketGenerator ticketGenerator;
    @Autowired
    private TicketIdentifierDao ticketIdentifierDao;
    @Autowired
    private CustomerDao customerDao;
    @Autowired
    private ReservationDao reservationDao;
    @Autowired
    private EmployeeDao employeeDao;
    @Autowired
    private TicketDao ticketDao;
    @Autowired
    private PremiumDao premiumDao;


    private Random random = new Random();

    private static final String[][] merchandiseArticleNames = {{"T-Shirt - S", "A great T-Shirt Size S"}, {"T-Shirt - M", "A great T-Shirt Size M"},
            {"T-Shirt - L", "A great T-Shirt Size L"}, {"T-Shirt - XL", "A great T-Shirt Size XL"}, {"Keychain", "Beautiful Keychain"}, {"Scarf", "Pretty and warm"},
            {"Shoelaces", "Never lose your shoes again!"}, {"Sweatband", "No deo? No Problem!"}, {"Cap", "A pretty cap for him or her"},
            {"Sweatshirt - S", "A pretty Sweatshirt Size S"}, {"Sweatshirt - M", "A pretty Sweatshirt Size M"}, {"Sweatshirt - L", "A pretty Sweatshirt Size L"},
            {"Girly-shirt - XS", "Beautiful Shirt for her Size XS"}, {"Girly-shirt - S", "Beautiful Shirt for her Size S"}, {"Girly-shirt - M", "Beautiful Shirt for her Size M"},
            {"Girly-shirt - L", "Beautiful Shirt for her Size L"}, {"Wallet", "Useful for your money"}, {"Pants", "No more cold legs!"}, {"Fan-Underwear", "Only for hardcore fans!"},
            {"DVD", "Watch the performance again at home"}, {"Umbrella", "Prevents you from getting wet"}, {"Raincoat", "Prevents you from getting wet"},
            {"Poster - S", "Hang it on your wall"}, {"Poster - L", "Hang it on your wall"}, {"Belt", "Never lose your pants anymore!"}};

    private static final String[] firstNamesFemale = {"Mia", "Emma", "Hannah", "Sofia", "Anna", "Emilia", "Lina", "Marie", "Lena",
            "Mila", "Emily", "Lea", "Leonie", "Amelie", "Sophie", "Johanna", "Luisa", "Clara", "Lilly", "Laura", "Nele", "Lara",
            "Charlotte", "Leni", "Maja", "Frieda", "Mathilda", "Ida", "Ella", "Pia", "Sarah", "Lia", "Lotta", "Greta", "Melina",
            "Julia", "Paula", "Lisa", "Marlene", "Zoe", "Alina", "Victoria", "Mira", "Elisa", "Isabella", "Helena", "Josephine",
            "Mara", "Isabell", "Nora", "Antonia", "Lucy", "Emely", "Jana", "Pauline", "Amy", "Anni", "Merle", "Finja", "Katharina",
            "Luise", "Elena", "Theresa", "Annika", "Luna", "Romy", "Maria", "Stella", "Fiona", "Jasmin", "Magdalena", "Jule",
            "Milena", "Mina", "Carla", "Eva", "Martha", "Nina", "Annabell", "Melissa", "Elina", "Carlotta", "Paulina", "Maila",
            "Elif", "Elisabeth", "Ronja", "Zoey", "Chiara", "Tilda", "Miriam", "Franziska", "Valentina", "Juna", "Linda",
            "Thea", "Elli", "Rosalie", "Selina", "Fabienne", "Olivia", "Helene", "Lotte", "Lenja", "Amalia", "Alexandra", "" +
            "Sina", "Vanessa", "Jolina", "Elsa", "Leila", "Emmi", "Hailey", "Mariella", "Jette", "Aaliyah", "Anastasia",
            "Amelia", "Lucia", "Samira", "Aurelia", "Tessa", "Marla", "Luana", "Alisa", "Celina", "Aylin", "Joleen", "Kira",
            "Milla", "Ela", "Carolin", "Lana", "Amira", "Diana", "Leticia", "Alicia", "Lynn", "Liana", "Angelina", "Carolina",
            "Malia", "Mona", "Vivien", "Julie", "Rebecca", "Laila", "Liv", "Lene", "Marleen"};

    private static final String[] firstNamesMale = {"Ben", "Jonas", "Leon", "Elias", "Finn", "Noah", "Paul", "Luis", "Lukas",
            "Luca", "Felix", "Maximilian", "Henry", "Max", "Emil", "Moritz", "Jakob", "Niklas", "Tim", "Julian", "Oskar",
            "Anton", "Philipp", "David", "Liam", "Alexander", "Theo", "Tom", "Mats", "Jan", "Matteo", "Samuel", "Erik",
            "Fabian", "Milan", "Leo", "Jonathan", "Rafael", "Simon", "Vincent", "Lennard", "Carl", "Linus", "Hannes", "Jona",
            "Mika", "Jannik", "Nico", "Till", "Johannes", "Marlon", "Leonard", "Benjamin", "Johann", "Mattis", "Adrian",
            "Julius", "Florian", "Constantin", "Daniel", "Aaron", "Maxim", "Nick", "Lenny", "Valentin", "Ole",
            "Luke", "Levi", "Nils", "Jannis", "Sebastian", "Tobias", "Marvin", "Joshua", "Mohammed", "Timo", "Phil", "Joel",
            "Benedikt", "John", "Robin", "Toni", "Dominic", "Damian", "Artur", "Pepe", "Lasse", "Malte", "Sam", "Bruno",
            "Gabriel", "Lennox", "Justus", "Kilian", "Theodor", "Oliver", "Jamie", "Levin", "Lian", "Noel", "Jayden",
            "Bennet", "Bastian", "Colin", "Lias", "Matti", "Jason", "Adam", "Michael", "Emilio", "Tyler", "Lars", "Marc",
            "Leopold", "Milo", "Richard", "Christian", "Nicolas", "Frederik", "Franz", "Fritz", "Lio", "Matthias", "Ferdinand",
            "Jannes", "Manuel", "Piet", "Elia", "Ilias", "Ludwig", "Silas", "Emilian", "Fiete", "Malik", "Carlo", "Joris",
            "Ali", "Emir", "Martin", "Michel", "Neo", "Jonte", "Dean", "Henrik", "Lion", "Leonardo", "Fabio", "Friedrich",
            "Len", "Alessio", "Thilo"};


    private static final String[] lastNames = {"Müller", "Schmidt", "Schneider", "Fischer", "Weber", "Meyer", "Wagner",
            "Becker", "Schulz", "Hoffmann", "Schäfer", "Koch", "Bauer", "Richter", "Klein", "Wolf", "Schröder", "Neumann",
            "Schwarz", "Zimmermann", "Braun", "Krüger", "Hofmann", "Hartmann", "Lange", "Schmitt", "Werner", "Schmitz",
            "Krause", "Meier", "Lehmann", "Schmid", "Schulze", "Maier", "Köhler", "Herrmann", "König", "Walter", "Mayer",
            "Huber", "Kaiser", "Fuchs", "Peters", "Lang", "Scholz", "Müller", "Weiß", "Jung", "Hahn", "Schubert", "Vogel",
            "Friedrich", "Keller", "Günther", "Frank", "Berger", "Winkler", "Roth", "Beck", "Lorenz", "Baumann", "Franke",
            "Albrecht", "Schuster", "Simon", "Ludwig", "Böhm", "Winter", "Kraus", "Martin", "Schumacher", "Krämer", "Vogt",
            "Stein", "Jäger", "Otto", "Sommer", "Groß", "Seidel", "Heinrich", "Brandt", "Haas", "Schreiber", "Graf", "Schulte",
            "Dietrich", "Ziegler", "Kuhn", "Kühn", "Pohl", "Engel", "Horn", "Busch", "Bergmann", "Thomas", "Voigt", "Sauer",
            "Arnold", "Wolff", "Pfeiffer"};

    private static final String[] festivalBandNames = {"Rammstein", "The Offspring", "Bullet for my Valentine", "Disturbed",
            "Red hot chilli peppers", "Wanda", "Zoe", "System of a down", "Seiler und Speer", "Korn", "Skindred",
            "Bloodsucking Zombies from outer space", "Children of Bodom", "Twisted Sistter", "Killswitch engage", "KIZ", "Billy Talent",
            "Sum41", "Blink 182", "EAV", "Wolfgang Ambros", "Amon Amarth", "Attila", "Viech", "Dropkick Murphys", "Alice Cooper", "August burns red"};

    private static final String[] concertRoles = {"Lead-Guitarist", "Bass-Player", "Background-singer", "Pianist", "Drummer", "Percussionist"};
    private static final String[] operaRoles = {"Bassbariton", "Bariton", "Sopran", "Tenor", "Maestro", "Violonist", "Pianist", "Flautist", "Bassoonist"};
    private static final String[] theaterRoles = {"Souffleur", "Protagonist", "Bad guy", "Good guy"};
    private static final String[] musicalRoles = {"Lead-Singer", "Souffleur", "Background-Singer", "Good guy", "Bad guy", "Pianist", "Maestro", "Bassoonist"};
    private static final String[] movieRoles = {"Superhero", "Good guy", "Bad guy"};
    private static final String[] festivalRoles = {"Band"};

    private  Artist[] festivalBands;

    public void generate() {

        LOGGER.info("+++++ Generate Performance Data +++++");


        // generate performances
        List<Performance> performances = generatePerformances();

        LOGGER.info("+++++ Generated " + performances.size() + " performances +++++");

        // save performances, generate its Articles and its Participators

        performances = performanceDao.save(performances);

        for (Performance p : performances) {
            generateArticlesForPerformance(p);
            generateParticipatorForPerformance(p);
        }


        LOGGER.info("+++++ Generate Location Data +++++");
        Map<PerformanceType, List<Location>> locationMap = generateLocations();

        LOGGER.info("+++++ Generate Room Data +++++");
        Map<PerformanceType, List<Room>> roomMap = generateRooms(locationMap);

        Map<Room, List<Seat>> seatMap = new HashMap<>();

        LOGGER.info("+++++ Generate Seat Data +++++");
        LOGGER.info("+++++ Generate Concert Seats  +++++");
        generateSeats(roomMap, seatMap, PerformanceType.CONCERT, ticketGenerator::generateSeatsConcert);

        LOGGER.info("+++++ Generate Festival Seats  +++++");
        generateSeats(roomMap, seatMap, PerformanceType.FESTIVAL, ticketGenerator::generateSeatsConcert);

        LOGGER.info("+++++ Generate Movie Seats  +++++");
        generateSeats(roomMap, seatMap, PerformanceType.MOVIE, ticketGenerator::generateSeatsConcert);

        LOGGER.info("+++++ Generate Opera Seats  +++++");
        generateSeats(roomMap, seatMap, PerformanceType.OPER, ticketGenerator::generateSeatsTheater);

        LOGGER.info("+++++ Generate Theater Seats  +++++");
        generateSeats(roomMap, seatMap, PerformanceType.THEATER, ticketGenerator::generateSeatsTheater);


        LOGGER.info("+++++ Generate Show Data +++++");
        Map<Show, Room> shows = generateShows(performances, roomMap);


        LOGGER.info("+++++ Generate Tickets  +++++");
        int amount = shows.keySet().size();
        int count = 0;
        for (Show s : shows.keySet()) {
            ticketGenerator.generateTickets(seatMap.get(shows.get(s)), s, 50);
            count++;
            printProgressMsg(count, amount);
        }
        System.out.println("");
        LOGGER.info("+++++ Generate Premium  +++++");
        generatePremiums();

    }


    private List<Performance> generatePerformances() {
        List<Performance> performances = new ArrayList<>(30);
        performances.add(generatePerformance("Nova Rock", "Rock is life! \\m/", 4320, PerformanceType.FESTIVAL));
        performances.add(generatePerformance("Rock in Vienna", "We try to be as successful as Nova Rock", 4320, PerformanceType.FESTIVAL));
        performances.add(generatePerformance("Donauinselfest", "Not the best bands, but hey, it's free!", 4320, PerformanceType.FESTIVAL));
        performances.add(generatePerformance("Frequency", "Best festival for alternative fans", 4320, PerformanceType.FESTIVAL));
        performances.add(generatePerformance("Two days a week", "Nice little festival for music fans", 2880, PerformanceType.FESTIVAL));

        performances.add(generatePerformance("Lady Gaga", "Wanna see her pokerface?", 120, PerformanceType.CONCERT));
        performances.add(generatePerformance("Britney Spears", "Wait? She still makes music?", 100, PerformanceType.CONCERT));
        performances.add(generatePerformance("Rammstein", "The hardest German band on earth", 80, PerformanceType.CONCERT));
        performances.add(generatePerformance("WIZO", "A German punk band", 100, PerformanceType.CONCERT));
        performances.add(generatePerformance("Hansi Hinterseer", "You're probably old if you're going there", 90, PerformanceType.CONCERT));
        performances.add(generatePerformance("Russkaja", "Yes, they also perform outside of the 'Willkommen \u00D6sterreich' Studio!", 120, PerformanceType.CONCERT));
        performances.add(generatePerformance("Elton John", "Will he wear new glasses?", 180, PerformanceType.CONCERT));
        performances.add(generatePerformance("Zoe", "Does she have more than one song? Find out by going to her concert!", 120, PerformanceType.CONCERT));
        performances.add(generatePerformance("Tenacious D", "This is not a concert. It's just a tribute!", 120, PerformanceType.CONCERT));

        performances.add(generatePerformance("Wolfgang Amadeus Mozarts Die Zauberfl\u00f6te", "The classic Opera by Mozart", 120, PerformanceType.OPER));
        performances.add(generatePerformance("Richard Wagners Tristan und Isolde", "It's gonna be really long", 400, PerformanceType.OPER));

        performances.add(generatePerformance("Friedrich D\u00fcrrenmatts Die Physiker", "Funny piece about physicians", 120, PerformanceType.THEATER));
        performances.add(generatePerformance("William Shakespeare's Romeo and Juliet", "The classic tragedy restaged in a modern life", 100, PerformanceType.THEATER));
        performances.add(generatePerformance("Goethes Faust", "Will Mephisto win?", 130, PerformanceType.THEATER));
        performances.add(generatePerformance("Friedrich D\u00fcrrenmatts Der Besuch der alten Dame", "Will the old Lady die?", 150, PerformanceType.THEATER));
        performances.add(generatePerformance("Rumpelstiltskin", "For little knows my royal dame that Rumpelstiltskin is my name!", 130, PerformanceType.THEATER));
        performances.add(generatePerformance("Agatha Christie's The mousetrap", "Who did it?", 130, PerformanceType.THEATER));
        performances.add(generatePerformance("Oscar Wilde's The picture of Dorian Gray", "Spooky picture that ages", 130, PerformanceType.THEATER));
        performances.add(generatePerformance("Hansel and Gretel", "How long do you need to cook a witch?", 80, PerformanceType.THEATER));

        performances.add(generatePerformance("Harvey Fiersteins La cage aux folles", "A cage full of crazy persons", 130, PerformanceType.MUSICAL));
        performances.add(generatePerformance("Rocky Horror Show", "Brad Majors and Janet Weiss meet Dr. Frank N. Furter ", 110, PerformanceType.MUSICAL));
        performances.add(generatePerformance("Cats", "People disguised as cats acting like cats (and they are also singing)", 130, PerformanceType.MUSICAL));
        performances.add(generatePerformance("Tanz der Vampire", "People disguised as vampires acting and singing like vampires", 130, PerformanceType.MUSICAL));
        performances.add(generatePerformance("Hair", "Bald people are not allowed here!", 110, PerformanceType.MUSICAL));

        performances.add(generatePerformance("Star Wars VII", "Yes, the saga continues", 90, PerformanceType.MOVIE));
        performances.add(generatePerformance("Indiana Jones", "Another Harrison Ford Film", 110, PerformanceType.MOVIE));
        performances.add(generatePerformance("Shrek", "A green Ogre with its donkey try to save a princess", 120, PerformanceType.MOVIE));
        performances.add(generatePerformance("Monsters Inc", "Monsters scaring children to get electricity", 100, PerformanceType.MOVIE));
        performances.add(generatePerformance("Monsters university", "Monsters learning to scare children to gain electricity", 130, PerformanceType.MOVIE));
        performances.add(generatePerformance("Minions", "Hundreds of little yellow things walking around London trying to find a new leader", 110, PerformanceType.MOVIE));
        performances.add(generatePerformance("Finding Nemo", "A clownfish loses his son and tries to save him before he becomes sushi", 115, PerformanceType.MOVIE));

        return performances;
    }

    private Performance generatePerformance(String name, String description, int duration, PerformanceType type) {
        Performance p = new Performance();
        p.setName(name);
        p.setDescription(description);
        p.setDuration(duration);
        p.setPerformanceType(type);

        return p;
    }

    private void generateArticlesForPerformance(Performance p) {
        List<Integer> merchandiseIndices = getRandomIndices(merchandiseArticleNames.length - random.nextInt(10), merchandiseArticleNames.length);

        for (int i : merchandiseIndices) {
            Article a = generateArticle(merchandiseArticleNames[i][0], merchandiseArticleNames[i][1], random.nextInt(50) + 1, random.nextInt(90) + 10);

            a.setPerformance(p);
            merchandiseDao.save(a);
        }
    }

    private Article generateArticle(String name, String description, int stock, int price) {
        Article a = new Article();
        a.setName(name);
        a.setDescription(description);
        a.setAvailable(stock);
        a.setPrice(price);

        return a;
    }

    private List<Integer> getRandomIndices(int amount, int max) {
        if (amount > max) {
            amount = max;
        }

        List<Integer> indices = new ArrayList<>(amount);
        for (int i = 0; i < amount; i++) {
            int randomIndex;
            do {
                randomIndex = random.nextInt(max);

            } while (indices.contains(randomIndex));
            indices.add(randomIndex);
        }

        return indices;
    }

    private void generateParticipatorForPerformance(Performance p) {

        if (p.getPerformanceType() == PerformanceType.FESTIVAL) {
            int amount = random.nextInt(4) + 10;

            if(festivalBands == null || festivalBands.length == 0) {
                createFestivalBands();
            }

            List<Integer> festivalBandsIndices = getRandomIndices(amount, festivalBands.length);

            for (int i : festivalBandsIndices) {

              Artist a = festivalBands[i];

                Participation par = new Participation();
                par.setDescription(a.getDescription());
                par.setPerformance(p);
                par.setArtist(a);
                par.setArtistRole("Band");

                participationDao.save(par);

            }
        } else {

            int amount = random.nextInt(5) + 2;


            for (int i = 0; i < amount; i++) {
                String role = "";
                if (p.getPerformanceType() == PerformanceType.CONCERT) {
                    role = concertRoles[random.nextInt(concertRoles.length)];
                } else if (p.getPerformanceType() == PerformanceType.FESTIVAL) {
                    role = festivalRoles[random.nextInt(festivalRoles.length)];
                } else if (p.getPerformanceType() == PerformanceType.MOVIE) {
                    role = movieRoles[random.nextInt(movieRoles.length)];
                } else if (p.getPerformanceType() == PerformanceType.MUSICAL) {
                    role = musicalRoles[random.nextInt(musicalRoles.length)];
                } else if (p.getPerformanceType() == PerformanceType.OPER) {
                    role = operaRoles[random.nextInt(operaRoles.length)];
                } else if (p.getPerformanceType() == PerformanceType.THEATER) {
                    role = theaterRoles[random.nextInt(theaterRoles.length)];
                }


                Artist a = artistDao.save(generateRandomArtist(random.nextBoolean(), role));

                Participation par = new Participation();
                par.setDescription(a.getDescription());
                par.setPerformance(p);
                par.setArtist(a);
                par.setArtistRole(role);

                participationDao.save(par);

            }
        }
    }


    private void createFestivalBands() {
        festivalBands = new Artist[festivalBandNames.length];
        for (int i = 0; i < festivalBandNames.length; i++) {

            Artist a = new Artist();
            a.setFirstname(festivalBandNames[i]);
            a.setLastname("");
            String description = (Math.random() < 0.5 ? "Best" : "Coolest") + " and " + (Math.random() < 0.5 ? "loudest" : "hardest")
                    + " Band" + (Math.random() < 0.5 ? " in the world" : " of the universe");

            a.setDescription(description);

            festivalBands[i] = artistDao.save(a);
        }
    }

    private Artist generateRandomArtist(boolean male, String role) {
        Artist a = new Artist();

        a.setFirstname(male ? firstNamesMale[random.nextInt(firstNamesMale.length)] : firstNamesFemale[random.nextInt(firstNamesFemale.length)]);
        a.setLastname(lastNames[random.nextInt(lastNames.length)]);
        a.setDescription("Born in " + (random.nextInt(55) + 1940) + (male ? " he" : " she") + " is one of the best " + role + " of all time!");

        return a;
    }

    private Map<PerformanceType, List<Location>> generateLocations() {
        Map<PerformanceType, List<Location>> locations = new HashMap<>();
        List<Location> concertLocations = new ArrayList<>();
        List<Location> festivalLocations = new ArrayList<>();
        List<Location> movieLocations = new ArrayList<>();
        List<Location> theaterAndMusicalLocations = new ArrayList<>();
        List<Location> operaLocations = new ArrayList<>();

        LOGGER.info("+++++ Concert Locations +++++");
        concertLocations.add(generateLocation("Gasometer", "Nice location for medium concerts", "Guglgasse 6", "1110", "Vienna", "Austria"));
        concertLocations.add(generateLocation("Stadthalle", "Big location for big bands with a lot of fans", "Roland Rainer Platz 1", "1150", "Vienna", "Austria"));
        concertLocations.add(generateLocation("Arena", "Outdoor and indoor rooms", "Baumgasse 80", "1030", "Vienna", "Austria"));
        concertLocations.add(generateLocation("U4", "Little Club where little bands can play", "Sch\u00f6nbrunner Stra\u00dfe 222-228", "1120", "Vienna", "Austria"));
        concertLocations.add(generateLocation("Krieau", "Outdoor location", "Nordportalstra\u00dfe 247", "1020", "Vienna", "Austria"));

        LOGGER.info("+++++ Festival Locations +++++");
        festivalLocations.add(generateLocation("Pannonia Fields II", "Festival Location for Nova Rock", "Nova Rock Stra\u00dfe", "2425", "Nickelsdorf", "Austria"));
        festivalLocations.add(generateLocation("Green Park", "Festival Location for Frequency", "Kelsengasse 1", "3100", "St. P\u00f6lten", "Austria"));
        festivalLocations.add(generateLocation("Wiesen", "Festival Location for Two days a week", "Sch\u00f6llingstra\u00dfe", "7203", "Wiesen", "Austria"));
        festivalLocations.add(generateLocation("Donauinsel", "Outdoor festival location", "Donauinsel", "1220", "Wien", "Austria"));


        LOGGER.info("+++++ Movie Locations +++++");
        movieLocations.add(generateLocation("Apollo", "Nice Cinema in an old house that is build like a maze", "Gumpendorferstra\u00dfe 65", "1060", "Vienna", "Austria"));
        movieLocations.add(generateLocation("Village Cinema", "Nice Cinema in the center of Vienna", "Landstra\u00dfer Hauptstra\u00dfe 2A", "1030", "Vienna", "Austria"));
        movieLocations.add(generateLocation("Cineplexx Donauplex", "Cineplex in the 22th district of Vienna", "Wagramer Stra\u00dfe 79", "1220", "Vienna", "Austria"));
        movieLocations.add(generateLocation("UCI", "Cinema at the Shopping City South (SCS)", "SCS Shopping Mall", "2334", "Wiener Neudorf", "Austria"));
        movieLocations.add(generateLocation("Haydn Cinema", "English speaking cinema", "Mariahilfer Stra\u00dfe 57", "1060", "Vienna", "Austria"));
        movieLocations.add(generateLocation("Actors Studio", "Cineplex in the 1st district of Vienna", "Tuchlauben 13", "1010", "Vienna", "Austria"));
        movieLocations.add(generateLocation("Artis International", "Cineplex in the 1st district of Vienna", "Schultergasse 5", "1010", "Vienna", "Austria"));
        movieLocations.add(generateLocation("Cineplexx Amstetten", "Cineplex in Amstetten", "Waidhofner Stra\u00dfe 42b", "3300", "Amstetten", "Austria"));
        movieLocations.add(generateLocation("Cineplexx Graz", "Cineplex in Graz", "Alte Poststra\u00dfe 470", "8055", "Graz", "Austria"));
        movieLocations.add(generateLocation("Cineplexx Innsbruck", "Cineplex in Innsbruck", "Tschamlerstra\u00dfe 7", "6020", "Innsbruck", "Austria"));
        movieLocations.add(generateLocation("Cineplexx Linz", "Cineplex in Linz", "Prinz Eugen Stra\u00dfe 4020", "4020", "Linz", "Austria"));

        LOGGER.info("+++++ Theater and Musical Locations +++++");
        theaterAndMusicalLocations.add(generateLocation("Burgtheater", "A big theater in Vienna", "Universit\u00e4tsring 2", "1010", "Vienna", "Austria"));
        theaterAndMusicalLocations.add(generateLocation("Volkstheater", "A big theater in Vienna", "Neustiftgasse 1", "1070", "Vienna", "Austria"));
        theaterAndMusicalLocations.add(generateLocation("Kabarett Simpl", "A little theater in Vienna", "Wollzeile 36", "1010", "Vienna", "Austria"));
        theaterAndMusicalLocations.add(generateLocation("Rabenhof Theater", "A little theater in the 3rd district of Vienna", "Rabengasse 3", "1030", "Vienna", "Austria"));
        theaterAndMusicalLocations.add(generateLocation("Kammerspiele", "A little theater in the 1st district of Vienna", "Rotenturmstra\u00dfe 20", "1010", "Vienna", "Austria"));
        theaterAndMusicalLocations.add(generateLocation("Kom\u00f6die am Kai", "A little theater in the 1st district of Vienna", "Franz-Josefs-Kai 29", "1010", "Vienna", "Austria"));
        theaterAndMusicalLocations.add(generateLocation("Ronacher", "A little theater in the 1st district of Vienna", "Seilerst\u00e4tte 9", "1010", "Vienna", "Austria"));
        theaterAndMusicalLocations.add(generateLocation("Wiener Lustspielhaus", "A little theater in the 1st district of Vienna", "Am Hof", "1010", "Vienna", "Austria"));

        LOGGER.info("+++++ Opera Locations +++++");
        operaLocations.add(generateLocation("Staatsoper", "The Famouse Opera House of Vienna", "Opernring 2", "1010", "Vienna", "Austria"));
        operaLocations.add(generateLocation("Volksoper", "The other Opera House of Vienna", "W\u00e4hringer Stra\u00dfe 78", "1090", "Vienna", "Austria"));


        locations.put(PerformanceType.CONCERT, locationDao.save(concertLocations));
        locations.put(PerformanceType.FESTIVAL, locationDao.save(festivalLocations));
        locations.put(PerformanceType.MOVIE, locationDao.save(movieLocations));
        locations.put(PerformanceType.THEATER, locationDao.save(theaterAndMusicalLocations));
        locations.put(PerformanceType.OPER, locationDao.save(operaLocations));
        locations.put(PerformanceType.MUSICAL, locations.get(PerformanceType.THEATER)); // musicals and theaters are played in the same locations

        return locations;
    }

    private Location generateLocation(String name, String description, String street, String postalcode, String city, String country) {
        Location l = new Location();

        l.setName(name);
        l.setDescription(description);
        l.setAddress(GeneratorUtils.generateAddress(street, postalcode, city, country));

        String ownerName = random.nextBoolean() ? firstNamesMale[random.nextInt(firstNamesMale.length)] : firstNamesFemale[random.nextInt(firstNamesFemale.length)];
        ownerName += " " + lastNames[random.nextInt(lastNames.length)];
        l.setOwner(ownerName);

        return l;
    }

    private Map<PerformanceType, List<Room>> generateRooms(Map<PerformanceType, List<Location>> locationMap) {
        List<Room> concertRooms = new ArrayList<>();
        List<Room> theaterAndMusicalRooms = new ArrayList<>();
        List<Room> festivalRooms = new ArrayList<>();
        List<Room> movieRooms = new ArrayList<>();
        List<Room> operaRooms = new ArrayList<>();

        for (Location l : locationMap.get(PerformanceType.CONCERT)) {
            concertRooms.add(generateRoom("Gro\u00dfe Konzerthalle", random.nextInt(20) + 50, random.nextInt(10) + 50, l, false));
            concertRooms.add(generateRoom("Kleine Konzerthalle", random.nextInt(10) + 20, random.nextInt(10) + 20, l, false));
            concertRooms.add(generateRoom("Winzige Konzerthalle", random.nextInt(10) + 5, random.nextInt(10) + 5, l, false));
//            concertRooms.add(generateRoom("Mittlere Konzerthalle 1", random.nextInt(30) + 30, random.nextInt(30) + 30, l, false));
//            concertRooms.add(generateRoom("Mittlere Konzerthalle 2", random.nextInt(30) + 30, random.nextInt(30) + 30, l, false));
        }


        for (Location l : locationMap.get(PerformanceType.THEATER)) {
            theaterAndMusicalRooms.add(generateRoom("Gro\u00dfer Saal", random.nextInt(20) + 50, random.nextInt(10) + 50, l, false));
            theaterAndMusicalRooms.add(generateRoom("Kleiner Saal", random.nextInt(10) + 20, random.nextInt(10) + 20, l, false));
            theaterAndMusicalRooms.add(generateRoom("Winziger Saal", random.nextInt(10) + 5, random.nextInt(10) + 5, l, false));
//            theaterAndMusicalRooms.add(generateRoom("Mittlerer Saal", random.nextInt(30) + 30, random.nextInt(30) + 30, l, false));
        }

        for (Location l : locationMap.get(PerformanceType.FESTIVAL)) {
            festivalRooms.add(generateRoom("Red Stage", 55, 70, l, false));
            festivalRooms.add(generateRoom("Green Stage", 50, 50, l, false));
            festivalRooms.add(generateRoom("Purple Stage", 20, 20, l, false));
//            festivalRooms.add(generateRoom("Blue Stage", 30, 20, l, false));
        }

        for (Location l : locationMap.get(PerformanceType.MOVIE)) {
            movieRooms.add(generateRoom("Imax Saal", random.nextInt(10) + 100, random.nextInt(10) + 100, l, true));
//            movieRooms.add(generateRoom("Gro\u00dfer Saal", random.nextInt(10) + 70, random.nextInt(70) + 10, l, true));
            movieRooms.add(generateRoom("Mittlerer Saal", random.nextInt(20) + 10, random.nextInt(20) + 10, l, true));
            movieRooms.add(generateRoom("Kleiner Saal", random.nextInt(3) + 10, random.nextInt(3) + 10, l, true));
//            movieRooms.add(generateRoom("Saal A", random.nextInt(20) + 15, random.nextInt(10) + 10, l, true));
//            movieRooms.add(generateRoom("Saal B", random.nextInt(10) + 50, random.nextInt(4) + 30, l, true));
//            movieRooms.add(generateRoom("Saal C", random.nextInt(5) + 15, random.nextInt(10) + 20, l, true));
//            movieRooms.add(generateRoom("Saal D", random.nextInt(4) + 20, random.nextInt(10) + 15, l, true));
//            movieRooms.add(generateRoom("Saal E", random.nextInt(10) + 30, random.nextInt(10) + 20, l, true));
//            movieRooms.add(generateRoom("Saal F", random.nextInt(20) + 30, random.nextInt(5) + 30, l, true));
        }

        for (Location l : locationMap.get(PerformanceType.OPER)) {
            operaRooms.add(generateRoom("Hauptsaal", random.nextInt(10) + 40, random.nextInt(10) + 40, l, false));
            operaRooms.add(generateRoom("Nebensaal", random.nextInt(20) + 30, random.nextInt(10) + 30, l, false));
        }


        Map<PerformanceType, List<Room>> result = new HashMap<>();
        result.put(PerformanceType.CONCERT, roomDao.save(concertRooms));
        result.put(PerformanceType.FESTIVAL, roomDao.save(festivalRooms));
        result.put(PerformanceType.MOVIE, roomDao.save(movieRooms));
        result.put(PerformanceType.THEATER, roomDao.save(theaterAndMusicalRooms));
        result.put(PerformanceType.OPER, roomDao.save(operaRooms));
        result.put(PerformanceType.MUSICAL, result.get(PerformanceType.THEATER)); // Theater and Musicals share the same locations

        return result;
    }

    private Room generateRoom(String name, int x, int y, Location l, boolean screen) {
        Room r = new Room();
        r.setName(name);


        int separatorHSide = (y * (random.nextInt(5) + 15)) / 100;
        int separatorHCenter = y - 2 * separatorHSide;

        int separatorVSide = (x * (random.nextInt(5) + 15)) / 100;
        int separatorVCenter = x - 2 * separatorVSide;

        // {20X30}-(HS 5|20|5|,VS 10|5|5|)-Screen:Große Halle
//        String description = String.format("{%dX%d}-(HS %d|%d|%d|,VS %d|%d|%d|)-%s:%s",
//                x, y, separatorHSide, separatorHCenter, separatorHSide, separatorVSide, separatorVCenter, separatorVSide,
//                screen ? "Screen" : "Stage", name);

        String description = "{" + x + "X" + y + "}-(HS " + separatorHSide + "|" + separatorHCenter + "|" + separatorHSide + "|"
                + ",VS " + separatorVSide + "|" + separatorVCenter + "|" + separatorVSide + "|)-" + (screen ? "Screen" : "Stage") + ":" + name;

        r.setDescription(description);
        r.setLocation(l);

        return r;
    }

    private Map<Show, Room> generateShows(List<Performance> performances, Map<PerformanceType, List<Room>> roomMap) {
        List<Room> concertRooms = roomMap.get(PerformanceType.CONCERT);
        List<Room> festivalRooms = roomMap.get(PerformanceType.FESTIVAL);
        List<Room> movieRooms = roomMap.get(PerformanceType.MOVIE);
        List<Room> theaterAndMusicalRooms = roomMap.get(PerformanceType.THEATER);
        List<Room> operaRooms = roomMap.get(PerformanceType.OPER);

        Map<Show, Room> shows = new HashMap<>();

        for (Performance p : performances) {
            Room r = null;

            if (p.getPerformanceType() == PerformanceType.CONCERT) {
                r = concertRooms.get(random.nextInt(concertRooms.size()));
            } else if (p.getPerformanceType() == PerformanceType.FESTIVAL) {
                r = festivalRooms.get(random.nextInt(festivalRooms.size()));
            } else if (p.getPerformanceType() == PerformanceType.MOVIE) {
                r = movieRooms.get(random.nextInt(movieRooms.size()));
            } else if (p.getPerformanceType() == PerformanceType.THEATER || p.getPerformanceType() == PerformanceType.MUSICAL) {
                r = theaterAndMusicalRooms.get(random.nextInt(theaterAndMusicalRooms.size()));
            } else if (p.getPerformanceType() == PerformanceType.OPER) {
                r = operaRooms.get(random.nextInt(operaRooms.size()));
            }

            // Festivals should only appear once
            int amount = p.getPerformanceType() == PerformanceType.FESTIVAL ? 1 : (random.nextInt(6) + 1);

            for (int i = 0; i < amount; i++) {

                int year = Calendar.getInstance().get(Calendar.YEAR) + random.nextInt(2);
                int month = random.nextInt(12) + 1;
                int day = random.nextInt(28) + 1;
                int hour = random.nextInt(5) + 17;
                int minute = random.nextInt(60);
                minute -= minute % 10;

                Show s = generateShow(p, year, month, day, hour, minute, 0, r);
                s = showDao.save(s);
                shows.put(s, r);
            }
        }
        return shows;

    }

    private Show generateShow(Performance p, int year, int month, int day, int hour, int minute, int second, Room r) {
        Show s = new Show();
        s.setPerformance(p);
        s.setCanceled(false);
        s.setDateOfPerformance(GeneratorUtils.generateDate(year, month, day, hour, minute, second));
        s.setRoom(r);

        return s;
    }

    private void generateSeats(Map<PerformanceType, List<Room>> roomMap, Map<Room, List<Seat>> seatMap,
                               PerformanceType type, Function<Room, List<Seat>> function) {
        int count = 0;
        int amount = roomMap.get(type).size();
        for (Room r : roomMap.get(type)) {
            printProgressMsg(count, amount);
            seatMap.put(r, function.apply(r)); //ticketGenerator.generateSeatsTheater(r));
            count++;
        }
        printProgressMsg(count, amount);
        System.out.println("");
    }

    private void printProgressMsg(int counter, int amount) {
        for (int i = 0; i < 115; i++) {
            System.out.print("\r");
        }
        System.out.print(counter + "/" + amount + " created [");

        // draw percent bar
        int percent = counter * 100 / amount;
        for (int i = 0; i < 100; i++) {
            System.out.print(i < percent ? "#" : " ");

        }
        System.out.print("]");
    }


    private void generatePremiums() {
        String[][] premiumNames = {{"T-Shirt - S", "A great T-Shirt - Size S"}, {"T-Shirt - M", "A great T-Shirt - Size M"},
                {"T-Shirt - L", "A great T-Shirt - Size L"}, {"T-Shirt - XL", "A great T-Shirt - Size XL"}, {"Keychain", "Beautiful Keychain"}, {"Scarf", "Pretty and warm"},
                {"Shoelaces", "Never lose your shoes again!"}, {"Sweatband", "No deo? No Problem!"}, {"Cap", "A pretty cap for him or her"},
                {"Sweatshirt - S", "A pretty Sweatshirt - Size S"}, {"Sweatshirt - M", "A pretty Sweatshirt - Size M"}, {"Sweatshirt - L", "A pretty Sweatshirt - Size L"},
                {"Girly-shirt - XS", "Beautiful Shirt for her (or him, we don't judge you) - Size XS"}, {"Girly-shirt - S", "Beautiful Shirt for her (or him, we don't judge you) - Size S"},
                {"Girly-shirt - M", "Beautiful Shirt for her (or him, we don't judge you) - Size M"}, {"Girly-shirt - L", "Beautiful Shirt for her (or him, we don't judge you) - Size L"},
                {"Wallet", "Useful for your money"}, {"Pants", "No more cold legs!"}, {"Fan-Underwear", "Only for hardcore fans!"},
                {"DVD", "Watch the performance again at home"}, {"Umbrella", "Prevents you from getting wet"}, {"Raincoat", "Keeps you warm and prevents you from getting wet"},
                {"Poster - S", "Hang it on your wall"}, {"Poster - L", "Hang it on your wall"}, {"Belt", "Never lose your pants anymore!"}};

        for (int i  =0 ; i<20; i++){
            Premium p = new Premium();
            p.setName(premiumNames[i][0] );
            p.setAvailable((int)(Math.random()*20+5));
            p.setDescription(premiumNames[i][1]);
            p.setPrice((int)(Math.random()*20+10));
            premiumDao.save(p);
        }
    }


}
