package at.ac.tuwien.inso.ticketline.datagenerator.generator;

import at.ac.tuwien.inso.ticketline.dao.EmployeeDao;
import at.ac.tuwien.inso.ticketline.model.Employee;
import at.ac.tuwien.inso.ticketline.model.Permission;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.time.ZonedDateTime;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * This class generates data for employees
 * @see at.ac.tuwien.inso.ticketline.model.Employee
 */
@Component
public class EmployeeGenerator implements DataGenerator {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmployeeGenerator.class);

    @Autowired
    private EmployeeDao dao;

    @Autowired
    private PasswordEncoder encoder;

    /**
     * {@inheritDoc}
     */
    public void generate() {
        LOGGER.info("+++++ Generate Employee Data +++++");

        Date lastlogin = GregorianCalendar.from(ZonedDateTime.now().minusDays(5)).getTime();

        Employee e1 = new Employee("Admin","Admin","admin",this.encoder.encode("admin"));
        e1.setLastLogin(lastlogin);
        e1.setPermission(Permission.ADMINISTRATOR);
        dao.save(e1);
        e1 = new Employee("Ahmed","Othman","ahmed",this.encoder.encode("1325531"));
        e1.setPermission(Permission.USER);
        e1.setLastLogin(lastlogin);
        dao.save(e1);
        e1 = new Employee("Betim","Bryma","betim",this.encoder.encode("1325255"));
        e1.setPermission(Permission.USER);
        e1.setLastLogin(lastlogin);
        dao.save(e1);
        e1 = new Employee("Enrik","Ndou","enrik",this.encoder.encode("1426910"));
        e1.setPermission(Permission.USER);
        e1.setLastLogin(lastlogin);
        dao.save(e1);
        e1 = new Employee("Florian","Lux","florian",this.encoder.encode("1126939"));
        e1.setPermission(Permission.USER);
        e1.setLastLogin(lastlogin);
        dao.save(e1);
        e1 = new Employee("Jakob","Knapp","jakob",this.encoder.encode("1327386"));
        e1.setPermission(Permission.USER);
        e1.setLastLogin(lastlogin);
        dao.save(e1);
        e1 = new Employee("Michael","Oppitz","michael",this.encoder.encode("1227129"));
        e1.setPermission(Permission.USER);
        e1.setLastLogin(lastlogin);
        dao.save(e1);

    }

}
