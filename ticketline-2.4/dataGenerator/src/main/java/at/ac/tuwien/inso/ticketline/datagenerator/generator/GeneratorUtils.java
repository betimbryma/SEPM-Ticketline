package at.ac.tuwien.inso.ticketline.datagenerator.generator;

import at.ac.tuwien.inso.ticketline.model.Address;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Florian on 31.05.2016.
 */
public class GeneratorUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(GeneratorUtils.class);

    public static Address generateAddress(String street, String postalcode, String city, String country) {
        Address a = new Address();
        a.setStreet(street);
        a.setPostalCode(postalcode);
        a.setCity(city);
        a.setCountry(country);

        return a;
    }

    public static Date generateDate(int year, int month, int day) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
        if (year <= 0 || month <= 0 || day <= 0 || month > 12 || day > 31) {
            LOGGER.debug("Invalid date: " + day + "." + month + "." + year);
            return null;
        }
        try {
            return formatter.parse(year + "/" + month + "/" + day);
        } catch (ParseException e) {
            LOGGER.debug("Invalid date: " + day + "." + month + "." + year);
            return null;
        }
    }

    public static Date generateDate(int year, int month, int day, int hour, int minute, int second) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");

        if (year <= 0 || month <= 0 || day <= 0 || hour < 0 || minute < 0 || second < 0
                || month > 12 || day > 31 || hour > 23 || minute > 59 || second > 59) {
            LOGGER.debug("Invalid date: " + day + "." + month + "." + year + " - " + hour + ":" + minute + ":" + second);
            return null;
        }
        try {
//            LOGGER.debug("Valid date: " + day + "." + month + "." + year + " - " + hour + ":" + minute + ":" + second);
            return formatter.parse(year + "/" + month + "/" + day + " " + hour + ":" + minute + ":" + second);
        } catch (ParseException e) {
            LOGGER.debug("Invalid date: " + day + "." + month + "." + year + " - " + hour + ":" + minute + ":" + second);
            return null;
        }
    }


}
