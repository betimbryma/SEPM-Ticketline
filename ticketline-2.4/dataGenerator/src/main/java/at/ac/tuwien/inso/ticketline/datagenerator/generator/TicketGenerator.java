package at.ac.tuwien.inso.ticketline.datagenerator.generator;

import at.ac.tuwien.inso.ticketline.dao.*;
import at.ac.tuwien.inso.ticketline.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Betim on 5/15/2016.
 */
@Component
public class TicketGenerator implements DataGenerator {

    @Autowired
    private CategoryDao categoryDao;

    @Autowired
    private RowDao rowDao;

    @Autowired
    private GalleryDao galleryDao;

    @Autowired
    private SeatDao seatDao;

    @Autowired
    private TicketDao ticketDao;

    private Category vip, specialGuest, normal;

    private Row row1, row2, row3, row4, row5, row6, row7, row8, row9, row10;

    private Gallery gallery1, gallery2, gallery3, gallery4, gallery5, gallery6, gallery7;

    public void generate() {

        vip = new Category();
        vip.setDescription("This category of tickets is meant for VIP");
        vip.setName("VIP");
        categoryDao.save(vip);

        specialGuest = new Category();
        specialGuest.setDescription("This category of tickets is meant for special guests");
        specialGuest.setName("Special guest");
        categoryDao.save(specialGuest);

        normal = new Category();
        normal.setDescription("This category of tickets is meant for normal attendees");
        normal.setName("Normal");
        categoryDao.save(normal);

        row1 = new Row();
        row1.setDescription("The first row");
        row1.setName("#1Row");
        row1.setOrder(1);

        row2 = new Row();
        row2.setDescription("The second row");
        row2.setName("#2Row");
        row2.setOrder(2);

        row3 = new Row();
        row3.setDescription("The third row");
        row3.setName("#3Row");
        row3.setOrder(3);

        row4 = new Row();
        row4.setDescription("The fourth row");
        row4.setName("#4Row");
        row4.setOrder(4);

        row5 = new Row();
        row5.setDescription("The fifth row");
        row5.setName("#5Row");
        row5.setOrder(5);

        row6 = new Row();
        row6.setDescription("The sixth row");
        row6.setName("#6Row");
        row6.setOrder(6);

        row7 = new Row();
        row7.setDescription("The seventh row");
        row7.setName("#7Row");
        row7.setOrder(7);

        row8 = new Row();
        row8.setDescription("The eighth row");
        row8.setName("#8Row");
        row8.setOrder(8);

        row9 = new Row();
        row9.setDescription("The ninth row");
        row9.setName("#9Row");
        row9.setOrder(9);

        row10 = new Row();
        row10.setDescription("The tenth row");
        row10.setName("#10Row");
        row10.setOrder(10);

        row1 = rowDao.save(row1);
        row2 = rowDao.save(row2);
        row3 = rowDao.save(row3);
        row4 = rowDao.save(row4);
        row5 = rowDao.save(row5);
        row6 = rowDao.save(row6);
        row7 = rowDao.save(row7);
        row8 = rowDao.save(row8);
        row9 = rowDao.save(row9);
        row10 = rowDao.save(row10);

        gallery1 = new Gallery();
        gallery1.setName("#1");
        gallery1.setDescription("First column");
        gallery1.setOrder(1);
        gallery1 = galleryDao.save(gallery1);

        gallery2 = new Gallery();
        gallery2.setName("#2");
        gallery2.setDescription("Second column");
        gallery2.setOrder(2);
        gallery2 = galleryDao.save(gallery2);

        gallery3 = new Gallery();
        gallery3.setName("#3");
        gallery3.setDescription("Third column");
        gallery3.setOrder(3);
        gallery3 = galleryDao.save(gallery3);

        gallery4 = new Gallery();
        gallery4.setName("#4");
        gallery4.setDescription("Fourth column");
        gallery4.setOrder(4);
        gallery4 = galleryDao.save(gallery4);

        gallery5 = new Gallery();
        gallery5.setName("#5");
        gallery5.setDescription("Fifth column");
        gallery5.setOrder(5);
        gallery5 = galleryDao.save(gallery5);

        gallery6 = new Gallery();
        gallery6.setName("#6");
        gallery6.setDescription("Sixth column");
        gallery6.setOrder(6);
        gallery6 = galleryDao.save(gallery6);

        gallery7 = new Gallery();
        gallery7.setName("#7");
        gallery7.setDescription("Seventh column");
        gallery7.setOrder(7);
        gallery7 = galleryDao.save(gallery7);

    }

    public List<Seat> generateSeats(Room room) {
        List<Seat> seatList = new ArrayList<>();
        int coulumn = 0;
        for (int i = 1; i <= 70; i++) {
            coulumn++;
            if (coulumn == 8)
                coulumn = 1;
            Seat seat = new Seat();
            seat.setName("Seat #" + i);
            seat.setDescription("Seat nr." + i + " located in" + room.getName());
            if (i <= 28) {
                seat.setCategory(specialGuest);
                if (i <= 7)
                    seat.setRow(row1);
                else if (i > 7 && i <= 14)
                    seat.setRow(row2);
                else if (i > 14 && i <= 21)
                    seat.setRow(row3);
                else
                    seat.setRow(row4);
            } else if ((28 < i) && (i <= 49)) {
                seat.setCategory(vip);
                if (i <= 35)
                    seat.setRow(row5);
                else if (i > 35 && i <= 42)
                    seat.setRow(row6);
                else
                    seat.setRow(row7);
            } else {
                seat.setCategory(normal);
                if (i > 49 && i <= 56)
                    seat.setRow(row8);
                else if (i > 56 && i <= 63)
                    seat.setRow(row9);
                else
                    seat.setRow(row10);
            }
            seat.setRoom(room);

            switch (coulumn) {
                case 1:
                    seat.setGallery(gallery1);
                    break;
                case 2:
                    seat.setGallery(gallery2);
                    break;
                case 3:
                    seat.setGallery(gallery3);
                    break;
                case 4:
                    seat.setGallery(gallery4);
                    break;
                case 5:
                    seat.setGallery(gallery5);
                    break;
                case 6:
                    seat.setGallery(gallery6);
                    break;
                default:
                    seat.setGallery(gallery7);
            }
            seat.setOrder(i);
            seatList.add(seatDao.save(seat));
        }
        return seatList;
    }

    public List<Seat> generateSeatsTheater(Room room) {
        List<Seat> seatList = new ArrayList<>();

        String description = room.getDescription();

        int x = -1;
        int y = -1;
        for (int i = 1; i < description.length(); i++) {
            if (description.charAt(i) == 'X') {
                x = i;
            }
            if (description.charAt(i) == '}') {
                y = i;
            }
        }

        int rows = Integer.parseInt(description.substring(1, x));
        int col = Integer.parseInt(description.substring(x + 1, y));

        int counter = 0;
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < col; j++) {
                Seat seat = new Seat();
                seat.setCategory(normal);
                seat.setDescription("Seat nr." + counter + "in the "+j+".row, located in" + room.getName());
                seat.setOrder(counter);
                seat.setRoom(room);



                Gallery g = new Gallery();
                g.setOrder(j+1);
                g.setName("#" + j+1);
                g.setDescription(j+1 + ". column");
                g = galleryDao.save(g);
                seat.setGallery(g);

                Row row = new Row();
                row.setDescription("The " + i+1 + ". row");
                row.setName("#" + i+1 + "Row");
                row.setOrder(i+1);
                row = rowDao.save(row);
                seat.setRow(row);

                seatList.add(seatDao.save(seat));

                counter++;
            }

        }

        return seatList;
    }

    public List<Seat> generateSeatsConcert(Room room) {
        List<Seat> seatList = new ArrayList<>();

        String description = room.getDescription();

        int x = -1;
        int y = -1;
        for (int i = 1; i < description.length(); i++) {
            if (description.charAt(i) == 'X') {
                x = i;
            }
            if (description.charAt(i) == '}') {
                y = i;
            }
        }

        int rows = Integer.parseInt(description.substring(1, x));
        int col = Integer.parseInt(description.substring(x + 1, y));

        int counter = 0;
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < col; j++) {
                Seat seat = new Seat();
                if(i<4) {
                    seat.setCategory(specialGuest);
                }else if(i>=4&&i<8){
                    seat.setCategory(vip);
                }else{
                    seat.setCategory(normal);
                }
                seat.setDescription("Seat nr." + counter + "in the "+j+1+".row, located in" + room.getName());
                seat.setOrder(counter);
                seat.setRoom(room);

                Gallery g = new Gallery();
                g.setOrder(j+1);
                g.setName("#" + j+1);
                g.setDescription(j+1 + ". column");
                g = galleryDao.save(g);
                seat.setGallery(g);

                Row row = new Row();
                row.setDescription("The " + i+1 + ". row");
                row.setName("#" + i+1 + "Row");
                row.setOrder(i+1);
                row = rowDao.save(row);
                seat.setRow(row);

                seatList.add(seatDao.save(seat));

                counter++;
            }

        }

        return seatList;
    }

    public List<Ticket> generateTickets(List<Seat> seats, Show show, Integer price) {
        List<Ticket> ticketList = new ArrayList<>();
        for (Seat seat : seats) {
            Ticket ticket = new Ticket();
            ticket.setDescription("Ticket for: " + show.getPerformance().getName());
            Category category = seat.getCategory();
            switch (category.getId()) {
                case 1:
                    ticket.setPrice(price + 10);
                    break;
                case 2:
                    ticket.setPrice(price + 20);
                    break;
                default:
                    ticket.setPrice(price);
            }
            ticket.setSeat(seat);
            ticket.setShow(show);
            ticketList.add(ticketDao.save(ticket));
        }
        return ticketList;
    }
}
