BEGIN TRANSACTION

----------------------------------- News -----------------------------------

INSERT INTO news(id, submittedon, title, newstext) VALUES (1, '2013-06-18 00:00:00.000', 'News 1', 'Text 1');
INSERT INTO news(id, submittedon, title, newstext) VALUES (2, '2013-06-18 00:00:00.000', 'News 2', 'Text 2');
INSERT INTO news(id, submittedon, title, newstext) VALUES (3, '2013-06-30 00:00:00.000', 'News 3', 'Text 3');
INSERT INTO news(id, submittedon, title, newstext) VALUES (4, '2013-06-30 00:00:00.000', 'News 4', 'Text 4');
INSERT INTO news(id, submittedon, title, newstext) VALUES (5, '2013-07-05 00:00:00.000', 'News 5', 'Text 5');
INSERT INTO news(id, submittedon, title, newstext) VALUES (6, '2013-07-08 00:00:00.000', 'News 6', 'Text 6');
INSERT INTO news(id, submittedon, title, newstext) VALUES (7, '2013-07-08 00:00:00.000', 'News 7', 'Text 7');
INSERT INTO news(id, submittedon, title, newstext) VALUES (8, '2013-07-08 00:00:00.000', 'News 8', 'Text 8');
INSERT INTO news(id, submittedon, title, newstext) VALUES (9, '2013-07-08 00:00:00.000', 'News 9', 'Text 9');
INSERT INTO news(id, submittedon, title, newstext) VALUES (10, '2013-07-09 00:00:00.000', 'News 10', 'Text 10');
COMMIT