package at.ac.tuwien.inso.ticketline.dao;

import at.ac.tuwien.inso.ticketline.model.*;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class TicketDaoTest extends AbstractDaoTest {

    @Autowired
    private ShowDao showDao;
    @Autowired
    private PerformanceDao performanceDao;
    @Autowired
    private LocationDao locationDao;
    @Autowired
    private RoomDao roomDao;
    @Autowired
    private TicketDao ticketDao;
    @Autowired
    private CategoryDao categoryDao;
    @Autowired
    private RowDao rowDao;
    @Autowired
    private GalleryDao galleryDao;
    @Autowired
    private SeatDao seatDao;
    @Autowired
    private TicketIdentifierDao ticketIdentifierDao;
    @Autowired
    private CustomerDao customerDao;
    @Autowired
    private EmployeeDao employeeDao;
    @Autowired
    private ReceiptDao receiptDao;
    @Autowired
    private ReceiptEntryDao receiptEntryDao;
    @Autowired
    private CashDao cashDao;

    public Integer initShowAndOtherStuff(){
        Performance performance = new Performance("dummy", "description", 120, PerformanceType.CONCERT);
        performance = performanceDao.save(performance);
        Address address = new Address("dummy", "dummy", "dummy", "dummy");
        Location location = new Location("dummy", "descritpion", "owner", address);
        location = locationDao.save(location);
        Room room = new Room("dummy", "description", location);
        room = roomDao.save(room);
        Date date =  new Date(new java.util.Date().getTime());
        Show show = new Show(false, date, room, performance);
        show = showDao.save(show);
        Category category = new Category("dummy", "description");
        category = categoryDao.save(category);
        Row row = new Row("dummy", "description", 1);
        row = rowDao.save(row);
        Gallery gallery = new Gallery("name", "description", 1);
        gallery = galleryDao.save(gallery);
        Seat seat = new Seat("dummy", "description", 1, room, category, row, gallery);
        seat = seatDao.save(seat);

        for(int i = 5; i <= 50; i += 5){
            Ticket t = new Ticket("description", i, show, seat);
            t = ticketDao.save(t);
            TicketIdentifier ticketIdentifier = new TicketIdentifier(true, t);
            ticketIdentifierDao.save(ticketIdentifier);
        }
        return show.getId();
    }

    @Test
    public void testMinPrice(){
        int min = ticketDao.getMinPrice(showDao.findOne(initShowAndOtherStuff()));
        assertEquals("min price should be 5", 5, min);
    }

    @Test
    public void testMaxPrice(){
        int max = ticketDao.getMaxPrice(showDao.findOne(initShowAndOtherStuff()));
        assertEquals("max price should be 50", 50, max);
    }

    @Test
    public void testSoldAmount(){
        Show show = showDao.findOne(initShowAndOtherStuff());
        Long count = ticketDao.countSoldByShow(show);
        assertEquals("no tickets sold", 0, count.intValue());

        sellTicket(show);

        count = ticketDao.countSoldByShow(show);
        assertEquals("one ticket sold", 1, count.intValue());
    }

    public void sellTicket(Show show){
        Date date =  new Date(new java.util.Date().getTime());
        Customer customer = new Customer(1, Gender.MALE, "x", "y", "z", "a", date, CustomerStatus.VALID, CustomerGroup.NORMAL, "b", date);
        customer = customerDao.save(customer);
        Employee employee = new Employee("a", "b", "c", "d");
        employee = employeeDao.save(employee);
        Cash cash = new Cash();
        cash.setCustomer(customer);
        cash = cashDao.save(cash);
        Receipt receipt = new Receipt(date, TransactionState.PAID, customer, employee, cash);
        receipt = receiptDao.save(receipt);
        List<Ticket> tickets = ticketDao.findAllTicketsForShow(show);
        List<TicketIdentifier> ticketIdentifiers = ticketIdentifierDao.findTicketIdentifierForTicket(tickets.get(0));
        ReceiptEntry receiptEntry = new ReceiptEntry(1, 1, 1, ticketIdentifiers.get(0), receipt);
        receiptEntryDao.save(receiptEntry);
    }


    @Test
    public void checkTicketUpdate(){

/*
        initShowAndOtherStuff();
        Ticket t = ticketDao.getOne(1);
        ticketDao.updateTicketForCartAsUsed(t);

        assertTrue(1==ticketDao.checkFlag(t));
        ticketDao.updateTicketForCartAsNotUsed(t);
        assertTrue(0==ticketDao.checkFlag(t));
t.getFlagdate();
        t = ticketDao.findOne(t.getId());
        java.util.Date d = new java.util.Date();

        Date newDate = new Date(d.getTime() + 2 * (3600*1000));

        ticketDao.updateDate(t,newDate);
        System.out.println(ticketDao.checkDate(t).after(d));
        System.out.println(d);

*/


    }
}
