package at.ac.tuwien.inso.ticketline.dao;

import at.ac.tuwien.inso.ticketline.model.*;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class ShowDaoTest extends AbstractDaoTest {

    @Autowired
    private ShowDao showDao;
    @Autowired
    private PerformanceDao performanceDao;
    @Autowired
    private LocationDao locationDao;
    @Autowired
    private RoomDao roomDao;

    @Test
    public void testFindAll() {
        List<Show> shows = showDao.findAll();
        assertEquals("Check DB initial data - is zero first", 0, shows.size());

        Performance p = new Performance("dummy", "description", 120, PerformanceType.CONCERT);
        p = performanceDao.save(p);
        Address a = new Address("dummy", "dummy", "dummy", "dummy");
        Location l = new Location("dummy", "descritpion", "owner", a);
        l = locationDao.save(l);
        Room r = new Room("dummy", "description", l);
        r = roomDao.save(r);
        Date d =  new Date(new java.util.Date().getTime());
        Show s = new Show(false, d, r, p);
        showDao.save(s);

        shows = showDao.findAll();
        assertEquals("Check DB initial data - should be two now", 1, shows.size());
    }

    @Test
    public void testFindOneById_NegativId() {
        assertNull(showDao.findOne(-1));
    }

    @Test
    public void testFindOneById_InvalidId() {
        assertNull(showDao.findOne(0));
    }

}
