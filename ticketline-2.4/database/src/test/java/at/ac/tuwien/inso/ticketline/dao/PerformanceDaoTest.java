package at.ac.tuwien.inso.ticketline.dao;

import at.ac.tuwien.inso.ticketline.model.Performance;
import at.ac.tuwien.inso.ticketline.model.PerformanceType;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static junit.framework.TestCase.assertTrue;

/**
 * Created by eni on 09.05.2016.
 */
public class PerformanceDaoTest extends AbstractDaoTest {
    @Autowired
    private MerchandiseDao merchandiseDao;
    @Autowired
    private PerformanceDao performanceDao;

    @Test
    public void testCreateCustomer() {
        Performance performance = new Performance();
        performance.setName("first performance");
        performance.setDescription("the first performance");
        performance.setDuration(2);
        performance.setPerformanceType(PerformanceType.MOVIE);
        Performance p1=performanceDao.save(performance);


        assertTrue(performance.getDescription().equals(p1.getDescription()));
        assertTrue(performance.getName().equals(p1.getName()));
        assertTrue(performance.getDuration()==p1.getDuration());
        assertTrue(performance.getPerformanceType()==p1.getPerformanceType());


    }

}
