package at.ac.tuwien.inso.ticketline.dao;

import at.ac.tuwien.inso.ticketline.model.Participation;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * @author Michael Oppitz
 * @date 09.05.2016
 */
public class ParticipationDaoTest extends AbstractDaoTest {

    @Autowired
    private ParticipationDao participationDao;

    @Test
    public void testFindAll() {
        List<Participation> participations = participationDao.findAll();
        assertEquals("Check DB initial data - is zero first", 0, participations.size());
    }

    @Test
    public void testFindOneById_NegativId() {
        assertNull(participationDao.findOne(-1));
    }

    @Test
    public void testFindOneById_InvalidId() {
        assertNull(participationDao.findOne(0));
    }

}
