package at.ac.tuwien.inso.ticketline.dao;

import at.ac.tuwien.inso.ticketline.model.Address;
import at.ac.tuwien.inso.ticketline.model.Customer;
import at.ac.tuwien.inso.ticketline.model.Gender;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.InvalidDataAccessApiUsageException;

import static junit.framework.TestCase.assertTrue;

/**
 * Created by Jakob on 09.05.2016.
 */
public class CustomerDaoTest extends AbstractDaoTest {

    @Autowired
    private CustomerDao cdao;

    @Test
    public void testCreateCustomer() {
        Customer c = new Customer();
        c.setFirstname("Testfirstname");
        c.setLastname("Testlastname");

        Address a = new Address();
        a.setStreet("Teststreet");
        a.setPostalCode("Testpostalcode");
        a.setCity("Testcity");
        a.setCountry("Testcountry");

        c.setAddress(a);
        c.setGender(Gender.MALE);

        int id = cdao.save(c).getId();

        assertTrue(id >= 1);

        Customer c2 = cdao.findOne(id);

        assertTrue(c.getFirstname().equals(c2.getFirstname()));
        assertTrue(c.getLastname().equals(c2.getLastname()));
        assertTrue(c.getAddress().equals(c2.getAddress()));
        assertTrue(c.getGender() == c2.getGender());

    }

    @Test(expected = InvalidDataAccessApiUsageException.class)
    public void testCreateCustomerNull() {
        Customer c = null;
        int id = cdao.save(c).getId();
    }
}
