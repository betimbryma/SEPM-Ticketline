package at.ac.tuwien.inso.ticketline.dao;

import at.ac.tuwien.inso.ticketline.model.*;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

import static junit.framework.TestCase.assertTrue;

/**
 * Created by eni on 09.05.2016.
 */
public class ReceiptDaoTest  extends AbstractDaoTest{

    @Autowired
    private MerchandiseDao merchandiseDao;
    @Autowired
    private PerformanceDao performanceDao;
    @Autowired
    private CustomerDao customerDao;
    @Autowired
    private EmployeeDao employeeDao;
    @Autowired
    private ReceiptDao receiptDao;
    @Autowired
    private CashDao cashDao;

    @Test
    public void testCreateCustomer() {
        Performance performance = new Performance();
        performance.setName("first performance");
        performance.setDescription("the first performance");
        performance.setDuration(2);
        performance.setPerformanceType(PerformanceType.MOVIE);
        Performance p1=performanceDao.save(performance);

        Article articel1 = new Article();
        articel1.setDescription("the first article");
        articel1.setName("the first Article");
        articel1.setAvailable(4);
        articel1.setPrice(5);
        articel1.setPerformance(p1);
        Article article2 = merchandiseDao.save(articel1);



        Customer c = new Customer();
        c.setFirstname("Testfirstname");
        c.setLastname("Testlastname");
        Address a = new Address();
        a.setStreet("Teststreet");
        a.setPostalCode("Testpostalcode");
        a.setCity("Testcity");
        a.setCountry("Testcountry");
        c.setAddress(a);
        c.setGender(Gender.MALE);
        Customer c2 = customerDao.save(c);

        Employee employee = new Employee();
        employee.setEmployeedSince(new Date());
        employee.setInsuranceNumber("sdfsdf");
        employee.setPasswordHash("sfdfsf");
        employee.setPermission(Permission.ADMINISTRATOR);
        employee.setEmail("a@aa.a");
        employee.setFirstname("dsf");
        employee.setLastname("dfadf");
        Employee employee2= employeeDao.save(employee);


        Cash cash = new Cash();
        cash.setDeleted(false);
        cash.setCustomer(c2);
        Cash cash2 = cashDao.save(cash);

        Receipt receipt = new Receipt();
        receipt.setCustomer(c2);
        receipt.setEmployee(employee2);
        receipt.setMethodOfPayment(cash2);
        receipt.setTransactionDate(new Date());
        receipt.setTransactionState(TransactionState.PAID);
        Receipt r1=receiptDao.save(receipt);


        assertTrue(receipt.getTransactionDate()==r1.getTransactionDate());
        assertTrue(receipt.getTransactionState()==r1.getTransactionState());
        assertTrue(receipt.getMethodOfPayment().getId()==r1.getMethodOfPayment().getId());
        assertTrue(receipt.getEmployee().getId()==r1.getEmployee().getId());

        assertTrue(receipt.getCustomer().getId()==r1.getCustomer().getId());
    }

}
