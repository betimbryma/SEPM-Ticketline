package at.ac.tuwien.inso.ticketline.dao;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertNull;

/**
 * Created by Betim on 5/15/2016.
 */
public class CategoryDaoTest extends AbstractDaoTest{

    @Autowired
    private CategoryDao categoryDao;

    @Test
    public void testNullvalue(){
        assertNull(categoryDao.findOne(0));
    }
}
