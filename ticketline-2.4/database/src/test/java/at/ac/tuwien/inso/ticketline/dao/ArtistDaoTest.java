package at.ac.tuwien.inso.ticketline.dao;

import at.ac.tuwien.inso.ticketline.model.Artist;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.Assert.*;

/**
 * @author Michael Oppitz
 * @date 09.05.2016
 */
public class ArtistDaoTest extends AbstractDaoTest {

    @Autowired
    private ArtistDao artistDao;

    @Test
    public void testFindAll() {
        List<Artist> artists = artistDao.findAll();
        assertEquals("Check DB initial data - is zero first", 0, artists.size());
    }

    @Test
    public void testAddArtist() {
        assertEquals("Check DB initial data - is zero first", 0, artistDao.count());
        Artist a = new Artist();
        a.setFirstname("Michael");
        a.setLastname("Oppitz");
        a.setDescription("Best musician on this planet!");
        Artist saved = artistDao.save(a);
        assertEquals("Check Artist count - should be 1", 1, artistDao.count());
        a = artistDao.findOne(saved.getId());
        assertNotNull(a);
    }

    @Test
    public void testFindOneById_NegativId() {
        assertNull(artistDao.findOne(-1));
    }

    @Test
    public void testFindOneById_InvalidId() {
        assertNull(artistDao.findOne(0));
    }

}
