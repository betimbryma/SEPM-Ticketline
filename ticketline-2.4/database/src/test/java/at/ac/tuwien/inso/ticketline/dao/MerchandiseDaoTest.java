package at.ac.tuwien.inso.ticketline.dao;

import at.ac.tuwien.inso.ticketline.model.Article;
import at.ac.tuwien.inso.ticketline.model.Performance;
import at.ac.tuwien.inso.ticketline.model.PerformanceType;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static junit.framework.TestCase.assertTrue;
/**
 * Created by eni on 09.05.2016.
 */
public class MerchandiseDaoTest extends AbstractDaoTest{


    @Autowired
    private MerchandiseDao merchandiseDao;
    @Autowired
    private PerformanceDao performanceDao;

    @Test
    public void testCreateCustomer() {
        Performance performance = new Performance();
        performance.setName("first performance");
        performance.setDescription("the first performance");
        performance.setDuration(2);
        performance.setPerformanceType(PerformanceType.MOVIE);
        Performance p1=performanceDao.save(performance);

        Article articel1 = new Article();
        articel1.setDescription("the first article");
        articel1.setName("the first Article");
        articel1.setAvailable(4);
        articel1.setPrice(5);
        articel1.setPerformance(p1);


        Article ai = merchandiseDao.save(articel1);

        assertTrue(articel1.getDescription().equals(ai.getDescription()));
        assertTrue(articel1.getName().equals(ai.getName()));
        assertTrue(articel1.getAvailable()==ai.getAvailable());
        assertTrue(articel1.getPrice()==ai.getPrice());
        assertTrue(articel1.getPerformance().getId()==ai.getPerformance().getId());


    }



}
