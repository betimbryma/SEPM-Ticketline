package at.ac.tuwien.inso.ticketline;

import at.ac.tuwien.inso.ticketline.dao.ArtistDaoTest;
import at.ac.tuwien.inso.ticketline.dao.NewsDaoTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(value = Suite.class)
@SuiteClasses(value = {NewsDaoTest.class, ArtistDaoTest.class})
public class AppTestSuite {

}
