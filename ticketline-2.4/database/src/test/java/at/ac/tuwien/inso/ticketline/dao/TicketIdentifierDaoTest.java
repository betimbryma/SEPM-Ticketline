package at.ac.tuwien.inso.ticketline.dao;

import at.ac.tuwien.inso.ticketline.model.*;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static junit.framework.TestCase.assertTrue;

/**
 * Created by eni on 29.05.2016.
 */
public class TicketIdentifierDaoTest extends AbstractDaoTest{



    @Autowired
    private MerchandiseDao merchandiseDao;
    @Autowired
    private PerformanceDao performanceDao;
    @Autowired
    private CustomerDao customerDao;
    @Autowired
    private EmployeeDao employeeDao;
    @Autowired
    private ReceiptDao receiptDao;
    @Autowired
    private ReceiptEntryDao receiptEntryDao;
    @Autowired
    private CashDao cashDao;
    @Autowired
    private LocationDao locationDao;
    @Autowired
    private RoomDao roomDao;
    @Autowired
    private ShowDao showDao;
    @Autowired
    private TicketDao ticketDao;
    @Autowired
    private SeatDao seatDao;
    @Autowired
    private CategoryDao categoryDao;
    @Autowired
    private GalleryDao galleryDao;
    @Autowired
    private RowDao rowDao;
    @Autowired
    private TicketIdentifierDao ticketIdentifierDao;

    @Test
    public void testCreateCustomer() {
        Performance performance = new Performance();
        performance.setName("first performance");
        performance.setDescription("the first performance");
        performance.setDuration(2);
        performance.setPerformanceType(PerformanceType.MOVIE);
        Performance p1=performanceDao.save(performance);

        Customer c = new Customer();
        c.setFirstname("Testfirstname");
        c.setLastname("Testlastname");
        Address a = new Address();
        a.setStreet("Teststreet");
        a.setPostalCode("Testpostalcode");
        a.setCity("Testcity");
        a.setCountry("Testcountry");
        c.setAddress(a);
        c.setGender(Gender.MALE);
        Customer c2 = customerDao.save(c);

        Location location = new Location();
        location.setName("asa");
        location = locationDao.save(location);

        Room room = new Room();
        room.setLocation(location);
        room = roomDao.save(room);

        Show show  =new Show();
        show.setRoom(room);
        show.setPerformance(p1);
        show=showDao.save(show);


        Category category = new Category();
        category.setName("erwe");
        category=categoryDao.save(category);

        Row row = new Row();
        row.setOrder(2);
        row.setName("asdrf");
        row=rowDao.save(row);

        Gallery gallery = new Gallery();
        gallery.setName("d");
        gallery.setOrder(1);
        galleryDao.save(gallery);
        Seat seat = new Seat();
        seat.setCategory(category);
        seat.setRow(row);
        seat.setRoom(room);
        seat.setGallery(gallery);
        seat.setOrder(1);
        seat=seatDao.save(seat);


        Ticket t = new Ticket();
        t.setPrice(23);
        t.setShow(show);
        t.setSeat(seat);
        t=ticketDao.save(t);


        TicketIdentifier ticketIdentifier = new TicketIdentifier();
        ticketIdentifier.setValid(true);
        ticketIdentifier.setTicket(t);
        ticketIdentifier = ticketIdentifierDao.save(ticketIdentifier);
        System.out.println(ticketIdentifier.getId());
        ticketIdentifier.setValid(false);
       ticketIdentifierDao.updateTicketIdentifiers(ticketIdentifier);

        TicketIdentifier t2  = ticketIdentifierDao.findOne(ticketIdentifier.getId());
        assertTrue(false==t2.getValid());


    }

}
