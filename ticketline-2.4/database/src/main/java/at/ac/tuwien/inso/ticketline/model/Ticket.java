package at.ac.tuwien.inso.ticketline.model;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * The ticket entity.
 */
@Entity
public class Ticket implements Serializable {

    private static final long serialVersionUID = 2355163364458707580L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = true)
    private String description;

    @Column(nullable = true)
    private Integer flag;

    @Temporal(TemporalType.TIMESTAMP)
    private Date flagdate;

    @Column(nullable = false)
    private Integer price;

    @ManyToOne
    @JoinColumn(name = "show_id", nullable = false)
    private Show show;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "seat_id", nullable = false)
    private Seat seat;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ticket")
    @Fetch(value = FetchMode.SUBSELECT)
    private List<TicketIdentifier> ticketIdentifiers;



    public Ticket(){

    }

    public Ticket(String description, Integer price, Show show, Seat seat){
        this.description = description;
        this.price = price;
        this.show = show;
        this.seat = seat;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the description.
     *
     * @param description the new description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets the price.
     *
     * @return the price
     */
    public Integer getPrice() {
        return price;
    }

    /**
     * Sets the price.
     *
     * @param price the new price
     */
    public void setPrice(Integer price) {
        this.price = price;
    }

    /**
     * Gets the show.
     *
     * @return the show
     */
    public Show getShow() {
        return show;
    }

    /**
     * Sets the show.
     *
     * @param show the new show
     */
    public void setShow(Show show) {
        this.show = show;
    }

    /**
     * Gets the seat.
     *
     * @return the seat
     */
    public Seat getSeat() {
        return seat;
    }

    /**
     * Sets the seat.
     *
     * @param seat the new seat
     */
    public void setSeat(Seat seat) {
        this.seat = seat;
    }

    /**
     * Gets the ticket identifiers.
     *
     * @return the ticket identifiers
     */
    public List<TicketIdentifier> getTicketIdentifiers() {
        return ticketIdentifiers;
    }

    public Integer getFlag() {
        return flag;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }

    /**
     * Sets the ticket identifiers.
     *
     * @param ticketIdentifiers the new ticket identifiers
     */


    public void setTicketIdentifiers(List<TicketIdentifier> ticketIdentifiers) {
        this.ticketIdentifiers = ticketIdentifiers;
    }

    public Date getFlagdate() {
        return flagdate;
    }

    public void setFlagdate(Date flagdate) {
        this.flagdate = flagdate;
    }
}
