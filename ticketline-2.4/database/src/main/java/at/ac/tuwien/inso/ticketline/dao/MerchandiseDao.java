package at.ac.tuwien.inso.ticketline.dao;

import at.ac.tuwien.inso.ticketline.model.Article;
import at.ac.tuwien.inso.ticketline.model.Performance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface MerchandiseDao extends JpaRepository<Article, Integer> {

    @Query(value = "SELECT t FROM Article t Where t.performance=:p ")
    List<Article> getByPerformance(@Param("p") Performance performance);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(value = "UPDATE Article t SET t.name =t.name , t.description =t.description, t.price=t.price, t.available= (t.available-1), t.performance=t.performance where t=:article")
    int decreaseArticle(@Param("article") Article article);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(value = "UPDATE Article t SET t.name =t.name , t.description =t.description, t.price=t.price, t.available= (t.available+1), t.performance=t.performance where t=:article")
    int increaseArticle(@Param("article") Article article);
}
