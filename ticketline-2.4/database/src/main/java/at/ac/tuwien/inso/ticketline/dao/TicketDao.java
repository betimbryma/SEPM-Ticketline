package at.ac.tuwien.inso.ticketline.dao;

import at.ac.tuwien.inso.ticketline.model.Show;
import at.ac.tuwien.inso.ticketline.model.Ticket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Repository
public interface TicketDao extends JpaRepository<Ticket, Integer> {

    @Query(value = "SELECT t FROM Ticket t Where t.show=:show")
    List<Ticket> findAllTicketsForShow(@Param("show")Show show);

    @Query(value = "SELECT COUNT(t) FROM Ticket t WHERE t IN(SELECT ticket FROM TicketIdentifier ticketId WHERE ticketId.valid=TRUE AND ticketId IN(SELECT ticketIdentifier FROM ReceiptEntry r)) AND t.show=:show")
    Long countSoldByShow(@Param("show")Show show);

    @Query(value = "SELECT MIN(price) FROM Ticket t WHERE t.show=:show")
    Integer getMinPrice(@Param("show")Show show);

    @Query(value = "SELECT MAX(price) FROM Ticket t WHERE t.show=:show")
    Integer getMaxPrice(@Param("show")Show show);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(value = "UPDATE Ticket t SET t.description =t.description , t.flag =1, t.price=t.price, t.seat=t.seat, t.show=t.show where t=:ticketE")
    int updateTicketForCartAsUsed(@Param("ticketE") Ticket ticketE);


    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(value = "UPDATE Ticket t SET t.description =t.description , t.flag =0, t.price=t.price, t.seat=t.seat, t.show=t.show where t=:ticketE")
    int updateTicketForCartAsNotUsed(@Param("ticketE") Ticket ticketE);
    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(value = "UPDATE Ticket t SET t.description =t.description , t.flag =2, t.price=t.price, t.seat=t.seat, t.show=t.show where t=:ticketE")
    int updateTicketForCartAsReserved(@Param("ticketE") Ticket ticketE);
    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(value = "UPDATE Ticket t SET t.description =t.description , t.flag =3, t.price=t.price, t.seat=t.seat, t.show=t.show where t=:ticketE")
    int updateTicketForCartAsSold(@Param("ticketE") Ticket ticketE);
    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(value = "UPDATE Ticket t SET t.description =t.description , t.flag =3,t.flagdate=:date, t.price=t.price, t.seat=t.seat, t.show=t.show where t=:ticketE")
    int updateDate(@Param("ticketE") Ticket ticketE,@Param("date") Date date);


    @Query(value = "SELECT t.flag FROM Ticket t WHERE t=:ticket")
    Integer checkFlag(@Param("ticket")Ticket ticket);
    @Query(value = "SELECT t.flagdate FROM Ticket t WHERE t=:ticket")
    Date checkDate(@Param("ticket")Ticket ticket);
}
