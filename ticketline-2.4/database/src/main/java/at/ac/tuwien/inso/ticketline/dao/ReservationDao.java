package at.ac.tuwien.inso.ticketline.dao;
import at.ac.tuwien.inso.ticketline.model.Customer;
import at.ac.tuwien.inso.ticketline.model.Reservation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReservationDao extends JpaRepository<Reservation, Integer> {

    @Query(value = "SELECT r FROM Reservation r Where r.customer=:customer")
    List<Reservation> findReservationsForCustomer(@Param("customer")Customer customer);
}
