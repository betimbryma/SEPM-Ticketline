package at.ac.tuwien.inso.ticketline.dao;

import at.ac.tuwien.inso.ticketline.model.News;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.Temporal;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.persistence.TemporalType;
import java.util.Date;
import java.util.List;

@Repository
public interface NewsDao extends JpaRepository<News, Integer> {

    /**
     * Finds all news ordered by the submission date.
     *
     * @return the list of news
     */
    @Query(value = "SELECT n FROM News n ORDER BY n.submittedOn ASC")
    List<News> findAllOrderBySubmittedOnAsc();

    @Query(value = "SELECT n FROM News n WHERE n.submittedOn >= :date ORDER BY n.submittedOn ASC")
    List<News> findBeforeOrderBySubmittedOn(@Param("date") @Temporal(TemporalType.TIMESTAMP) Date date);

}
