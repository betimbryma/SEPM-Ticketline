package at.ac.tuwien.inso.ticketline.dao;

import at.ac.tuwien.inso.ticketline.model.Premium;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by eni on 04.06.2016.
 */
@Repository
public interface PremiumDao extends JpaRepository<Premium,Integer> {
}
