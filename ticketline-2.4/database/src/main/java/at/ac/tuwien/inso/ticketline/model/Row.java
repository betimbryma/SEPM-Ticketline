package at.ac.tuwien.inso.ticketline.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * The row entity.
 */
@Entity
public class Row implements Serializable {

    private static final long serialVersionUID = 334212542943742103L;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

    private String description;

    @Column(nullable = false, name = "sequence")
    private Integer order;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "row")
    private List<Seat> seats;

    public Row() {
    }

    public Row(String name, String description, Integer order) {
        this.name = name;
        this.description = description;
        this.order = order;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name.
     *
     * @param name the new name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the description.
     *
     * @param description the new description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets the order.
     *
     * @return the order
     */
    public Integer getOrder() {
        return order;
    }

    /**
     * Sets the order.
     *
     * @param order the new order
     */
    public void setOrder(Integer order) {
        this.order = order;
    }

    /**
     * Gets the seats.
     *
     * @return the seats
     */
    public List<Seat> getSeats() {
        return seats;
    }

    /**
     * Sets the seats.
     *
     * @param seats the new seats
     */
    public void setSeats(List<Seat> seats) {
        this.seats = seats;
    }

}
