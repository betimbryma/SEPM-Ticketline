package at.ac.tuwien.inso.ticketline.dao;

import at.ac.tuwien.inso.ticketline.model.Reservation;
import at.ac.tuwien.inso.ticketline.model.Ticket;
import at.ac.tuwien.inso.ticketline.model.TicketIdentifier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface TicketIdentifierDao extends JpaRepository<TicketIdentifier, Integer> {

    @Query(value = "SELECT t FROM TicketIdentifier t Where t.ticket=:ticket AND t.valid=TRUE")
    List<TicketIdentifier> findTicketIdentifierForTicket(@Param("ticket")Ticket ticket);

    @Query(value = "SELECT t FROM TicketIdentifier t Where t.reservation=:reservation AND t.valid=TRUE")
    List<TicketIdentifier> getTicketIdentifiersForReservation(@Param("reservation")Reservation reservation);


    @Query(value = "SELECT t FROM TicketIdentifier t Where t.ticket=:ticket AND t.valid=TRUE")
    List<TicketIdentifier> getTicketIdentifier(@Param("ticket")Ticket ticket);

     @Transactional
    @Modifying(clearAutomatically = true)
    @Query(value = "UPDATE TicketIdentifier t SET t.cancellationReason ='Paid' , t.reservation =t.reservation, t.ticket=t.ticket, t.uuid=t.uuid, t.valid=false, t.voidationTime=t.voidationTime, t.voidedBy=t.voidedBy where t=:ticketE")
    int updateTicketIdentifiers(@Param("ticketE") TicketIdentifier ticketE);


    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(value = "UPDATE TicketIdentifier t SET t.cancellationReason ='Canceled' , t.reservation =t.reservation, t.ticket=t.ticket, t.uuid=t.uuid, t.valid=false, t.voidationTime=t.voidationTime, t.voidedBy=t.voidedBy where t=:ticketE")
    int cancelTicketIdentifiers(@Param("ticketE") TicketIdentifier ticketE);

//cancellationReason, reservation_id, ticket_id, uuid, valid, voidationTime, voidedBy

}
