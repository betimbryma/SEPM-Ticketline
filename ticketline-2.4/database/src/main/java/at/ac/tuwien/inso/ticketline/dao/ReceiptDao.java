package at.ac.tuwien.inso.ticketline.dao;

import at.ac.tuwien.inso.ticketline.model.Customer;
import at.ac.tuwien.inso.ticketline.model.Receipt;
import at.ac.tuwien.inso.ticketline.model.ReceiptEntry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReceiptDao extends JpaRepository<Receipt, Integer> {

    @Query(value = "SELECT r FROM Receipt r Where r.customer=:customer")
    List<Receipt> getForCustomer(@Param("customer")Customer customer);

    @Query(value = "SELECT r FROM ReceiptEntry r Where r.receipt=:receipt and r.ticketIdentifier is not null")
    List<ReceiptEntry> getForTicketsCustomer(@Param("receipt")Receipt receipt);
}
