package at.ac.tuwien.inso.ticketline.model;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * The customer entity.
 */
@Entity
public class Customer extends Person {

    private static final long serialVersionUID = 4192214529072911981L;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "customer", fetch = FetchType.EAGER)
    private List<MethodOfPayment> methodOfPayments;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "customer")
    private List<Receipt> receipts;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "customer")
    private List<Reservation> reservations;

    @Column
    private Integer premiumpoints = 0;


    public Integer getPremiumpoints() {
        return premiumpoints;
    }

    public void setPremiumpoints(Integer premium) {
        this.premiumpoints = premium;
    }

    /**
     * Instantiates a new customer.
     */
    public Customer() {
    }

    /**
     * Instantiates a new customer.
     *
     * @param id                  the id
     * @param gender              the gender
     * @param firstname           the firstname
     * @param lastname            the lastname
     * @param email               the email
     * @param phoneNumber         the phone number
     * @param dateOfBirth         the date of birth
     * @param customerStatus      the customer status
     * @param customerGroup       the customer group
     * @param ticketcardNumber    the ticketcard number
     * @param ticketcardValidThru the ticketcard valid thru
     */
    public Customer(Integer id, Gender gender, String firstname, String lastname, String email,
                    String phoneNumber, Date dateOfBirth, CustomerStatus customerStatus, CustomerGroup customerGroup,
                    String ticketcardNumber, Date ticketcardValidThru) {
        super(id, gender, firstname, lastname, email, phoneNumber, dateOfBirth);
    }

    /**
     * Instantiates a new customer.
     *
     * @param id                  the id
     * @param gender              the gender
     * @param firstname           the firstname
     * @param lastname            the lastname
     * @param email               the email
     * @param phoneNumber         the phone number
     * @param dateOfBirth         the date of birth
     * @param address             the address
     * @param customerStatus      the customer status
     * @param customerGroup       the customer group
     * @param ticketcardNumber    the ticketcard number
     * @param ticketcardValidThru the ticketcard valid thru
     */
    public Customer(Integer id, Gender gender, String firstname, String lastname, String email,
                    String phoneNumber, Date dateOfBirth, Address address, CustomerStatus customerStatus, CustomerGroup customerGroup,
                    String ticketcardNumber, Date ticketcardValidThru) {
        super(id, gender, firstname, lastname, email, phoneNumber, dateOfBirth, address);
    }

    /**
     * Gets the method of payments.
     *
     * @return the method of payments
     */
    public List<MethodOfPayment> getMethodOfPayments() {
        return methodOfPayments;
    }

    /**
     * Sets the method of payments.
     *
     * @param methodOfPayments the new method of payments
     */
    public void setMethodOfPayments(List<MethodOfPayment> methodOfPayments) {
        this.methodOfPayments = methodOfPayments;
    }

    /**
     * Gets the receipts.
     *
     * @return the receipts
     */
    public List<Receipt> getReceipts() {
        return receipts;
    }

    /**
     * Sets the receipts.
     *
     * @param receipts the new receipts
     */
    public void setReceipts(List<Receipt> receipts) {
        this.receipts = receipts;
    }

    /**
     * Gets the reservations.
     *
     * @return the reservations
     */
    public List<Reservation> getReservations() {
        return reservations;
    }

    /**
     * Sets the reservations.
     *
     * @param reservations the new reservations
     */
    public void setReservations(List<Reservation> reservations) {
        this.reservations = reservations;
    }


}
